defmodule DHTest do
  use ExUnit.Case
  require Logger

  test "dh" do
    # 有一个大素数P, 以及一个整数g (2或者5)
    [p, g] = [0xFFFFFFFFFFFFFFC5, 5]
    # [p, g] = [83, 2]
    dh_params = [p, g]
    # # alice生成私钥a, 以及用下面公式生成公钥A
    # # g^a mod p = A
    # {alice_public, alice_private} = :crypto.generate_key(:dh, dh_params)
    # # bob生成私钥b, 以及用下面公式生成公钥B
    # # g^b mod p = B
    # {bob_public, bob_private} = :crypto.generate_key(:dh, dh_params)
    # # 有 B^a mod p = K
    # alice_key = :crypto.compute_key(:dh, bob_public, alice_private, dh_params)
    # # 有 A^b mod p = K
    # bob_key = :crypto.compute_key(:dh, alice_public, bob_private, dh_params)
    # assert alice_key == bob_key
    # alice_key_64 = Base.encode64(alice_key)
    # Logger.warn("dh_params:#{dh_params} key:#{inspect(alice_key_64)}")

    Logger.warn("dh_params:#{inspect(dh_params)}")
    #   {alice_public, alice_private} = :crypto.generate_key(:dh, dh_params)
    #   {bob_public, bob_private} = :crypto.generate_key(:dh, dh_params)
    alice_private = :crypto.strong_rand_bytes(8)
    bob_private = :crypto.strong_rand_bytes(8)
    alice_public = Gangplank.EjoyToken.exchange_secret(alice_private)
    bob_public = Gangplank.EjoyToken.exchange_secret(bob_private)
    {alice_public2, alice_private2} = :crypto.generate_key(:dh, dh_params, alice_private)
    {bob_public2, bob_private2} = :crypto.generate_key(:dh, dh_params, bob_private)
    Logger.warn("alice_private:#{inspect(to_integer(alice_private))}")
    Logger.warn("alice_public:#{inspect(to_integer(alice_public))}")
    Logger.warn("alice_private2:#{inspect(to_integer(alice_private2))}")
    Logger.warn("alice_public2:#{inspect(to_integer(alice_public2))}")
    Logger.warn("bob_private:#{inspect(to_integer(bob_private))}")
    Logger.warn("bob_public:#{inspect(to_integer(bob_public))}")
    Logger.warn("bob_private2:#{inspect(to_integer(bob_private2))}")
    Logger.warn("bob_public2:#{inspect(to_integer(bob_public2))}")
    key1 = Gangplank.EjoyToken.gen_secret(bob_public, alice_private)
    key2 = Gangplank.EjoyToken.gen_secret(alice_public, bob_private)
    key3 = :crypto.compute_key(:dh, bob_public, alice_private, dh_params)
    key4 = :crypto.compute_key(:dh, alice_public, bob_private, dh_params)
    Logger.warn("key1:#{inspect(to_integer(key1))}")
    Logger.warn("key2:#{inspect(to_integer(key2))}")
    Logger.warn("key3:#{inspect(to_integer(key3))}")
    Logger.warn("key4:#{inspect(to_integer(key4))}")
    #     assert key1 == key2 and key2 == key3 and key3 == key4
  end

  defp to_integer(bin) do
    :binary.decode_unsigned(bin)
  end
end
