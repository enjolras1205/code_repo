local M = {}

function M:init(var)
	self.data = var
end

function M:get(k)
	return self[k]
end

return {
	init = init,
	get = get,
}
