-- class implement in lua
--
--
local tostring = tostring
local string = string
local format = string.format

local function addr(t)
	return string.gsub(tostring(t), 'table', '')
end

local zh_tostring = function(cls)
	return cls.__type.."<"..cls.__name..">"..cls.__addr
end

LuaClass = {
	__type = "class",
	__superclass = nil, 
}

LuaClass.__index = LuaClass
LuaClass.__tostring = function(cls)
	return cls.__type.."<"..cls.__name..">"..cls.__addr
end

-- used for new a object instance
-- 如果new 有参数，则要求它为一个 table, 使用table 传具名参数！
function LuaClass:new(...)
	local obj = { __type = "obj" }
	obj.__addr = addr(obj)
	setmetatable(obj, self)
	if obj.__init__ then
		obj:__init__(...)
	end
	return obj
end

-- create a class of lua, all class in game should use this method
-- overwrite class of cocos2dx engine "class" (in extern.lua)
-- differences with cocos classs: 
-- 		1 costom some features like "derive_hook",  
-- 		2 no copy of attrs, use __index	 ! 更新时可以获得父类的变化，在仅更新父类文件的情况下		
function class( classname, super )
	if super and (type(super) ~= "table" or super.__type ~= "class") then
		assert(format("Error: super class illegal.( super should be nil or class type), classname=%s", classname)) 
	end

	super = super or LuaClass
	local cls = {}
	cls.__name = classname
	cls.__type = "class"
	cls.__addr = addr(cls)
	cls.__superclass = super
	cls.__tostring = super.__tostring
	cls.__index = cls			-- ready for being metaclass

	setmetatable(cls, super )
	if rawget(super, 'derive_hook') then
		super:derive_hook(cls)
	end
	
	return cls
end

function super(cls)
	return cls.__superclass
end

function issubclass(cls, ancestor)
	local tmp = cls
	while 1 do
		local mt = getmetatable(tmp)
		if not mt then
			return false
		end
		tmp = mt.__index
		if ancestor == tmp then
			return true
		end
	end
end

function isinstance(ins, ancestor)
	return issubclass(ins, ancestor)
end

-----------------------------------------------

-- 返回一个与实际entity 绑定的 远程代理
-- entity 在这里会不会因为 循环引用了导致不释放啊
-- 应该剥离 出_define,  ent 则简化为 eid
function LuaClass:proxy(ent)
	local obj = { __type = 'proxy' }
	obj.__class =  self					
	obj.__addr = addr(obj)
	obj.uid_ = ent.uid_				-- use for proxy send
	local define = ent._define		-- 去掉 ent 在upvalue的引用
	local eid = ent.eid_
	local mt = {}
	mt.__tostring = self.__tostring
	mt.__index = function(obj, name)
		local cls_attr = obj.__class[name]
		if cls_attr then
			-- print ("find method <"..name.."> in class") 
			return cls_attr
		end
		-- no find method in class, then proxy
		return function(...)
			local msg = define:pack_method(eid, name, {...})
			obj:send( msg )
		end
	end

	setmetatable(obj, mt) 
	return obj
end
