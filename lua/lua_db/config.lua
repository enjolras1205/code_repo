-- avoid pollute global namespace
local _G = _G
_ENV = nil

local lfs = _G.require "lfs"
local db = _G.require "db"

local function init_config(path)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local file_path = path.."/"..file
            if lfs.attributes(file_path, "mode") == "directory" then
                init_config(file_path)
            else
                local file_env = {}
                _G.dofile(file_path)
                db.set(file, file_env)
            end
        end
    end
end

init_config("./config")
--db.dump()
--_G.print(db.get("user.max_user"))
for k, v in _G.pairs(_G) do
    _G.print(k, v)
end
_G.print(_G.max_user)
_G.print(_G.port)

return {
    init_config = init_config,
}
