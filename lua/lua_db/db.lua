-- avoid pollute global namespace
local _G = _G
_ENV = nil

local db_t = {}

local function set(k_path, v)
    local t = db_t 
    for w, d in _G.string.gmatch(k_path, "([%w_]+)(%.?)") do
        if d == "." then
            t[w] = t[w] or {}
            t = t[w]
        else
            t[w] = v
        end
    end
end

local function get(k_path)
    local v = db_t
    for w in _G.string.gmatch(k_path, "[%w_]+") do
        v = v[w]
    end
    return v
end

local function dump1(t)
    for key, value in _G.pairs(t) do
        _G.print("key:", key, "value:", value)
        if _G.type(value) == "table" then
            dump1(value)
        end
    end
end

local function dump()
    dump1(db_t)
end

return {
    set = set,
    get = get,
    dump = dump
}
