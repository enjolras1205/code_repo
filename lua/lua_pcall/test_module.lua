-- 文件名为 module.lua
-- 定义一个名为 module 的模块
module = {}

-- 定义一个常量
module.constant = "这是一个常量"

-- 定义一个函数
function module.func1()
    local ok, msg = pcall(function()
        print("func1");
        a = nil;
        error("hello")
        a[0] = 1;
    end)
    if ok then
        print("success", msg);
    else
        print("not_success", msg);
    end
end

local function func2()
    print("这是一个私有函数！");
end

function module:func3(var)
    print("func3", self, var);
    func2();
end

function module:func4()
    local ok, msg = pcall(function()
        print("func4");
        self.func1();
    end)
    if ok then
        print("func4_success", msg);
    else
        print("func4_not_success", msg);
    end
end

return module
