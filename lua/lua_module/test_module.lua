-- 文件名为 module.lua
-- 定义一个名为 module 的模块
module = {}
 
-- 定义一个常量
module.constant = "这是一个常量"

local val = 0

local function get_session_id(user_id1, user_id2)
    if user_id1 > user_id2 then
        user_id1, user_id2 = user_id2, user_id1 
    end
    return string.format("%s:%s", user_id1, user_id2)
end
 
-- 定义一个函数
function module.func1()
    val1 = get_session_id("1", "2")
    val2 = get_session_id("2", "1")
    val3 = get_session_id("3", "3")
    print(val1, val2, val3)
end
 
local function func2()
    print("这是一个私有函数！")
end
 
function module:func3(var)
    print("func3", self, var)
    func2()
end

 
return module