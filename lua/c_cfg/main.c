#include <stdio.h>
#include <lua.h>
#include <lualib.h>

void load(lua_State *L, const char *fname)
{
    if (luaL_loadfilex(L, fname, NULL) || lua_pcall(L, 0, 0, 0)) {
        error(L, "cannot run config. file : %s", lua_tostring(L, -1));
    }
    return;
}

int getfield(lua_State *L, const char *key) {
    int result;
    /*get table value by key*/
    lua_getfield(L, -1, key);
    if (!lua_isnumber(L, -1)) {
        error(L, "invalid component");
    }
    result = (int)(lua_tonumber(L, -1) * 255);
    lua_pop(L, 1);
    return result;
}

int main()
{
    lua_State *lua_state = luaL_newstate();
    load(lua_state, "cfg");

    /*make sure background table at file cfg last line*/
    lua_getglobal(lua_state, "background");
    if (!lua_istable(lua_state, -1)) {
        error(lua_state, "background is not a table");
    }

    int red = getfield(lua_state, "r");
    int green = getfield(lua_state, "g");
    int blue = getfield(lua_state, "b");

    printf("red %d green %d blue %d \n", red, green, blue);

    lua_close(lua_state);
    return 0;
}
