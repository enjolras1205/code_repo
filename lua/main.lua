#!/usr/local/bin/lua

local coroutine_count = 0
local coroutine_pool = {}
local coroutine_yield = coroutine.yield

local function co_create(f)
    local print = print
    co = coroutine.create(function(...)
        f(...)
        while true do
            f = nil
            coroutine_pool[#coroutine_pool+1] = co
            print "a"
            f = coroutine_yield "EXIT"
            print "b" 
            f(coroutine_yield())
            print "c" 
        end
    end)
    coroutine_count = coroutine_count + 1
    if coroutine_count > 1024 then
        print("May overload, create 1024 task")
        coroutine_count = 0
    end
    return co
end

co1 = co_create(function() print(1) end)
co2 = co_create(function() print(2) end)

print "a1"
print(coroutine.resume(co1, co1))
print "a2"
print(coroutine.resume(co1, co1))
print "a3"
print(coroutine.resume(co1, co1))
print "a4"
-- print(coroutine.resume(co1, 100))
-- print(coroutine.resume(co2))
