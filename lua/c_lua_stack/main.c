#include <stdio.h>
#include <lua.h>
#include <lualib.h>

static void stackDump(lua_State *L)
{
    int i;
    int top = lua_gettop(L);
    for (i = 1; i <= top; ++i) {
        int t = lua_type(L, i);
        switch (t) {
            case LUA_TSTRING: {
                printf("'%s'", lua_tostring(L, i));
                break;
            }
            case LUA_TBOOLEAN: {
                printf(lua_toboolean(L, i)? "true": "false");
                break;
            }
            case LUA_TNUMBER: {
                printf("%g", lua_tonumber(L, i));
                break;
            }
            default: {
                printf("%s", lua_typename(L, t));
                break;
            }
        }
        printf("-");
    }
    printf("\n");
}

int main() {
    lua_State *L = luaL_newstate();
    lua_pushboolean(L, 1);
    lua_pushnumber(L, 10);
    lua_pushnil(L);
    lua_pushstring(L, "enj");
    /*true 10 nil enj*/
    stackDump(L);

    /*positive number from stack bottom negative from stack top*/
    /*push value copy in stack pos xx */
    /*true 10 nil enj nil*/
    lua_pushvalue(L, -2);
    stackDump(L);

    /*true nil nil enj*/
    /*pop stack top, replace to place n*/
    lua_replace(L, 2);
    stackDump(L);

    /*true nil nil enj nil nil nil nil*/
    /*set stack size to 8 nil to empty val*/
    lua_settop(L, 8);
    stackDump(L);

    /*true nil enj nil nil nil nil*/
    lua_remove(L, 3);
    stackDump(L);

    /*true nil enj nil nil nil*/
    /*set the new stack top pos*/
    lua_settop(L, -2);
    stackDump(L);

    lua_close(L);
    return 0;
}
