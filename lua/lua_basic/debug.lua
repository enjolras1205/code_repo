#!/usr/local/bin/lua

function traceback()
    for level = 0, math.huge do
        local info = debug.getinfo(level, "Sl")
        if not info then break end
        if info.what == "C" then 
            print(level, "C function")
        else
            print(string.format("[%s]:%d", info.short_src, info.currentline))
        end
    end
end

function test()
    traceback()
    print("---------------")
    print(debug.traceback())
end

function main()
    test()
end

main()
