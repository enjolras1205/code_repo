#!/usr/local/bin/lua
local modname = "table_util"
local M = {}
_G[modname] = M

--output function
function M.print(table)
    for i,v in ipairs(table) do
        print(i, v)
    end
end

return M

