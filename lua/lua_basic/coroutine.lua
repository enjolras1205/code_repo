#!/usr/local/bin/lua
require("table_util")

students = {70, 80, 90, 100}
table_util.print(students)

co = coroutine.create(function(var) 
    table_util.print(var)
end)
print(co)
print(coroutine.status(co))
coroutine.resume(co, students)
print(coroutine.status(co))
