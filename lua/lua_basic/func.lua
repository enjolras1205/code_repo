#!/usr/local/bin/lua
require("table_util")

function multi_return()
    return 1, 2
end

print(multi_return())

x,y,z = multi_return()
print(x)
print(y)
print(z)

function multi_param(...)
    for i,v in ipairs({...}) do
        print(i,v)
    end
end

multi_param(10,20,30,40,50)

--named arguments
function named_arg(var1, var2)
    print(var1)
    print(var2)
end

named_arg("param1", "param2")

--closure
--students = {zhou = 80, enj = 90, yun = 100}
students = {80, 90, 100}

table_util.print(students)

table.sort(students, function(s1, s2)
    return s2 < s1 
end)

table_util.print(students)

