#!/usr/local/bin/lua
require("table_util")

function main_loop()
    while true do
        local val = io.read()
        local co= coroutine.create(function () 
            do_sth(val)
        end)
        coroutine.resume(co)
    end
end

function do_sth(val)
    sleep(3)
    print("do sth after 3" .. val)
end

function sleep(n)
   os.execute("sleep " .. n)
end

main_loop()
