#!/usr/local/bin/lua

a = {}
b = {__mode = "k"}
setmetatable(a, b)
key = {}
a[key] = 1
-- key已经指向了另一个table 又是弱引用，故该 k v被回收了
key = {}
a[key] = 2
collectgarbage()

for k, v in pairs(a) do print(k, v) end
