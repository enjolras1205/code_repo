#!/usr/local/bin/lua

--while
a = {"one", "two", "three"}
i = 1
while a[i] do
    print(a[i])
    i=i+1
end

--repeat
i = 1
repeat
    print(a[i])
    i=i+1
until i>3

--for
for var=1,10,2 do
    print(var)
end
