#!/usr/local/bin/lua

local modname = "Account"
local M = {balance = 0}
_G[modname] = M

--output function
--语法糖 冒号隐藏了self参数
function M:withraw(v)
    self.balance = self.balance - v
end

function M:new(o)
    o = o or {}
    -- metatable 地址不变 所以在setmetatable 之前之后都可以
    self.__index = self
    setmetatable(o, self)
    return o
end

return M

