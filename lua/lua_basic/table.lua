#!/usr/local/bin/lua

a = {}
for i = 1, 3 do
    a[i] = io.read()
end

for k, v in pairs(a) do
    print(type(k), k, v)
end

for i = 1, #a do
    print(a[i])
end


polyline = {color="blue", thickness=2,
    {x = 0, y = 0},
    {x = 2, y = 2}
}

print(polyline.color)
print(polyline[1].x)
print(polyline[2].x)

for k, v in pairs(polyline) do
    print(type(k), k, v)
end
