#!/usr/local/bin/lua

require "object"
require "table_util"

a = Account:new({balance = 100})
print(a["balance"])
a:withraw(20)
print(a["balance"])

b = Account:new()
print(b["balance"])
b:withraw(20)
print(b["balance"])
