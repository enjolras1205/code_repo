db = db.getSiblingDB("package_new")

branch_ids = db.branch.distinct("branch_id");
for (var branch_id of branch_ids) {
  db.branch.find({ "branch_id": branch_id }).limit(1).forEach(function (branch_info) {
    // print("branch_info", branch_info);
    if (branch_info.branch_id != branch_info.revision_id) {
      print("branch_id_not_equal_to_revision_id", JSON.stringify(branch_info));
      db.branch.update({ "_id": branch_info["_id"] }, { $set: { "revision_id": branch_info["branch_id"] } }, upsert = false)
    }
  }
  );
}