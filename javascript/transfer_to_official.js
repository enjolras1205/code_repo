// mongo --eval "var product_name='P10121', suffix='a'" transfer_to_official.js

var game_account_db_name = product_name + "_game_account";
var ejoy_token_col_name = "ejoy_token"
var ejoy_sub_account_col_name = "ejoy_sub_account"
var ejoy_sub_account_col_name_bk = ejoy_sub_account_col_name + "_bk_" + suffix
var game_account_db = db.getSiblingDB(game_account_db_name)

print("backup", game_account_db, ejoy_sub_account_col_name, "to", ejoy_sub_account_col_name_bk)
game_account_db[ejoy_sub_account_col_name].copyTo(ejoy_sub_account_col_name_bk)

game_account_db[ejoy_sub_account_col_name].find().forEach(
  function (sub_account_obj) {
    var ejoy_token_cursor = game_account_db[ejoy_token_col_name].find({ "sub_account": sub_account_obj["sub_account"] })
    var ejoy_token_obj = ejoy_token_cursor.hasNext() ? ejoy_token_cursor.next() : null;
    if (ejoy_token_obj) {
      var with_account = ejoy_token_obj["_id"];
      if (typeof(with_account) == "object") {
        with_account = with_account.str;
      }
      var target_platform = new Array("AGST", "GOOGLE", "FB")
      var platform_type = sub_account_obj["platform"]
      if (target_platform.includes(platform_type)) {
        if (platform_type == "AGST") {
          game_account_db[ejoy_sub_account_col_name].update(
            { "_id": sub_account_obj["_id"] },
            { "$set": { "with": "OFFICIAL", "with_account": with_account, "guest": false } });
        } else {
          game_account_db[ejoy_sub_account_col_name].update(
            { "_id": sub_account_obj["_id"] },
            { "$set": { "with": "OFFICIAL", "with_account": with_account } });
        }
        var gangplank_db = db.getSiblingDB(product_name + "_gangplank")
        var gangplank_uid = with_account;
        var thirdpart_uid = sub_account_obj["platform_account"];
        gangplank_db["official_bind"].insert(
          {
            "gangplank_uid": gangplank_uid, "thirdpart_type": platform_type,
            "thirdpart_uid": thirdpart_uid, "thirdpart_info": { "ext": sub_account_obj["ext"] },
            "thirdpart_token": ""
          });
      }
    } else {
      print("cant find sub account ", to_json(sub_account_obj));
    }
  }
)