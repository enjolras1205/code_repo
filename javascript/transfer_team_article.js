var bbs_db = product_name + "_bbs";
db = db.getSiblingDB(bbs_db);

// db.bbs_info
// { "_id" : ObjectId("646df9a7c33b24f2b0ceecb4"), "category" : "forum", "closed" : false, "created_at" : ISODate("2023-05-24T11:48:55.958Z"), "desc" : null, "icon" : null, "lock_state" : 0, "locked" : false, "meta_info" : { "tag" : [] }, "name" : "op_team", "namespace" : null, "new_at" : null, "order" : 1, "origin_id" : "op_team", "pic" : null, "thread_count" : 0, "updated_at" : ISODate("2023-05-24T11:48:55.958Z") }

var cur_date = new Date();
db.bbs_info.insert({ "category": "forum", "closed": false, "created_at": cur_date, "desc": "配队板块",
  "icon": null, "lock_state": 0, "locked": false, "meta_info": { "tag": [] },
  "name": "op_team", "namespace": null, "new_at": null, "order": 1, "origin_id": "op_team",
  "pic": null, "thread_count": 0, "updated_at": cur_date });

var result = [];
db.article.find({ "origin_id": "op_dynamic", "tags": "custom-recommend" }, { _id: 1, tags: 1, tags2: 1 }).forEach(
  v => {
    if (v.tags2 != null) {
      print("tags2 not null", v._id, v.tags, v.tags2);
    }
    var tags = v.tags;
    var tag_tree = [];
    tags.forEach(v => {
      if (v != "custom-recommend") {
        tag_tree.push("official_team," + v + ",");
      }
    });
    var set_tags2 = { "tag_attr": { "official_team": { "select": true } }, "tag_tree": tag_tree };
    result.push({
      id: v._id,
      tags: v.tags,
      tags2: set_tags2
    });
    db.article.update({ _id: v._id }, { "$set": { "tags2": set_tags2, "origin_id": "op_team" } });
  }
);

print("result:\n")
result.forEach(v => {
  print(JSON.stringify(v));
});
print("success complete!")