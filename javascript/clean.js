function pad(number, length) {

    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;

}

Date.prototype.YYYYMMDDHHMMSS = function () {
    var yyyy = this.getFullYear().toString();
    var MM = pad(this.getMonth(), 2);
    var dd = pad(this.getDate(), 2);
    var hh = pad(this.getHours(), 2);
    var mm = pad(this.getMinutes(), 2);
    var ss = pad(this.getSeconds(), 2);

    return yyyy+MM+dd+hh+mm+ss;
};

function getDate() {
    d = new Date();
    return d.YYYYMMDDHHMMSS();
}

function backup_and_clear(db_name, table_names) {
    db = db.getSiblingDB(db_name);
    db.copyDatabase(db_name, db_name+"backup"+getDate());
    table_names.forEach(function(table_name) {
        result = db[table_name].remove({});
        print("cleardb:" + db_name + " table:" + table_name, result);
    });
}

function backup_and_clear_all(db_name) {
    db = db.getSiblingDB(db_name);
    return backup_and_clear(db_name, db.getCollectionNames());
}

/*
清除账户信息 (game_account: ejoy_token, ejoy_sub_account)
清除用户信息 （game_server: game_player, account_info)
清除统计信息 (两个库里的全部collection: indicator_stat, repository)
清除奖励mark (reward: reward_mark, campaign_mark)
好友清档 (friend: friend_relation, friends_ext, friend_apply, friend_blank, friend_group)
聊天清档
1.关{product}_chat docker
2.清{product}_chat下的所有table
3.开{product}_chat docker
 */

var product_name = "P10020";
var game_account_db = product_name + "_game_account";
backup_and_clear(game_account_db, ["ejoy_token", "ejoy_sub_account"]);
var game_server_db = product_name + "_game_server";
backup_and_clear(game_server_db, ["game_player", "account_info"]);
backup_and_clear_all(product_name + "_indicator_stat");
backup_and_clear_all(product_name + "_repository");
var reward_db = product_name + "_reward";
backup_and_clear(reward_db, ["reward_mark", "campaign_mark"]);
var friend_db = product_name + "_friend";
backup_and_clear(friend_db, ["friend_relation", "friends_ext", "friend_apply", "friend_blank", "friend_group"]);
var chat_db = product_name + "_chat";
backup_and_clear_all(chat_db);
