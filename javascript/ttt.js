var chat_db = product_name + "_chat";
var ns = product_name + "_chat.msg";
db = db.getSiblingDB(chat_db);
print(db);
db.createCollection("msg_tmp", {})
db.msg_tmp.createIndexes([
  {
    "v": 2,
    "unique": true,
    "key": {
      "msg_id": 1
    },
    "name": "msg_id_1",
  },
  {
    "v": 2,
    "key": {
      "session_id": 1,
      "reader_status": 1,
      "to_id": 1
    },
    "name": "session_id_1_reader_status_1_to_id_1",
  },
  {
    "v": 2,
    "key": {
      "session_id": 1,
      "ts": 1
    },
    "name": "session_id_1_ts_1",
  }
]);