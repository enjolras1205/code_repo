guild_create_log = [
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215958055"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "363792"
      },
      "accInfo": {
        "accountId": "7a8b3a9174680960758e70511a4c3bab"
      },
      "devInfo": {
        "model": "V1818A",
        "utdid": "XCv/B0eAJOcDAFGgrS9VjdbN",
        "os": "android",
        "deviceId": "862889040077292"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 129,
      "stray": 2,
      "manor": [
        21,
        39
      ],
      "roleId": 22762012,
      "celebrity": 3961.0138888888887,
      "time": 1585215958,
      "guildName": "桃园",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 225,
      "accountId": "7a8b3a9174680960758e70511a4c3bab"
    },
    "time": {
      "$date": 1585215958000
    },
    "_id": {
      "$oid": "5e7c79d6fff85f3f00078611"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215949258"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "e8f4edb7a4b48ac25fde29f2fd7d2a54"
      },
      "devInfo": {
        "model": "SM-N9760",
        "utdid": "XmOpmvlIkJ0DAAQ+7NCpoQBd",
        "os": "android",
        "deviceId": "865166023349593"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 163,
      "stray": 2,
      "manor": [
        32,
        39
      ],
      "roleId": 22763362,
      "celebrity": 3911.0222222222224,
      "time": 1585215949,
      "guildName": "红颜霸业",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 224,
      "accountId": "e8f4edb7a4b48ac25fde29f2fd7d2a54"
    },
    "time": {
      "$date": 1585215949000
    },
    "_id": {
      "$oid": "5e7c79cdfff85f3f000773b7"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215948627"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "831dc1f42100bb18ef1d94316647d339"
      },
      "devInfo": {
        "model": "HD1900",
        "utdid": "XmmeZkfgIFUDAKJvZLvh7JQE",
        "os": "android",
        "deviceId": "865166021050771"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 139,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762823,
      "celebrity": 3911,
      "time": 1585215948,
      "guildName": "风云再骑",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 223,
      "accountId": "831dc1f42100bb18ef1d94316647d339"
    },
    "time": {
      "$date": 1585215948000
    },
    "_id": {
      "$oid": "5e7c79ccfff85f3f000771d6"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215947236"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "952381154afbeb366cf6ded786687556"
      },
      "devInfo": {
        "model": "SM-G9750",
        "utdid": "XmmeZWzglXQDAOaPoaB5aja+",
        "os": "android",
        "deviceId": "865166027438806"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 125,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762813,
      "celebrity": 3961.0222222222224,
      "time": 1585215947,
      "guildName": "一骑绝世",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 222,
      "accountId": "952381154afbeb366cf6ded786687556"
    },
    "time": {
      "$date": 1585215947000
    },
    "_id": {
      "$oid": "5e7c79cbfff85f3f00076ebe"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933586"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "3106bad7b5317cdeae10ada369761864"
      },
      "devInfo": {
        "model": "V1824A",
        "utdid": "XmmeZhoSf/0DAHKAD+/XcVKY",
        "os": "android",
        "deviceId": "865166020326123"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 118,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762822,
      "celebrity": 3911.011111111111,
      "time": 1585215933,
      "guildName": "三国风月",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 215,
      "accountId": "3106bad7b5317cdeae10ada369761864"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f0007509c"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933719"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "9781de7dbfcd8eea27192a45f5a37c81"
      },
      "devInfo": {
        "model": "DT1901A",
        "utdid": "XmmeZYcfvPIDAHmLcrbsxkgE",
        "os": "android",
        "deviceId": "865166020765734"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 142,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762821,
      "celebrity": 3911.025,
      "time": 1585215933,
      "guildName": "征战七州",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 218,
      "accountId": "9781de7dbfcd8eea27192a45f5a37c81"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f000750e8"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933566"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "7d0345c55f168d0f85a3bda76b8f8187"
      },
      "devInfo": {
        "model": "PCT-AL10",
        "utdid": "XmmeZY+BtqMDAN+fmAWjZmIi",
        "os": "android",
        "deviceId": "865166027017709"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 139,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762820,
      "celebrity": 3961.0194444444446,
      "time": 1585215933,
      "guildName": "三国风云",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 214,
      "accountId": "7d0345c55f168d0f85a3bda76b8f8187"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f0007508e"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933817"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "01c39d88554f1f1c39a494c8cb205ac9"
      },
      "devInfo": {
        "model": "V1923A",
        "utdid": "XmmeZQBlPP0DAE7KHZanFTFv",
        "os": "android",
        "deviceId": "865166024853015"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 125,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762818,
      "celebrity": 3911.0194444444446,
      "time": 1585215933,
      "guildName": "热血三国",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 221,
      "accountId": "01c39d88554f1f1c39a494c8cb205ac9"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f00075138"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933734"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "b20793688b6915e9a101ee05f0c49b0a"
      },
      "devInfo": {
        "model": "V1824A",
        "utdid": "XmmeZVEgka4DAHKBeFCXb53I",
        "os": "android",
        "deviceId": "865166020329424"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 132,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762817,
      "celebrity": 3911.0027777777777,
      "time": 1585215933,
      "guildName": "征战七霸",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 219,
      "accountId": "b20793688b6915e9a101ee05f0c49b0a"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f000750fe"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933673"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "01949f90765bbca919c441741934aadc"
      },
      "devInfo": {
        "model": "HMA-AL00",
        "utdid": "XmmeZQ7l/G4DAHk2dNaS0lKs",
        "os": "android",
        "deviceId": "865166020704816"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 125,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762815,
      "celebrity": 3961.0027777777777,
      "time": 1585215933,
      "guildName": "无心风月",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 217,
      "accountId": "01949f90765bbca919c441741934aadc"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f000750d1"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933631"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "7bc9df86bee422b3d65a93a7d267cf98"
      },
      "devInfo": {
        "model": "SM-N9760",
        "utdid": "XmmeZnV8icEDAEKtChj89tKr",
        "os": "android",
        "deviceId": "865166024121264"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 132,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762814,
      "celebrity": 3911.011111111111,
      "time": 1585215933,
      "guildName": "三国之族",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 216,
      "accountId": "7bc9df86bee422b3d65a93a7d267cf98"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f000750b5"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215933762"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "be1eac53edf3af3f6c056735de67f879"
      },
      "devInfo": {
        "model": "PCRT00",
        "utdid": "XmmeZYvmYr0DAFc9/zS+xAPL",
        "os": "android",
        "deviceId": "865166029849620"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 128,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762811,
      "celebrity": 3961.008333333333,
      "time": 1585215933,
      "guildName": "无关风月",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 220,
      "accountId": "be1eac53edf3af3f6c056735de67f879"
    },
    "time": {
      "$date": 1585215933000
    },
    "_id": {
      "$oid": "5e7c79bdfff85f3f00075115"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215895353"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "WM_49163",
        "ch": "511314"
      },
      "accInfo": {
        "accountId": "bd303e6d60dc69806a8f83dee5ff2fb1"
      },
      "devInfo": {
        "model": "mi note lte",
        "utdid": "XnbgaDd1LccDAFd2WdRWC9zb",
        "os": "android",
        "deviceId": "865166029889550"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1848,
      "force": 105,
      "manor": [
        22,
        40
      ],
      "stray": 2,
      "roleId": 22743374,
      "celebrity": 4031.0222222222224,
      "season": 1,
      "guildName": "春秋霸业",
      "time": 1585215895,
      "serverId": "S100848",
      "subServerId": 1848,
      "guildId": 175,
      "accountId": "bd303e6d60dc69806a8f83dee5ff2fb1"
    },
    "time": {
      "$date": 1585215895000
    },
    "_id": {
      "$oid": "5e7c7997e0f85f3f00b5984a"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215878240"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "KM_10296",
        "ch": "511314"
      },
      "accInfo": {
        "accountId": "1401868fe5911274bc164783fe6a6ee4"
      },
      "devInfo": {
        "model": "vivo X9",
        "utdid": "WL48MKzKddkDAOlXeOelExaD",
        "os": "android",
        "deviceId": "863504031219923"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 135,
      "stray": 2,
      "manor": [
        20,
        35
      ],
      "roleId": 22762424,
      "celebrity": 3561.0222222222224,
      "time": 1585215878,
      "guildName": "山东纯人帮",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 213,
      "accountId": "1401868fe5911274bc164783fe6a6ee4"
    },
    "time": {
      "$date": 1585215878000
    },
    "_id": {
      "$oid": "5e7c7986fff85f3f0006d808"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215824566"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "84"
      },
      "chInfo": {
        "ch2": "AAAD_17835",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "8c41979ad249ce25e59bc3a70ab48792"
      },
      "devInfo": {
        "model": "V1932A",
        "utdid": "XkYnumL4y0MDACXaA6jzFBvM",
        "os": "android",
        "deviceId": "866259043709414"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1840,
      "force": 344,
      "stray": 2,
      "manor": [
        40,
        40
      ],
      "roleId": 22679253,
      "celebrity": 4063.022222222222,
      "season": 1,
      "guildName": "我装比了吗",
      "time": 1585215824,
      "serverId": "S100840",
      "subServerId": 1840,
      "guildId": 301,
      "accountId": "8c41979ad249ce25e59bc3a70ab48792"
    },
    "time": {
      "$date": 1585215824000
    },
    "_id": {
      "$oid": "5e7c7950e8f85f3f0091c36f"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215772184"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982300"
      },
      "accInfo": {
        "accountId": "04b0a7732e1211a0fd6de0529c08a856"
      },
      "devInfo": {
        "model": "PBBM00",
        "utdid": "WkkJp5811/MDAKrYocsUu921",
        "os": "android",
        "deviceId": "868501043634896"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1847,
      "force": 295,
      "stray": 2,
      "manor": [
        32,
        40
      ],
      "roleId": 22753878,
      "celebrity": 4055.0222222222224,
      "time": 1585215772,
      "guildName": "笑傲宫",
      "season": 1,
      "serverId": "S100847",
      "subServerId": 1847,
      "guildId": 196,
      "accountId": "04b0a7732e1211a0fd6de0529c08a856"
    },
    "time": {
      "$date": 1585215772000
    },
    "_id": {
      "$oid": "5e7c791ce1f85f3f006836dd"
    },
    "event": "guild.create"
  },
  {
    "time": {
      "$date": 1585215769000
    },
    "ts": {
      "$long": "1585215769306"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "50"
      },
      "chInfo": {
        "ch2": "",
        "ch": ""
      },
      "accInfo": {
        "accountId": "e1054eeef7e80b816bd707a2e11f62b7"
      },
      "devInfo": {
        "model": "",
        "utdid": "",
        "os": "windows"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1844,
      "force": 172,
      "stray": 2,
      "manor": [
        21,
        37
      ],
      "roleId": 22765161,
      "celebrity": 3758.025,
      "season": 1,
      "guildName": "江汉西蜀",
      "time": 1585215769,
      "serverId": "S100844",
      "subServerId": 1844,
      "guildId": 350,
      "accountId": "e1054eeef7e80b816bd707a2e11f62b7"
    },
    "appId": 10000100,
    "_id": {
      "$oid": "5e7c7919e4f85f3f00521d60"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215768020"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "a6b5be331b4e87b9bee59d959e49a201"
      },
      "devInfo": {
        "model": "vivo x6a",
        "utdid": "XnVwGWju/KoDAOiK5T02ILRy",
        "os": "android",
        "deviceId": "865166027588568"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 105,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762201,
      "celebrity": 3961,
      "time": 1585215768,
      "guildName": "一怒为红颜",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 212,
      "accountId": "a6b5be331b4e87b9bee59d959e49a201"
    },
    "time": {
      "$date": 1585215768000
    },
    "_id": {
      "$oid": "5e7c7918fff85f3f0005e097"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215703090"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "67"
      },
      "chInfo": {
        "ch2": "AAGV_30",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "948f42cd5fc160d68b4065c812a5b33f"
      },
      "devInfo": {
        "model": "PAR-AL00",
        "utdid": "W+pWp6bsb1oDAL2Uc+uSTiH/",
        "os": "android",
        "deviceId": "860477046981162"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 131,
      "stray": 2,
      "manor": [
        24,
        35
      ],
      "roleId": 22762387,
      "celebrity": 3576.0027777777777,
      "time": 1585215703,
      "guildName": "戎马倥偬",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 211,
      "accountId": "948f42cd5fc160d68b4065c812a5b33f"
    },
    "time": {
      "$date": 1585215703000
    },
    "_id": {
      "$oid": "5e7c78d7fff85f3f00054de5"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215691888"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "77"
      },
      "chInfo": {
        "ch2": "AAGV_42",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "3512e698dbcfe59d3efed5fea43fe6fe"
      },
      "devInfo": {
        "model": "SM-G8870",
        "utdid": "XFASixJiYsEDAH2ElonB/Nrs",
        "os": "android",
        "deviceId": "352806100147846"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 192,
      "stray": 2,
      "manor": [
        26,
        39
      ],
      "roleId": 22761387,
      "celebrity": 3936.0222222222224,
      "time": 1585215691,
      "guildName": "唯尊天下",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 210,
      "accountId": "3512e698dbcfe59d3efed5fea43fe6fe"
    },
    "time": {
      "$date": 1585215691000
    },
    "_id": {
      "$oid": "5e7c78cbfff85f3f00053736"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215683705"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "41e0e54ea4b1c626ffa884f7ec142918"
      },
      "devInfo": {
        "model": "iPhone 6s",
        "utdid": "XbbB7ZrT/X8DAEgKiPSwkWR9",
        "os": "ios",
        "deviceId": "3381DDFD-B095-4F49-8179-2E1331723702"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 173,
      "stray": 2,
      "manor": [
        31,
        39
      ],
      "roleId": 22762324,
      "celebrity": 3936.0055555555555,
      "time": 1585215683,
      "guildName": "十里桃花",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 209,
      "accountId": "41e0e54ea4b1c626ffa884f7ec142918"
    },
    "time": {
      "$date": 1585215683000
    },
    "_id": {
      "$oid": "5e7c78c3fff85f3f00052328"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215682898"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "363792"
      },
      "accInfo": {
        "accountId": "8ea05592f7004399239ce418546353be"
      },
      "devInfo": {
        "model": "V1809A",
        "utdid": "WzgakFIFQVADACkEjSSO+ILS",
        "os": "android",
        "deviceId": "869902045014898"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 125,
      "stray": 2,
      "manor": [
        23,
        39
      ],
      "roleId": 22761242,
      "celebrity": 3911.0055555555555,
      "time": 1585215682,
      "guildName": "土匪三国",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 208,
      "accountId": "8ea05592f7004399239ce418546353be"
    },
    "time": {
      "$date": 1585215682000
    },
    "_id": {
      "$oid": "5e7c78c2fff85f3f0005218e"
    },
    "event": "guild.create"
  },
  {
    "time": {
      "$date": 1585215652000
    },
    "ts": {
      "$long": "1585215652838"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "67"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982299"
      },
      "accInfo": {
        "accountId": "6cd26242acbba0f6e22da8b9f8d3dae8"
      },
      "devInfo": {
        "model": "VOG-AL00",
        "utdid": "XnYKerIBEd4DAA/hDd2uz5wt",
        "os": "android",
        "deviceId": "865166023297479"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1655,
      "force": 195,
      "manor": [
        21,
        40
      ],
      "stray": 2,
      "roleId": 22752291,
      "celebrity": 4014.0055555555555,
      "time": 1585215652,
      "guildName": "羽毛贰盟",
      "season": 1,
      "serverId": "S100655",
      "subServerId": 1655,
      "guildId": 261,
      "accountId": "6cd26242acbba0f6e22da8b9f8d3dae8"
    },
    "appId": 10000100,
    "_id": {
      "$oid": "5e7c78a4d0f05f3f00be393b"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215627805"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982300"
      },
      "accInfo": {
        "accountId": "10699b6ea55632a6b026a8596fb53057"
      },
      "devInfo": {
        "model": "PAFM00",
        "utdid": "WkkJnZRQDlYDABcOTHN0+/QY",
        "os": "android",
        "deviceId": "867278042879779"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1848,
      "force": 106,
      "manor": [
        20,
        37
      ],
      "stray": 2,
      "roleId": 22763559,
      "celebrity": 3710.0194444444446,
      "season": 1,
      "guildName": "凨雪阁",
      "time": 1585215627,
      "serverId": "S100848",
      "subServerId": 1848,
      "guildId": 174,
      "accountId": "10699b6ea55632a6b026a8596fb53057"
    },
    "time": {
      "$date": 1585215627000
    },
    "_id": {
      "$oid": "5e7c788be0f85f3f00b45cde"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215622534"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "edc6edf8b2da32b523b9711b92927b1e"
      },
      "devInfo": {
        "model": "SEA-AL10",
        "utdid": "XmIl2SdlH/sDAEtWhCLxI2zW",
        "os": "android",
        "deviceId": "865166029151563"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 105,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22763734,
      "celebrity": 3910.0249999999996,
      "time": 1585215622,
      "guildName": "战武四",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 207,
      "accountId": "edc6edf8b2da32b523b9711b92927b1e"
    },
    "time": {
      "$date": 1585215622000
    },
    "_id": {
      "$oid": "5e7c7886fff85f3f00049346"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215586140"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982300"
      },
      "accInfo": {
        "accountId": "8ba9816891a1ccf752b0a9c5e3331776"
      },
      "devInfo": {
        "model": "OPPO A83",
        "utdid": "WvQ2bvIruLUDAK5glgZX4Ml5",
        "os": "android",
        "deviceId": "868902032594937"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 161,
      "stray": 2,
      "manor": [
        28,
        37
      ],
      "roleId": 22763881,
      "celebrity": 3749.008333333333,
      "time": 1585215586,
      "guildName": "无间双龙",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 206,
      "accountId": "8ba9816891a1ccf752b0a9c5e3331776"
    },
    "time": {
      "$date": 1585215586000
    },
    "_id": {
      "$oid": "5e7c7862fff85f3f00044302"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215469370"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982298"
      },
      "accInfo": {
        "accountId": "73c4eeb8ba89f197c78e140e740004f0"
      },
      "devInfo": {
        "model": "V1732A",
        "utdid": "WzepnJEoXE0DAJ0blyFdqY5f",
        "os": "android",
        "deviceId": "865033043567911"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1842,
      "force": 420,
      "manor": [
        26,
        45
      ],
      "stray": 2,
      "roleId": 22678796,
      "celebrity": 4583,
      "time": 1585215469,
      "guildName": "东吴",
      "season": 1,
      "serverId": "S100842",
      "subServerId": 1842,
      "guildId": 281,
      "accountId": "73c4eeb8ba89f197c78e140e740004f0"
    },
    "time": {
      "$date": 1585215469000
    },
    "_id": {
      "$oid": "5e7c77ede6f85f3f007df1bd"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215462650"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "3dda91b385ddde7026d4b32188da3c49"
      },
      "devInfo": {
        "model": "iPhone X",
        "utdid": "XZykjDUjgGgDAMZikR4AhwcJ",
        "os": "ios",
        "deviceId": "8434A3A0-3077-4AD9-A646-72F7CF478364"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 109,
      "stray": 2,
      "manor": [
        20,
        37
      ],
      "roleId": 22764132,
      "celebrity": 3749,
      "time": 1585215462,
      "guildName": "三分天下",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 205,
      "accountId": "3dda91b385ddde7026d4b32188da3c49"
    },
    "time": {
      "$date": 1585215462000
    },
    "_id": {
      "$oid": "5e7c77e6fff85f3f00032b25"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215461726"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "59"
      },
      "chInfo": {
        "ch2": "AAFH_8324",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "081046681b0397f7c860c7b88e400b72"
      },
      "devInfo": {
        "model": "HWI-AL00",
        "utdid": "XEutTNR+YH0DAOnUXS7a7qYi",
        "os": "android",
        "deviceId": "862698048208720"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 186,
      "stray": 2,
      "manor": [
        21,
        37
      ],
      "roleId": 22760982,
      "celebrity": 3760.011111111111,
      "time": 1585215461,
      "guildName": "一飞冲天",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 204,
      "accountId": "081046681b0397f7c860c7b88e400b72"
    },
    "time": {
      "$date": 1585215461000
    },
    "_id": {
      "$oid": "5e7c77e5fff85f3f000328d8"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215446256"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "c968b1a0d6678920183ab0f8ed28cf19"
      },
      "devInfo": {
        "model": "iPhone 7",
        "utdid": "XZxxKkb1KVMDAB7bwIKX7rwR",
        "os": "ios",
        "deviceId": "4B1D4078-08EB-4E4E-B42B-EC3E5FCDC0F4"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 122,
      "stray": 2,
      "manor": [
        22,
        40
      ],
      "roleId": 22762880,
      "celebrity": 4010.0138888888887,
      "time": 1585215446,
      "guildName": "西岭",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 203,
      "accountId": "c968b1a0d6678920183ab0f8ed28cf19"
    },
    "time": {
      "$date": 1585215446000
    },
    "_id": {
      "$oid": "5e7c77d6fff85f3f00030642"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215410072"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "ee50658236d5f058738c5d34513800d8"
      },
      "devInfo": {
        "model": "M5 Note",
        "utdid": "WfGm0AsbEuYDAExFGwxS4J+V",
        "os": "android",
        "deviceId": "864105034252082"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 128,
      "stray": 2,
      "manor": [
        24,
        39
      ],
      "roleId": 22762401,
      "celebrity": 3960.0111111111114,
      "time": 1585215410,
      "guildName": "手摘星辰",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 202,
      "accountId": "ee50658236d5f058738c5d34513800d8"
    },
    "time": {
      "$date": 1585215410000
    },
    "_id": {
      "$oid": "5e7c77b2fff85f3f0002b7ee"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215372082"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "81254fb67a3fcc21fbf8f2acdc1c295a"
      },
      "devInfo": {
        "model": "V1938CT",
        "utdid": "XmTK2XYYUdUDAKomUzLF2EtL",
        "os": "android",
        "deviceId": "865166020634617"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 115,
      "stray": 2,
      "manor": [
        23,
        39
      ],
      "roleId": 22759609,
      "celebrity": 3961.025,
      "time": 1585215372,
      "guildName": "明眸皓齿",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 201,
      "accountId": "81254fb67a3fcc21fbf8f2acdc1c295a"
    },
    "time": {
      "$date": 1585215372000
    },
    "_id": {
      "$oid": "5e7c778cfff85f3f00025c94"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215357765"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "58"
      },
      "chInfo": {
        "ch2": "AO_800",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "5b721643adc8fc587f678a5b499f25f5"
      },
      "devInfo": {
        "model": "JSN-AL00a",
        "utdid": "XP5fvlzXqIkDAM7x5R3WTCCK",
        "os": "android"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 335,
      "stray": 2,
      "manor": [
        25,
        39
      ],
      "roleId": 22763328,
      "celebrity": 3959,
      "time": 1585215357,
      "guildName": "酒池肉林",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 200,
      "accountId": "5b721643adc8fc587f678a5b499f25f5"
    },
    "time": {
      "$date": 1585215357000
    },
    "_id": {
      "$oid": "5e7c777dfff85f3f0002393a"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215356908"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "71f3861f38d278936498c78a73efed44"
      },
      "devInfo": {
        "model": "LIO-AN00",
        "utdid": "XnLxVwjgnK0DAIaGMVn8RxvU",
        "os": "android",
        "deviceId": "865166023782942"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 125,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22760783,
      "celebrity": 3960.016666666667,
      "time": 1585215356,
      "guildName": "霸气",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 199,
      "accountId": "71f3861f38d278936498c78a73efed44"
    },
    "time": {
      "$date": 1585215356000
    },
    "_id": {
      "$oid": "5e7c777cfff85f3f00023774"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215351507"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "363792"
      },
      "accInfo": {
        "accountId": "33743f601b51d93a510f93cc24dc077d"
      },
      "devInfo": {
        "model": "OPPO R9tm",
        "utdid": "VoVRBBLi6QsDAAigqpQOuyV1",
        "os": "android",
        "deviceId": "861463038735435"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 212,
      "stray": 2,
      "manor": [
        20,
        39
      ],
      "roleId": 22760823,
      "celebrity": 3960.0055555555555,
      "time": 1585215351,
      "guildName": "耀武扬威",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 198,
      "accountId": "33743f601b51d93a510f93cc24dc077d"
    },
    "time": {
      "$date": 1585215351000
    },
    "_id": {
      "$oid": "5e7c7777fff85f3f00022af2"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215339218"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982300"
      },
      "accInfo": {
        "accountId": "c6521282062e0a03db7962fe5c40b5ce"
      },
      "devInfo": {
        "model": "OPPO R11s",
        "utdid": "XbtIlVQE0mwDAA04rftnh0UI",
        "os": "android",
        "deviceId": "868393037599032"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1847,
      "force": 258,
      "stray": 2,
      "manor": [
        32,
        40
      ],
      "roleId": 22750107,
      "celebrity": 4049.0166666666664,
      "time": 1585215339,
      "guildName": "爱好和平",
      "season": 1,
      "serverId": "S100847",
      "subServerId": 1847,
      "guildId": 195,
      "accountId": "c6521282062e0a03db7962fe5c40b5ce"
    },
    "time": {
      "$date": 1585215339000
    },
    "_id": {
      "$oid": "5e7c776be1f85f3f00670d96"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215316881"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "1149b977a89c9645dd266ad41dfd39aa"
      },
      "devInfo": {
        "model": "iPhone 7 Plus",
        "utdid": "Xmbpxbc1vPwDAKTZ0OPpabGV",
        "os": "ios",
        "deviceId": "00000000-0000-0000-0000-000000000000"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 153,
      "stray": 2,
      "manor": [
        20,
        40
      ],
      "roleId": 22764577,
      "celebrity": 4008.0194444444446,
      "time": 1585215316,
      "guildName": "踏平西凉",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 197,
      "accountId": "1149b977a89c9645dd266ad41dfd39aa"
    },
    "time": {
      "$date": 1585215316000
    },
    "_id": {
      "$oid": "5e7c7754fff85f3f0001de3c"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215275568"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "bb74081b67797bb337c42e16eb0a96c8"
      },
      "devInfo": {
        "model": "vivo Y66",
        "utdid": "XV6FRcPM2F4DAEqysaRjahll",
        "os": "android"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 215,
      "stray": 2,
      "manor": [
        28,
        37
      ],
      "roleId": 22761445,
      "celebrity": 3710.008333333333,
      "time": 1585215275,
      "guildName": "龙族",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 196,
      "accountId": "bb74081b67797bb337c42e16eb0a96c8"
    },
    "time": {
      "$date": 1585215275000
    },
    "_id": {
      "$oid": "5e7c772bfff85f3f000175fe"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215260148"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "67"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982299"
      },
      "accInfo": {
        "accountId": "df5c32f99bc917be5f7a17aed2bcddd7"
      },
      "devInfo": {
        "model": "ANE-AL00",
        "utdid": "XSHxyMDIA0kDAFQAC6kkr97G",
        "os": "android",
        "deviceId": "860207046220471"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 145,
      "stray": 2,
      "manor": [
        26,
        39
      ],
      "roleId": 22761712,
      "celebrity": 3985.0138888888887,
      "time": 1585215260,
      "guildName": "煮酒论英雄",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 195,
      "accountId": "df5c32f99bc917be5f7a17aed2bcddd7"
    },
    "time": {
      "$date": 1585215260000
    },
    "_id": {
      "$oid": "5e7c771cfff85f3f0001544e"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215256236"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "f03af985c3d0f4b664791ff6cf67d80f"
      },
      "devInfo": {
        "model": "M973Q",
        "utdid": "XmIl2bdSzFsDAA9ujryqVbq9",
        "os": "android",
        "deviceId": "865166023213823"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 116,
      "stray": 2,
      "manor": [
        20,
        39
      ],
      "roleId": 22763738,
      "celebrity": 3909.0055555555555,
      "time": 1585215256,
      "guildName": "战武三",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 194,
      "accountId": "f03af985c3d0f4b664791ff6cf67d80f"
    },
    "time": {
      "$date": 1585215256000
    },
    "_id": {
      "$oid": "5e7c7718fff85f3f00014bb7"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215256228"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "87755872d2ed4ca1aef401261459d81a"
      },
      "devInfo": {
        "model": "WLZ-AN00",
        "utdid": "XmIl2uk9JioDABmVXfrmXMD1",
        "os": "android",
        "deviceId": "865166028305798"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 116,
      "stray": 2,
      "manor": [
        20,
        39
      ],
      "roleId": 22763736,
      "celebrity": 3909.011111111111,
      "time": 1585215256,
      "guildName": "战武二",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 193,
      "accountId": "87755872d2ed4ca1aef401261459d81a"
    },
    "time": {
      "$date": 1585215256000
    },
    "_id": {
      "$oid": "5e7c7718fff85f3f00014bb1"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215256216"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "bebc6c737bd5cf4749652762999709b1"
      },
      "devInfo": {
        "model": "YAL-AL10",
        "utdid": "XmIl2e2HQ4YDAIOg/3oaTa6/",
        "os": "android",
        "deviceId": "865166025840797"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 126,
      "stray": 2,
      "manor": [
        21,
        39
      ],
      "roleId": 22763735,
      "celebrity": 3909.0055555555555,
      "time": 1585215256,
      "guildName": "战武",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 192,
      "accountId": "bebc6c737bd5cf4749652762999709b1"
    },
    "time": {
      "$date": 1585215256000
    },
    "_id": {
      "$oid": "5e7c7718fff85f3f00014baa"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215250425"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "cb89be76051b79ff4ed792fb5a3ce0ca"
      },
      "devInfo": {
        "model": "iPhone12,5",
        "utdid": "XntG18yS2QwDAM42+dKAeUXe",
        "os": "ios",
        "deviceId": "A40B015E-1EF4-4432-8A30-4360F89BBC0F"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 136,
      "stray": 2,
      "manor": [
        21,
        36
      ],
      "roleId": 22760361,
      "celebrity": 3610.0055555555555,
      "time": 1585215250,
      "guildName": "大秦纵横",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 191,
      "accountId": "cb89be76051b79ff4ed792fb5a3ce0ca"
    },
    "time": {
      "$date": 1585215250000
    },
    "_id": {
      "$oid": "5e7c7712fff85f3f00013d8f"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215239902"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "896c7072f7e67182e7827193eff927dd"
      },
      "devInfo": {
        "model": "iPhone XS Max",
        "utdid": "Xlp8OYaVIo8DABq9rUd4bGfb",
        "os": "ios",
        "deviceId": "AE40CF28-558C-459A-A630-B887D35D49D7"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 188,
      "stray": 2,
      "manor": [
        24,
        39
      ],
      "roleId": 22762108,
      "celebrity": 3959.011111111111,
      "time": 1585215239,
      "guildName": "琅琊阁",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 190,
      "accountId": "896c7072f7e67182e7827193eff927dd"
    },
    "time": {
      "$date": 1585215239000
    },
    "_id": {
      "$oid": "5e7c7707fff85f3f00012410"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215208289"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "50"
      },
      "chInfo": {
        "ch2": "",
        "ch": ""
      },
      "accInfo": {
        "accountId": "5d7fb98d8a7dcd2616ae696d18ffedff"
      },
      "devInfo": {
        "model": "",
        "utdid": "",
        "os": "windows"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 177,
      "stray": 2,
      "manor": [
        27,
        39
      ],
      "roleId": 22761528,
      "celebrity": 3960.0027777777777,
      "time": 1585215208,
      "guildName": "战無不胜",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 189,
      "accountId": "5d7fb98d8a7dcd2616ae696d18ffedff"
    },
    "time": {
      "$date": 1585215208000
    },
    "_id": {
      "$oid": "5e7c76e8fff85f3f0000d814"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215177577"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "69"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "249c45c1f3b1e05df02c4865d3965e83"
      },
      "devInfo": {
        "model": "iPad 4",
        "utdid": "XmTh0fDtOlkDAJTBOC5iKiSj",
        "os": "ios",
        "deviceId": "2AFB2746-536C-47C8-82C0-0252732E92D8"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 206,
      "stray": 2,
      "manor": [
        23,
        38
      ],
      "roleId": 22761119,
      "celebrity": 3885.011111111111,
      "time": 1585215177,
      "guildName": "义统天下",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 188,
      "accountId": "249c45c1f3b1e05df02c4865d3965e83"
    },
    "time": {
      "$date": 1585215177000
    },
    "_id": {
      "$oid": "5e7c76c9fff85f3f00009006"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215150822"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982299"
      },
      "accInfo": {
        "accountId": "5cb6d841667816cc4fbc0d30aa6b0005"
      },
      "devInfo": {
        "model": "HLK-AL00",
        "utdid": "XcT1OnbcL1wDAMFMfY+PME/k",
        "os": "android",
        "deviceId": "860530046857848"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 109,
      "stray": 2,
      "manor": [
        21,
        36
      ],
      "roleId": 22761398,
      "celebrity": 3609.0027777777777,
      "time": 1585215150,
      "guildName": "落花倾天下",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 187,
      "accountId": "5cb6d841667816cc4fbc0d30aa6b0005"
    },
    "time": {
      "$date": 1585215150000
    },
    "_id": {
      "$oid": "5e7c76aefff85f3f00004b2c"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215122477"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982298"
      },
      "accInfo": {
        "accountId": "41dc4721cfd1edf056bb5da81cc4ed85"
      },
      "devInfo": {
        "model": "V1809T",
        "utdid": "WzgZ86AB9DADAPtqR2yjAD5/",
        "os": "android",
        "deviceId": "868080046974096"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 133,
      "stray": 2,
      "manor": [
        20,
        39
      ],
      "roleId": 22761189,
      "celebrity": 3984,
      "time": 1585215122,
      "guildName": "如烟阁",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 186,
      "accountId": "41dc4721cfd1edf056bb5da81cc4ed85"
    },
    "time": {
      "$date": 1585215122000
    },
    "_id": {
      "$oid": "5e7c7692fff85f3f0000017d"
    },
    "event": "guild.create"
  },
  {
    "time": {
      "$date": 1585215103000
    },
    "ts": {
      "$long": "1585215103235"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "2a5295882a27e371f0863d5961945fcb"
      },
      "devInfo": {
        "model": "iPhone12,1",
        "utdid": "Xjj5bz8fkgoDAKWwuYutuzEk",
        "os": "ios",
        "deviceId": "B1B2846A-3CD6-40CA-9E29-0B62025C09E7"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1843,
      "force": 175,
      "stray": 2,
      "manor": [
        21,
        39
      ],
      "roleId": 22660966,
      "celebrity": 3928.022222222221,
      "time": 1585215103,
      "guildName": "云轩楼",
      "season": 1,
      "serverId": "S100843",
      "subServerId": 1843,
      "guildId": 295,
      "accountId": "2a5295882a27e371f0863d5961945fcb"
    },
    "appId": 10000100,
    "_id": {
      "$oid": "5e7c767fe5f85f3f006d501b"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215096699"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "1e40a2fcb4af08c30688ed130d16b905"
      },
      "devInfo": {
        "model": "SM-G9750",
        "utdid": "XmhqLqC5nxMDAONOvDk5Uk4D",
        "os": "android",
        "deviceId": "865166027265043"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 112,
      "stray": 2,
      "manor": [
        21,
        39
      ],
      "roleId": 22762316,
      "celebrity": 3909.011111111111,
      "time": 1585215096,
      "guildName": "十年",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 185,
      "accountId": "1e40a2fcb4af08c30688ed130d16b905"
    },
    "time": {
      "$date": 1585215096000
    },
    "_id": {
      "$oid": "5e7c7678fff85f3f00ffc7a5"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215084583"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "3701a109b08380a83b3b4e855687be6f"
      },
      "devInfo": {
        "model": "YAL-AL10",
        "utdid": "XmTK2a+CLpMDAP4LA7+xiGKk",
        "os": "android",
        "deviceId": "865166028636218"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 110,
      "stray": 2,
      "manor": [
        26,
        39
      ],
      "roleId": 22759622,
      "celebrity": 3910.025,
      "time": 1585215084,
      "guildName": "浮华一世",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 184,
      "accountId": "3701a109b08380a83b3b4e855687be6f"
    },
    "time": {
      "$date": 1585215084000
    },
    "_id": {
      "$oid": "5e7c766cfff85f3f00ffab9c"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215073410"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "363792"
      },
      "accInfo": {
        "accountId": "ee11148fc1fa5336bbbcc505a14b394a"
      },
      "devInfo": {
        "model": "mi pad",
        "utdid": "XmhGuJQg4ygDAEZtM4lxhtmX",
        "os": "android",
        "deviceId": "865166024385919"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 130,
      "stray": 2,
      "manor": [
        28,
        37
      ],
      "roleId": 22760807,
      "celebrity": 3759.0055555555555,
      "time": 1585215073,
      "guildName": "一枝独秀",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 183,
      "accountId": "ee11148fc1fa5336bbbcc505a14b394a"
    },
    "time": {
      "$date": 1585215073000
    },
    "_id": {
      "$oid": "5e7c7661fff85f3f00ff9221"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215057144"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "67"
      },
      "chInfo": {
        "ch2": "AO_8",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "dd090f3bc549912f3b297c5da574e9dc"
      },
      "devInfo": {
        "model": "SEA-AL10",
        "utdid": "Xjkm5Hogz5cDAB2vvyoNti4m",
        "os": "android",
        "deviceId": "868997041688993"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 109,
      "stray": 2,
      "manor": [
        21,
        35
      ],
      "roleId": 22761582,
      "celebrity": 3559.011111111111,
      "time": 1585215057,
      "guildName": "戰歌",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 182,
      "accountId": "dd090f3bc549912f3b297c5da574e9dc"
    },
    "time": {
      "$date": 1585215057000
    },
    "_id": {
      "$oid": "5e7c7651fff85f3f00ff648d"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215053686"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "902412"
      },
      "accInfo": {
        "accountId": "bb8f56c404abbcd55c56b56d4a2458db"
      },
      "devInfo": {
        "model": "OXF-AN10",
        "utdid": "XlJh04Q735YDANsVf2o0BAyu",
        "os": "android",
        "deviceId": "865166023500591"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 112,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22762350,
      "celebrity": 3959,
      "time": 1585215053,
      "guildName": "驿城盟会",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 181,
      "accountId": "bb8f56c404abbcd55c56b56d4a2458db"
    },
    "time": {
      "$date": 1585215053000
    },
    "_id": {
      "$oid": "5e7c764dfff85f3f00ff5b03"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215045308"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "348b9de0c44086bdb9f0f09a64de9715"
      },
      "devInfo": {
        "model": "iPhone12,1",
        "utdid": "XnLU6milVIQDABnTemX1o5xM",
        "os": "ios",
        "deviceId": "29BDA316-EE2F-405A-8C82-1FDF1F92E388"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 218,
      "stray": 2,
      "manor": [
        24,
        37
      ],
      "roleId": 22761756,
      "celebrity": 3749.0027777777777,
      "time": 1585215045,
      "guildName": "御仙阁",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 180,
      "accountId": "348b9de0c44086bdb9f0f09a64de9715"
    },
    "time": {
      "$date": 1585215045000
    },
    "_id": {
      "$oid": "5e7c7645fff85f3f00ff473f"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215024190"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "54"
      },
      "chInfo": {
        "ch2": "",
        "ch": "982298"
      },
      "accInfo": {
        "accountId": "b7bbd75413d897d2f668cc1b1dd3364a"
      },
      "devInfo": {
        "model": "vivo X9L",
        "utdid": "WkTG5zPBUTMDABqWmtS+ziTc",
        "os": "android",
        "deviceId": "864743030792337"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 132,
      "stray": 2,
      "manor": [
        21,
        37
      ],
      "roleId": 22761037,
      "celebrity": 3749.0194444444446,
      "time": 1585215024,
      "guildName": "大唐王朝",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 179,
      "accountId": "b7bbd75413d897d2f668cc1b1dd3364a"
    },
    "time": {
      "$date": 1585215024000
    },
    "_id": {
      "$oid": "5e7c7630fff85f3f00ff19dc"
    },
    "event": "guild.create"
  },
  {
    "appId": 10000100,
    "ts": {
      "$long": "1585215000500"
    },
    "envInfo": {
      "gmInfo": {
        "appVer": "79"
      },
      "chInfo": {
        "ch2": "",
        "ch": "998233"
      },
      "accInfo": {
        "accountId": "759f3997d89414e656d6622b12f77171"
      },
      "devInfo": {
        "model": "iPhone 6 Plus",
        "utdid": "Xju/enTlfM0DAH191jxiIw7K",
        "os": "ios",
        "deviceId": "0791A14F-5E84-4720-BA5F-11A81B538D3B"
      }
    },
    "params": {
      "src": "gms",
      "seasonServerId": 1849,
      "force": 152,
      "stray": 2,
      "manor": [
        22,
        39
      ],
      "roleId": 22759788,
      "celebrity": 3985,
      "time": 1585215000,
      "guildName": "一统江山",
      "season": 1,
      "serverId": "S100849",
      "subServerId": 1849,
      "guildId": 178,
      "accountId": "759f3997d89414e656d6622b12f77171"
    },
    "time": {
      "$date": 1585215000000
    },
    "_id": {
      "$oid": "5e7c7618fff85f3f00fedf32"
    },
    "event": "guild.create"
  }
]

var chat_db = product_name + "_chat";
db = db.getSiblingDB(chat_db);
users = [];
guild_create_log.forEach(function (guild_log_data) {
  insert_guild_data = {
    "attr":
    {
      "direct_send": true,
      "enable_voice": false,
      "expire_ts": NumberInt(-1),
      "max_msg_in_one_min": NumberInt(0),
      "sync_member": false
    },
    "children_group_ids": [],
    "invited_members": [], "members": [],
    "owner_type": "game_server",
    "parent_group_id": ""
  };
  owner = `${guild_log_data["params"]["subServerId"]}`;
  info = { "name": guild_log_data["params"]["guildName"], "type": "alliance" };
  sub_server_id = guild_log_data["params"]["subServerId"];
  guild_id = guild_log_data["params"]["guildId"];
  group_id = `group_alliance_${sub_server_id}_${guild_id}`;
  insert_guild_data["group_id"] = group_id;
  insert_guild_data["owner"] = owner;
  insert_guild_data["info"] = info;
  ret = db.chat_group.insert(insert_guild_data);
  if (ret["nInserted"] == 1) {
    users.push(guild_log_data["params"]["roleId"]);
  }
});

print("affect users", users);