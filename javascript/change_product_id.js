
db = db.getSiblingDB("package_new");

old_id_to_new_id = { "P10020": "D10275" };

for (var key in old_id_to_new_id) {
  // check if the property/key is defined in the object itself, not in parent
  if (old_id_to_new_id.hasOwnProperty(key)) {
    print("changing_product", key, "to", old_id_to_new_id[key]);
    db.branch.find({ "product": key }).forEach(function (branch_info) {
      print("changing_revision", branch_info["_id"].str);
      db.branch.update({ "_id": branch_info["_id"] }, { $set: { "product": old_id_to_new_id[key] } }, upsert = false)
    });
  }
}