Date.prototype.YYYYMMDDHHMMSS = function () {
    var yyyy = this.getFullYear().toString();
    var MM = pad(this.getMonth() + 1,2);
    var dd = pad(this.getDate(), 2);
    var hh = pad(this.getHours(), 2);
    var mm = pad(this.getMinutes(), 2)
    var ss = pad(this.getSeconds(), 2)

    return yyyy + MM + dd+  hh + mm + ss;
};

function getDate() {
    d = new Date();
    return d.YYYYMMDDHHMMSS();
}

/*
清除账户信息 (game_account: ejoy_token, ejoy_sub_account)
清除用户信息 （game_server: game_player, account_info)
清除统计信息 (两个库里的全部collection: indicator_stat, repository)
清除奖励mark (reward: reward_mark, campaign_mark)
好友清档 (friend: friend_relation, friends_ext, friend_apply, friend_blank, friend_group)
聊天清档
1.关{product}_chat docker
2.清{product}_chat下的所有table
3.开{product}_chat docker
 */