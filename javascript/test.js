function pad(number, length) {

    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;

}

Date.prototype.YYYYMMDDHHMMSS = function () {
    var yyyy = this.getFullYear().toString();
    var MM = pad(this.getMonth(), 2);
    var dd = pad(this.getDate(), 2);
    var hh = pad(this.getHours(), 2);
    var mm = pad(this.getMinutes(), 2);
    var ss = pad(this.getSeconds(), 2);

    return yyyy+MM+dd+hh+mm+ss;
};

function getDate() {
    d = new Date();
    return d.YYYYMMDDHHMMSS();
}

function backup_and_clear(db_name, table_names) {
    db = db.getSiblingDB(db_name);
    db.copyDatabase(db_name, db_name+"backup"+getDate());
    table_names.forEach(function(table_name) {
        result = db[table_name].remove({});
        print("cleardb:" + db_name + " table:" + table_name, result);
    });
}

function backup_and_clear_all(db_name) {
    db = db.getSiblingDB(db_name);
    return backup_and_clear(db_name, db.getCollectionNames());
}

var game_account_db = product_name + "_game_account";
backup_and_clear(game_account_db, ["ejoy_token", "ejoy_sub_account"]);
var game_server_db = product_name + "_game_server";
backup_and_clear(game_server_db, ["game_player", "account_info", "search_player"]);
backup_and_clear_all(product_name + "_indicator_stat");
backup_and_clear_all(product_name + "_repository");
var reward_db = product_name + "_reward";
backup_and_clear(reward_db, ["reward_mark", "campaign_mark", "account_reward", "player_reward", "external_resource_mark"]);
var rt_campaign_data_db = product_name + "_rt_campaign_data";
backup_and_clear_all(rt_campaign_data_db);
var friend_db = product_name + "_friend";
backup_and_clear(friend_db, ["friend_relation", "friends_ext", "friend_apply", "friend_blank", "friend_group"]);
var follow_db = product_name + "_follow";
backup_and_clear(follow_db, ["follow_relation", "follow_group", "follow_ext"]);
var favor_db = product_name + "_favor";
backup_and_clear(favor_db, ["favor_relation"]);
var chat_db = product_name + "_chat";
backup_and_clear_all(chat_db, ["msg", "system_msg", "chat_group", "chat_group_broker", "user", "rule_msg", "push_template"]);
var moment_db = product_name + "_holo";
backup_and_clear_all(moment_db);
var top_db = product_name + "_top";
backup_and_clear_all(top_db);
var session_db = product_name + "_session";
backup_and_clear(session_db, ["moment_token"]);
var active_code_db = product_name + "_active_code";
backup_and_clear(active_code_db, ["active_code_mark"]);
var pusher_db = product_name + "_pusher";
backup_and_clear_all(pusher_db);