
var game_account_db_name = product_name + "_game_account";
var ejoy_token_col_name = "ejoy_token"
var ejoy_sub_account_col_name = "ejoy_sub_account"
var game_account_db = db.getSiblingDB(game_account_db_name)

var rows = [];
var platform_type_dict = {
  "AGST": "guest",
  "GOOGLE": "google",
  "FB": "facebook"
};
game_account_db[ejoy_sub_account_col_name].find().forEach(
  function (sub_account_obj) {
    var ejoy_token_cursor = game_account_db[ejoy_token_col_name].find({ "sub_account": sub_account_obj["sub_account"] })
    var ejoy_token_obj = ejoy_token_cursor.hasNext() ? ejoy_token_cursor.next() : null;
    if (ejoy_token_obj) {
      var with_account = ejoy_token_obj["_id"];
      if (typeof(with_account) == "object") {
        with_account = with_account.str;
      }
      var target_platform = new Array("AGST", "GOOGLE", "FB")
      var platform_type = sub_account_obj["platform"];
      var thirdpart_uid = sub_account_obj["platform_account"];
      if (target_platform.includes(platform_type)) {
        var target_platform_type = platform_type_dict[platform_type];
        rows.push([with_account, target_platform_type, thirdpart_uid]);
      }
    } else {
      print("cant find sub account ", to_json(sub_account_obj));
    }
  }
)

let csvContent = "";
rows.forEach(function(rowArray) {
  let row = rowArray.join(",");
  csvContent += row + "\r\n";
});

writeFile("/home/enjolras/code_repo/javascript/mcd.csv", csvContent);