function pad(number, length) {

  var str = '' + number;
  while (str.length < length) {
    str = '0' + str;
  }

  return str;

}

Date.prototype.YYYYMMDDHHMMSS = function () {
  var yyyy = this.getFullYear().toString();
  var MM = pad(this.getMonth(), 2);
  var dd = pad(this.getDate(), 2);
  var hh = pad(this.getHours(), 2);
  var mm = pad(this.getMinutes(), 2);
  var ss = pad(this.getSeconds(), 2);

  return yyyy + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss;
};

function getDate() {
  d = new Date();
  return d.YYYYMMDDHHMMSS();
}

var start_time = getDate();
print("start_time", start_time);
var chat_db = product_name + "_chat";
var rename_target = "msg_bk";
db = db.getSiblingDB(chat_db);
db.msg_tmp.drop();
db.createCollection("msg_tmp", {})
db.msg_tmp.createIndex(
  {
    "msg_id": 1
  },
  {
    "v": 2,
    "unique": true,
    "name": "msg_id_1",
  });
db.msg_tmp.createIndex(
  {
    "session_id": 1,
    "reader_status": 1,
    "to_id": 1
  },
  {
    "v": 2,
    "name": "session_id_1_reader_status_1_to_id_1",
  });
db.msg_tmp.createIndex(
  {
    "session_id": 1,
    "ts": 1
  },
  {
    "v": 2,
    "name": "session_id_1_ts_1",
  });
print(db);

// 这里不会覆盖target, 所以这个脚本是可以重复跑的。但两次跑之间的消息会丢失。
db.msg.renameCollection(rename_target, false);
db.msg_tmp.renameCollection("msg", false);
// 这里做一下容错， 防止聊天服务在开启中， 通过新消息创建了msg表，导致collection没有index
db.msg.createIndex(
  {
    "msg_id": 1
  },
  {
    "v": 2,
    "unique": true,
    "name": "msg_id_1",
  });
db.msg.createIndex(
  {
    "session_id": 1,
    "reader_status": 1,
    "to_id": 1
  },
  {
    "v": 2,
    "name": "session_id_1_reader_status_1_to_id_1",
  });
db.msg.createIndex(
  {
    "session_id": 1,
    "ts": 1
  },
  {
    "v": 2,
    "name": "session_id_1_ts_1",
  });


var cursor = db.msg_bk.find({ "session_id": { "$not": /^system/ } });
var counter = 0;
cursor.forEach(function (msg_info) {
  db.msg.insert(msg_info);
  counter += 1;
  if (counter % 10000 == 0) {
    print("counter", counter, "time", getDate());
  }
});
print("all_counter", counter, "start_time", start_time, "finish_time", getDate());
