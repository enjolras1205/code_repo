defmodule DeflateTestTest do
  use ExUnit.Case
  doctest DeflateTest

  require Logger

  test "different windowBits" do
    compress_ref = :zlib.open()
    decompress_ref = :zlib.open()

    :zlib.deflateInit(compress_ref, :default, :deflated, 31, 8, :default)
    :zlib.inflateInit(decompress_ref, 31)

    for num <- 1..100_0000 do
      compress_data = :zlib.deflate(compress_ref, "#{num}", :sync)
      # Logger.warn("compress_data:#{inspect(compress_data)}")
      decompress_data = :zlib.inflate(decompress_ref, compress_data)
      decompress_data = :erlang.iolist_to_binary(decompress_data)
      assert decompress_data == "#{num}"
    end

    :zlib.close(compress_ref)
    :zlib.close(decompress_ref)
  end
end
