defmodule HelloworldTest do
  use ExUnit.Case
  require Logger
  doctest Helloworld

  test "greets the world" do
    photo_path = "ddd"
    photo_origin_url = "https://p10082.com"

    result =
      case URI.parse(photo_origin_url) do
        %URI{host: host} = uri_data when is_binary(host) and byte_size(photo_path) > 0 ->
          Logger.warn("111111")
          URI.merge(uri_data, %URI{path: photo_path}) |> to_string

        _ ->
          # 如果没有配置 resource_origin,　图片不生效
          # 避免法规问题
          Logger.warn("222222")
          ""
      end

    Logger.warn("res:#{result}")
  end
end
