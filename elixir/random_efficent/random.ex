#!/usr/bin/env elixir

defmodule Script do
  @node_name :"update_game_server_node@127.0.0.1"
  @cookie "12345"
  @test_list_data Enum.map(1..100, fn x -> x end)
  @test_tuple_data Enum.map(1..100, fn x -> x end) |> List.to_tuple

  def test_list_random() do
    test_data = @test_list_data
    for _ <- 1..100000 do
      Enum.random(test_data)
    end
  end

  def test_tuple_random() do
    test_data = @test_tuple_data
    for _ <- 1..1000000 do
      size = tuple_size(test_data)
      idx = :rand.uniform(size)-1
      Kernel.elem(test_data, idx)
    end
  end

  def main(args) do
    Node.start @node_name
    Node.set_cookie String.to_atom(@cookie)
    IO.inspect {Node.self, Node.get_cookie}
	ret = :timer.tc(__MODULE__, :test_list_random, [])
    IO.inspect("list: #{inspect ret}")
	ret = :timer.tc(__MODULE__, :test_tuple_random, [])
    IO.inspect("tuple: #{inspect ret}")
  end


end

Script.main(System.argv)
