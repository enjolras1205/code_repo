defmodule BencheeTest do
  use ExUnit.Case
  doctest Benchee

  test "system_info" do
    Benchee.run(
      %{
        "xx" => fn ->
          :erlang.system_info(:schedulers)
        end
      },
      time: 10,
      memory_time: 2
    )
  end

  test "message_queue_len" do
    Benchee.run(
      %{
        "xx" => fn ->
          case Process.info(self(), :message_queue_len) do
            {:message_queue_len, val} ->
              :ok
          end
        end
      },
      time: 10,
      memory_time: 2
    )
  end

  test "greets the world" do
    group =
      for v <- 1..15 do
        %{
          group_id: "group_world_hello_world_{v}",
          group_type: "hello",
          receive_ts: 123_412_341_234,
          key: 12_341_234,
          unique_key: "world",
          dddddd: 12_341_234
        }
      end

    group = MapSet.new(Enum.map(group, fn v -> v.group_id end))

    Benchee.run(
      %{
        "flat_map" => fn ->
          for _ <- 1..10000 do
            Enum.any?(group, fn v -> "group_world_hello_world_8" in group end)
          end
        end
      },
      time: 10,
      memory_time: 2
    )
  end
end
