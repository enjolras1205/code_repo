defmodule Script do
  require Logger

  def main(_args) do
    opts = [strategy: :one_for_one, name: TestWorkerSuper]
    Supervisor.start_link(TestWorker.Supervisor, opts)

    start_ts = :erlang.system_time(:millisecond)
    Logger.info("start_time:{start_ts}")

    for idx <- 1..99_999_999_999 do
      start_ts = :erlang.system_time(:millisecond)

      for _ <- 1..100 do
        TestWorker.Supervisor.call_random_worker()
      end

      end_ts = :erlang.system_time(:millisecond)
      Logger.info("#{idx} call_worker_start:#{start_ts} end:#{end_ts}")

      :timer.sleep(20)
    end
  end
end

defmodule TestWorker.Supervisor do
  @moduledoc """
  """
  @worker_num 1000

  use Supervisor
  require Logger

  def start_link() do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def call_random_worker() do
    try do
      worker = TestUtil.get_module_atom(Test.Worker, :rand.uniform(@worker_num))
      GenServer.call(worker, :hello)
    catch
      error, error_detail ->
        Logger.warn("error:#{inspect(error)} error_detail:#{inspect(error_detail)}")
    end
  end

  # callback
  def init(_) do
    children = get_children()

    opts = [strategy: :one_for_one, name: __MODULE__, max_restarts: 3, max_seconds: 5]
    Supervisor.init(children, opts)
  end

  defp get_children() do
    worker_num = @worker_num

    Enum.map(1..worker_num, fn service_seq ->
      %{
        id: TestUtil.get_module_atom(Test.Worker, service_seq),
        start:
          {Test.Worker, :start_link,
           [
             service_seq
           ]},
        type: :worker
      }
    end)
  end
end

defmodule Test.Worker do
  use GenServer
  require Logger

  def start_link(worker_id) do
    GenServer.start_link(__MODULE__, worker_id,
      name: TestUtil.get_module_atom(__MODULE__, worker_id)
    )
  end

  def init(worker_id) do
    Logger.info("worker_id:#{worker_id}")
    {:ok, nil}
  end

  def handle_call(_msg, from, state) do
    a =
      for _ <- 1..100 do
        :rand.uniform(9999)
      end

    b =
      for _ <- 1..100 do
        :rand.uniform(9999)
      end

    {:reply, a ++ b, state}
  end
end

defmodule TestUtil do
  def get_module_atom(module_name, module_id) when is_atom(module_name) do
    get_module_atom(Atom.to_string(module_name), module_id)
  end

  def get_module_atom(module_name, module_id) when is_binary(module_name) do
    (module_name <> Integer.to_string(module_id)) |> String.to_atom()
  end
end

Script.main(System.argv())
