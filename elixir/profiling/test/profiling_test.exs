defmodule ProfilingTest do
  use ExUnit.Case
  require Logger
  doctest Profiling

  test "naked" do
    do_sth(:cpu_intensive)
    Logger.warn("naked")
  end

  test "cprof" do
    :cprof.start()
    do_sth(:cpu_intensive)
    :cprof.pause()
    data = :cprof.analyse()
    Logger.warn("cprof_result:\n#{inspect(data, pretty: true)}")
  end

  test "eprof" do
    :eprof.start()
    :eprof.start_profiling(:erlang.processes())
    do_sth(:cpu_intensive)
    :eprof.stop_profiling()
    data = :eprof.analyze(:total, sort: :time)
    Logger.warn("eprof_result:\n#{inspect(data, pretty: true)}")
  end

  test "fprof" do
    :fprof.trace([:start, {:procs, :erlang.processes()}])
    do_sth(:cpu_intensive)
    # 默认写入fprof.trace文件
    :fprof.trace(:stop)
    # 读取文件并分析
    :fprof.profile()
    # 输出结果，按函数自身调用时间排序，不包含调用其他函数的时间
    Logger.warn("+++++++++++++++")
    :fprof.analyse(sort: :own)
    # 按累计时间排序，包括调用其他函数的时间
    Logger.warn("+++++++++++++++")
    :fprof.analyse(sort: :acc)
  end

  test "eflame" do
    spawn(fn -> do_sth(:cpu_intensive) end)
    :eflame2.write_trace(:global_and_local_calls, './eflame.test', :all, 15 * 1000)
    :eflame2.format_trace('./eflame.test', './eflame.test.out')
  end

  test "looking grass" do
    # 不限制采集范围会直接爆内存
    :lg.trace(
      [{:scope, [self()]}],
      :lg_file_tracer,
      'traces.lz4',
      %{process_dump: true}
    )

    do_sth(:cpu_intensive)
    Logger.warn("1")
    :lg.stop()
    Logger.warn("2")
    :lg_flame.profile_many('traces.lz4.*', 'output')
    Logger.warn("3")
  end

  defp do_sth(:cpu_intensive) do
    start_time = :erlang.system_time(:microsecond)
    end_time = start_time + 10_000_000
    do_until_time(0, start_time, end_time)
  end

  def do_until_time(count, start_time, end_time) do
    now = :erlang.system_time(:microsecond)

    if now >= end_time do
      Logger.warn(
        "\ntotal_time: #{div(end_time - start_time, 1000_000)} second\ncount: #{count}, avg_μs: #{div(end_time - start_time, count)}"
      )
    else
      for v <- 1..100 do
        a = %{v => Base.encode64(:crypto.strong_rand_bytes(20))}
        b = Jason.encode!(a)
        Jason.decode!(b)
      end

      do_until_time(count + 1, start_time, end_time)
    end
  end

  # defp do_sth(:cpu_intensive) do
  #   Benchee.run(
  #     %{
  #       "json" => fn ->
  #         for v <- 1..100 do
  #           a = %{v => Base.encode64(:crypto.strong_rand_bytes(20))}
  #           b = Jason.encode!(a)
  #           Jason.decode!(b)
  #         end
  #       end
  #     },
  #     time: 10,
  #     memory_time: 2
  #   )
  # end
end
