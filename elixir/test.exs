#!/usr/bin/env elixir
#
## elixir --erl "-start_epmd false -epmd_port 4370" update_game_server.ex

defmodule Script do
  @monitor_node_name :"monitor@127.0.0.1"
  @node_name :"update_game_server_node@172.18.159.45"
  @cookie "12345"
  @servers [
    # 猜成语
    %{server_type: "ecs",url: "120.78.158.146:12345",protocol_type: "tcp",game_id: "ccy",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gn"},
    # 答题
    %{server_type: "ecs",url: "47.106.111.72:12346",protocol_type: "tcp",game_id: "dt",state: "online",is_alive: true,ecs_instance_id: "i-wz9fd7vnt5dfwdommcbd"},

    # 一步两步
    %{server_type: "ecs",url: "ws://47.106.97.232:10020",protocol_type: "web_socket",game_id: "yblb",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gi"},
    %{server_type: "ecs",url: "ws://47.106.97.232:10021",protocol_type: "web_socket",game_id: "yblb",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gi"},
    %{server_type: "ecs",url: "ws://47.106.97.232:10022",protocol_type: "web_socket",game_id: "yblb",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gi"},
    %{server_type: "ecs",url: "ws://47.106.97.232:10023",protocol_type: "web_socket",game_id: "yblb",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gi"},
    # 俄罗斯方块
    %{server_type: "ecs",url: "ws://47.106.96.252:13001/ws/game_server",protocol_type: "web_socket",game_id: "elsfk",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gl"},
    # 连连看
    %{server_type: "ecs",url: "ws://47.106.98.134:8081/ws/game",protocol_type: "web_socket",game_id: "llk",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gk"},
    # 五子棋
    %{server_type: "ecs",url: "47.106.98.132:10501",protocol_type: "tcp",game_id: "wzq",state: "online",is_alive: true,ecs_instance_id: "i-wz95elyg5kq2x5tye6gp"},
    # 斗兽棋
    %{server_type: "ecs",url: "ws://120.79.91.80:8082/ws/game",protocol_type: "web_socket",game_id: "dsq",state: "online",is_alive: true,ecs_instance_id: "i-wz9fd7vnt5dfwdommcbc"},
    # 泡泡龙
    %{server_type: "ecs",url: "ws://47.106.79.107:12345/ws/game",protocol_type: "web_socket",game_id: "ppl",state: "online",is_alive: true,ecs_instance_id: "i-wz9fd7vnt5dfwdommcbb"},
    # 火力全开
    %{server_type: "ecs",url: "ws://47.106.112.150:10080/ws/game",protocol_type: "web_socket",game_id: "hlqk",state: "online",is_alive: true,ecs_instance_id: "i-wz91edtwvvewow1da83b"},
    # 翻翻乐  	
    %{server_type: "ecs",url: "ws://47.106.79.223:10081/ws/game",protocol_type: "web_socket",game_id: "ffl",state: "online",is_alive: true,ecs_instance_id: "i-wz91edtwvvewow1da83c"},



    # 奔向傻白甜
    %{server_type: "ecs",url: "ws://172.18.159.38:9000/ws/game",protocol_type: "web_socket",game_id: "bxsbt",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 六角拼拼
    %{server_type: "ecs",url: "ws://172.18.159.38:9001/ws/game",protocol_type: "web_socket",game_id: "ljpp",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 消砖块
    %{server_type: "ecs",url: "ws://172.18.159.38:9002/ws/game",protocol_type: "web_socket",game_id: "xzk",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 教室大战
    %{server_type: "ecs",url: "ws://172.18.159.38:9003/ws/game",protocol_type: "web_socket",game_id: "jsdz",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 娃娃机
    %{server_type: "ecs",url: "ws://172.18.159.38:9004/ws/game",protocol_type: "web_socket",game_id: "wwj",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 消砖块-单机版
    %{server_type: "ecs",url: "ws://172.18.159.38:9005/ws/game",protocol_type: "web_socket",game_id: "xzkdjb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 跳冰箱
    %{server_type: "ecs",url: "ws://172.18.159.38:9006/ws/game",protocol_type: "web_socket",game_id: "tbx",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 老鼠偷蛋
    %{server_type: "ecs",url: "ws://172.18.159.38:9007/ws/game",protocol_type: "web_socket",game_id: "lstd",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 幸福年夜饭
    %{server_type: "ecs",url: "ws://172.18.159.38:9008/ws/game",protocol_type: "web_socket",game_id: "xfnyf",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},
    # 极限躲避
    %{server_type: "ecs",url: "ws://172.18.159.38:9009/ws/game",protocol_type: "web_socket",game_id: "jxdb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fv7"},





    # 奔向傻白甜
    %{server_type: "ecs",url: "ws://172.18.159.39:9000/ws/game",protocol_type: "web_socket",game_id: "bxsbt",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 六角拼拼
    %{server_type: "ecs",url: "ws://172.18.159.39:9001/ws/game",protocol_type: "web_socket",game_id: "ljpp",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 消砖块
    %{server_type: "ecs",url: "ws://172.18.159.39:9002/ws/game",protocol_type: "web_socket",game_id: "xzk",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 教室大战
    %{server_type: "ecs",url: "ws://172.18.159.39:9003/ws/game",protocol_type: "web_socket",game_id: "jsdz",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 娃娃机
    %{server_type: "ecs",url: "ws://172.18.159.39:9004/ws/game",protocol_type: "web_socket",game_id: "wwj",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 消砖块-单机版
    %{server_type: "ecs",url: "ws://172.18.159.39:9005/ws/game",protocol_type: "web_socket",game_id: "xzkdjb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 跳冰箱
    %{server_type: "ecs",url: "ws://172.18.159.39:9006/ws/game",protocol_type: "web_socket",game_id: "tbx",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 老鼠偷蛋
    %{server_type: "ecs",url: "ws://172.18.159.39:9007/ws/game",protocol_type: "web_socket",game_id: "lstd",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 幸福年夜饭
    %{server_type: "ecs",url: "ws://172.18.159.39:9008/ws/game",protocol_type: "web_socket",game_id: "xfnyf",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},
    # 极限躲避
    %{server_type: "ecs",url: "ws://172.18.159.39:9009/ws/game",protocol_type: "web_socket",game_id: "jxdb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fve"},




    # 奔向傻白甜
    %{server_type: "ecs",url: "ws://172.18.159.40:9000/ws/game",protocol_type: "web_socket",game_id: "bxsbt",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 六角拼拼
    %{server_type: "ecs",url: "ws://172.18.159.40:9001/ws/game",protocol_type: "web_socket",game_id: "ljpp",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 消砖块
    %{server_type: "ecs",url: "ws://172.18.159.40:9002/ws/game",protocol_type: "web_socket",game_id: "xzk",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 教室大战
    %{server_type: "ecs",url: "ws://172.18.159.40:9003/ws/game",protocol_type: "web_socket",game_id: "jsdz",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 娃娃机
    %{server_type: "ecs",url: "ws://172.18.159.40:9004/ws/game",protocol_type: "web_socket",game_id: "wwj",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 消砖块-单机版
    %{server_type: "ecs",url: "ws://172.18.159.40:9005/ws/game",protocol_type: "web_socket",game_id: "xzkdjb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 跳冰箱
    %{server_type: "ecs",url: "ws://172.18.159.40:9006/ws/game",protocol_type: "web_socket",game_id: "tbx",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 老鼠偷蛋
    %{server_type: "ecs",url: "ws://172.18.159.40:9007/ws/game",protocol_type: "web_socket",game_id: "lstd",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 幸福年夜饭
    %{server_type: "ecs",url: "ws://172.18.159.40:9008/ws/game",protocol_type: "web_socket",game_id: "xfnyf",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},
    # 极限躲避
    %{server_type: "ecs",url: "ws://172.18.159.40:9009/ws/game",protocol_type: "web_socket",game_id: "jxdb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvc"},



    # 奔向傻白甜
    %{server_type: "ecs",url: "ws://172.18.159.41:9000/ws/game",protocol_type: "web_socket",game_id: "bxsbt",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 六角拼拼
    %{server_type: "ecs",url: "ws://172.18.159.41:9001/ws/game",protocol_type: "web_socket",game_id: "ljpp",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 消砖块
    %{server_type: "ecs",url: "ws://172.18.159.41:9002/ws/game",protocol_type: "web_socket",game_id: "xzk",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 教室大战
    %{server_type: "ecs",url: "ws://172.18.159.41:9003/ws/game",protocol_type: "web_socket",game_id: "jsdz",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 娃娃机
    %{server_type: "ecs",url: "ws://172.18.159.41:9004/ws/game",protocol_type: "web_socket",game_id: "wwj",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 消砖块-单机版
    %{server_type: "ecs",url: "ws://172.18.159.41:9005/ws/game",protocol_type: "web_socket",game_id: "xzkdjb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 跳冰箱
    %{server_type: "ecs",url: "ws://172.18.159.41:9006/ws/game",protocol_type: "web_socket",game_id: "tbx",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 老鼠偷蛋
    %{server_type: "ecs",url: "ws://172.18.159.41:9007/ws/game",protocol_type: "web_socket",game_id: "lstd",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 幸福年夜饭
    %{server_type: "ecs",url: "ws://172.18.159.41:9008/ws/game",protocol_type: "web_socket",game_id: "xfnyf",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},
    # 极限躲避
    %{server_type: "ecs",url: "ws://172.18.159.41:9009/ws/game",protocol_type: "web_socket",game_id: "jxdb",state: "online",is_alive: true,ecs_instance_id: "i-wz90invijpitpknc2fvd"},



  ]

  def main(args) do
    Node.start @node_name
    # Node.set_cookie String.to_atom(@cookie)
    IO.inspect {Node.self, Node.get_cookie}
    servers = @servers
    IO.puts "updateing gameserver:"
    IO.inspect servers
    for server_info <- servers, do: update_game_server(server_info)
    # gameserver not in servers will be offline
    urls = for server <- servers, do: server.url
    IO.inspect(urls)
    ret = GenServer.call({Monitor.Director, @monitor_node_name}, {:reverse_offline_gameserver, urls})
    IO.inspect(ret)
  end

  def update_game_server(server_info) do
    ret = GenServer.call({Monitor.Director, @monitor_node_name}, {:register_gameserver, server_info})
    IO.inspect(server_info)
    IO.inspect(ret)
  end

end

Script.main(System.argv)

