use Mix.Config

config :logger,
  level: :info,
  backends: [
    :console
  ]

metadata = [
  :product,
  :application,
  :module,
  :file,
  :line,
  :pid,
  :trace_id,
  :user_id,
  :user_type,
  :event_type,
  :event_data
]

config :logger, :console,
  metadata: metadata,
  format: "[$level] $date $time $metadata $message\n"

import_config "#{Mix.env()}.exs"
