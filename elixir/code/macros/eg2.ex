#---
# Excerpted from "Programming Elixir",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/elixir for more book information.
#---
defmodule My do
  defmacro macro(code, code1) do
    IO.inspect code
    IO.inspect code1
    quote do
      IO.puts(unquote(code))
    end
  end
end

defmodule Test do
  require My

  {body, options} =
  cond do
    true ->
      {"a", "b"}
  end
  My.macro(body, option)

  #  My.macro 3 + 4 do
  #    3 + 5
  #  end
end
