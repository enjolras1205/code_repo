defmodule Http2ClientTest do
  use ExUnit.Case
  require Logger
  doctest Http2Client

  test "greets the world" do
    headers = [{"Content-Type", "multipart/form-data"}]
    options = []

    for _ <- 1..100_000_000 do
      binary_file_content =
        :jiffy.encode(%{
          event: "sdk.boot",
          data: :crypto.strong_rand_bytes(:random.uniform(10000)) |> Base.encode64(),
          time: :erlang.system_time(:millisecond)
        })
        |> :zlib.gzip()

      headers = [{"trace-id", UUID.uuid1()} | headers]

      resp =
        HTTPoison.request!(
          :post,
          # "http://localhost:#{port}/log/gbi_log",
          "https://p10442-a-log-collector.ejoy.com/log/gbi_log",
          {:multipart,
           [
             {"file", binary_file_content, {"form-data", [name: "data", filename: "xdata-log"]},
              []}
           ]},
          headers,
          options
        )

    end
  end
end
