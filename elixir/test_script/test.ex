defmodule Hello do
  require Logger

  def hehe(api \\ :a, req \\ :b)

  def hehe(api, req) when is_binary(api) do
    Logger.info("api #{inspect(api)} #{inspect(req)}")

    {{:get_banner_info, []},
     [
       %{
         _id: "5efdb0317499e3001fd9c0a9",
         banner_type: 1,
         order: 1,
         picture_id: "5efdafa37499e3001a9c19c2",
         updated_at: %{ms: 1_593_684_065_461},
         url: "https://sgzbbs.lingxigames.com/detail/5ecf1e0b7499e30028ef9e4b"
       },
       %{
         _id: "5efdb0217499e3001a9c19c7",
         banner_type: 2,
         order: 1,
         picture_id: "5efdafc47499e3001a9c19c4",
         updated_at: %{ms: 1_593_684_001_361},
         url: "https://sgzbbs.lingxigames.com/detail/5ecf1e0b7499e30028ef9e4b"
       }
     ], 1_597_284_827}

#      iex(p10445_bbs@p10445_bbs.)9> :ets.tab2list(EtsCache.Lock)
# [{{:get_banner_info, []}, 1}]
  end
end
