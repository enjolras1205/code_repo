defmodule EredisClusterTestTest do
  use ExUnit.Case
  require Logger
  doctest EredisClusterTest

  test "xx" do
    # redix()
    redix_cluster()
    eredis()
    # eredis2()
  end

  test "fail recovery" do
    loop_count = 200_000
    RedisTestApp.command(["SET", "Q", "W"])
    # 对比两个库的快速恢复能力
    task1 =
      Task.async(fn ->
        Enum.reduce(1..loop_count, {0, 0}, fn _, {acc_success, acc_fail} ->
          case RedisTestApp.command(["GET", "Q"]) do
            {:ok, "W"} ->
              {acc_success + 1, acc_fail}

            _ ->
              {acc_success, acc_fail + 1}
          end
        end)
      end)

    task2 =
      Task.async(fn ->
        Enum.reduce(1..loop_count, {0, 0}, fn _, {acc_success, acc_fail} ->
          case :eredis_cluster.q(["GET", "Q"]) do
            {:ok, "W"} ->
              {acc_success + 1, acc_fail}

            _ ->
              {acc_success, acc_fail + 1}
          end
        end)
      end)

    {success1, fail1} = Task.await(task1, 60000)
    {success2, fail2} = Task.await(task2, 60000)

    Logger.warn(
      "fail_rate1:#{fail1 / (success1 + fail1)} #{success1 + fail1} \nfail_rate2:#{fail2 / (success2 + fail2)} #{success2 + fail2}"
    )
  end

  test "benchmark" do
    Benchee.run(
      %{
        "redix" => fn ->
          redix()
        end,
        "redix_cluster" => fn ->
          redix_cluster()
        end,
        "eredis" => fn ->
          eredis()
        end
      },
      time: 10,
      parallel: 100,
      memory_time: 2
    )
  end

  defp redix_cluster() do
    for idx <- 1..100 do
      key = "Q:#{idx}"

      {:ok, ["OK"]} =
        MyApp.RedisClusterCache.pipeline([["SET", key, "W"]], name: :redix_cluster, key: key)

      {:ok, ["W"]} =
        MyApp.RedisClusterCache.pipeline([["GET", key]], name: :redix_cluster, key: key)
    end
  end

  defp redix() do
    {:ok, "OK"} = RedisTestApp.command(["SET", "Q", 1])
    {:ok, "1"} = RedisTestApp.command(["GET", "Q"])
    # {:ok, 3} = RedisTestApp.command(["RPUSH", "R", 1, 2, 3])
    {:ok, ["1", "2", "3"]} = RedisTestApp.command(["LRANGE", "R", 0, "-1"])
  end

  defp eredis() do
    for idx <- 1..100 do
      key = "Q:#{idx}"
      {:ok, _} = :eredis_cluster.q(["SET", key, "Q"])
      {:ok, "Q"} = :eredis_cluster.q(["GET", key])
      # resp = :eredis_cluster.transaction([["GET", "a"]])
      resp = :eredis_cluster.qmn([["GET", "a"], ["GET", "b"], ["GET", "c"]])
      Logger.warn("resp:#{inspect(resp)}")
    end
  end

  defp eredis2() do
    commands = [["SET", "E", "Q"], ["SET", "E", "Q"]]
    reply = :eredis_cluster.qp(commands)
    reply1 = Redix.pipeline(RedisTestApp.random_conn(), commands)
    Logger.warn("reply:#{inspect(reply)} reply1:#{inspect(reply1)}")
    [{:ok, "Q"}, {:ok, "Q"}] = :eredis_cluster.qmn([["GET", "E"], ["GET", "E"]])

    {:ok, ["Q", "Q"]} = Redix.pipeline(RedisTestApp.random_conn(), [["GET", "E"], ["GET", "E"]])
  end
end
