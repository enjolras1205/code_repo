defmodule EredisClusterTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :eredis_cluster_test,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {RedisTestApp, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nebulex_redis_adapter, "~> 2.3"},
      {:redix, "~> 1.2"},
      {:crc, "~> 0.10"},    #=> Needed when using Redis Cluster
      {:jchash, "~> 0.1.4"},
      {:benchee, "~> 1.1"},
      {:eredis_cluster, "~> 0.9.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
