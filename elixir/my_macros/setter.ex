defmodule Setter1 do
  defmacro bind_name(xxx) do
    quote do
      name = unquote(xxx)
    end
  end
end

defmodule Setter2 do
  defmacro bind_name(xxx) do
    quote do
      var!(name) = unquote(xxx)
    end
  end
end
