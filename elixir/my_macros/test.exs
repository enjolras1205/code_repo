defmodule TestMacro do
  defmacro xxx(aaa) do
    quote do
      unquote(aaa)
    end
  end
end
