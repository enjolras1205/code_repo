defmodule Mod do
  defmacro definfo do
    IO.puts("in macros context #{__MODULE__}")

    quote do
      IO.puts("in callers context #{__MODULE__}")

      def friendly_info do
        IO.puts("my name is #{__MODULE__} function are #{inspect(__info__(:functions))}")
      end
    end
  end
end

defmodule MyModule do
  require Mod
  Mod.definfo()
end
