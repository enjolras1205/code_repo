defmodule ControlFlow do
  defmacro unless(condition, do: do_clause, else: else_clause) do
    quote do
      if !unquote(condition), do: unquote(do_clause), else: unquote(else_clause)
    end
  end
end
