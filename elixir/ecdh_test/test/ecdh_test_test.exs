defmodule EcdhTestTest do
  use ExUnit.Case
  require Logger

  test "ecdh" do
    for ecdh_curve <- [:prime256v1, :prime192v1, :prime192v2] do
      # ecdh_curve 是一个大素数P, 以及一个整数g (2或者5)
      # alice生成私钥a, 以及用下面公式生成公钥A
      # g^a mod p = A
      {alice_public, alice_private} = :crypto.generate_key(:ecdh, ecdh_curve)
      # bob生成私钥b, 以及用下面公式生成公钥B
      # g^b mod p = B
      {bob_public, bob_private} = :crypto.generate_key(:ecdh, ecdh_curve)
      # 有 B^a mod p = K
      alice_key = :crypto.compute_key(:ecdh, bob_public, alice_private, ecdh_curve)
      # 有 A^b mod p = K
      bob_key = :crypto.compute_key(:ecdh, alice_public, bob_private, ecdh_curve)
      assert alice_key == bob_key
      alice_key_64 = Base.encode64(alice_key)
      Logger.warn("curve:#{ecdh_curve} key:#{inspect(alice_key_64)}")
    end
  end

  test "dh" do
    # 有一个大素数P, 以及一个整数g (2或者5)
    [p, g] = [83, 5]
    dh_params = [p, g]
    # alice生成私钥a, 以及用下面公式生成公钥A
    # g^a mod p = A
    {alice_public, alice_private} = :crypto.generate_key(:dh, dh_params)
    # bob生成私钥b, 以及用下面公式生成公钥B
    # g^b mod p = B
    {bob_public, bob_private} = :crypto.generate_key(:dh, dh_params)
    # 有 B^a mod p = K
    alice_key = :crypto.compute_key(:dh, bob_public, alice_private, dh_params)
    # 有 A^b mod p = K
    bob_key = :crypto.compute_key(:dh, alice_public, bob_private, dh_params)
    assert alice_key == bob_key
    alice_key_64 = Base.encode64(alice_key)
    Logger.warn("dh_params:#{dh_params} key:#{inspect(alice_key_64)}")
    Logger.warn("alice_public:#{to_integer(alice_public)}")
    Logger.warn("alice_private:#{to_integer(alice_private)}")
    Logger.warn("bob_public:#{to_integer(bob_public)}")
    Logger.warn("bob_private:#{to_integer(bob_private)}")
  end

  defp to_integer(bin) do
    :binary.decode_unsigned(bin)
  end
end
