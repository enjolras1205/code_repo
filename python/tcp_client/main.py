import socket
import struct
import json

address = ('127.0.0.1', 12345)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(address)
data = {"a":"b"}

body_data = json.dumps(data)
len_data = struct.pack(">I", len(body_data))
send_data = len_data + body_data
s.send(send_data)

