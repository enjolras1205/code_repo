# -*- coding:utf-8 -*-
import bottle
import return_code


def get_user_match_location():
    return dict(code=return_code.OK)


def init_app():
    app = bottle.Bottle()
    routers = [
        ('/get_user_match_location', get_user_match_location),
    ]
    for path, func in routers:
        app.route(path, ['GET', 'POST'], func)
    return app
