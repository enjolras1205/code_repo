import bottle
import imp
import config


def init_app():
    app = bottle.Bottle()
    modules = [
        ('gate', 'app/gate'),
        ('location', 'app/location'),
    ]
    for route, app_name in modules:
        filename, path, desc = imp.find_module(app_name)
        mod = imp.load_module(app_name, filename, path, desc)
        module_app = mod.init_app()
        app.mount(route, module_app)

    return app


if __name__ == '__main__':
    application = init_app()
    bottle.run(application, **config.SERVER_ADDR)
