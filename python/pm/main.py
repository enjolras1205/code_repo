import requests

def get_remote_md5():
    url = 'https://pm2.ejoy.com/admin/P10075/get_branch'
    query = {
        'branch_id': '5bad8824577c6f001ccf28bf',
        'token': '07FD17A483789A373',
        'product': 'P10075'
    }
    r = requests.post(url, data=query, timeout=10)
    json = r.json()
    # import pprint
    # pprint.pprint(json)
    sub_key_map = {
        u'ios': u'ipa',
        u'android': u'apk',
    }
    subkey = sub_key_map[json[u'data'][u'package_type']]
    md5 = json[u'data'][u'spec_items'][subkey][u'md5']
    print ' * [REMOTE-MD5]', md5
    return md5

md5 = get_remote_md5()
print("md5", md5)
