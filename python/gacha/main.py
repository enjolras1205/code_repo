from fractions import Fraction
class Genshin:
  UP = "1"
  NON_UP = "0"

  def __init__(self, target_times):
    self.p = Fraction(0)
    self.target_times = target_times

  def record(self, history, up_count, p):
    # print(history, up_count, p)
    self.p += p*up_count

  def gacha(self, current_time=0, history="", last_result=UP, up_count=0, p=Fraction(1, 1)):
    if current_time == self.target_times:
      self.record(history, up_count, p)
      return
    if last_result == self.UP:
      self.gacha(current_time + 1, history+self.NON_UP, self.NON_UP, up_count, p=p/2)
      self.gacha(current_time + 1, history+self.UP, self.UP, up_count+1, p=p/2)
    else:
     self.gacha(current_time + 1, history+self.UP, self.UP, up_count+1, p=p)

# for i in range(1, 100):
#   genshin = Genshin(target_times=i)
#   genshin.gacha()
#   print(f"{i}: {genshin.p/genshin.target_times} = {float(genshin.p/genshin.target_times)}")
genshin = Genshin(target_times=25)
genshin.gacha(0, "", genshin.NON_UP, up_count=0)
print(f"nonup first: {genshin.p/genshin.target_times} = {float(genshin.p/genshin.target_times)}")
genshin = Genshin(target_times=25)
genshin.gacha(0, "", genshin.UP, up_count=0)
print(f"up first: {genshin.p/genshin.target_times} = {float(genshin.p/genshin.target_times)}")