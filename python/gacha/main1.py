from fractions import Fraction


def gacha(n):
    if n <= 1:
        return Fraction(1, 2)
    else:
        last_gacha = gacha(n - 1)
        return 1 - (last_gacha * Fraction(1, 2))


for n in range(2, 32):
    res = gacha(n)
    print("gacha", n, res, 1 - res)
