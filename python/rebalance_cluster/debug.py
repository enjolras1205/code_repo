import requests
import sys
import json
import time
import logging

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
root.addHandler(handler)


def rebalance(cluster_name, service_data, step):
    replicas = service_data["replicas"]
    if replicas < 2:
        logging.info("replicas_less_than_2")
        exit(1)
    node_start = 0
    node_end = replicas // 2
    if step == "1":
        node_start = node_end
        node_end = replicas
    entity_nodes = []
    if cluster_name == "chat":
        entity_nodes = get_chat_targets(service_data, node_start, node_end)
    success = do_rebalance(cluster_name, entity_nodes)
    if not success:
        logging.info("start_rebalance_fail")
        exit(1)
    while True:
        time.sleep(3)
        (success, resp) = do_get_cluster_state(cluster_name)
        if success and resp["state_detail"]["state"] == "wait_migrate":
            return


def do_get_cluster_state(cluster_name):
    request_data = {
        "cluster_name": cluster_name
    }
    resp = request("get_cluster_state", request_data)
    if resp.status_code == 200:
        resp_json = json.loads(resp.content)
        return (True, resp_json)
    else:
        return (False, None)


def do_rebalance(cluster_name, entity_nodes):
    request_data = {
        "cluster_name": cluster_name,
        "entity_nodes": entity_nodes,
        "broadcast_nodes": []
    }
    resp = request("rebalance_cluster", request_data)
    return resp.status_code == 200


def request(api, request_data):
    headers = {
        "Content-Type": "application/json"
    }
    url = "http://localhost:1233/{0}".format(api)
    logging.info("url:{0} request_data:{1}".format(url, request_data))
    resp = requests.request(
        "POST", url, headers=headers, data=json.dumps(request_data))
    logging.info("resp_content:{0}".format(resp.content))
    return resp


def get_chat_targets(service_data, node_start, node_end):
    product = service_data["groupKey"]
    nodes = [
        "{product}_{app}_{chat_role}@{product}-{name}-{seq}.{product}-{name}.{namespace}.svc.cluster.local".format(
            product=product, app=service_data["app"], name=service_data["appName"], seq=vv,
            chat_role=service_data["chat_role"], namespace=service_data["namespace"]) for vv in range(node_start, node_end)]

    return nodes


if __name__ == "__main__":
    logging.info("rebalance_cluster_param:{0}".format(sys.argv))
    kun_data = json.loads(sys.argv[1])
    service_data = kun_data[0]
    updates = {v["variable"]: v["value"] for v in service_data["vars"]}
    service_data.update(updates)
    cluster_name = sys.argv[2]
    step = sys.argv[3]
    logging.info("cluster_name:{0} step:{1}".format(cluster_name, step))
    rebalance(cluster_name, service_data, step)
