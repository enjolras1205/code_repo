import sys
import json
import logging
import os

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
root.addHandler(handler)

if __name__ == "__main__":
    logging.info("get_rebalance_info_param:{0}".format(sys.argv))
    kun_data = json.loads(sys.argv[1])
    service_data = kun_data[0]
    replicas = service_data["replicas"]
    command ="replicas=#{0}".format(replicas)
    os.system(command)
