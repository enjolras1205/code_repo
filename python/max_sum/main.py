
def max_sum(arr):
    length = len(arr)
    if length <= 0:
      return 0
    if length == 1:
      return arr[0]
    # max_num_2 为 dp[i-2] max_num_1 为 dp[i-1]
    max_num_2, max_num_1 = arr[0], max(arr[1], arr[0])
    for i in range(2, length):
      max_num_2, max_num_1 = max_num_1, max(max_num_2 + arr[i], max_num_1)
    return max_num_1

if __name__=="__main__":
    print("ok1")
    assert max_sum([5, 2, 3, 4]) == 9
    assert max_sum([1]) == 1
    assert max_sum([1, 2, 3, 4]) == 6
    assert max_sum([5, 2, 3, 4]) == 9
    print("ok2")
