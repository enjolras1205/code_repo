#!/usr/bin/env python3
import pymongo
import time
import json

db_host = "127.0.0.1"
db_port = 27017

# 1. ts 2. msg_id 3. _id 4 content_id 5 session_id 6 src_id src_info 7 to_id
msg_example = '{"content":{"data":{"extend_data":{"extend_data":{},"param":{"chat_box":0,"client_notice_id":12,"client_notice_params":["2"]}},"text":""},"type":"rich_text"},"content_id":"6705f68aa53c3633c3cb2071","delete_msg_members":[],"is_auto_reply":false,"migrate_session_id":null,"platform":null,"read_bits":0,"reader_status":0,"send_id":null,"session_id":"181738-110029:719014-110029","src_id":"181738-110029","src_info":{"user_id":"181738-110029","user_type":"player"},"src_type":"user","status_msg":"","to_id":"719014-110029","ts":1728444042}'
# 
msg_example = json.loads(msg_example)

MAX_USER_ID = 300000
MSG_COUNT = 10
TO_ID_RANGE = 50

def main():
    client = pymongo.MongoClient(
        host=db_host, port=db_port)
    db = client.P11072_chat
    insert_arr = []
    for user_id in range(MAX_USER_ID):
        # 1 to [1,10] 2 to [2, 11]
        # ...
        for to_id in range(user_id, min(MAX_USER_ID, user_id + TO_ID_RANGE)):

    
    for doc in from_db["msg"].find():
        insert_arr.append(doc)
        counter += 1
        if len(insert_arr) >= 100:
            now = get_current_millsecond()
            last_write_time = last_write_time or now
            diff = now - last_write_time
            if diff < one_batch_ms:
              time.sleep((one_batch_ms - diff) / 1000.0)
            try:
                last_write_time = now
                to_db["msg_tmp"].insert_many(insert_arr)
            except:
                pass
            insert_arr = []
        if counter % 10000 == 0:
            print("time", datetime.now().strftime(
                "%Y/%m/%d, %H:%M:%S"), "counter", counter)
    if insert_arr:
        to_db["msg_tmp"].insert_many(insert_arr)
    print("finshed", counter)


if __name__ == '__main__':
    main()
