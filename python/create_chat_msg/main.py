# !/usr/bin/env python3
# -*- coding:utf8 -*-
import hashlib
import time
import uuid
from pprint import pprint

import requests
import json

app_id = "ucenter"
app_key = "D33DtYQuZp7YgrVAKGYbKbd2tG/Ep/chP+BjAoiyX/8="
gate_url = "https://p10119-gate.ejoy.com"


def main():
    for n in range(2000):
        call_send(n)


def call_send(n):
    destination = "chat"
    method = "send"
    now = int(time.time())
    session = str(uuid.uuid1())
    headers = {
        "ag-sign-time": "{0}".format(now),
        "ag-sign-valid-time": "{0}".format(now + 180),
        "srpc-destination": destination,
        "srpc-method": method,
        "srpc-codec": "json",
        "srpc-session": session,
        "srpc-trace": session,
    }
    body = {
        "cmd": "send",
        "src_type": "user",
        "src_id": "0EACFDA30B57D81B49C2CADB32D90224",
        "sessions": ["0EACFDA30B57D81B49C2CADB32D90224:896BF282E0518F7CD2AC9E3A7E6C4BD9"],
        "content": {"type": "rich_text", "data": {"text": "{0}".format(n)}}
    }

    # 1、格式化输出 srpc
    url = "{0}/srpc".format(gate_url)

    # 2、循环 headers，排序 header 字段
    header_string = "\n".join(
        sorted(["{0}:{1}".format(k, v) for k, v in headers.items()]))

    # 3、打印 headers
    print("1、header_string:\n{0}".format(header_string))

    # 4、签名值
    signature_value = "{method}{request_path}{query_string}\n{header_string}\n".format(
        method="post", request_path="/srpc", query_string="", header_string=header_string,
    )

    print("2、signature_value:{signature_value}".format(
        signature_value=signature_value))

    # 5、字典转 json 字符串，再用 utf-8 编码
    body_string = json.dumps(body, ensure_ascii=False).encode("utf-8")

    # 6、打印 body_string
    print("3、body_string:\n{0}".format(body_string))

    # 7、合并
    signature_value = bytes(signature_value, "utf-8") + \
        body_string + b"\n" + bytes(app_key, "utf-8")
    print("4、signature_value:\n{0}".format(signature_value))

    # 8、sha256 加密
    m = hashlib.sha256()
    m.update(signature_value)
    signature = m.hexdigest()

    # 9、打印 sha256 加密后的签名值
    print("5、signature:\n{0}".format(signature))

    # 10、签名头
    signature_header = "version=1&signature={signature}&auth_type=gate_auth&app_id={app_id}".format(
        signature=signature, app_id=app_id)
    headers["Authorization"] = signature_header

    # 11、打印签名头
    print("6、Authorization:\n{signature_header}".format(
        signature_header=signature_header))

    # 12、发起请求
    r = requests.post(url, headers=headers, data=body_string)

    # 13、打印响应头、响应体
    print("7、headers:\n", r.headers, "\nbody:\n{0}".format(r.json()))


if __name__ == '__main__':
    main()
