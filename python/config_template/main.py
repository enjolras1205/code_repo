import sys
import re
import os
import json


def main():
    for root, dirs, files in os.walk("./"):
        for filename in files:
            file_path = os.path.join(root, filename)  # 拼接目录路径
            if file_path.find("main.py") == -1:
                do_file(file_path)


def check_type(file_path, name, value_type, value):
    args = dict(file_path=file_path, name=name,
                value_type=value_type, value=value)
    if value_type == "INT":
        if not isinstance(value, int):
            output_error(args)
    elif value_type == "BOOLEAN":
        if not isinstance(value, bool):
            output_error(args)
    elif value_type == "STRING":
        if not isinstance(value, str):
            output_error(args)
    else:
        print("unknown_type", value_type, value)


def output_error(args):
    print(
        "file_path:#{file_path} name:#{name} value_type:#{value_type} value:#{value}".format(**args))


def do_file(file_path):
    print("do_file", file_path)
    with open(file_path, "r") as f:
        for vv in f:
            vv = json.loads(vv)
            name = vv["name"]
            fields = vv["fields"]
            for field in fields:
                value_type = field.get("value_type")
                value = field.get("default")
                if value is None or value_type is None:
                    # print("value_type_or_default_value_not_exist", name, value_type, value, field)
                    pass
                else:
                    check_type(file_path, name, value_type, value)


if __name__ == "__main__":
    main()
    print("complete")
