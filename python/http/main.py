import requests
import timeit
import gevent
from gevent import monkey
monkey.patch_all()


url = "http://waservice.ejoy.com/gp/p10042/notify/c1/open/frog/mail/push"

payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"a\"\r\n\r\n12\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"b\"\r\n\r\n33\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
headers = {
    'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
    'code': "4381b45abf06309896aec24d685e2868",
    'cache-control': "no-cache",
    'postman-token': "54310706-16fa-a4bb-a67e-5f470405d38f"
    }


def run():
        time_arr = []

        while True:
                start = timeit.default_timer()
                response = requests.request("POST", url, data=payload, headers=headers)
                stop = timeit.default_timer()
                estlimiate_time = stop - start
                if len(time_arr) < 10:
                        time_arr.append(estlimiate_time)
                else:
                        print(time_arr)
                        time_arr = []


threads = [gevent.spawn(run) for i in xrange(100)]
gevent.joinall(threads)
