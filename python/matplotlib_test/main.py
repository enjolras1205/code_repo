import os
import sys
root_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(root_dir, '../../lib'))
sys.path.append(os.path.join(root_dir, '..'))
import cStringIO
import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
from matplotlib import font_manager
root_dir = os.path.dirname(os.path.abspath(__file__))
font_path = os.path.join(root_dir, 'fonts', 'DroidSansFallbackFull.ttf')
font_manager.X11FontDirectories.append(os.path.join(root_dir, 'fonts'))
font_manager._rebuild()
rcParams['font.family'] = 'Droid Sans Fallback'
rcParams['axes.unicode_minus'] = False
import matplotlib.pyplot as plt
