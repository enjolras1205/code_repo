import socket
import time

ip = "192.168.26.119"
port = 12344
connect_num = 4

def do_connection():
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  try:
    s.connect((ip, port))
    print("connection", ip, port, s)
    return s
  except:
    return None

connections = [do_connection() for _ in range(connect_num)]

time.sleep(6)

counter = 0
while True:
  if counter >= len(connections):
    counter = counter % len(connections)
  if connections[counter]:
    connections[counter].close()
  connections[counter] = do_connection()
  counter += 1
  time.sleep(1)
