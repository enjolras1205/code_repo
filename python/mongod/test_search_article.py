import pymongo


def chunks(xs, n):
    n = max(1, n)
    return (xs[i:i+n] for i in range(0, len(xs), n))


client = pymongo.MongoClient('localhost', 27017)
db = client["test"]
col = db["test_index"]

for a in range(10000):
    docs = []
    for b in range(10000):
        doc = {
            "title": "AAAAAAAAAAAAAAAAAAAAAAAA",
            "content": "qwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwerqwer",
            "tags2": ["official_tiny,tag{0},".format(b % 10)]
        }
        docs.append(doc)
    col.insert_many(docs)
