import pymongo

def chunks(xs, n):
    n = max(1, n)
    return (xs[i:i+n] for i in range(0, len(xs), n))

client = pymongo.MongoClient('localhost', 27017)
db = client["test"]
col = db["test_index1"]

for a in range(10000):
    print("a", a)
    docs = []
    for b in range(100000):
        doc = {
            "key1": 0,
            "key2": "{0}".format(b % 2),
            "key3": "{0}".format(b % 2 + 1),
            "key4": "{0}".format(b % 2),
            "key5": b,
            "key6": a * 100000 + b,
        }
        docs.append(doc)
    col.insert_many(docs)

