import pymongo

client = pymongo.MongoClient('localhost', 27017)
db = client["test"]
col = db["test_index"]

data = list(col.find({"$or": [{"tags2": {"$regex": "^official_tiny,tag1,"}}, {
            "tags2": {"$regex": "^official_tiny,tag2,"}}]}, {"tags2": 1}))
print(len(data))
