import pymongo
import json
import datetime

client = pymongo.MongoClient('localhost', 27017)
db = client["test"]

link_player_ids = set()
for v in db["bbs_user"].find({}, {"link_player_id": 1}):
    link_player_ids.add(v["link_player_id"])

link_player_ids.remove(None)

counter = 1
max_count = 0
affect_time = 0
for player_id in link_player_ids:
    docs = list(db["bbs_user"].find({"link_player_id": player_id}, {
                "account_id": 1, "last_get_notice_time": 1, "create_time": 1}))
    count = len(docs)
    if count > 1:
        docs = sorted(docs, key=lambda x: x["create_time"], reverse=True)
        for doc in docs[1:]:
            if doc["last_get_notice_time"] != None and doc["last_get_notice_time"] > datetime.datetime(2023, 10, 30, 16, 0, 0, 0):
                print("doc", doc, docs)
                affect_time += 1
        # arr = [doc["account_id"] for doc in docs]
        # arr.append(player_id)
        # arr.reverse()
        # print(json.dumps(arr))
print("affect_time", affect_time)
