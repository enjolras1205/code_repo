#!/usr/bin/env python
# -*- coding:utf-8 -*-

PATH = "./../generate_code_tool/"

import xlrd
import sys
sys.path.append(PATH)
import generate_code_tool
import xls2python
from encoding import chinese_encode
from Python2Lpc import PythonData2Lpc 
from multiprocessing import Process, Queue

CONFIG_SRC_CODE = {
        "code_format" : '''
static mapping mpHeroIDCfg = %(heroIDCfg)s;
'''
}

CONFIG_DB = {
        "heroIDCfg" : {
            #表
            "table_sheet" : u"战神",
            #字段行数
            "row_line" : 4,
            #索引键
            "index_key" : [ u"id" ],
            #索引行
            "field_line" : 1,
            #白名单
            "whitelist" : [u"type", u"rank"],
            #黑名单
            "blacklist" : [],
            #字段属性
            "field_attribute"   :   {
                },
            },
        }

output = '''
{0}\nstatic mapping heroCfg = {1};\n\n
{2}\n
{3}\n
'''

func = '''mapping get_hero_cfg(int rank, int lev)
{
    int base;
    mapping tmp, ret;
    string attr_key;

    tmp = heroCfg[rank];
    if (undefinedp(tmp)) {
        return 0;
    }
    base = tmp["base"];
    attr_key = sprintf("%04d%02d", base, lev);
    ret = ([]);

    ret["career"] = tmp["career"];
    ret["equip"] = tmp["equip"];
    ret["ieffect"] = tmp["ieffect"];
    ret["skill"] = tmp["skill"];
    ret["sex"] = tmp["sex"];
    ret["attr"] = copy(tmp["attr"][attr_key]);

    return ret;
}

int is_legal(int rank, int lev)
{
    int base;
    mapping tmp;
    string attr_key;

    tmp = heroCfg[rank];
    if (undefinedp(tmp)) {
        return 0;
    }
    base = tmp["base"];
    attr_key = sprintf("%04d%02d", base, lev);

    return !undefinedp(tmp["attr"][attr_key]);
}

'''

# 索引表读取配置
t_index_cfg = {
        #表
        "table_sheet" : u"战神",
        #索引键
        "index_key" : ["type", "rank"],
        #黑名单
        "blacklist" : ["id"],
        #字段属性
        "field_attribute"   :   {
            "equip" :{
                "func"  : xls2python.lua_to_lpc,
                },
            },
        }

# 修正表读取配置
t_fix_cfg = {
        #表
        "table_sheet" : u"战神成长",
        #索引键
        "index_key" : [ u"id" ],
        #字段属性
        "field_attribute"   :   {
            },
        }

# 基础表读取配置
t_base_cfg = {
        #表
        "table_sheet" : u"基础属性",
        #索引键
        "index_key" : [u"type", "level"],
        #字段属性
        "field_attribute" : {
            },
        }

# wrapper of read by cfg
def read_from_table(file_name, cfg, q):
    cfg["field_line"] = 1
    cfg["db_file"] = file_name
    cfg["row_line"] = 4
    cfg["whitelist"] = []
    sheet = cfg["table_sheet"]

    # 通过第4行的字段计算出黑名单
    # 1 服务端客户端共用 2 服务端专用 非这2个值的都加入黑名单
    if not cfg.has_key("blacklist"):
        cfg["blacklist"] = []
    data = xlrd.open_workbook(file_name)
    table = data.sheet_by_name(sheet)
    filter_line = table.row_values(3)
    for i in range(len(filter_line)):
        var = filter_line[i]
        if var != 1 and var != 2: 
            black = table.cell(1, i).value
            cfg["blacklist"].append(black)

    ret = generate_code_tool.xls2python_data(data, sheet, cfg)
    q.put(ret)

def gen_file(fight_type, one_type_index, fix_table, base_table, out_path):
    fix_suffix = "_fix"
    file_contain = {}
    for rank, info2 in one_type_index.iteritems():
        file_contain[rank] = info2
        fix_key = info2["fix"]
        base_key = info2["base"]
        base = base_table[base_key]
        fix = fix_table[fix_key]
        attr_dict = {}
        for level, info3 in base.iteritems():
            attr = {}
            attr_key = "{:04d}{:02d}".format(base_key, level)
            for key, value in fix.iteritems():
                if key.rfind(fix_suffix) > 0:
                    r_key = key[:-len(fix_suffix)]
                    r_value = int(info3[r_key] * (value / 10000.0))
                    attr[r_key] = r_value
                    #print("fight_type:", fight_type, "rank:", rank, "level", level, "fix:", value, "r_key" , r_key
                    #		, "r_value:", r_value, "base_value:", info3[r_key])
                else:
                    attr[key] = value
            attr_dict[attr_key] = attr
        file_contain[rank]["attr"] = attr_dict

    # 按战神类型(如后羿)拆分文件
    file_name = "{0}.c".format(fight_type)
    lpc_mapping = PythonData2Lpc(file_contain)
    lpc_file_contain = output.format(generate_code_tool.begin, lpc_mapping, func, generate_code_tool.end)
    generate_code_tool.write_file(out_path+file_name, lpc_file_contain)

def gen(xls_name, out_path):
    q1 = Queue()
    q2 = Queue()
    q3 = Queue()
    proc_arr = []
    proc_arr.append(Process(target=read_from_table, args=(xls_name, t_index_cfg, q1)))
    proc_arr.append(Process(target=read_from_table, args=(xls_name, t_fix_cfg, q2)))
    proc_arr.append(Process(target=read_from_table, args=(xls_name, t_base_cfg, q3)))
    for p in proc_arr:
        p.start()
    for p in proc_arr:
        p.join()
    index_table = q1.get()
    fix_table = q2.get()
    base_table = q3.get()

    proc_arr = []
    # 按索引表，在导表过程中计算出 战神*品阶*职业*等级 条数据
    for fight_type, info1 in index_table.iteritems():
        proc_arr.append(Process(target=gen_file, args=(fight_type, info1, fix_table, base_table, out_path)))
    for p in proc_arr:
        p.start()
    for p in proc_arr:
        p.join()

def main():
    if len(sys.argv) < 4 :
        xls_name = u"./战神表.xls"
        out_path = u"./"
        out_name = u"./hero_id.h"
    else :
        xls_name = sys.argv[1]
        out_path = sys.argv[2]
        out_name = sys.argv[3]
    xls_name = chinese_encode(xls_name)
    print sys.argv
    gen(xls_name, out_path)
    generate_code_tool.set_file_path(CONFIG_SRC_CODE, CONFIG_DB, xls_name, out_name)
    generate_code_tool.deal(CONFIG_SRC_CODE, CONFIG_DB)

if __name__ == "__main__":
    main()
