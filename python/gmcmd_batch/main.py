import csv
from StringIO import StringIO

file = open("ttt.csv")
metadata = file.readline()
metadata += file.readline()
metadata += file.readline()
file.seek(0)
filedata = file.read()
reader = csv.reader(StringIO(metadata))
note = reader.next()[0].decode('utf-8')
desc = reader.next()[0].decode('utf-8')
name = reader.next()[0].decode('utf-8')
print(note, desc, name)
