# -*- coding: UTF-8 ...
import hashlib


def getsorted_compact_data(data):
    data = [[k, v] for k, v in data.iteritems()]
    data.sort()
    return "".join(["{0}={1}".format(v[0], getsorted_compact_value(v[1])) for v in data])


def getsorted_compact_value(data):
    if isinstance(data, dict):
        return getsorted_compact_data(data)
    else:
        if isinstance(data, bool) or data is None:
            return str(data).lower()
        else:
            return data


data = {
    "ucid": 2010,
    "nickName": "九游玩家2010",
    "sid": "ssh1201487621349sdfskfjadfsafksdffjdk"
}

data = getsorted_compact_data(data)
caller = "sdkserver"
secret = "16ed2c94c5676478005239496c3c7b92"
sign_str = caller + data + secret
md = hashlib.md5()
md.update(sign_str)
print(md.hexdigest())
