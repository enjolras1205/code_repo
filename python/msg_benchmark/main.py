# -*- coding: UTF-8 -*-

import pymongo
import secrets
import bson
import random
import time
import base64
import threading

client = pymongo.MongoClient('mongodb://localhost:27017,localhost:27017')

action_weights = [
    (1440, "msg_query"),
    (10000, "msg_insert")
]

total_weight = 0
for v in action_weights:
    total_weight += v[0]


def get_col():
    col = client["TEST_chat"]["msg"]
    return col


def get_action():
    randval = random.randint(1, total_weight)
    acc = 0
    for v in action_weights:
        acc += v[0]
        if acc >= randval:
            return v[1]
    return None


def get_random_session_id():
    randval = random.randint(1, 100000)
    player_id1 = "noone_player_{0}".format(randval)
    randval2 = random.randint(-10, 10)
    randval2 = min(max(0, randval2 + randval), 100000)
    player_id2 = "noone_player_{0}".format(randval)
    if player_id1 < player_id2:
      player_id1, player_id2 = player_id2, player_id1
    session_id = "{player_id1}:{player_id2}".format(player_id1=player_id1, player_id2=player_id2)
    return session_id


def msg_query():
    session_id = get_random_session_id()
    col = get_col()
    print("query_session_id", session_id)
    resp = list(col.find({"session_id": session_id}).sort("_id").limit(25))
    return resp


def msg_insert():
    session_id = get_random_session_id()
    object_id = bson.objectid.ObjectId()
    object_id_str = str(object_id)
    ts = int(time.time())

    data = {"_id": object_id, "content": {"data": "send_from_{0}".format(base64.b64encode(secrets.token_bytes(100))), "type": "text"},
            "content_id": object_id_str, "delete_msg_members": [], "msg_id": object_id_str, "reader_status": 0, "send_id": "1639473845048140",
            "session_id": session_id, "src_id": "noone_player_146129", "src_info": {"user_id": "noone_player_146129", "user_type": "player"}, "src_type": "user", "status_msg": "", "to_id": "noone_player_146990", "ts": ts}
    col = get_col()
    print("insert_session_id", session_id)
    col.insert_one(data)


def main():
    while True:
        action = get_action()
        if action == "msg_query":
            msg_query()
        elif action == "msg_insert":
            msg_insert()


if __name__ == '__main__':
  threads = [threading.Thread(target=main) for v in range(10)]
  for v in threads:
    v.start()
  time.sleep(999999999999)
