#!/usr/bin/env python3
# -*- coding:utf8 -*-
import csv
import pymongo

csv_file_path = "p10488_star.csv"

client = pymongo.MongoClient()
db = client["p10488_star"]
col = db["user_label"]


def get_account_ids():
    account_ids = set()
    with open(csv_file_path, 'r') as file:
        csv_reader = csv.reader(file)
        for row in csv_reader:
            account_ids.add(row[0])
    print("account_count", len(account_ids))
    return account_ids


def main():
    account_ids = get_account_ids()
    account_ids2 = set()
    csv_arr = []
    db_count = col.count_documents({})
    print("db_count", db_count)
    count = 0
    for doc in col.find({}):
        run_result = doc["run_result"][5:]
        for v in run_result:
            vv = v.split(",")
            vv_count = len(vv)
            for idx in range(vv_count // 6):
                start_idx = 6 * idx
                account_id = vv[start_idx]
                account_id = account_id.replace("\n", "")
                result = vv[start_idx + 5]
                if result != '成功':
                    print("account_id_result", account_id, result)
                account_ids2.add(account_id)
                count += 1
                # csv_arr.append([account_id] + vv[start_idx + 1: start_idx + 6])
    print("account_ids2", len(account_ids2), count)
    account_ids3 = account_ids.difference(account_ids2)
    print("account_ids3", len(account_ids3))
    for account_id in account_ids3:
        print("account_id", account_id, account_id in account_ids2,
              account_id in account_ids)
    # with open("p10488_db_user_label.csv", 'w', newline='') as file:
    #     csv_writer = csv.writer(file)
    #     csv_writer.writerows(csv_arr)


if __name__ == "__main__":
    main()
