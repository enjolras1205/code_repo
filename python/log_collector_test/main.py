import requests
import base64
import uuid
import gzip
import json
import random
from hyper.contrib import HTTP20Adapter

# url = "https://p10442-a-log-collector.ejoy.com/log/gbi_log"
url = "http://localhost:2606/log/gbi_log"
s = requests.Session()
# s.mount(url, HTTP20Adapter())
data = b'{"event": "client.boot", "time": 0}'
data = gzip.compress(data)
files = [
    ('data', ('xdata-log', data, 'application/octet-stream'))
]
headers = {
    'trace-id': str(uuid.uuid1())
}


resp = s.request("POST", url, headers=headers, files=files)
print(resp.text, resp.headers)
