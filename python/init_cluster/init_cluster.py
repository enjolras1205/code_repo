import requests
import sys
import json
import time
import logging

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
root.addHandler(handler)


def init_cluster(cluster_name, entity_nodes, broadcast_nodes):
    request_data = {
        "cluster_name": cluster_name,
        "is_first_init": True,
        "entity_nodes": entity_nodes,
        "broadcast_nodes": broadcast_nodes
    }
    headers = {
        "Content-Type": "application/json"
    }
    logging.info("request_data:{0}".format(request_data))
    resp = requests.request(
        "POST", "http://localhost:1233/init_cluster", headers=headers, data=json.dumps(request_data))
    logging.info("resp_content:{0}".format(resp.content))
    success = False
    if resp.status_code == 200:
        resp_data = json.loads(resp.content)
        if resp_data["code"] == 0:
            logging.info("init_cluster_success:{0}".format(resp_data))
        success = True

    if not success:
        logging.info("init_cluster_fail:{0}".format(resp.content))
        exit(1)


def get_chat_node_name(service_data):
    product = service_data["groupKey"]
    nodes = [
        "{product}_{app}_{chat_role}@{product}-{name}-{seq}.{product}-{name}.{namespace}.svc.cluster.local".format(
            product=product, app=service_data["app"], name=service_data["name"], seq=vv,
            chat_role=service_data["chat_role"], namespace=service_data["namespace"]) for vv in range(service_data["replicas"])]

    return nodes


def get_chat_node_name(service_data):
    product = service_data["groupKey"]
    nodes = [
        "{product}_{app}_{chat_role}@{product}-{name}-{seq}.{product}-{name}.{namespace}.svc.cluster.local".format(
            product=product, app=service_data["app"], name=service_data["name"], seq=vv,
            chat_role=service_data["chat_role"], namespace=service_data["namespace"]) for vv in range(service_data["replicas"])]

    return nodes


def init_chat_cluster(chat_gate_data, chat_logic_data):
    broadcast_nodes = get_chat_node_name(chat_gate_data)
    entity_nodes = get_chat_node_name(chat_logic_data)
    init_cluster("chat", entity_nodes, broadcast_nodes)


def get_node_name(service_data):
    product = service_data["groupKey"]
    nodes = [
        "{product}_{app}@{product}-{name}-{seq}.{product}-{name}.{namespace}.svc.cluster.local".format(
            product=product, app=service_data["app"], name=service_data["name"], seq=vv,
            namespace=service_data["namespace"]) for vv in range(service_data["replicas"])]
    return nodes


def init_normal_cluster(cluster_name, service_data):
    entity_nodes = get_node_name(service_data)
    init_cluster(cluster_name, entity_nodes, [])


if __name__ == "__main__":
    kun_data = json.loads(sys.argv[1])
    service_data = dict()
    for v in kun_data:
        service_data[v["name"]] = {vv["variable"]
            : vv["value"] for vv in v["vars"]}
        service_data[v["name"]].update(v)
    logging.info("all_service_data:{0}".format(service_data))
    init_chat_cluster(service_data["chatgate"], service_data["chatlogic"])
    init_normal_cluster("game_adapter_ex", service_data["gameadapterex"])
