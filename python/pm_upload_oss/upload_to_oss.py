#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import os
import oss2

oss_config = dict()
endpoint = oss_config.get('endpoint', 'oss-cn-shenzhen.aliyuncs.com')
access_key_id = oss_config.get('access_key_id', 'LTAIiAPpDIF63M3E')
access_key_secret = oss_config.get('access_key_secret', 'EzgpSaOYJNH8ISwdCMQS7NKLgowgnp')
bucket_name = oss_config.get('bucket_name', 'ejoy-pm')
auth = oss2.Auth(access_key_id, access_key_secret)
bucket = oss2.Bucket(auth, endpoint, bucket_name)

def upload_dir_to_oss(base_path):
    for short_path in os.listdir(base_path):
        print("short_path", short_path)
        if os.path.isdir(os.path.join(base_path, short_path)):
            upload_product_dir_to_oss(base_path, short_path)

def upload_product_dir_to_oss(base_path, sub_path):
    print("upload_dir_to_oss", base_path, sub_path)
    for short_path in os.listdir(os.path.join(base_path, sub_path)):
        full_path = os.path.join(base_path, sub_path, short_path)
        if os.path.isdir(full_path):
            upload_product_dir_to_oss(base_path, os.path.join(sub_path, short_path))
        else:
            if short_path == "version.json":
                continue
            _name, ext = os.path.splitext(short_path)
            object_path = os.path.join("pm_package", sub_path, short_path)
            oss2.resumable_upload(bucket, object_path, full_path)
            bucket.put_symlink(object_path, os.path.join("pm_package", sub_path, "latest{0}".format(ext)))
    return True

def main(path):
    print("path : ", path)
    raw_input("Press <ENTER> to continue...")
    upload_dir_to_oss(path)


if __name__ == '__main__':
    main(sys.argv[1])
