#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import os
import json
import pymongo
import datetime


mongo_client = pymongo.MongoClient()
col = mongo_client["package_new"]["branch"]


def build_dir_to_package_collection(base_path):
    for short_path in os.listdir(base_path):
        if os.path.isdir(os.path.join(base_path, short_path)):
            build_collection(base_path, short_path)

def build_collection(base_path, sub_path):
    print("upload_dir_to_oss", base_path, sub_path)
    for short_path in os.listdir(os.path.join(base_path, sub_path)):
        full_path = os.path.join(base_path, sub_path, short_path)
        if os.path.isdir(full_path):
            build_collection(base_path, os.path.join(sub_path, short_path))
        else:
            if short_path == "version.json":
                with open (full_path) as f:
                    data = json.load(f)
                    product = full_path.split(os.sep)[-3]
                    if product == "P10020":
                        continue
                    update_time = datetime.datetime.strptime(data["update_time"], "%Y-%m-%d %H:%M:%S")
                    update_time -= datetime.timedelta(hours=8)
                    insert_data = {}
                    insert_data["branch_id"] = data["id"]
                    insert_data["revision_id"] = data["id"]
                    insert_data["name"] = data["name"]
                    insert_data["product"] = product
                    insert_data["version"] = data["version"]
                    insert_data["desc"] = data["title"]
                    insert_data["package_type"] = data["os"]
                    insert_data["sort_key"] = 0
                    insert_data["permission"] = data["permission"]
                    insert_data["share"] = data["share"]
                    insert_data["remark"] = ""
                    insert_data["spec_items"] = data["spec_items"]
                    insert_data["create_time"] = update_time
                    insert_data["update_time"] = update_time
                    insert_data["status"] = "synced"
                    insert_data["sh_pid"] = 0
                    print("insert_data to db", insert_data)
                    col.insert(insert_data)

def main(path):
    print("path : ", path)
    raw_input("Press <ENTER> to continue...")
    build_dir_to_package_collection(path)

if __name__ == '__main__':
    main(sys.argv[1])
