#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymongo
import datetime

# PRODUCT_ID = "p10082"
# STAKE_ID = "zhounianqing_choujian_2020"
# stake_client = pymongo.MongoClient('localhost', 27017)
# user_info_client = pymongo.MongoClient('localhost', 27017)

# PRODUCT_ID = "p10445"
# STAKE_ID = "zhounianqing_choujiang_2020"
# stake_client = pymongo.MongoClient('s3-cn-ob-platform054', 27017)
# user_info_client = pymongo.MongoClient('s3-cn-ob-platform001', 27017)

PRODUCT_ID = "p10071"
STAKE_ID = "zhounianqing_choujiang_2020"
stake_client = pymongo.MongoClient('192.168.27.124', 27017)
user_info_client = pymongo.MongoClient('192.168.27.124', 27017)

player_list = [
    "10001",
    "10002"
]

is_small = False


def replace_stake_reward_list():
    # 检测player_id合法
    for player_id in player_list:
        resp = user_info_client["{0}_game_server".format(
            PRODUCT_ID.upper())].game_player.find_one({"player_id": player_id})
        if not resp:
            print("cant_find", player_id)
            return
    # 获取当前发奖批次
    db = stake_client["{0}_stake".format(PRODUCT_ID.upper())]
    resp = db.register_batch.find_one({"stake_id": STAKE_ID, "status": 1})
    # 中奖名单为当前报名的上一批次
    batch_id = resp["batch_id"] - 1
    print("cur_reward_batch_id", batch_id)
    old_list = list(db.draw_result.find(
        {"stake_id": STAKE_ID, "batch_id": batch_id}))
    print("old_data", old_list)
    db.draw_result.remove({"stake_id": STAKE_ID, "batch_id": batch_id})
    now = datetime.datetime.now()
    for idx, player_id in enumerate(player_list):
        db.draw_result.insert(
            {"stake_id": STAKE_ID, "batch_id": batch_id, "is_small": is_small,
             "dispatched": False, "player_id": player_id,
             "reward_id": reward_id, "dispatch_ts": now + datetime.timedelta(milliseconds=idx)})
    new_list = list(db.draw_result.find(
        {"stake_id": STAKE_ID, "batch_id": batch_id}))
    print("new_data", new_list)


if __name__ == '__main__':
    replace_stake_reward_list()
