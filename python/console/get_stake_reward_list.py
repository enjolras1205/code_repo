#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymongo

# PRODUCT_ID = "p10082"
# STAKE_ID = "zhounianqing_choujian_2020"
# stake_client = pymongo.MongoClient('localhost', 27017)
# user_info_client = pymongo.MongoClient('localhost', 27017)

# PRODUCT_ID = "p10445"
# STAKE_ID = "zhounianqing_choujiang_2020"
# stake_client = pymongo.MongoClient('s3-cn-ob-platform054', 27017)
# user_info_client = pymongo.MongoClient('s3-cn-ob-platform001', 27017)

PRODUCT_ID = "p10071"
STAKE_ID = "zhounianqing_choujiang_2020"
stake_client = pymongo.MongoClient('192.168.27.124', 27017)
user_info_client = pymongo.MongoClient('192.168.27.124', 27017)


def get_stake_reward_list():
    db = stake_client["{0}_stake".format(PRODUCT_ID.upper())]
    resp = db.register_batch.find_one({"stake_id": STAKE_ID, "status": 1})
    cur_batch_id = resp["batch_id"]
    print("cur_batch_id", cur_batch_id)
    result = [get_one_result(v) for v in db.draw_result.find(
        {"stake_id": STAKE_ID, "batch_id": cur_batch_id})]
    print("result", result)


def get_one_result(one_reward_result):
    player_id = one_reward_result["player_id"]
    resp = user_info_client["{0}_game_server".format(
        PRODUCT_ID.upper())].game_player.find_one({"player_id": player_id})
    one_reward_result["name"] = resp["player_info"]["name"]
    del one_reward_result["_id"]
    return one_reward_result


if __name__ == '__main__':
    get_stake_reward_list()
