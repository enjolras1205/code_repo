# -*- coding: utf-8 -*-
# 旅行青蛙的导表。
import os
import json
import xlrd
import config
import exporter.default
import string


def main(root_dir):
    for export_file_data in config.EXPORT_DATA:
        file_path = os.path.join(root_dir, export_file_data['file'])
        book = xlrd.open_workbook(file_path)
        out_dir = os.path.join(root_dir, export_file_data['out_dir'])
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        out_sheet = export_file_data['out_sheet']
        for out_sheet_data in out_sheet:
            sheet_idx = out_sheet_data['sheet_idx']
            sheet = book.sheet_by_index(sheet_idx)
            # out_file = out_sheet_data['out_file']
            out_file = sheet.cell_value(0, 2)
            out_file = string.replace(out_file, "DataBase", ".json")
            exporter_file_name = os.path.splitext(out_file)[0].lower()
            try:
                sheet_export_module = __import__("exporter.{0}".format(exporter_file_name), fromlist=[exporter_file_name])
            except ImportError:
                sheet_export_module = exporter.default
            json_data = sheet_export_module.export(sheet, out_sheet_data)
            # 输出到文件
            out_file_path = os.path.join(root_dir, out_dir, out_file)
            with open(out_file_path, 'w') as f:
                json.dump(json_data, f, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ': '))


if __name__ == '__main__':
    main(os.path.dirname(os.path.abspath(__file__)))
