# -*- coding: UTF-8 -*-

from pymongo import MongoClient
import sys
import getopt
import six
import os
import json
import pytz


_UTC_TZ = pytz.timezone("UTC")
_CST_TZ = pytz.timezone("Asia/Shanghai")


class DefaultConfig(object):
    product = []
    mongo = "127.0.0.1:27017"
    src = "/srv/upload/package_manager/"
    dst = "./"


def migrate_pm_data_dir(product, mongo, src, dst):
    assert os.path.isdir(src), "src is not a directory"
    assert os.path.isdir(dst), "dst is not a directory"
    # todo check mongo config (mongo connect timeout)
    client = MongoClient(mongo)
    collection = client.package.branch
    find_condition = {}
    if product:
        find_condition = {'product': {'$all': product}}
    all_branch = collection.find(find_condition)
    for v in all_branch:
        migrate_one_branch(src, dst, v)


def migrate_one_branch(src, dst, v):
    # 1. generate version.json to dst
    branch_id = str(v["_id"])
    product_name = v['product']
    branch_dir = os.path.join(dst, product_name, branch_id)
    branch_version_file = os.path.join(branch_dir, "version.json")
    package_type = v['package_type']
    time = v['update_time'].replace(tzinfo=_UTC_TZ).astimezone(_CST_TZ)
    branch_data = {
        "id": branch_id
        , "product": product_name
        , "name": v['name']
        , "title": v['desc']
        , "version": v['version']
        , "os": package_type
        , "update_time": time.strftime("%Y-%m-%d %H:%M:%S")
        , "permission": v.get('permission', 'team')
        , "share": v.get('share', 'forbidden')
        , "top": v.get('top', False)
    }
    spec = v['spec_items']
    if package_type == "ios":
        file_name = spec['ipa']['name']
        branch_data['bundle_id'] = spec['bundle_id']
        branch_data['filename'] = file_name
        branch_data['filesize'] = spec['ipa']['size']
        branch_data['md5'] = spec['ipa']['md5']
    else:
        file_name = spec['apk']['name']
        branch_data['filename'] = file_name
        branch_data['filesize'] = spec['apk']['size']
        branch_data['md5'] = spec['apk']['md5']
    branch_data['spec_items'] = spec
    if not os.path.exists(branch_dir):
        try:
            os.makedirs(branch_dir)
        except Exception as e:
            raise 'package create dir %s fail %s' % branch_dir, e.message
    with open(branch_version_file, "w") as f:
        print("writing {0}".format(branch_version_file))
        f.write(json.dumps(branch_data).encode('utf-8'))
    # 2. hard link dst file to src file
    src_branch_package_file = os.path.join(src, product_name, branch_id, file_name)
    dst_branch_package_file = os.path.join(branch_dir, file_name)
    cmd = "ln -f {src} {dst}".format(src=src_branch_package_file, dst=dst_branch_package_file)
    print(cmd)
    if os.system(cmd) != 0:
        print("link error", cmd)


def usage():
    print("usage:")
    print("python {file_name} --product LR項目,A2 --mongo 127.0.0.1:27017 --src {src} --dst {dst} \n".format(
        file_name=__file__, src=DefaultConfig.src, dst=DefaultConfig.dst))
    print("default value:")
    print("product : [] (to migrate all product)")
    print("mongodb : ", DefaultConfig.mongo)
    print("src : ", DefaultConfig.src)
    print("dst : ", DefaultConfig.dst)


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hp:m:s:d:", ["product=", "mongo=", "src=", "dst="])
    except getopt.GetoptError:
        usage()
        sys.exit()
    actual_args = dict(
        product=DefaultConfig.product
        , mongo=DefaultConfig.mongo
        , src=DefaultConfig.src
        , dst=DefaultConfig.dst
    )
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-p", "--product"):
            actual_args['product'] = arg.split(',')
        elif opt in ("-m", "--mongo"):
            actual_args['mongo'] = arg
        elif opt in ("-s", "--src"):
            actual_args['src'] = arg
        elif opt in ("-d", "--dst"):
            actual_args['dst'] = arg
    print("actual_args : ", actual_args)
    if six.PY2:
        raw_input("Press <ENTER> to continue...")
    else:
        input("Press <ENTER> to continue...")
    migrate_pm_data_dir(**actual_args)


if __name__ == '__main__':
    main(sys.argv[1:])
