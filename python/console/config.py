# -*- coding: UTF-8 -*-


# file/out_dir 路径为export_data.py 脚本所在路径的相对路径
EXPORT_DATA = [
    # title_row 配置为excel里看到的行号（程序中下标-1）
    # 默认导出所有(title)下的内容。不需要导出的，配置在ignore_title里，无需配置括号。
    # img 字段会强制去后缀（发现所有需要去后缀的title都是叫img）
    # 根据out_file选择exporter里的exporter，没有实现则使用上面提到的默认exporter
    {"file": "makedata/MainData.xlsx", "out_dir": "my_config/MainData", "out_sheet": [
        {"sheet_idx": 1, "title_row": 3, "ignore_titles": []}
        , {"sheet_idx": 2, "title_row": 6, "ignore_titles": []}
        , {"sheet_idx": 3, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 4, "title_row": 5, "ignore_titles": []}
        , {"sheet_idx": 5, "title_row": 7, "ignore_titles": []}
        , {"sheet_idx": 6, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 7, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 8, "title_row": 4, "ignore_titles": ["flag.Array"]},
    ]},
    {"file": "makedata/PictureData.xlsx", "out_dir": "my_config/PictureData", "out_sheet": [
        {"sheet_idx": 1, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 2, "title_row": 7, "ignore_titles": []}
        , {"sheet_idx": 3, "title_row": 5, "ignore_titles": []}
        , {"sheet_idx": 4, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 5, "title_row": 4, "ignore_titles": []}
    ]},
    {"file": "makedata/TravelData.xlsx", "out_dir": "my_config/TravelData", "out_sheet": [
        {"sheet_idx": 1, "title_row": 3, "ignore_titles": []}
        , {"sheet_idx": 2, "title_row": 5, "ignore_titles": []}
        , {"sheet_idx": 3, "title_row": 4, "ignore_titles": []}
        , {"sheet_idx": 4, "title_row": 4, "ignore_titles": []} # 找不到title，待特殊处理
        , {"sheet_idx": 5, "title_row": 4, "ignore_titles": []}
    ]},
]
