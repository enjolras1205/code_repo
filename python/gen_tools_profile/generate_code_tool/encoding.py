# -*- coding: utf-8 -*-

from types import *
import string
import locale		
encoding = locale.getdefaultlocale()[1]
		
@profile
def chinese_encode(name):

	res = ""

	if isinstance(name, list) :
		res = "["
		for x in name :
			res += chinese_encode(x)+","
		res += "]"

	elif isinstance(name, tuple) :
		res = "("
		for x in name :
			res += chinese_encode(x)+","
		res += ")"

	elif isinstance(name, dict) :
		res = "{"
		for x,y in name.items():
			res += chinese_encode( str(x)+"="+str(y) ) + ","
		res += "}"

	elif isinstance(name, unicode) :
		res = name.encode(encoding)
	elif isinstance(name, str):
		res = name

	elif isinstance(name, int) or isinstance(name, float) or isinstance(name, long):
		res = str(name)

	else :
		print type(name), "诡异type"

		res = str(name)

	return res


###################################LpcPython##############################
class LpcPython(object):
        def __init__(self):
                self.lpcstr=''
                self._deep = 0

        ################public function#####################################
        def Lpc2Python(self,lpcstr):
                """
                translate lpc string value to python object 
                return python obj
                """
                lpcstr=string.strip(lpcstr)
                if( not lpcstr):
                        return None

                pyObj=None

                value=string.replace(string.replace(lpcstr,"([","{"),"])","}")
                value=string.replace(string.replace(value,"({","["),"})","]")
                value=string.replace(value,'":,','":None,')
                
                exec('pyObj=%s' % value)
                return pyObj

                           
        def Python2Lpc(self,pyObj):
                self.lpcstr=''
                return self._Python2Lpc(pyObj)

        ###########################internal function###########################################
        dispatch={}

        def _Python2Lpc(self,pyObj):
                t = type(pyObj)
                f = self.dispatch.get(t)
                if f:
                    f(self, pyObj) 
                return self.lpcstr.replace("\n,", ",\n") 
                #return self.lpcstr
        def push_none(self, obj):
                self.push('')
        dispatch[NoneType] = push_none
        
        def push_bool(self, obj):
                self.push(obj and TRUE or FALSE)
        dispatch[bool] = push_bool
        
        def push_int(self,obj):
                self.push(`obj`)
        dispatch[IntType] = push_int

        def push_long(self, obj):
                self.push(`obj`)

        dispatch[LongType] = push_long

        def push_float(self, obj):
                self.push(`obj`)
        dispatch[FloatType] = push_float

        def push_string(self, obj):
                s="%s" % obj
                #s=string.replace(s,"\\",'\\\\')
                #obj=string.replace(s,'"','\\"')
                self.push('"')
                self.push(obj)
                self.push('"')

        dispatch[StringType] = push_string
	dispatch[UnicodeType] = push_string

        def push_tuple(self, obj):
                for element in obj:
                        self._Python2Lpc(element)
            
        dispatch[TupleType] = push_tuple

        def push_list(self, obj):
                self.push('({')
                for element in obj:
                        self._Python2Lpc(element)
                        self.push(',')
                self.push('})')
            
        dispatch[ListType] = push_list

        def push_dict(self, obj):

                self.push('([')
                for k, v in obj.items():
                	self._Python2Lpc(k)
                        self.push(':')
                        self._deep += 1
                        self._Python2Lpc(v)
			if self._deep < 1 :
	             	   	self.push("\t")
        	        else :
        	        	self.push(",")
        	        self._deep -= 1

                self.push('])\n')
                
                if self._deep <= 1 :
                	self.push("\n")
                
        dispatch[DictionaryType] = push_dict

        def push(self,lpcstr):
                self.lpcstr+=lpcstr

def test_chinese_encode():
	print chinese_encode( ['我', "A", "你", "B"] )
	print chinese_encode( {'我': "A", "你": "B"} )
	print chinese_encode( ('我', "A", "你", "B") )
	print chinese_encode( 1234 )
	print chinese_encode( "ABCD" )
	print chinese_encode( ("ABCD",) )
	print chinese_encode( ("五?一",) )

if __name__ == "__main__":
	test_chinese_encode()
