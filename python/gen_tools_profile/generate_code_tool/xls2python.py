#!/usr/bin/env python
# -*- coding:utf-8 -*-

import xlrd
import sys
import os
import string
import re
from encoding import chinese_encode, LpcPython
from Python2Lpc import PythonData2Lpc
from slpp.slpp import slpp

#eg:
'''
配置文件
CONFIG_SRC_CODE = {
	#输出文件
	"output_file_name"  :   "输出文件名",
	#代码格式
	"code_format" : 
static mapping mpOrnamentAttrInfo = %(CONFIG_DB_KEY)s;


CONFIG_DB = {
"CONFIG_DB_KEY" : {
	#输入文件
	"db_file"  :   u"*.xls",
	#表
	"table_sheet"   :   u"表名字",
	#字段行数
	"row_line"  : 2,
	#索引键
	"index_key"   :   ["索引1","索引2","索引3",],

	#唯一值，由indexs直接映射到value，可以少一层从fieldName到value的映射
	"unique_value" : "string",

	#白名单
	"whitelist" : ["field_name","field_name","field_name",],
	#黑名单
	"blacklist" : ["field_name","field_name","field_name",],

	#字段属性
	"field_attribute"   :   {
		field_name :{
		   "alias" : "别名",
		   "func"  : 处理函数,
			},
		
		},
	},
}

'''

def lua_to_lpc( nRow, nCol, szFieldName, CellValue, dicParam ): 
	return slpp.decode(CellValue)

def to_lpc( nRow, nCol, szFieldName, CellValue, dicParam ): 
	return LpcPython().Lpc2Python(CellValue)

def str_to_array( nRow, nCol, szFieldName, CellValue, dicParam ): 
	val = CellValue.strip()
	ret =  map(lambda x: int(x), val.split(','))
	return ret

def OpenFile( szFileName ):
	szFileName = chinese_encode(szFileName)
	
	try:
		book = xlrd.open_workbook(szFileName)
		print "OpenFile " + szFileName
	except:
		msg = "can't open file " + szFileName
		print msg
		sys.exit(-1)
	return book
	

def GetTable( hBook, szTable ):
	szTable = chinese_encode(szTable)

	for x in xrange(hBook.nsheets):
		sh = hBook.sheet_by_index(x)
		if chinese_encode(sh.name) == szTable:
			print "Open Sheet " + chinese_encode(sh.name)
			return sh
	
	print "Can't open sheet " + szTable
	return None
	

#白名单过滤
def WhitelistFilter( szFieldName, plist ):
	if isinstance(plist, list) == False:
		return True
	if len(plist) ==0:
		return True
	if (szFieldName in plist) == True:
		return True
	return False
	


#黑名单过滤
def BlacklistFilter( szFieldName, plist ):
	if isinstance(plist, list) == False:
		return True
	if len(plist) ==0:
		return True
	if (szFieldName in plist) == True:
		return False
	return True
	

def Parse( hTable, cfg ):
	nRow = 0
	nCol = 0
	tmplist = {}
	uniqKey = None;
	nField = cfg["row_line"]
	field_line = nField - 1
	if cfg.has_key("field_line"):
		field_line = cfg["field_line"];
	
	dicFieldAttr = cfg["field_attribute"]

	#"index_key"
	listKey = []
	for szIndex in cfg["index_key"]:
		szIndex = chinese_encode(szIndex)
		bFind = False

		for y in xrange(hTable.ncols):
			szFieldName = chinese_encode(hTable.cell_value(field_line, y))
			if szFieldName == szIndex:
				listKey = listKey + [y]
				bFind = True
				break
		if bFind == False:
			print( "can not find index_key " + szIndex );
			sys.exit(-1)


	#读每一行
	for x in xrange(hTable.nrows):
		if x < nField:
			continue

		#填充节点
		pNode = tmplist
		pNodeParent = pNode
		for nIndexCol in listKey:
			value = hTable.cell_value(x, nIndexCol)
			szFieldName = chinese_encode(hTable.cell_value(field_line, nIndexCol))
			
			if isinstance(value, float):
				value = int(value)
			else:
				value = chinese_encode(value)

			if dicFieldAttr.has_key( szFieldName ):
				dicField = dicFieldAttr[szFieldName]
				
				if dicField.has_key( "func" ):
					value = dicField["func"]( x, y, szFieldName, value, hTable )

			pNodeParent = pNode
			if pNode.has_key(value) == False:
				pNode[value] = {}   
			pNode = pNode[value]

			if cfg.has_key("unique_value"):
				uniqKey = value
			
		#读每一列
		for y in xrange(hTable.ncols):
			szFieldName = chinese_encode(hTable.cell_value(field_line, y))

			#白名单过滤
			if WhitelistFilter( szFieldName, cfg["whitelist"] ) == False:
				continue
			#黑名单过滤
			if BlacklistFilter( szFieldName, cfg["blacklist"] ) == False:
				continue

			value = hTable.cell_value(x, y)

			if isinstance(value, float):	
				value = int(value)
			else:
				value = chinese_encode(value)
			
			if (value == ''):
				continue;

			szAlias = szFieldName
			if dicFieldAttr.has_key( szFieldName ):
				dicField = dicFieldAttr[szFieldName]
				
				if dicField.has_key( "alias" ):
					szAlias = chinese_encode(dicField["alias"])

				if dicField.has_key( "func" ):
					value = dicField["func"]( x, y, szFieldName, value, hTable )

			# 有唯一值，那么最后一层将不是dict，而是value
			if uniqKey:
				if szFieldName == cfg["unique_value"]:
					pNodeParent[uniqKey] = value
					uniqKey = None
					break
			else:
				pNode[szAlias] = value


	return tmplist
	
		
