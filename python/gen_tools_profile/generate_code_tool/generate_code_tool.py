#!/usr/bin/env python
# -*- coding:utf-8 -*-


import xlrd
import sys
import os
import string
import re
from encoding import chinese_encode
from Python2Lpc import PythonData2Lpc 
import xls2python

begin = r"// -------------------  Auto Generate Begin --------------------"
end   = r"// -------------------  Auto Generate End   --------------------" + "\n"

szVersion = "1.0.0.0"

szDescribe = '''
/*
Auto Generate By Generate Code Tool
Version: %s
*/
'''

# 单个表格到python dict
def xls2python_data(hBook, szSheet, config):
	szSheet = chinese_encode( szSheet )
	hTable = xls2python.GetTable( hBook, szSheet )
	if hTable == None:
		sys.exit(-1)
		
	return xls2python.Parse( hTable, config )

def xls2Lpc( config ):
	szfilename = config["db_file"]
	szfilename = chinese_encode( szfilename )
	hBook = xls2python.OpenFile( szfilename )
	pythonData = None
		
	szSheets = config["table_sheet"]
	if not isinstance(szSheets, list):
		szSheets = [szSheets]
	for szSheet in szSheets:
		if pythonData:
			pythonData = dict(xls2python_data(hBook, szSheet, config), **pythonData)
		else:
			pythonData = xls2python_data(hBook, szSheet, config)

	szLpc = PythonData2Lpc(pythonData)
	#szLpc = PythonData2Lpc(pythonData, True, 0)
	return szLpc
	

def read_context( szFileName ):
	bFlag = True
	try :
		f = open(szFileName, "rb+")
	except :
		bFlag = False
	
	if bFlag == False:
		try:
			f = open(szFileName, "wb+")
			all_the_text = init_file_context()
			f.write(all_the_text)
			
		except:
			msg = "can not write to", szFileName
			print msg
			sys.exit(-1)
				
	else:
		try:
			all_the_text = f.read( )
		except :
			print( "can not read " + szFileName )
			f.close( )
			sys.exit(-1)
		
	f.close( )

	return all_the_text
		

def deal( codeCfg, DBCfg ):
	szFileName = codeCfg["output_file_name"]
	
	szFileContext = read_context( szFileName )

	mpLpcContext = {}
	for k,v in DBCfg.items():
		mpLpcContext[k] = xls2Lpc( v )
	
	szLpc = codeCfg["code_format"] % mpLpcContext
	
	# 替换文件中的表格段 
	p = re.compile(begin + r".*?" + end, re.S | re.M)
	szResult = p.sub(begin + "\n" + chinese_encode(szLpc) + "\n" + end, szFileContext)
	
	print szResult
	
	write_file(szFileName,szResult)
	

def set_file_path( codeCfg, DBCfg, inputName, outputName ):
	if outputName != None:
		codeCfg["output_file_name"] = outputName

	if inputName != None:
		for k, v in DBCfg.items():
			v["db_file"] = inputName
	

def init_file_context():
	szContext = "\n" + begin + "\n\n" + end + "\n"

	return szContext
	

def write_file(filename, content ):
	try :
		f = file(filename, "w+b")
	except :
		msg = "can not write to", filename
		sys.exit(-1)
	
	f.write(content)
	f.close()

