# -*- coding: utf-8 -*-

from encoding import chinese_encode

indent_space = '        '
max_indent_cnt = 2


# 计算缩进
@profile
def getIndent( indentFlg, indentCnt):
        result = ''
        
        if not indentFlg:
                return result
        
        for i in range( 0, indentCnt):
                result = result + indent_space
                
        return result

# 将python dict 转换为lpc dict
@profile
def PythonDict2Lpc( data, indentFlg=False, indentCnt=0):
        
        if indentCnt > max_indent_cnt:
                indentFlg = False
                
        result = '(['
        if indentFlg :
                result = result + "\n"
        
        # 遍历 数据
        keys = data.keys()
        
        keys.sort()
        
        for key in keys:
                value = data[key]

                result = result + getIndent(indentFlg, indentCnt+1)

                strKey = (PythonData2Lpc(key,indentFlg, indentCnt+1))
		if strKey == "\"types\"":
			strValue = (PythonData2Lpc(value,indentFlg, 4))
		else:
			strValue = (PythonData2Lpc(value,indentFlg, indentCnt+1))
                
		tab = "\t"
		for i in xrange(indentCnt):
			tab += "\t"
                result = result  + ("\n%s%s : %s, "%(tab, strKey, strValue))
                if indentFlg:
                        result = result + "\n"
        
        result = result + getIndent(indentFlg, indentCnt) + '\n\t])'
        return result

# 将python list 转换为lpc list
@profile
def PythonList2Lpc( data, indentFlg=False, indentCnt=0):
        if indentCnt > max_indent_cnt:
                indentFlg = False
        
	if indentCnt > 2 :
		result = "\n\t\t\t({"
	else:
        	result = '({'
        
        if indentFlg:
                result = result + "\n"
        # 遍历 数据
        tempIndex = 0
        for value in data:
                result = result + getIndent(indentFlg, indentCnt+1) + ("%s, "%(PythonData2Lpc(value,indentFlg, indentCnt+1)))
                if indentFlg:
                        result = result + "\n"
                        
                tempIndex = tempIndex + 1
                if tempIndex > 10:
                    result = result + "\n\t\t\t"
                    tempIndex = 0
                
        
	if indentCnt < 3:
        	result = result + getIndent(indentFlg, indentCnt) + '\n\t\t})'
	else:
        	result = result + getIndent(indentFlg, indentCnt) + '})'
        return result


# 将python tuple 转换为lpc tuple
def PythonTuple2Lpc( data, indentFlg=False, indentCnt=0):
        return PythonList2Lpc(data, indentFlg, indentCnt)
        
# 将python数据转换为lpc数据
@profile
def PythonData2Lpc( data, indentFlg=False, indentCnt=0):
        if isinstance(data, str):
                return '"%s"'%chinese_encode(data)
        elif isinstance(data, unicode):
                return '"%s"'%chinese_encode(data)
        elif isinstance(data, int):
                return "%d"%data
        elif isinstance(data, list):
                return PythonList2Lpc( data, indentFlg, indentCnt)
        elif isinstance(data, tuple):
                return PythonList2Lpc( data, indentFlg, indentCnt)
        elif isinstance(data, dict):
                return PythonDict2Lpc( data, indentFlg, indentCnt)
                

if __name__ == "__main__":
        testData = { "a":1, "b":2, "c":3, "d":(1,2,3,4,5,6,), "d":{"a":3232, "b":42343}}
        print( PythonData2Lpc(testData, True, 0))
