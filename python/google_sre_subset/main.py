
import random

back_num = 8
client_num = 300
subset_size = 2

backends_count = dict()
round_count = dict()


def Subset(client_id, subset_size):
    backends = ["gate{0}".format(idx) for idx in range(back_num)]
    subset_count = len(backends) / subset_size
    # Group clients into rounds; each round uses the same shuffled list:
    round = int(client_id / subset_count)
    # round_count[round] = round_count.get(round, 0) + 1
    random.seed(round)
    random.shuffle(backends)
    # The subset id corresponding to the current client:
    subset_id = client_id % subset_count
    start = int(subset_id * subset_size)
    return backends[start:start + subset_size]


client_ids = random.sample(range(1, 10000), 2000)

for client_id in client_ids:
    client_backend = Subset(client_id, subset_size)
    for v in client_backend:
        backends_count[v] = backends_count.get(v, 0) + 1


print("backends_count", backends_count, round_count)
