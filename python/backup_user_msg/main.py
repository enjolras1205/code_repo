#!/usr/bin/env python3
import pymongo
import time
from datetime import datetime

s3_chat_db_host_to = "127.0.0.1"
s3_chat_db_port_to = 27017
s3_chat_db_host_from = "127.0.0.1"
s3_chat_db_port_from = 27017

one_batch_ms = 33

def get_current_millsecond():
  return int(round(time.time() * 1000))

def main():
    from_client = pymongo.MongoClient(
        host=s3_chat_db_host_from, port=s3_chat_db_port_from)
    to_client = pymongo.MongoClient(
        host=s3_chat_db_host_to, port=s3_chat_db_port_to)
    from_db = from_client.P10445_chat
    to_db = to_client.P10445_chat
    # ensure index
    to_db.msg_tmp.create_index([("msg_id", pymongo.ASCENDING)], unique=True)
    to_db.msg_tmp.create_index([("session_id", pymongo.ASCENDING), (
        "reader_status", pymongo.ASCENDING), ("to_id", pymongo.ASCENDING)])
    to_db.msg_tmp.create_index([("session_id", pymongo.ASCENDING), (
        "ts", pymongo.ASCENDING)])
    insert_arr = []
    counter = 0
    last_write_time = None
    for doc in from_db["msg"].find():
        insert_arr.append(doc)
        counter += 1
        if len(insert_arr) >= 100:
            now = get_current_millsecond()
            last_write_time = last_write_time or now
            diff = now - last_write_time
            if diff < one_batch_ms:
              time.sleep((one_batch_ms - diff) / 1000.0)
            try:
                last_write_time = now
                to_db["msg_tmp"].insert_many(insert_arr)
            except:
                pass
            insert_arr = []
        if counter % 10000 == 0:
            print("time", datetime.now().strftime(
                "%Y/%m/%d, %H:%M:%S"), "counter", counter)
    if insert_arr:
        to_db["msg_tmp"].insert_many(insert_arr)
    print("finshed", counter)


if __name__ == '__main__':
    main()
