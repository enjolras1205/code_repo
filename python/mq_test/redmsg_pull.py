#!/usr/bin/env python
import pika
import json
import time
import sys
import uuid
import random


def main(config_file, send_num):
        send_num = int(send_num)
        print("config_file", config_file, "sleep 0.01s and send", send_num)
        cfg = __import__(config_file, fromlist=[config_file])
        connection = pika.BlockingConnection(pika.URLParameters(cfg.url))
        exchange = cfg.exchange
        channel = connection.channel()
        now = int(time.time() * 1000)
        data = {"createTime" : now, "gameId" : "10000071"}
        send_cnt = 0
        while True:
                accountId = random.randint(0, 10000000)
                data["requestId"] = str(uuid.uuid1())
                data["accountId"] = accountId
                channel.basic_publish(exchange=exchange, routing_key='redmsg_pull', body=json.dumps(data))
                if send_cnt >= send_num:
                        time.sleep(0.01)
                        send_cnt = 0
                else:
                        send_cnt = send_cnt + 1
                if cfg.send_once:
                        break


if __name__ == '__main__':
        main(sys.argv[1], sys.argv[2])
