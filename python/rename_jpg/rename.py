#!/usr/bin/python
# -*- coding:utf-8 -*-

import os, sys, exifread
from datetime import datetime
from dateutil.relativedelta import relativedelta

# class datetime.datetime(year, month, day[, hour[, minute[, second[, microsecond[, tzinfo]]]]])
date_time_base = datetime(2014, 8, 7, 19, 0, 0) 

# avoid same name
counter = 0

def get_new_name(date_time):
    global counter
    date_gap = relativedelta(date_time, date_time_base)
    #print(date_gap)
    counter = counter + 1
    return "{} {}岁{}个月{}天 {}.jpg".format(date_time, date_gap.years, 
        date_gap.months, date_gap.days, counter)

def do_with_file(path, file):
    f = open(file, 'rb')
    tags = exifread.process_file(f, details = False, stop_tag='DateTimeOriginal')
    if tags:
        date_time_jpg = datetime.strptime(str(tags['EXIF DateTimeOriginal']), "%Y:%m:%d %H:%M:%S")
        if date_time_jpg > date_time_base:
            os.rename(file, get_new_name(date_time_jpg))

def find_in_path(path):
    for root, dirs, files in os.walk(path, True):
        for name in files:
            do_with_file(root, os.path.join(root, name))

if __name__ == "__main__":
    find_path = "."
    if len(sys.argv) == 2:
        find_path = sys.argv[1]
    find_in_path(find_path)
