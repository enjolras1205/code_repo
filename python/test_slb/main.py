import socket
import sys
import gevent
import time
from gevent import monkey
monkey.patch_all()

# Create a TCP/IP socket


def do_connect():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    server_address = ('localhost', 11111)
    sock.settimeout(None)
    sock.connect(server_address)
    print("connected")
    time.sleep(999999)

jobs = [gevent.spawn(do_connect) for _ in range(1000)]

gevent.wait(jobs)