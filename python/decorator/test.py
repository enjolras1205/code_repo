def json_rpc(file_path):
    def validator(wrapped):
        def wrapper(params):
            return wrapped(**params)
        for attr in ('__module__', '__name__', '__doc__'):
            setattr(wrapper, attr, getattr(wrapped, attr))
        for attr in ('__dict__',):
            getattr(wrapper, attr).update(getattr(wrapped, attr, {}))
        return wrapper
    return validator

@json_rpc('t')
def hello():
    print(1)

def world():
    print(2)

params = dict(t=100)
hello(params)
#json_rpc("t")(world)()
