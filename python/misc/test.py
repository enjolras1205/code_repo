import random

def rand_at_list(l, default = 0):
	"""
	"""
	if not isinstance(l, list):
		return default
	l_len = len(l)
	if l_len <= 0:
		return default
	idx = random.randint(0, l_len)
	print idx
	return l[idx]

print(rand_at_list([10, 20, 30, 40]))
