class Class(object):
	@staticmethod
	def method1():
		print("1")
		return 1

	@staticmethod
	def method2():
		Class.method1()
		print("2")

	val = method1()

print Class.val
