class Ca(object):
	def __init__(self, val):
		self.val = val

class Cb(object):
	def __init__(self, val):
		self.val = val

type_map = {1 : Ca, 2 : Cb}

t = type_map[1](100)
print(t.val)
