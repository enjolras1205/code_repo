#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os.path
import requests
import time
import datetime
import json
import Levenshtein
import product_config


def get_similarity_degree(log1, log2):
    cmp_length = max(min(len(log1), len(log2)), 1)
    distance = Levenshtein.distance(log1, log2)
    similarity_degree = 1 - (float(distance) / cmp_length)
    return similarity_degree


def get_log_path():
    date_str = datetime.date.fromtimestamp(time.time() - 86400).strftime("%Y%m%d")
    log_path = "/srv/log/rsyslog/ejoy_errors/error.log.{0}".format(date_str)
    return log_path


def analyze_error_log():
    log_path = get_log_path()
    if not os.path.isfile(log_path):
        print("log_path_not_exist", log_path)
        return
    logs = []
    with open(log_path, "r") as f:
        for line in f:
            has_new_log = True
            for log in logs:
                contain = log['contain']
                similarity_degree = get_similarity_degree(line, contain)
                if similarity_degree > 0.9:
                    has_new_log = False
                    count = log['count']
                    log['count'] = count + 1
                    break
            if has_new_log:
                new_log = dict(contain=line, count=1)
                # print("new_log", new_log)
                logs.append(new_log)
    log_text = "product: {0}".format(product_config.product)
    for idx, v in enumerate(logs):
        contain = v['contain'].replace('#012', '\n')
        log_text = "{0}\nerror_idx: {1}\ncount: {2}\ncontain:\n{3}".format(
            log_text, idx, v['count'], contain)
    # 超过12K的消息不发送也无任何返回. 也不应该发送这么大的消息.
    # 如果真的错误太多, 被截断, 人力去分析日志
    log_text = log_text[:10000]
    notice_text('5cd8509b015b43931ea95f4ee15c60fd016bf42c450a145e5a1ade882c6da4dc', log_text)


def notice_text(token, message, at_mobiles=None):
    at_mobiles = at_mobiles if at_mobiles else []
    payload = dict(msgtype='text', text=dict(
        content=message, at=dict(atMobiles=at_mobiles, isAtAll=False)))
    headers = {'Content-Type': 'application/json'}
    url = 'https://oapi.dingtalk.com/robot/send?access_token={}'.format(token)
    for _ in range(3):
        try:
            r = requests.post(url, data=json.dumps(payload), headers=headers)
            if r.status_code != 200:
                raise Exception('request fail')
            return r.json()
        except Exception as e:
            print(e)
            time.sleep(1)
    return None


if __name__ == '__main__':
    analyze_error_log()
