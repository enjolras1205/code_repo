# rc4.py
import ctypes
import os

# Try to locate the .so file in the same directory as this file
_file = './librc4.so'
_path = os.path.join(*(os.path.split(__file__)[:-1] + (_file,)))
print("path", _path)
_mod = ctypes.cdll.LoadLibrary(_path)


class RC4Key(ctypes.Structure):
    _fields_ = [
        ('state', ctypes.c_uint32 * 256),
        ('x', ctypes.c_uint32),
        ('y', ctypes.c_uint32),
    ]

RC4_set_key = _mod.RC4_set_key
RC4_set_key.argtypes = (ctypes.POINTER(RC4Key), ctypes.c_uint32, ctypes.POINTER(ctypes.c_ubyte))
RC4_set_key.restype = None

RC4 = _mod.RC4
RC4.argtypes = (ctypes.POINTER(RC4Key), ctypes.c_uint32, ctypes.POINTER(ctypes.c_ubyte), ctypes.POINTER(ctypes.c_ubyte))
RC4.restype = None



