#!/usr/bin/env python
import pika
import json
import time
import uuid
import sys

connection = pika.BlockingConnection(pika.URLParameters('amqp://P10042:P100421989@192.168.43.186/P10042'))
channel = connection.channel()

data = {"platform_account": [{"game_meta": {"ext" : {"channelId" : "taobao"}},
 "pid": "0fad52ac9d9191be749ea05131f50127", "platform": "C1"}],
  "uid": "5ae28417bac7e679da687825"}

start = int(sys.argv[1])
end = int(sys.argv[2])

for i in range(start, end):
        # data['uid'] = str(uuid.uuid1())
        data['uid'] = str(i)
        # time.sleep(1)
        channel.basic_publish(exchange='P10043', routing_key='gangplank_POND', body=json.dumps(data))
