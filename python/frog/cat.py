#!/usr/bin/env python
import pika
import json
import time
import uuid
import sys

EXCHANGE = "P10049"


def func1(channel, uid_start, uid_end, routing_key):
    data = {}
    for i in range(uid_start, uid_end):
        data["user_id"] = str(i)
        ret = channel.basic_publish(exchange=EXCHANGE, routing_key=routing_key, body=json.dumps(data))

def func2(channel, uid_start, uid_end, routing_key):
    data = {}
    for i in range(uid_start, uid_end):
        data["user_id"] = str(i)
        channel.basic_publish(exchange=EXCHANGE, routing_key=routing_key, body=json.dumps(data))


def func4(channel, uid_start, uid_end, routing_key):
    data = {"ammount":100, "source"="2", type="xxx"}
    for i in range(uid_start, uid_end):
        data["user_id"] = str(i)
        channel.basic_publish(exchange=EXCHANGE, routing_key=routing_key, body=json.dumps(data))


def main(params):
    connection = pika.BlockingConnection(pika.URLParameters('amqp://P10049:P100491989@10.81.61.46/P10049'))
    channel = connection.channel()
    routing_key = params[0]
    uid_start = int(params[1])
    uid_end = int(params[2])
    forever = bool(int(params[3]))
    datas = {
        # adopt = create_user
        "cat_adopt": func1
        , "cat_getcatinfo": func2
        , "cat_saverelationship": func3
        , "energy_save": func4
    }
    func = datas[routing_key]
    while True:
        func(channel, uid_start, uid_end, routing_key+"_P10049")
        if not forever:
            break


if __name__ == '__main__':
    main(sys.argv[1:])
