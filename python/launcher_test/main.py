import sys
import re
import requests
import time
from ratelimit import limits
import json

pattern = (r''
           '"GET\s(.+)\s\w+/.+"\s'  # requested file
           '(\d+)\s'  # status
           '(\d+)\s'  # bandwidth
           '"(.+)"\s'  # referrer
           '"(.+)"'  # user agent
           )


def do_test():
    access_log = "realm_access.log"
    counter = 0
    time1 = time.time()
    with open(access_log, "r") as f:
        one_line = f.readline()
        while one_line:
            parse_result = re.findall(pattern, one_line)
            if parse_result:
                counter += 1
                resp1 = get_ann(
                    "https://p11072-launcher.ejoy.com/{0}".format(parse_result[0][0]))
                resp2 = get_ann(
                    "https://p11255-launcher.ejoy.com/{0}".format(parse_result[0][0]))
                data1 = json.loads(resp1.content)
                data2 = json.loads(resp2.content)
                if data1["code"] == 0:
                    if "hash" in data1 and "hash" in data2:
                        if data1["hash"] != data2["hash"]:
                            print("hash not equal", data1, data2)
                            exit(-1)
                    elif "anns" in data1 and "anns" in data2:
                        if data1["anns"] != data2["anns"]:
                            print("anns not equal", data1, data2)
                            exit(-1)
                    else:
                        print("data", data1, data2)
                else:
                    print("data1", data1)
                now = time.time()
                if now - time1 > 1.0:
                    time1 = now
                    print("counter", counter)
                    counter = 0
            one_line = f.readline()


# @limits(calls=2000, period=10)
def get_ann(url):
    print("url", url)
    resp = requests.get(url)
    return resp
    # if resp.status_code == 200:
    #     print("url", url)
    #     print("resp", resp.content)


if __name__ == "__main__":
    do_test()
