import sys
import re
import requests
import time
import argparse

pattern = (r''
           '"GET\s(.+)\s\w+/.+"\s'  # requested file
           '(\d+)\s'  # status
           '(\d+)\s'  # bandwidth
           '"(.+)"\s'  # referrer
           '"(.+)"'  # user agent
           )


def do_test(file_path):
    access_log = file_path
    with open(access_log, "r") as f:
        one_line = f.readline()
        while one_line:
            parse_result = re.findall(pattern, one_line)
            if parse_result:
                get_ann(
                    "https://p11072-launcher.ejoy.com/{0}".format(parse_result[0][0]))
            one_line = f.readline()


# @limits(calls=2000, period=10)
def get_ann(url):
    requests.get(url)
    # if resp.status_code == 200:
    #     print("url", url)
    #     print("resp", resp.content)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-f", "--file_path", required=True, dest="file_path",
                        help="file_path")

    args = parser.parse_args()
    do_test(**vars(args))
