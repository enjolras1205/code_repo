import sys
import requests
import urllib3
import time
import json

urllib3.response

base_url1 = "https://p10082-launcher.ejoy.com/ann"
base_url2 = "https://p10082-ex-launcher.ejoy.com/ann"
cmp_url = ["v2/login"]

def do_test():
    for v in cmp_url:
        req1 = "{0}/{1}?lang=CN&zone=CN&channel=998233".format(base_url1, v)
        req2 = "{0}/{1}?lang=CN&zone=CN&channel=998233".format(base_url2, v)
        resp1 = requests.get(req1)
        resp2 = requests.get(req2)
        resp1 = json.dumps(json.loads(resp1.content), sort_keys=True)
        resp2 = json.dumps(json.loads(resp2.content), sort_keys=True)
        if resp1 != resp2:
            print(req1, resp1)
            print(req2, resp2)
    req1 = "{0}/{1}?lang=CN&zone=CN".format(base_url1, "realm/ticket")
    req2 = "{0}/{1}?lang=CN&zone=CN".format(base_url2, "realm/ticket")
    resp1 = requests.get(req1)
    resp2 = requests.get(req2)
    hash1 = json.loads(resp1.content)["hash"]
    hash2 = json.loads(resp2.content)["hash"]
    req1 = "{0}/{1}?lang=CN&zone=CN".format(base_url1, "realm/detail/{0}".format(hash1))
    req2 = "{0}/{1}?lang=CN&zone=CN".format(base_url2, "realm/detail/{0}".format(hash2))
    resp1 = requests.get(req1)
    resp2 = requests.get(req2)
    resp1 = json.loads(resp1.content)
    resp2 = json.loads(resp2.content)
    resp1["anns"] = sorted(resp1["anns"])
    resp2["anns"] = sorted(resp2["anns"])
    resp1 = json.dumps(resp1, sort_keys=True)
    resp2 = json.dumps(resp2, sort_keys=True)
    if resp1 != resp2:
        print(1, req1, resp1)
        print(1, req2, resp2)


if __name__ == "__main__":
    do_test()
