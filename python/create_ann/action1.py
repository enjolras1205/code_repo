# -*- coding:utf-8 -*-
import requests
import urllib
import json
import time

class Test(object):
    data = {1: 1}

    @staticmethod
    def hello():
        Test.data[1] += 1
        return Test.data[1]

def hello():
    return Test.hello()

hello()
