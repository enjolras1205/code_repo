import json
import csv

ips = set()
tokens = set()
total_gap = 0
total_count = 0

with open('2.csv') as csvfile:
  spamreader = csv.reader(csvfile)
  for line in spamreader:
    content = line[7]
    a = content.split("event_data=")
    data = a[1]
    data = json.loads(data)
    # print("data", data["auth_data"])
    headers = data["headers"]
    ip = None
    now = data["now"]
    sign_time = None
    for v in headers:
      if v["header_key"] == "x-forwarded-for":
        ip = v["header_value"]
      elif v["header_key"] == "ag-sign-time":
        sign_time = int(v["header_value"])
    total_gap += abs(now - sign_time)
    total_count += 1
    ips.add(ip)
    tokens.add("ObjectId('{0}')".format(data["auth_data"]["token"].encode("ascii")))

print("ips", ips)
print("ips_length", len(ips))
print("total_gap", total_gap, "total_count", total_count)
print("avg_gap", total_gap // total_count)
print("tokens", tokens)
