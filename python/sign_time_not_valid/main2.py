import json

player_ids = set()
account_ids = set()

with open('out.json') as f:
  for line in f.readlines():
    line = json.loads(line)
    player_ids.add(line["moment_id"].encode("ascii"))
    account_ids.add(line["user"].encode("ascii"))

print("player_ids", player_ids)
print("account_ids", account_ids)
