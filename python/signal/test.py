import signal
# Define signal handler function
def myHandler(signum, frame):
    print("some", signum, frame)

# register signal.SIGALRM's handler 
signal.signal(signal.SIGUSR1, myHandler)

signal.pause()
