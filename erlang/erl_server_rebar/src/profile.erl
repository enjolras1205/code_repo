%%%-------------------------------------------------------------------
%%% @author enjolras
%%% @doc
%%%
%%% @end
%%% Created : 2014-07-24
%%%-------------------------------------------------------------------
-module(profile).
-author("enjolras").

%% API
-export([
    fprof_start/0
    ,fprof_start/1
    ,fprof_stop/0
]).

-spec fprof_start() -> void.
%% @doc fprof
fprof_start() ->
    fprof:trace([start, {file, "./log/fprof.trace"}]).

-spec fprof_start(Procs) -> void when
    Procs :: [Pid],
    Pid :: pid().
%% @doc fprof 指定进程
fprof_start(Procs) ->
    fprof:trace([start, {file, "./log/fprof.trace"}, {procs, Procs}]).

fprof_stop()->
    Now = util:unixtime(),

    ok = fprof:trace(stop),
    ok = fprof:profile({file, "./log/fprof.trace"}),
    Analyse = lists:concat(["./log/fprof-", Now(), ".analysis"]),
    % {sort, own}
    ok = fprof:analyse([{dest, Analyse}, {details, true}, {totals, true}, {sort, own}]),
    io:format(("fprof result:~s\n"), [Analyse]),
    ok.
