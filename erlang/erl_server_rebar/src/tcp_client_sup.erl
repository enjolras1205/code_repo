%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-06-13
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(tcp_client_sup).

-behaviour(supervisor).

%% API
-export([start_link/1, start_client/0]).

%% Supervisor callbacks
-export([init/1]).

%%%===================================================================
%%% API functions
%%%===================================================================

%% @doc a startup function for spawning new client connection handling FSM, be called by the TCP listener process.
start_client() ->
    supervisor:start_child(?MODULE, []).

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link([Module]) -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Module) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, [Module]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([Module]) ->
    {ok, {{simple_one_for_one, 5, 10}, [
		{undefined,
			{Module, start_link, []},
			temporary,
			2000,
			worker,
			[]
  		}
	]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
