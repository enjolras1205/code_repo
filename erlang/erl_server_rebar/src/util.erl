%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-06-09
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-include("log.hrl").

-module(util).

-export([stacktrace/0]).
-export([unixtime/0]).

-spec stacktrace() -> Stack when
    Stack :: term().
%% @doc 获得当前调用栈
stacktrace() ->
    try throw(a)
    catch throw:a ->
        erlang:get_stacktrace()
    end.

-spec unixtime() -> UnixTime when
    UnixTime :: integer().
%% @doc 获得unix时间戳
unixtime() ->
    {M, S, _} = erlang:now(),
    M * 1000000 + S.

