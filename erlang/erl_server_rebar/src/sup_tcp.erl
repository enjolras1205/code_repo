%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-06-09
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-include("log.hrl").

-module(sup_tcp).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(DEFAULT_PORT, 7500).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, [tcp_echo_fsm, ?DEFAULT_PORT]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([Module, Port]) ->
    ?DEBUG("sup_tcp init"),
    {ok, {{one_for_one, 5, 10}, [
		{tcp_listener, {tcp_listener, start_link, [Module, Port]}, permanent, 2000, worker, [tcp_listener]}
		,{tcp_client_sup, {tcp_client_sup, start_link, [Module]}, permanent, infinity, supervisor, []}
	]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
