-include("global.hrl").
-include("log.hrl").
-module(?APP_NAME).

-behaviour(application).

-export([start/0]).

%% Application callbacks
-export([start/2,
         prep_stop/1,
         stop/1]).

start() ->
    start(normal, []).

%%%===================================================================
%%% Application callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application is started using
%% application:start/[1,2], and should start the processes of the
%% application. If the application is structured according to the OTP
%% design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%
%% @spec start(StartType, StartArgs) -> {ok, Pid} |
%%                                      {ok, Pid, State} |
%%                                      {error, Reason}
%%      StartType = normal | {takeover, Node} | {failover, Node}
%%      StartArgs = term()
%% @end
%%--------------------------------------------------------------------
start(_StartType, _StartArgs) ->
    ?DEBUG2("start/2 ~w ~w ~n", [_StartType, _StartArgs]),
    fprof:apply(sup_top, start_link, []).
    %% sup_top:start_link().

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application has stopped. It
%% is intended to be the opposite of Module:start/2 and should do
%% any necessary cleaning up. The return value is ignored.
%%
%% @spec stop(State) -> void()
%% @end
%%--------------------------------------------------------------------
stop(_State) ->
    ?DEBUG2("stop ~p", [_State]),
    fprof:profile(),
    fprof:analyse({dest, "erl_server.analysis"}),
    ok.

prep_stop(State) ->
    ?DEBUG2("prep_stop ~p", [State]),
    State.


%%%===================================================================
%%% Internal functions
%%%===================================================================
