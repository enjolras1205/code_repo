%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-09-16
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(loop).

-export([
    loop/0
]).

loop() ->
    io:format("loop2 ~w~n", [calendar:now_to_local_time(erlang:now())]),
    timer:sleep(8000),
    ?MODULE:loop().

