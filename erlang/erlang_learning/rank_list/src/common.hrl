%%----------------------------------------------------
%% 基础数据结构（所有模块都会调用到
%% @author dogtapdsd26@gmail.com
%% 
%%----------------------------------------------------
-define(debugging, 1).  %% debugging | professional | release

-define(APPNAME, xgep).     %% 程序名
-ifdef(debug).
-define(DEBUG(Msg), logger:debug(Msg, [], ?MODULE, ?LINE)).
-define(DEBUG(F, A), logger:debug(F, A, ?MODULE, ?LINE)).
-else.
-define(DEBUG(Msg), ok).
-define(DEBUG(F, A), ok).
-endif.
-define(INFO(Msg), logger:info(Msg, [], ?MODULE, ?LINE)).
-define(INFO(F, A), logger:info(F, A, ?MODULE, ?LINE)).
-define(ERR(Msg), logger:error(Msg, [], ?MODULE, ?LINE)).
-define(ERR(F, A), logger:error(F, A, ?MODULE, ?LINE)).
-define(ELOG(F, A), logger:err_log(F, A, ?MODULE, ?LINE)).
-define(WARNING(Msg), logger:warning(Msg, [], ?MODULE, ?LINE)).
-define(WARNING(F, A), logger:warning(F, A, ?MODULE, ?LINE)).
-define(BIN(Msg), logger:to_bin(Msg, [], ?MODULE, ?LINE)).
-define(BIN(F, A), logger:to_bin(F, A, ?MODULE, ?LINE)).
-define(ECHO_MODULE_START, logger:info("~w 启动..", [?MODULE], ?MODULE, ?LINE)).
-define(ECHO_MODULE_START(Mod), logger:info("~w 启动..", [Mod], Mod, ?LINE)).
-define(ECHO_MODULE_ERR_STOP(Reason), logger:error("(!.!) ~w 关闭 error:~w", [?MODULE, Reason], ?MODULE, ?LINE)).
-define(true, 1).
-define(false, 0).

%% 时间相关
-define(week_s, 604800).
-define(week_day, 7).
-define(day_s,  86400).
-define(hour_s, 3600).
-define(half_hour_s, 1800).
-define(minu_s, 60).
-define(week_s(N), (?week_s * (N))).
-define(day_s(N),  (?day_s * (N))).
-define(hour_s(N), (?hour_s * (N))).
-define(minu_s(N), (?minu_s * (N))).

-define(week_ms, 604800000).
-define(day_ms,  86400000).
-define(hour_ms, 3600000).
-define(half_hour_ms, 1800000).
-define(minu_ms, 60000).
-define(sec_ms,  1000).
-define(week_ms(N), (?week_ms * (N))).
-define(day_ms(N),  (?day_ms * (N))).
-define(hour_ms(N), (?hour_ms * (N))).
-define(minu_ms(N), (?minu_ms * (N))).
-define(sec_ms(N),  (?sec_ms * (N))).
-define(ms_sec(N),  (N div ?sec_ms )).


%% 获取字节数值
-define(k_b(N), (1024 * (N))).
-define(m_b(N), (1048576 * (N))).
-define(g_b(N), (1073741824 * (N))).
-define(t_b(N), (1099511627776 * (N))).

-define(uint32, 4294967295).
-define(uint16, 65535).
-define(uint8,  255).

%% 类型转换
-define(i2l(I), integer_to_list(I)).
-define(a2l(A), atom_to_list(A)).
-define(b2l(L), binary_to_list(L)).
-define(l2b(L), list_to_binary(L)).
-define(l2i(L), list_to_integer(L)).
-define(l2t(L), list_to_tuple(L)).
-define(l2a(L), list_to_atom(L)).
-define(t2l(T), tuple_to_list(T)).


-define(i2b(L), ?l2b(?i2l(L))).
-define(b2i(L), ?l2i(?b2l(L))).

%% gen_server:call和gen_server:cast缩写
-define(lgs_call(Request), gen_server:call(?MODULE, Request)).
-define(lgs_cast(Request), gen_server:cast(?MODULE, Request)).

%%-define(ggs_call(Request), gen_server:call({global, ?MODULE}, Request)).
%%-define(ggs_cast(Request), gen_server:cast({global, ?MODULE}, Request)).
-define(ggs_send(Request), global:send(?MODULE, Request)).
%% Dest = pid() | port() | RegName | {RegName, Node}
%% Msg = term()
%% RegName = atom()
%% Node = node()
-define(e_send(Dest, Msg), erlang:send(Dest, Msg)).
%% Request = term()
-define(gs_call(Dest, Request), gen_server:call(Dest, Request)).
-define(gs_cast(Dest, Request), gen_server:cast(Dest, Request)).

%% 游戏主节点node 'mhxxplus_demo_main@127.0.0.1'
-define(main_node, zone_lib:main_node()).
%% 游戏跨线玩法点node 'mhxxplus_demo_cross@127.0.0.1'
-define(cross_node, zone_lib:cross_node()).


