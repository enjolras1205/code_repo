%% ---------------------------------------
%% list工具函数
%% @author cayleung@gmail.com
%% @end
%% ---------------------------------------
-module(list_util).
-export([
        keysdelete/2, delete/2
        ,keysort/2, keyresort/2, mkeysort/2, mkeysort2/2, mkeyresort/2, sort/1, resort/1
        ,index/2, index/3 ,keyindex/3, keyindex/4, implode/2
        ,rand/1, rand_lave/1, rand_more/2, keyrand/2, keyrand_more/3, rand_upset/1
        ,find/3 ,keyfind/2, keyfind/3, keyfind/4
        ,ukeymerge/3
        ,keyfind_all/3, keydelete_all/3
        ,keysfind_all/3,keysdelete_all/3
        ,keysum/2, keyfind_all_and_sum/4, mkeyfind_all_and_sum/4
        ,key2list/2, key2list/3
        ,keyupdate/4, keyadd/4
        ,split/2
        ,diff_split/3
        ,diff/2        %% 差集计算
        ,nth/2
        ,nthdelete/2, nthupdate/3
        ,verify_uniq/2,find_index/2
        ,min/1, keymax/2
        ,sublist/3
        ,ustore/2
        ,group_by/2
        ,append/2, add_uniq/2
		,filtermap/2
        ,replace/3
        ,combine_kvlist/1
    ]
).
-include("common.hrl").


%% @spec min(List) -> Value
%% List = list()
%% @doc 空列表不报错的lists:min
min([]) -> [];
min(List) -> lists:min(List).


%% @spec delete(DelList, List) -> List2
%% DelList = List = List2 = list()
%% @doc 删除多个元素
delete(_DelList, []) -> [];
delete([Elem|R], List) ->
    delete(R, lists:delete(Elem, List));
delete([], List) -> List.

%% @spec keysdelete(TList, KList) -> TList
%% TList = [tuple()]
%% KList = [int()]
%% @doc 根据多个keys值删除tuplelist
keysdelete(TList, KList) -> 
    keysdelete_f1(TList, KList).

keysdelete_f1(TList, []) -> TList;
keysdelete_f1(TList, [K|T]) ->
    keysdelete_f1(lists:keydelete(K, 1, TList), T).


%% @spec keymax(TList, Key) -> tuple()
%% TList = [tuple()]
%% KList = [int()]
%% @doc 根据多个keys值删除tuplelist
keymax([H|T], Key) -> 
    keymax(T, element(Key, H), H, Key).

keymax([H|T], Max, Tpl, Key) ->
    Elem = element(Key, H),
    if Elem > Max -> keymax(T, Elem, H, Key);
    true -> keymax(T, Max, Tpl, Key)
    end;
keymax([], _Max, Tpl, _Key) -> Tpl.


%% @spec verify_uniq(TPs, KeyPos)-> Tps2
%% KeyPos = int()
%% TPs = [ tuple() ]
%% @doc 校验tuple list 唯一性( TODO:会过滤掉重复主键的tuple主要用于过滤重复的物品
verify_uniq(TPs, KeyPos) ->
    verify_uniq(TPs, KeyPos, []).

verify_uniq([], _KeyPos, RtnL)->
    RtnL;
verify_uniq([Tp | T], KeyPos, RtnL) ->
    OneElement = erlang:element(KeyPos, Tp),
    case lists:keyfind(OneElement, KeyPos, RtnL) of
        false ->
            verify_uniq(T, KeyPos, [Tp|RtnL]);
        _Finded ->
            verify_uniq(T, KeyPos, RtnL)
    end.

%% @spec split(N, List) -> {List1, List2}
%% N = int()
%% List = List1 = List2 = list()
%% @doc N比List长的时候返回 {List, []} 的lists:split()
split(N, List) when is_integer(N), N >= 0, is_list(List) ->
    split(N, List, []);
split(N, List) ->
    erlang:error(badarg, [N, List]).

split(0, L, R) ->
    {lists:reverse(R, []), L};
split(N, [H|T], R) ->
    split(N-1, T, [H|R]);
split(_N, [], R) ->
    split(0, [], R).

%% @spec keysort(Key, L) -> L2
%% @doc 其实就是lists:keysort/2
keysort(Key, L) ->
    lists:keysort(Key, L).

%% @spec keyresort(Key, L) -> L2
%% @doc lists:keysort/2的反排序
keyresort(Key, L) ->
    lists:reverse(lists:keysort(Key, L)).

%% @spec mkeysort(KeyPoss::[int()], List::[tuple()]) -> [tuple()]
%% @doc 多键值排序
%% 
%% <pre> list_util:mkeysort([2,1], [{1,2,3}, {2, 1, 3}, {2, 2, 2}]).
%% [{2, 1, 3}, {1, 2, 3}, {2, 2, 2}]</pre>
mkeysort(_Keys, []) -> [];
mkeysort(_Keys, [F]) -> [F];
mkeysort([], TupleList) when is_list(TupleList) -> TupleList;
mkeysort([Key], TupleList) when is_list(TupleList) -> lists:keysort(Key, TupleList);
mkeysort([Key|Keys], TupleList) when is_list(TupleList) ->
    [F|T] = lists:keysort(Key, TupleList),
    mkeysort_fun(T, Key, Keys, element(Key, F), [F], []). %% 分组
mkeysort_fun([], _Key, Keys, _Val, Buff, Res) -> 
    NewBuff = mkeysort(Keys, Buff),
    Res ++ NewBuff;
mkeysort_fun([F|T], Key, Keys, Val, Buff, Res) ->
    case element(Key, F) of
    Val -> mkeysort_fun(T, Key, Keys, Val, [F|Buff], Res);  %% 同一组的放进buff
    Val2 -> 
        NewBuff = mkeysort(Keys, Buff),
        mkeysort_fun(T, Key, Keys, Val2, [F], Res ++ NewBuff)
    end.

%% @spec mkeysort(KeyPoss::[int()], List::[tuple()]) -> [tuple()]
%% @doc 支持正反逻辑多键值排序
%% 
%% <pre> list_util:mkeysort2([{s,2},{r,1}], [{1,2,3}, {2, 1, 3}, {2, 2, 2}]).
%% [{2, 1, 3}, {2, 2, 2}, {1, 2, 3}]</pre>
mkeysort2(_Keys, []) -> [];
mkeysort2(_Keys, [F]) -> [F];
mkeysort2([], TupleList) when is_list(TupleList) -> TupleList;
mkeysort2([Key], TupleList) when is_list(TupleList) -> mkeysort2_fun3(Key, TupleList);
mkeysort2([Key|Keys], TupleList) when is_list(TupleList) ->
    {_, Key2} = Key,
    [F|T] = mkeysort2_fun3(Key, TupleList),
    mkeysort_fun2(T, Key, Keys, element(Key2, F), [F], []). %% 分组
mkeysort_fun2([], _Key, Keys, _Val, Buff, Res) -> 
    NewBuff = mkeysort2(Keys, Buff),
    Res ++ NewBuff;
mkeysort_fun2([F|T], Key, Keys, Val, Buff, Res) ->
    {_, Key2} = Key,
    case element(Key2, F) of
    Val -> mkeysort_fun2(T, Key, Keys, Val, [F|Buff], Res);  %% 同一组的放进buff
    Val2 -> 
        NewBuff = mkeysort2(Keys, Buff),
        mkeysort_fun2(T, Key, Keys, Val2, [F], Res ++ NewBuff)
    end.

%% 分正反排序
mkeysort2_fun3({s,Key}, Tuplelist) ->
    lists:keysort(Key, Tuplelist);
mkeysort2_fun3({r,Key}, Tuplelist) ->
    lists:reverse(lists:keysort(Key, Tuplelist)).

%% @spec mkeyresort(KeyPoss::[int()], List::[tuple()]) -> [tuple()]
%% @doc 多键值反排序
mkeyresort(Keys, L) -> 
    lists:reverse(mkeysort(Keys, L)).

%% @spec sort(L) -> L2
%% @doc 由小到大排序
sort(L) -> lists:sort(L).

%% @spec resort(L) -> L2
%% @doc 由大到小排序 
resort(L) -> lists:reverse(lists:sort(L)).

%% @spec index(Elem::term(), List::list()) -> int()
%% @doc 获取元素在数组中索引
index(Elem, List) -> index_loop(Elem, List, 1, 0).
%% @spec index(Elem::term(), List::list(), Def::int()) -> int()
%% @doc 获取元素在数组中索引
index(Elem, List, Def) -> index_loop(Elem, List, 1, Def).
index_loop(_Elem, [], _I, Def) -> Def;
index_loop(Elem, [E|T], I, Def) ->
    case Elem =:= E of
        true -> I;
        false -> index_loop(Elem, T, I+1, Def)
    end.

%% @spec keyindex(Key, N, List) -> int()
%% @doc 根据key值获取元素在数组中索引，如果没有返回0
keyindex(Key, N, List) -> keyindex(Key, N, List, 0).

%% @spec keyindex(Key, N, List, Def) -> int() | Def
%% @doc 根据key值获取元素在数组中索引
keyindex(Key, N, List, Def) ->
    keyindex_f1(Key, N, List, 1, Def).
keyindex_f1(_Key, _N, [], _I, Def) -> Def;
keyindex_f1(Key, N, [E|T], I, Def) -> 
    case element(N, E) of
    Key -> I;
    _ -> keyindex_f1(Key, N, T, I+1, Def)
    end.

%% @spec implode(S::term(), L::list()) -> list()
%% @doc 插入元素到列
implode(S, L) ->
    implode_fun(L, S, []).
implode_fun([], _S, _Res) -> <<>>;
implode_fun([Key], _S, Res) ->
    lists:reverse([Key|Res]);
implode_fun([Key|T], S, Res) ->
    implode_fun(T, S, [S, Key|Res]).


%% @spec rand(L::list()) -> undefined | term()
%% @doc 从一个list中随机取出一项
rand([]) -> undefined;
rand([I]) -> I;
rand(List) -> 
    Idx = util:rand(1, length(List)),
    lists:nth(Idx, List).

%% @spec rand_lave(List) -> {Elem, LaveList} 
%% Elem = undefined | term()
%% LaveList = list()
%% @doc 从list中随机取出一项，返回选中的目标和余下的列表
rand_lave([]) -> {undefined, []};
rand_lave([I]) -> {I, []};
rand_lave(List) ->
    Idx = util:rand(1, length(List)),
    Elem = lists:nth(Idx, List),
    {Elem, lists:delete(Elem, List)}.

%% @spec rand_more(Tuplelist, Num) -> [undefined | tuple()]
%% TupleList = [tuple()]
%% Num = integer()
%% @doc 列表随机取出Num个不重复的数据
rand_more(List, Num) ->
    rand_more_f1(List, erlang:max(0, Num), []).
rand_more_f1(_, 0, Result) -> Result;
rand_more_f1(List, Num, Result) ->
    Tuple = rand(List),
    rand_more_f1(lists:delete(Tuple, List), Num-1, [Tuple|Result]).

%% @spec keyrand(TupleList, N) -> undefined | tuple()
%% TupleList = [tuple()]
%% @doc 从一个list中根据N中数值概率取出一项
keyrand([], _N) -> undefined;
keyrand(List, N) ->
    Sum = keyrand_f1(List, N, 0),
    Rand = util:rand(1, Sum),
    keyrand_f2(List, N, Rand, 0).

keyrand_f1([], _N, Sum) -> Sum;
keyrand_f1([Tuple|T], N, Sum) -> 
    keyrand_f1(T, N, Sum + element(N, Tuple)).
keyrand_f2([], _N, _Rand, _Sum) -> undefined;
keyrand_f2([Tuple|T], N, Rand, Sum) -> 
    Rate = element(N, Tuple),
    case Rate + Sum >= Rand of
    true -> Tuple;
    false -> keyrand_f2(T, N, Rand, Sum + Rate)
    end.

%%@spec keyrand_more(Tuplelist, Nth, Num) -> [undefined | tuple()]
%% TupleList = [tuple()]
%% Nth = integer()
%% Num = integer()
%%@doc 列表以第Nth个元素数值，随机取出Num个不重复的数据
keyrand_more(List, Nth, Num) ->
    rand_more_f1(List, Nth, erlang:max(0, Num), []).
rand_more_f1(_, _, 0, Result) -> Result;
rand_more_f1(List, Nth, Num, Result) ->
    Tuple = keyrand(List, Nth),
    rand_more_f1(lists:delete(Tuple, List), Nth, Num-1, [Tuple|Result]).

%% @spec rand_upset(List) -> RetList
%% List, RetList = [term()]
%% @doc 将一个输入的列表随机打乱
rand_upset(List) ->
  rand_upset(List, []).
rand_upset([], RetList) ->
  RetList;
rand_upset(List,RetList) ->
  {Elem, RemList} = rand_lave(List),
  rand_upset(RemList, [Elem | RetList]).

%% @spec find(Vals, List, Def) -> Def | Val
%% Vals = [term()]
%% List = [term()]
%% Def = term()
%% Val = term()
%% @doc 从List中查找Vals，如果有返回第一Val，如果没有返回Def，一般用在检查配置参数
find([], _List, Def) -> Def;
find([Val|T], List, Def) ->
    case lists:member(Val, List) of
    true -> Val;
    false -> find(T, List, Def)
    end.

%% @spec keyfind(Key, TupleList) -> undefined | term()
%% Key = atom()
%% TupleList = [{atom(), term()}]
%% @doc 返回Val值，其实就是 proplists:get_value 等同于 keyfind(Key, TupleList, undefined)
keyfind(Key, TupleList) ->
    keyfind(Key, TupleList, undefined).

%% @spec keyfind(Key, TupleList, Def) -> Def | term()
%% Key = atom()
%% TupleList = [{atom(), term()}]
%% @doc lists:keyfind返回Val值，其实就是 proplists:get_value
keyfind(Key, TupleList, Def) ->
    case lists:keyfind(Key, 1, TupleList) of
    false -> Def;
    {_, Val} -> Val
    end.

%% @spec keyfind(Key, Pos, TupleList, Def) -> Def | term()
%% Key = atom()
%% Pos = int()
%% TupleList = [{atom(), term()}]
%% @doc lists:keyfind返回Val值，其实就是 proplists:get_value
keyfind(Key, Pos, TupleList, Def) ->
    case lists:keyfind(Key, Pos, TupleList) of
    false -> Def;
    Val -> Val
    end.

%% @spec ukeymerge(N, TupleList1, TupleList2) -> TupleList3
%% N = int()
%% TupleList1 = [Tuple]
%% TupleList2 = [Tuple]
%% TupleList3 = [Tuple]
%% Tuple = tuple()
%% @doc lists_keymerge/3的无重复版本，TupleList2将替代TupleList1里拥有的
ukeymerge(N, TupleList1, TupleList2) ->
    ukeymerge_f1(N, lists:ukeysort(N, TupleList1), lists:ukeysort(N, TupleList2)).

ukeymerge_f1(N, [H1|T1], [H2|_]=L2) when element(N, H1) < element(N, H2) ->
    [H1|ukeymerge_f1(N, T1, L2)];
ukeymerge_f1(N, [H1|_]=L1, [H2|T2]) when element(N, H1) > element(N, H2) ->
    [H2|ukeymerge_f1(N, L1, T2)];
ukeymerge_f1(N, [_H1|T1], [H2|T2]) -> %% ==
    [H2|ukeymerge_f1(N, T1, T2)];
ukeymerge_f1(_N, [], L2) -> L2;
ukeymerge_f1(_N, L1, []) -> L1.

%% @spec keyfind_all(Key, N, TupleList) -> NewTupleList
%% Key = term()
%% N = int()
%% TupleList = [Tuple]
%% NewTupleList = [Tuple]
%% @doc 找出所有键值相同的tuple
keyfind_all(Key, N, TupleList) ->
    [T||T <- TupleList, element(N, T) == Key].

%% @spec keysfind_all(Keys, N, TupleList) -> NewTupleList
%% Key = term()
%% Keys = [Key]
%% N = int()
%% TupleList = [Tuple]
%% NewTupleList = [Tuple]
%% @doc 找出所有的多个主键
keysfind_all(Keys, N, TupleList) ->
    [T||T <- TupleList, lists:member(element(N, T), Keys)].

%% @spec keysdelete_all(Keys, N, TupleList) -> NewTupleList
%% Key = term()
%% Keys = [Key]
%% N = int()
%% TupleList = [Tuple]
%% NewTupleList = [Tuple]
%% @doc 删除所有多个主键键值相同的tuple
keysdelete_all(Keys, N, TupleList) ->
    [T||T <- TupleList, not lists:member(element(N, T), Keys)].


%% @spec keydelete_all(Key, N, TupleList) -> NewTupleList
%% Key = term()
%% N = int()
%% TupleList = [Tuple]
%% NewTupleList = [Tuple]
%% @doc 删除所有键值相同的tuple
keydelete_all(Key, N, TupleList) ->
    [T||T <- TupleList, element(N, T) /= Key].

%% @spec keyfind_all_and_sum(Key, N, SumPos, TupleList) -> integer()
%% Key = term()
%% N = SumPos = int()
%% TupleList = [Tuple]
%% @doc 找出所有键值相同的tuple, 并计算SumPos位置的和
keyfind_all_and_sum(Key, N, SumPos, TupleList) ->
    keyfind_all_and_sum(Key, N, SumPos, TupleList, 0).
keyfind_all_and_sum(_Key, _N, _SumPos, [], Sum) -> Sum;
keyfind_all_and_sum(Key, N, SumPos, [Tuple|T], Sum) -> 
    Sum2 = case element(N, Tuple) of
    Key -> element(SumPos, Tuple) + Sum;
    _ -> Sum
    end,
    keyfind_all_and_sum(Key, N, SumPos, T, Sum2).

%% @spec mkeyfind_all_and_sum(KeyList, N, SumPos, TupleList) -> integer()
%% KeyList = [term()]
%% N = SumPos = int()
%% TupleList = [Tuple]
%% @doc 找出所有键值相同的tuple, 并计算SumPos位置的和
mkeyfind_all_and_sum(KeyList, N, SumPos, TupleList) ->
    mkeyfind_all_and_sum(KeyList, N, SumPos, TupleList, 0).
mkeyfind_all_and_sum(_KeyList, _N, _SumPos, [], Sum) -> Sum;
mkeyfind_all_and_sum(KeyList, N, SumPos, [Tuple|T], Sum) -> 
    Sum2 = case lists:member(element(N, Tuple), KeyList) of
    true -> element(SumPos, Tuple) + Sum;
    _ -> Sum
    end,
    mkeyfind_all_and_sum(KeyList, N, SumPos, T, Sum2).

%% @spec keysum(N, TupleList) -> integer()
%% N = int()
%% TupleList = [Tuple]
%% NewTupleList = [Tuple]
%% @doc tuple_list()的某值求和，lists:sum的tuple_list版
keysum(N, TupleList) ->
    keysum_f1(N, TupleList, 0).
keysum_f1(_N, [], Sum) -> Sum;
keysum_f1(N, [Tuple|T], Sum) -> 
    keysum_f1(N, T, element(N, Tuple) + Sum).

%% @spec key2list(N, TupleList) -> [term()]
%% N = int()
%% TupleList = [Tuple]
%% @doc 取出tuple_list()的某字段，返回list
key2list(N, TupleList) ->
    [element(N, T) || T <- TupleList].

%% @spec key2list(N, TupleList, ResList) -> ResList2
%% N = int()
%% TupleList = [Tuple]
%% ResList = [term()]
%% ResList2
%% @doc 取出tuple_list()的某字段，返回list
key2list(_N, [], ResList) -> ResList;
key2list(N, [H|T], ResList) ->
    key2list(N, T, [element(N, H)|ResList]).

%% @spec keyupdate(Key, N, TupleList, IVList) -> TupleList2
%% Key = term()
%% N = int()
%% TupleList = [Tuple]
%% @doc 根据key更新tuple_list()里的元素的相关字段，返回list
keyupdate(Key, N, TupleList, IVList) ->
    case lists:keyfind(Key, N, TupleList) of
    false -> TupleList;
    T -> lists:keyreplace(Key, N, TupleList, lists:foldl(fun({I, V}, X) -> setelement(I, X, V) end, T, IVList))
    end.

%% @spec keyadd(Key, N, TupleList, IVList) -> TupleList2
%% Key = term()
%% N = int()
%% TupleList = [Tuple]
%% @doc 根据key更新tuple_list()里的元素的相关字段，返回list，没有检查是否能相加

keyadd(K, N, L, IVList) when is_integer(N), N > 0 ->
    keyadd3(K, N, L, IVList).

keyadd3(Key, Pos, [Tup|Tail], IVList) ->
    K = element(Pos, Tup),
    if K == Key ->
        New = lists:foldl(fun({I, V}, X) ->
            Elem = element(I, X),
            setelement(I, X, Elem + V)
        end, Tup, IVList),
        [New|Tail];
    true ->
        [Tup|keyadd3(Key, Pos, Tail, IVList)]
    end;
keyadd3(_, _, [], _) -> [].

%% @spec ustore(Elem, List) -> TupleList2
%% @doc 添加元素，不重复的elem
ustore(E, L) ->
    case lists:member(E, L) of
    true -> L;
    false -> [E|L]
    end.

%% @spec @(L1, L2) -> L3
%% L1 = [term()]
%% L2 = [term()]
%% L3 = [term()]
%% @doc 差集计算（L1存在并且L2不存在的元素
diff(L1, L2) ->
    diff_f1(L1, L2, []).

diff_f1([], _L2, Res) -> Res;
diff_f1([H|T], L2, Res) ->
    case lists:member(H, L2) of
    true -> diff_f1(T, L2, Res);
    false -> diff_f1(T, L2, [H|Res])
    end.

%% @spec nth(L, N) -> Elem | undefined
%% L = [term()]
%% N = int()
%% Elem = term()
%% @doc 获取第X位值
nth([], _N) -> undefined;
nth(_, N) when N < 1 -> undefined;
nth([H|_T], 1) -> H;
nth([_|T], N) -> nth(T, N-1).

%% @spec nthupdate(L, N, Val) -> L2
%% L = [term()]
%% N = int()
%% Val = term()
%% L2 = [term()]
%% @doc 按第X位更新
nthupdate(L, N, Val) ->
    tuple_to_list(setelement(N, list_to_tuple(L), Val)).

%% @spec nthdelete(L, M) -> L2
%% L = [term()]
%% N = int()
%% L2 = [term()]
%% @doc 按第X位删除
nthdelete(L, N) ->
    nthdelete_f1(L, N, 1, []).

nthdelete_f1([], _N, _I, Res) -> lists:reverse(Res);
nthdelete_f1([_H|T], N, N, Res) -> 
    nthdelete_f1(T, N, N+1, Res);
nthdelete_f1([H|T], N, I, Res) -> 
    nthdelete_f1(T, N, I+1, [H|Res]).

%% @spec diff_split(KeyPosFind, TL1, TL2) -> {UpNewL, SameL, NoneEl}
%% KeyPosFind = int() 对比的tuple 元素位置
%% TL1 = TL2 = [tuple(),...]
%% UpNewL = [tuple(),...] 变化+TL2新增列表
%% SameL  = [tuple(),...] T1, T2 中 tuple完全一致的列表 
%% NoneEl = [tuple(),...] TL1存在，TL2中不存在的列表
%% @doc 按照tuple的指定键值，对比TL1, TL2 两个 tuple list , 拆分成三个列表
diff_split(KeyPosFind, TL1, TL2) ->
    diff_split(KeyPosFind, TL1, TL2, [], [], []).
diff_split(_KeyPosFind, [], T2Rest, UpNewL, SameL, NoneEL)->
    {T2Rest++UpNewL, SameL, NoneEL};
diff_split(KeyPosFind, [HeadT|T], TL2, UpNewL, SameL, NoneEL)->
    ElemVal = element(KeyPosFind, HeadT),
    case lists:keyfind(ElemVal, KeyPosFind, TL2) of
        false -> 
            diff_split(KeyPosFind, T, TL2, UpNewL, SameL, [HeadT|NoneEL]);
        HeadT ->
            diff_split(KeyPosFind, T, lists:delete(HeadT, TL2), UpNewL, [HeadT|SameL], NoneEL);
        ElemtTL2 ->
            diff_split(KeyPosFind, T, lists:delete(ElemtTL2, TL2), [ElemtTL2|UpNewL], SameL, NoneEL)
    end.

find_index(_Val, []) ->
    0;

find_index(Val, [E|T])->
    case Val >= E of
        true -> find_index(Val, T);
        false -> E
    end.

%% 加兼容开始位置大于list长度的情况
sublist(List, S, E) -> 
    case S > length(List) of
    true -> [];
    _ -> lists:sublist(List, S, E)
    end.

%% @spec group_by(Pos, TupleList) -> [Val]
%% Pos = uint()
%% TupleList = [tuple()]
%% Val = term()
%% @doc 根据键值聚合，返回该键值的聚合值
group_by(Pos, TupleList) ->
    group_by(Pos, TupleList, []).

group_by(_Pos, [], Res) -> Res;
group_by(Pos, [E|T], Res) -> 
    V = element(Pos, E),
    case lists:member(V, Res) of
    true -> group_by(Pos, T, Res);
    false -> group_by(Pos, T, [V|Res])
    end.

%% @spec append(List1, List2) -> List3
%% List1, List2, List3 = [term()]
%% @doc 连接两个列表, 只合并一个维度
append([], List1) ->
  List1;
append([H | Rem], List1) ->
  append(Rem, [H | List1]).


%% @spec add_uniq(Elem, L) -> L2
%%
%% @doc 查找lists里面相等的元素，不存在的放置列表开头，存在的不修改
add_uniq(Elem, L) ->
    case lists:member(Elem, L) of
    true  -> L;
    false -> [Elem|L]
    end.

%% @doc filter + map 拷贝至lists.erl 目前所用的erlang版本没这个函数
-spec filtermap(Fun, List1) -> List2 when
      Fun :: fun((Elem) -> boolean() | {'true', Value}),
      List1 :: [Elem],
      List2 :: [Elem | Value],
      Elem :: term(),
      Value :: term().
filtermap(F, [Hd|Tail]) ->
    case F(Hd) of
	true ->
	    [Hd|filtermap(F, Tail)];
	{true,Val} ->
	    [Val|filtermap(F, Tail)];
	false ->
	    filtermap(F, Tail)
    end;
filtermap(F, []) when is_function(F, 1) -> [].



%% @spec replace(ItemFind, ObjReplace, List) -> NewList;
%%
%% @doc 查找第一个符合条件的元素，做替换, 规则类似 lists:delete/2
replace(ItemFind, ObjReplace, [ItemFind|Rest]) -> [ObjReplace|Rest];
replace(Item, ObjReplace, [H|Rest]) -> 
    [H|replace(Item, ObjReplace, Rest)];
replace(_, _, []) -> [].


%% ---------------------------------------------------------------------------
%% @doc 合并{Key, Val}列表 (Key相同的项 Val相加)
%% ---------------------------------------------------------------------------
-spec combine_kvlist([{Key, Val}]) -> [{Key, NewVal}] when
    Key     :: term(),
    Val     :: integer(),
    NewVal  :: integer().

combine_kvlist([]) -> [];
combine_kvlist(List) ->
    F = fun({Key, Value}, Result) ->
        case lists:keyfind(Key, 1, Result) of
            false -> [{Key, Value} | Result];
            {_, Value1} ->
                lists:keyreplace(Key, 1, Result, {Key, Value + Value1})
        end
    end,
    lists:foldl(F, [], List).



