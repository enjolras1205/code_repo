%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-06-09
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------


-module(rank).

-export([
	calc_test/1
]).

-record(ranker, {
        id               		%% 唯一识别ID
		,sort_key				%% 用此值来排序
        ,update_time = 0  		%% 更新日期
    }
).

-include_lib("eunit/include/eunit.hrl").

calc_test(ChangesNum) when is_integer(ChangesNum) andalso ChangesNum > 0 ->
	random:seed(erlang:now()),
	Rankers = [#ranker{id = ID, sort_key = random:uniform(10000) } || ID <- lists:seq(1, 100)],
	Rankers1 = lists:sort(fun(A, B) -> sort_func(A, B) end, Rankers),	%% 一般情况下是有序列表
	ChangeRankers = [#ranker{id = random:uniform(200), sort_key = random:uniform(10000)} || _ <- lists:seq(1, ChangesNum)],
	F1 = fun(_) ->
        do_calc_rank(Rankers1, ChangeRankers)
    end,
	F2 = fun(_) ->
        do_calc_rank1(Rankers1, ChangeRankers)
    end,
    test_util:time_compare(10000, [
        {"rank1", F1}
		,{"rank2", F2}
    ]),
	Res1 = do_calc_rank(Rankers1, ChangeRankers),
	Res2 = do_calc_rank1(Rankers1, ChangeRankers),
	%% io:format("res1 ~w~n", [Res1]),
	%% io:format("res2 ~w~n", [Res2]),
	Res1 = Res2,
	ok;
calc_test(_) ->
	ignore.

do_calc_rank(Rankers, ChangeRankers) ->
	Rankers1 = replace_ranker(Rankers, ChangeRankers),
	Rankers2 = lists:sort(fun(A, B) -> sort_func(A, B) end, Rankers1),
	lists:sublist(Rankers2, 100).

sort_func(A, B) ->
	if
	A#ranker.sort_key > B#ranker.sort_key ->
		true;
	A#ranker.sort_key < B#ranker.sort_key ->
		false;
	A#ranker.update_time < B#ranker.update_time ->
		true;
	A#ranker.update_time > B#ranker.update_time ->
		false;
	true ->
		A#ranker.id > B#ranker.id
	end.
%% 将改变过的玩家放在原列表队尾 保持原列表基本有序
replace_ranker(Rankers, []) -> Rankers;
replace_ranker(Rankers, [H|T]) ->
	Rankers1 = lists:keystore(H#ranker.id, #ranker.id, Rankers, H),
	replace_ranker(Rankers1, T).

do_calc_rank1(Rankers, ChangeRankers) ->
	Rankers1 = do_calc_rank2(Rankers, ChangeRankers),
	lists:sublist(Rankers1, 100).
do_calc_rank2(Rankers, []) -> Rankers;
do_calc_rank2(Rankers, [H|T]) ->
	Rankers1 = lists:keydelete(H#ranker.id, #ranker.id, Rankers), %% 删除再插入
	Rankers2 = do_calc_rank3(H, Rankers1),						  %% 插入排序
	do_calc_rank2(Rankers2, T).

do_calc_rank3(Chgs, [H|T]) ->  
	case sort_func(Chgs, H) of
		true ->
			[Chgs, H|T];
		false ->
			[H|do_calc_rank3(Chgs, T)]
	end;
do_calc_rank3(Chgs, []) -> [Chgs].

