%%----------------------------------------------------
%% @doc 工具包
%% @author cayleung@gmail.com
%% @end
%%----------------------------------------------------
-module(util).
-export(
    [
        %% 时间戳类
        unixtime/0, unixtime/1, unixtime_after_days_end/1
        ,unixtime_diff/1, unixtime_diff/2
        ,from_unixtime/1, from_unixtime/2, unix_timestamp/1
        ,format_datetime/2
        ,now_diff/2, time_left/2, nowintime/2

        %% 定时器类
        ,set_ms_timer/3, set_ms_timer/4
        ,set_timer/3, set_timer/4, get_timer/1
        ,unset_timer/1, unset_timers/1, unset_all_timer/0

        ,week_num/0

        %% 数字类
        ,floor/1 ,ceil/1, round/1
        ,ufloor/1 ,uceil/1, uround/1
        ,rand/2, rand_per_mille/1, rand2/2
        ,rand_list/1
        ,rand_weight_list/1
        ,rand_weight_list/2
        ,uint/1, uint/2, uint32/1, uint16/1, uint_i/1
        ,int_bool/1, int_to_range/3, int_int_percent/1

        %% 格式转换类
        ,format/2, format_ex/2, ip_to_binary/1, cn/2 
        ,term_to_binary/1, term_to_base_binary/1, binary_to_term/1

        %% ets类
        ,ets_lookup_element/3, ets_lookup_element/4
        ,ets_update_element/3
        

        %% 特殊类 ------------------------------------------
        ,if_in_list/3
        ,if_undefined/2, if_true/3, if_true_run/2, if_true_run/3
        ,is_process_alive/1
        
        ,get_gbtrees_value/3, set_gbtrees_value/4, unset_gbtrees_value/3  %% tuple中gbtress属性操作
        ,foldl_gbtrees/3
        %% ,get_tuplelist_value/3, set_tuplelist_value/4           %% tuple中tuplelist属性操作
        ,apply_begin/1,apply/4 ,apply_commit/1 ,apply_clean/1, apply_clean_all/1 %% 调用事务

        %% 身份证算法相关
        ,idcard_checksum/1      %% 15位/18位身份证校验码有效性检查 
        ,idcard_is_adult/1      %% 身份证号码是否满18周岁
        %% 分页
        ,pages/3
        %% 转百分比
        ,to_percentage/1
        ,auto_seed/0
		%% 对数计算
		,math_log/2
		%% 榜单类请求返回参数构造
		,get_legal_page/3        %% 进程字典
        ,dict_get/2
    ]
).

-include("common.hrl").
-define(week_sec, 604800).
-define(day_sec, 86400).
-define(hour_sec, 3600).
-define(minu_sec, 60).

%-define(uint32, 4294967295).
%-define(uint16, 65535).
%-define(uint8,  255).

-include_lib("kernel/include/file.hrl").

%% @spec unixtime() -> int()
%% @doc 时间类
%% 获得当前时间戳，等同于unixtime(now)

unixtime() ->
    unixtime(now).

%% @spec unixtime(Mix) -> int()
%% Mix = now | ms | today | tomorrow | monday | {hour, int()} | time() | {next, time()} | {int(), time()} | {next, int(), time()} 
%% @doc 获得当前时间戳
unixtime(now) ->
    {M, S, _} = erlang:now(),
    M * 1000000 + S;
unixtime(ms) ->
    {S1, S2, S3} = erlang:now(),
    trunc(S1 * 1000000000 + S2 * 1000 + S3 / 1000);
unixtime(today) ->
    {M, S, MS} = erlang:now(),
    {_Data, Time} = calendar:now_to_local_time({M, S, MS}),
    M * 1000000 + S - calendar:time_to_seconds(Time);
unixtime(tomorrow) ->
    unixtime(today) + ?day_sec;
unixtime(monday) ->
    unixtime(today) - ?day_sec * (week_num() - 1);
unixtime(next_monday) ->
    unixtime(today) + ?day_sec * (8 - week_num());
%% 今天0:0:0时间戳 + Hour
%% Hour = int() 今天0:0:0的时间戳
%% return int()
unixtime({hour, Hour}) when is_integer(Hour) ->
    unixtime(today) + Hour * ?hour_sec;
%% 获取某时间戳的00:00:00的时间戳当Unixtime为0时，返回值有可能是负值，因为这里有时区偏移值（例如北京时间就可能是-28800
unixtime({today, Unixtime}) ->
    Base = unixtime(today),  
    case Unixtime > Base of
        false -> Base - ceil((Base - Unixtime) / ?day_sec) * ?day_sec;
        true -> (Unixtime - Base) div ?day_sec * ?day_sec + Base
    end;
%% 今天0:0:0时间戳 + Hour
%% Hour = int() 今天0:0:0的时间戳
%% return int()
unixtime({H, M, S}) ->
    unixtime(today) + ?hour_sec * H + ?minu_sec * M + S;
unixtime({next, {H, M, S}}) ->
    Ts = unixtime({H, M, S}),
    case unixtime() >= Ts of
        true -> Ts + ?day_sec;
        false -> Ts
    end.

%% 获取特定时间， xx天后的 23:59:59 的时间戳
unixtime_after_days_end({AddDays, Unixtime}) ->
    {_Date, Time} = from_unixtime(Unixtime),
    Unixtime - calendar:time_to_seconds(Time)+86399+ ?day_sec*AddDays.

%% @spec unixtime_diff(UT2) -> int()
%% UT2 = now | ms | today | tomorrow | monday | {hour, int()} | time() | {next, time()} | {int(), time()} | {next, int(), time()} 
%% @doc UT2和当前时间的相差秒数 unixtime_diff(UT2, now)
unixtime_diff(UT2) ->
    unixtime_diff(UT2, now).

%% @spec unixtime_diff(UT2, UT1) -> int()
%% UT2 = now | ms | today | tomorrow | monday | {hour, int()} | time() | {next, time()} | {int(), time()} | {next, int(), time()} 
%% UT1 = now | ms | today | tomorrow | monday | {hour, int()} | time() | {next, time()} | {int(), time()} | {next, int(), time()} 
%% @doc UT2和UT1的相差秒数 UT2 - UT1 
unixtime_diff(UT2, UT1) -> 
    TS1 = case is_integer(UT1) of true -> UT1; false -> unixtime(UT1) end,
    TS2 = case is_integer(UT2) of true -> UT2; false -> unixtime(UT2) end,
    TS2 - TS1.

%% @spec from_unixtime(Unixtime) -> {{Y, M, D},{H, I, S}}
%% Unixtime = int()
%% @doc 时间戳转换成日期
from_unixtime(Unixtime) ->
    DT = calendar:gregorian_seconds_to_datetime(Unixtime + calendar:datetime_to_gregorian_seconds({{1970,1,1}, {0,0,0}})),
    erlang:universaltime_to_localtime(DT).
%% @spec from_unixtime(Unixtime, Format) -> string()
%% Unixtime = int()
%% Format = string()
%% @doc 时间戳转换成一定格式的日期
%% >from_unixtime(unixtime(), "Y-M-D H:I:S").
%% >"yyyy-mm-dd hh:ii:ss"
from_unixtime(Unixtime, Format) ->
    {{Y, M, D},{H, I, S}} = from_unixtime(Unixtime),
    from_unixtime_f1(Format, Y, M, D, H, I, S, []).

%% @doc 将日期转化成固定的格式
%% Date = date() = {Y, M, D}
%% Time = time() = {H, I, S}
%% Format = string()
%% @doc 时间戳转换成一定格式的日期
%% >format_datetime({{2013,6,7}, {10,0,0}}, "Y-M-D H:I:S").
%% >"yyyy-mm-dd hh:ii:ss"
%% 参数必须大于1970年1月1日
format_datetime({{Y, M, D},{H, I, S}}, Format) ->
    from_unixtime_f1(Format, Y, M, D, H, I, S, []).
    
    
%% @private
from_unixtime_f1([], _Y, _M, _D, _H, _I, _S, Buff) ->
    lists:append(lists:reverse(Buff));
from_unixtime_f1([$Y|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(Y)|Buff]);
from_unixtime_f1([$M|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(M)|Buff]);
from_unixtime_f1([$D|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(D)|Buff]);
from_unixtime_f1([$H|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(H)|Buff]);
from_unixtime_f1([$I|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(I)|Buff]);
from_unixtime_f1([$S|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [integer_to_list(S)|Buff]);
from_unixtime_f1([C|T], Y, M, D, H, I, S, Buff) ->
    from_unixtime_f1(T, Y, M, D, H, I, S, [[C]|Buff]).


%% @doc 生成一个指定日期的unix时间戳（无时区问题
%% Date = date() = {Y, M, D}
%% Time = time() = {H, I, S}
%% 参数必须大于1970年1月1日
unix_timestamp({Date, Time}) ->
    DT = erlang:localtime_to_universaltime({Date, Time}),
    calendar:datetime_to_gregorian_seconds(DT) - calendar:datetime_to_gregorian_seconds({{1970,1,1}, {0,0,0}}).

%% @spec now_diff(Now1, Now2) -> MS
%% Now1 = now()
%% Now2 = now()
%% MS = int()
%% @doc 返回时间差（毫秒
now_diff(Now1, Now2) ->
    timer:now_diff(Now1, Now2) div 1000.

%% @spec time_left(TimeMax::integer(), Begin::erlang:timestamp()) -> integer()
%% @doc 计算剩余时间，单位：毫秒
time_left(TimeMax, Begin)->
    TL = TimeMax - now_diff(erlang:now(), Begin),
    case TL > 0 of
    true -> TL;
    false -> 0
    end.

%% @spec nowintime(UnixTimeStart, UnixTimeEnd) -> bool()
%% UnixTimeStart = UnixTimeEnd = int()
%% @doc 检查当前时间，是否在时间段内
nowintime(UnixTimeStart, UnixTimeEnd) ->
    Now = unixtime(),
    (Now>=UnixTimeStart andalso Now=<UnixTimeEnd).

%% @spec set_ms_timer(Name, MS, Info) -> void()
%% Name = atom() | list()
%% MS = int()
%% Info = term()
%% @doc 设置用进程字典存放的定时器  set_ms_timer(Name, MS, self(), Info)
set_ms_timer(Name, MS, Info) -> set_ms_timer(Name, MS, self(), Info).
%% @spec set_ms_timer(Name, MS, Pid, Info) -> void()
%% Name = atom() | list()
%% MS = int()
%% Pid = pid()
%% Info = term()
%% @doc 设置用进程字典存放的定时器
set_ms_timer(Name, MS, Pid, Info) when is_integer(MS) ->
    case get('$dict_timer') of
    undefined -> 
        put('$dict_timer', 
            gb_trees:enter(Name, erlang:send_after(MS, Pid, Info), gb_trees:empty())
        );
    Tree ->
        case gb_trees:lookup(Name, Tree) of
        none -> 
            put('$dict_timer', gb_trees:enter(Name, erlang:send_after(MS, Pid, Info), Tree));
        {value, Ref} -> 
            erlang:cancel_timer(Ref),
            put('$dict_timer', 
                gb_trees:enter(Name, erlang:send_after(MS, Pid, Info), gb_trees:delete(Name, Tree))
            )
        end
    end.

%% @spec set_timer(Name, TimeMix, Info) -> void()
%% Name = atom() | list()
%% TimeMix = time() | Sec
%% Sec = int()
%% Info = term()
%% @doc 设置用进程字典存放的定时器
set_timer(Name, TimeMix, Info) -> set_timer(Name, TimeMix, self(), Info).
%% @spec set_timer(Name, TimeMix, Pid, Info) -> void()
%% Name = atom() | list()
%% TimeMix = time() | Sec
%% Sec = int()
%% Pid = pid()
%% Info = term()
%% @doc 设置用进程字典存放的定时器
set_timer(Name, {H, M, S}, Pid, Info) ->
    Sec = unixtime_diff({next, {H, M, S}}),
    set_timer(Name, Sec, Pid, Info);
set_timer(Name, Sec, Pid, Info) when is_integer(Sec) ->
    set_ms_timer(Name, Sec * 1000, Pid, Info).

%% @spec get_timer(Name) -> int() | false
%% Name = atom() | list()
%% @doc 获取用进程字典存放的定时器
get_timer(Name) ->
    case get('$dict_timer') of
    undefined -> false;
    Tree -> 
        case gb_trees:lookup(Name, Tree) of
        none -> false;
        {value, Ref} -> 
            erlang:read_timer(Ref)
        end
    end.

%% @spec unset_timer(Name) -> void()
%% @doc 清除用进程字典存放的Name定时器，清除set_timer/3,set_timer/4,set_ms_timer/3,set_ms_timer/4设置的定时器
unset_timer(Name) ->
    case get('$dict_timer') of
    undefined -> ok;
    Tree -> 
        case gb_trees:lookup(Name, Tree) of
        none -> ok;
        {value, Ref} -> 
            erlang:cancel_timer(Ref),
            put('$dict_timer', gb_trees:delete(Name, Tree))
        end
    end.

%% @spec unset_timers(Names) -> void()
%% Names = [term()]
%% @doc 清除多个定时器
unset_timers(Names) ->
    case get('$dict_timer') of
    undefined -> ok;
    Tree ->
        unset_timers_f1(Names, Tree), 
        ok
    end.
unset_timers_f1([], Tree) -> put('$dict_timer', Tree);
unset_timers_f1([Name|T], Tree) -> 
    case gb_trees:lookup(Name, Tree) of
    none -> unset_timers_f1(T, Tree);
    {value, Ref} -> 
        erlang:cancel_timer(Ref),
        unset_timers_f1(T, gb_trees:delete(Name, Tree))
    end.


%% @spec unset_all_timer() -> void()
%% @doc 清除用进程字典存放的所有定时器
unset_all_timer() ->
    case erase('$dict_timer') of
    undefined -> ok;
    Tree -> gb_trees:map(fun(_K, Ref) -> erlang:cancel_timer(Ref) end, Tree)
    end.

%% @spec week_num() -> WeekNum
%% WeekNum = int()
%% @doc 获取今天是星期几1-7
week_num() ->
    {Date, _} = calendar:local_time(),
    calendar:day_of_the_week(Date).

%% @spec ceil(Float) -> int()
%% Float = float()
%% @doc 取大于Float的最小整数
ceil(Float) ->
    T = erlang:trunc(Float),
    case Float > T of
        true -> T + 1;
        _ -> T
    end.
%% @spec uceil(Float) -> int()
%% Float = float()
%% @doc 取大于Float的最小自然数（最小的大于等于0的整数
uceil(Float) when Float > 0->
    ceil(Float);
uceil(_) ->
    0.

%% @spec floor(Float) -> int()
%% Float = float()
%% @doc  取小于Float的最大整数 
floor(Float) ->
    erlang:trunc(Float).

%% @spec ufloor(Float) -> int()
%% Float = float()
%% @doc  取小于Float的最大自然数（最大的大于等于0的整数
ufloor(Float) when Float > 0 ->
    erlang:trunc(Float);
ufloor(_Float) ->
    0.

%% @spec round(Float) -> int()
%% Float = float()
%% @doc  Float四舍五入
round(Float) -> 
    erlang:round(Float).

%% @spec uround(Float) -> int()
%% Float = float()
%% @doc  Float四舍五入（返回一个自然数即大于等于0的整数
uround(Float) when Float > 0 -> 
    erlang:round(Float);
uround(_) ->
    0.

auto_seed() ->
    RandSeed = erlang:now(),
    random:seed(RandSeed),
    put('rand_seed', RandSeed).

%% @spec rand(Min, Max) -> int()
%% Min = int()
%% Max = int()
%% @doc 产生一个介于Min到Max之间的随机整数（依赖于sys_rand进程
rand(Min, Min) -> Min;
rand(Min, Max) ->
    %% 如果没有种子，将从核心服务器中去获取一个种子，以保证不同进程都可取得不同的种子
    case get('rand_seed') of
    undefined -> auto_seed();
    _ -> ignore
    end,
    M = Min - 1,
    random:uniform(Max - M) + M.

%% @spec rand_per_mille(R) -> bool()
%% @doc 随机千分率判断是否命中
rand_per_mille(R) when R >= 1000 -> true;
rand_per_mille(R) ->
    rand(1, 10000) =< R * 10.

%% TODO rand2没必要使用，只用于测试
rand2(Min, Min) -> Min;
rand2(Min, Max) ->
    %% 如果没有种子，将从核心服务器中去获取一个种子，以保证不同进程都可取得不同的种子
    case get('rand_seed') of
    undefined -> auto_seed();
    _ -> ignore
    end,
    case get('rand_num') of
    I when is_integer(I), I > 10 -> 
        auto_seed(),
        put('rand_num', 1);
    I when is_integer(I) ->
        put('rand_num', I + 1);
    undefined -> put('rand_num', 1)
    end,
    M = Min - 1,
    random:uniform(Max - M) + M.

%% -------------------------------------------------
%% @doc 从一个list中随机出一项
%% -------------------------------------------------
-spec rand_list(L) -> Elem | null when
    L       :: list(),
    Elem    :: term().

rand_list([]) -> null;
rand_list([I]) -> I;
rand_list(List) ->
    Idx = rand(1, length(List)),
    lists:nth(Idx, List).

%% -------------------------------------------------
%% @doc 按权重从整数列表中随机出一项, 权重就是列表每项的值, 返回下标或0
%% -------------------------------------------------
-spec rand_weight_list(L) -> Index when
    L       :: [integer()],
    Index   :: integer().

rand_weight_list(L) ->
    SumW = lists:sum(L),
    Rand = rand(1, SumW),
    find_weight_list(Rand, L, 0).

find_weight_list(Rand, _L, _Idx) when Rand =< 0 -> 0;
find_weight_list(_Rand, [], _Idx) -> 0;
find_weight_list(Rand, [W|T], Idx) ->
    case Rand =< W of
        true -> Idx + 1;
        false -> find_weight_list(Rand - W, T, Idx + 1)
    end.

%% -------------------------------------------------
%% @doc 按权重从tuple列表中随机出一项, 需指定权重在元组中的下标, 返回下标或0
%% -------------------------------------------------
-spec rand_weight_list(L, WPos) -> Index when
    L       :: [tuple()],
    WPos    :: integer(),
    Index   :: integer().

rand_weight_list(L, WPos) when is_integer(WPos) ->
    rand_weight_list([element(WPos, E) || E <- L]).

%% @spec uint(Int) -> Uint
%% Int = int()
%% Uint = uint()
%% @doc 将有符号整型转成无符号整型，注意跟abs的区别
%% uint(-1) -> 0.
uint(Int) when Int > 0 -> trunc(Int);
uint(_) -> 0. 

uint_i(Int) when Int > 0 -> Int;
uint_i(_) -> 0.

%% @spec uint(Val, UpperLimit) -> uint()
%%
%% @doc 获取限制范围在UpperLimit内的正整数
uint(Val, UpperLimit) -> 
     min(uint(Val), UpperLimit).

%% @spec uint32(Int) -> Uint
%% Int = int()
%% Uint = uint32()
%% @doc 将有符号整型转成无符号整型32，注意跟abs的区别
%% uint32(-1) -> 0.
uint32(Int) when Int > ?uint32 -> ?uint32;
uint32(Int) -> uint(Int).

%% @spec uint16(Int) -> Uint
%% Int = int()
%% Uint = uint16()
%% @doc 将有符号整型转成无符号整型32，注意跟abs的区别
%% uint16(-1) -> 0.
uint16(Int) when Int > ?uint16 -> ?uint16;
uint16(Int) -> uint(Int).


%% @spec int_bool(Val) -> int_bool()
%% 
%% @doc 强制转化为int_bool()
int_bool(Val) -> 
    case Val of
    ?true ->
        ?true;
    _ ->
        ?false
    end.

%% @spec spec int_to_range(Val) -> Val2 Val2 >= LowerBound andalso Val2 =< UpperBound
%% @doc 将int转化为一定范围内的int
int_to_range(Val, Low, Up) ->
	if Val >= Low andalso Val =< Up ->
		Val;
	true ->
		Low
	end.

%% @spec int_intper(Val) -> Val2 Val2 >= 0 andalso Val2 =< 100
%% @doc 强制转化为0-100
int_int_percent(Val) ->
	int_to_range(Val, 0, 100).

%% @spec format(Format, Args) -> binary()
%% Format = iolist() | list() | binary()
%% Args = [term()]
%% @doc 格式化为binary，转换过的中文才能正常显示（用于提示信息合并
format(Format, Args) ->
    erlang:iolist_to_binary(io_lib:format(Format, Args)).

%% @spec format(Format, Args) -> binary()
%% Format = list() | binary()
%% Args = [int()|string()|binary()]
%% @doc 格式化为binary，转换过的中文才能正常显示（用于提示信息合并
format_ex(Format, Args) when is_binary(Format) ->
    format_ex(binary_to_list(Format), Args, []);
format_ex(Format, Args) when is_list(Format) ->
    format_ex(Format, Args, []).

format_ex([], _Args, Res) -> list_to_binary(lists:reverse(Res));
format_ex([$~,$w|T], [A|Args], Res) when is_integer(A) ->
    format_ex(T, Args, [integer_to_list(A)|Res]);
format_ex([$~,$w|T], [A|Args], Res) when is_atom(A) ->
    format_ex(T, Args, [atom_to_list(A)|Res]);
format_ex([$~,$s|T], [A|Args], Res) ->
    format_ex(T, Args, [A|Res]);
format_ex([C|T], Args, Res) ->
    format_ex(T, Args, [C|Res]).

%% @spec cn(Str, Args) -> ok
%% Str = list()
%% Args = [term()]
%% @doc 在控制台显示带中文的字符串
cn(Str, Args) ->
    io:format("~ts", [iolist_to_binary(io_lib:format(Str, Args))]).

%% @spec ip_to_binary(Ip) -> binary()
%% Ip = ip()
%% @doc  ip()转binary
ip_to_binary({P1, P2, P3, P4}) ->
    list_to_binary([integer_to_list(P1)++"."++integer_to_list(P2)++"."++integer_to_list(P3)++"."++integer_to_list(P4)]).

%% @spec term_to_binary(Term) -> binary()
%% Term = term()
%% @doc  数据结构序列化为可视binary（字符串不会被分解，可以用binary_to_term还原
term_to_binary(Term) ->
    list_to_binary(io_lib:format("~p", [Term])).

%% @spec term_to_base_binary(Term) -> binary()
%% Term = term()
%% @doc  数据结构序列化为可视binary（所有字符串会被分解为[int()]，可以用binary_to_term还原
term_to_base_binary(Term) ->
    list_to_binary(io_lib:format("~w", [Term])).

%% @spec binary_to_term(Bin) -> term()
%% Bin = binary()
%% @doc  binary反序列化为term()
binary_to_term(Bin) ->
    Bin2 = re:replace(Bin,"<\\d+\\.\\d+\\.\\d+>", "0" ,[{return,binary}]),    
    case erlang:size(Bin) =:= erlang:size(Bin2) of
    true -> ignore;
    false -> ?ELOG("骚年，你的这货~s,里面有存pid。干掉它吧！",[Bin])
    end,
    case erl_scan:string(binary_to_list(Bin2)++".") of
    {ok, Tokens, _} ->
        case erl_parse:parse_term(Tokens) of
        {ok, Term} -> Term;
        _Err -> undefined
        end;    
    _Error -> undefined
    end.

%% @spec ets_lookup_element(Tab, Key, Pos) -> null | term()
%% Tab = int() | atom()
%% Key = atom()
%% Pos = int()
%% Def = term()
%% @doc ets:lookup_element返回Val值，相当于ets_lookup_element(Tab, Key, Pos, null).
ets_lookup_element(Tab, Key, Pos) -> 
    ets_lookup_element(Tab, Key, Pos, null).
%% @spec ets_lookup_element(Tab, Key, Pos, Def) -> Def | term()
%% Tab = int() | atom()
%% Key = atom()
%% Pos = int()
%% Def = term()
%% @doc ets:lookup_element返回Val值，其实就是 ets:lookup_element/3做了catch处理
ets_lookup_element(Tab, Key, Pos, Def) ->
    case catch ets:lookup_element(Tab, Key, Pos) of
    {'EXIT', _} -> Def;
    Elem -> Elem
    end.

%% @spec ets_update_element(Tab, Key, Mix) -> void()
%% Tab = int() | atom()
%% Key = atom()
%% Mix = {Pos, Value} | [{Pos, Value}]
%% @doc ets:update_element做了catch处理
%% <div>当然这里要自己确定参数是否合法，加catch是为了捕捉找不到时的报错</div>
ets_update_element(Tab, Key, Mix) ->
    catch ets:update_element(Tab, Key, Mix).

%% 特殊类 --------------------------------------------
%% @spec if_in_list(Val, List, DefVal) -> NewVal
%%
%% @doc 值不在列表中则给一个默认值 
if_in_list(Val, List, DefVal) ->
    case lists:member(Val, List) of
    true ->
        Val;
    false ->
        DefVal
    end.


if_undefined(undefined, Def) -> Def;
if_undefined(Val, _Def) -> Val.

if_true(true, V1, _V2) -> V1;
if_true(_Is, _V1, V2) -> V2.

if_true_run(Bool, {M, F, A}) -> if_true_run(Bool, {M, F, A}, ignore).

if_true_run(true, {M, F, A}, _Def) -> erlang:apply(M, F, A);
if_true_run(_, _MFA, Def) -> Def.

%% @spec is_process_alive(P) -> true | false
%% P = pid()
%% @doc 检查进程是否存活(可检查远程节点上的进程), rpc 是单进程结构不能过分依赖, 建议使用erlang:monitor/2 监听
is_process_alive(P) when is_pid(P) ->
    case node() =:= node(P) of
        true ->
            erlang:is_process_alive(P);
        false ->
            case rpc:call(node(P), erlang, is_process_alive, [P]) of
                true -> true;
                false -> false;
                _ -> false
            end
    end;
is_process_alive(_) ->false.


%% @spec apply_begin(Key) -> void()
%% @doc 开启缓冲调用
apply_begin(Key) ->
    case get(Key) of
    undefined -> put(Key, []);
    Buffer -> put(Key, do_apply_begin(Buffer))    %%嵌套
    end.

%% 判断是否有子节点
has_child([P, C]) when is_list(P),is_list(C) ->
    true;
has_child(_) ->
    false.

%% 加一层
do_apply_begin(P) -> [P, []].

%% @spec apply(Key, M, F, A) -> void()
%% @doc 调用函数
apply(Key, M, F, A) ->
    case get(Key) of
    undefined -> apply(M, F, A);
    Buffer -> put(Key, do_apply(Buffer, {M, F, A}))
    end.



%% 将MFA放到相应的层
do_apply(P, MFA) ->
    case has_child(P) of
    true ->
        [P2, C2] = P,
        [P2, do_apply(C2, MFA)];
    false ->
        [MFA | P]
    end.

%% @spec apply_clean(Key) -> void()
%% @doc 清除缓冲调用
apply_clean(Key) ->
    case get(Key) of
    undefined -> io:format("error: apply_clean ~w~n", [Key]);
    Buffer -> 
        MewBuffer = do_apply_clean(Buffer),
        put(Key, MewBuffer),
        ok
    end.

%% @spec apply_clean_all(Key) -> void()
%% @doc 完整清除缓冲
apply_clean_all(Key) ->
    erase(Key).

%% 剥掉一层
do_apply_clean(P) ->
    case has_child(P) of
    true ->
       [C2, _P2] = P,
       C2;
    false ->
        undefined
    end.

%% @spec apply_commit(Key) -> void()
%% @doc 提交缓冲调用
apply_commit(Key) ->
    case get(Key) of
    undefined -> io:format("error: apply_commit ~w~n", [Key]);
    Buffer -> 
        %%?INFO("buff: ~p ", [Buffer]),
        MewBuffer = do_apply_commit(Buffer),
        put(Key, MewBuffer),
        ok
    end.

apply_commit_f1([]) -> ok;
apply_commit_f1([{M, F, A}|T]) -> 
    case catch apply(M, F, A) of
    {'EXIT', Reason} -> ?ELOG("~w", [Reason]);
    _ -> apply_commit_f1(T)
    end.

%% 合并一层
do_apply_commit(P) ->
    case has_child(P) of
    true ->
        [C2, P2] = P,
        apply_commit_f1(lists:reverse(P2)),
        C2;
    false ->
        apply_commit_f1(lists:reverse(P)),
        undefined
    end.


%% @spec get_gbtrees_value(Tuple, Pos, Key) -> undefined | Val
%% @doc 从tuple中获取取出特点位置的值，并进行gb_trees的key-value操作
%% 主要是用对record内部的一些数据操作，和set_gbtrees_value/4一起使用
get_gbtrees_value(Tuple, Pos, Key) ->
    case element(Pos, Tuple) of
    Tree when is_tuple(Tree), size(Tree) == 2 -> 
        case gb_trees:lookup(Key, Tree) of
        {value, V} -> V;
        none -> undefined
        end;
    _ -> undefined
    end.

%% @spec set_gbtrees_value(Tuple, Pos, Key, Val) -> NewTuple
%% @doc 从tuple中获取取出特点位置的值，并进行gb_trees的key-value操作
%% 主要是用对record内部的一些数据操作
set_gbtrees_value(Tuple, Pos, Key, Val) ->
    case element(Pos, Tuple) of
    Tree when is_tuple(Tree), size(Tree) == 2 -> 
        setelement(Pos, Tuple, gb_trees:enter(Key, Val, Tree));
    _ -> 
        setelement(Pos, Tuple, gb_trees:insert(Key, Val, gb_trees:empty()))
    end.

%% @spec foldl_gbtrees(Fun, Acc, Tree) -> AccFin
%% Fun = fun(Key, Val, Acc) -> NewAcc end
%% 
%% @doc 采用λ演算式迭代遍历树，速度比to_list/1 后遍历稍慢，好处是不用读取整个数据到内存
foldl_gbtrees(Fun, Acc, Tree) when is_tuple(Tree), size(Tree) == 2 ->
    gb_trees_foldl_iter(Fun, Acc, gb_trees:next(gb_trees:iterator(Tree)));
foldl_gbtrees(_, Acc, _) ->
    Acc.    

gb_trees_foldl_iter(_F, Acc, none) ->
    Acc;
gb_trees_foldl_iter(F, Acc, {Key, Val, Iter})->
    gb_trees_foldl_iter(F, F(Key, Val, Acc), gb_trees:next(Iter)).

%% @spec unset_gbtrees_value(Tuple, Pos, Key) -> NewTuple
%% @doc 从tuple中获取取出特点位置的值，并进行gb_trees的删除操作
%% 主要是用对record内部的一些数据操作
unset_gbtrees_value(Tuple, Pos, Key) ->
    case element(Pos, Tuple) of
    Tree when is_tuple(Tree), size(Tree) == 2 ->
       case gb_trees:is_defined(Key, Tree) of
          true -> setelement(Pos, Tuple, gb_trees:delete(Key, Tree));
          false -> Tuple
        end;
    _ -> 
        setelement(Pos, Tuple, gb_trees:empty())
    end.


%% @spec idcard_checksum(IdCard) when is_bitstring(IdCard) -> true | false
%% IdCard = binary|list() 身份证号码
%% @doc 18位身份证校验码有效性检查 
idcard_checksum(IdCard) when is_bitstring(IdCard) ->
    idcard_checksum(binary_to_list(IdCard));

idcard_checksum(IdCard) when length(IdCard)=:=18 ->
    IdCard2 = string:to_upper(IdCard),
    IdCardBase = string:substr(IdCard2, 1, 17),
    idcard_verify_number(IdCardBase)=:=string:to_upper(string:right(IdCard2, 1));

idcard_checksum(IdCard) when length(IdCard)=:=15 ->
    ListOldMan = ["996", "997", "998", "999"],
    IdCardBase = case lists:member(string:right(IdCard, 3), ListOldMan) of
        true ->
            string:substr(IdCard, 6)++"18"++string:right(IdCard, 9);
        false ->
            string:substr(IdCard, 6)++"19"++string:right(IdCard, 9)
    end,
    IdCard18 = IdCardBase ++ idcard_verify_number(IdCardBase),
    idcard_checksum(IdCard18);

idcard_checksum(_IdCard) ->
    false.

%% @spec idcard_verify_number(IdCardBase) when is_bitstring(IdCardBase) -> list()
%% @doc 计算身份证校验码，根据国家标准GB 11643-1999
idcard_verify_number(IdCardBase) when is_bitstring(IdCardBase) ->
    idcard_verify_number(binary_to_list(IdCardBase));

idcard_verify_number(IdCardBase) when length(IdCardBase)=:=17 ->
    Factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2],
    VerifyNumList = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"],
    {CheckSum, _} = lists:foldl(fun
            ("X", {Sum, Nth})-> 
                {Sum + 10 * lists:nth(Nth, Factor), Nth+1};
            (Char, {Sum, Nth})-> 
                {Sum + list_to_integer([Char]) * lists:nth(Nth, Factor), Nth+1}
    end, {0, 1}, IdCardBase),
    Mod = CheckSum rem 11,
    lists:nth(Mod+1, VerifyNumList);

idcard_verify_number(_IdCardBase) ->
    "0".

%% @spec idcard_is_adult(IdCard) when is_binary(IdCard) -> true | false
%% @doc 身份证号是否满18周岁
idcard_is_adult(IdCard) when is_binary(IdCard) ->
    idcard_is_adult(binary_to_list(IdCard));

idcard_is_adult(IdCard) when length(IdCard)=:=15 ->  %% 15位号码
    true;

idcard_is_adult(IdCard) when length(IdCard)=:=18 ->  %% 15位号码
    Year = list_to_integer(string:substr(IdCard, 7, 4)),
    Month = list_to_integer(string:substr(IdCard, 11, 2)),
    Day = list_to_integer(string:substr(IdCard, 13, 2)),
    {Y, M, D} = erlang:date(),
    case Y-Year of
        18 ->
            (M*100)+D >= (Month*100)+Day;
        Diff when Diff>18 ->
            true;
        _ ->
            false
    end;

idcard_is_adult(_IdCard) ->  %% 其它
    false.


%% @spec pages(PageSize, PageNum, Datas)-> {NewPageNum, MaxPageNum, Page}
%% PageSize = PageNum = NewPageNum = MaxPageNum = integer()
%% PageSize 每页的大小  PageNum 请求页号  Datas数据列表 NewPageNum 返回页号  MaxPageNum 返回最大页号 Page 页内容
%% Datas = list()
%% @doc 分页
pages(_PageSize, _PageNum, [])->
    {0, 0, []};
pages(PageSize, PageNum, Datas)->
    MaxPageNum = util:ceil(length(Datas) / PageSize),
    PageNum1 = if
        PageNum > MaxPageNum orelse PageNum =< 0 ->
            MaxPageNum;
        true ->
            PageNum
    end,
    Start = (PageNum1-1) * PageSize + 1,
    Page = lists:sublist(Datas, Start, PageSize),
    {PageNum1, MaxPageNum, Page}.

%% @spec to_percentage(Float) -> Num2
%% Float=float()
%% Num = int()
%% @doc 小数转百分比
to_percentage(Float) ->
    erlang:round(Float * 10000) div 100.

%% @spec math_log(a, b) -> logab
%% a = numbers
%% b = numbers
%% logab = float
%% @doc 计算对数
math_log(A, B) ->
	math:log(B) / math:log(A).

%% @spec dict_get(Key, Def) -> Val
%% Key = Def = Val = term()
%% @doc 对进程字段get的封装，能自定义默认返回
dict_get(Key, Def) ->
    case erlang:get(Key) of
    undefined -> Def;
    Val -> Val
    end.
%% @spec get_legal_page(Page, Len, PageNum) -> {Page1, TotalPage}
get_legal_page(Page, Len, PageNum) when PageNum > 0 ->
	TotalPage = 
	if Len rem PageNum =:= 0 andalso Len > 0 ->
		trunc(Len / PageNum);
	true ->
		trunc(Len / PageNum) + 1
	end,
	Page1 = %% 过滤不合法的页
	if Page > TotalPage ->
		1;
	true ->
		Page
	end,
	{Page1, TotalPage};
get_legal_page(_Page, _Len, _PageNum) ->
	{0, 0}.
