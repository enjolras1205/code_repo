%% ---------------------------------------
%% 测试工具
%% @author cayleung@gmail.com
%% @end
%% ---------------------------------------
-module(test_util).
-export([
        time_compare/2,
        time_compare/3
    ]
).
-include("common.hrl").

%% @spec time_compare(Num, TList) -> void()
%% Num = int()
%% TList = [tester()]
%% @doc 运行时间比较
%% test_util:time_compare(N, [{"fun match", F1}, {"Mod:call", F2}, {"Mod:call", F2, ProcessNum}]).
time_compare(Num, TList) -> time_compare(Num, TList, []).
time_compare(Num, TList, _Opt) when Num > 0 ->
    [[L, T1, T2] = H | T] = [do_time_compare(Num, Tester) || Tester <- TList],
    io:format("========================================================================~n"),
    io:format("~20s = ~9.2fms [~8.2f%] ~9.2fms [~8.2f%]~n", [L, T1 + 0.0, 100.0, T2 + 0.0, 100.0]),
    do_time_compare_f2(T, H),
    io:format("========================================================================~n").
%% @private
do_time_compare(Num, {Label, Func}) ->
    statistics(runtime),
    statistics(wall_clock),
    do_time_compare_f1(1, Num, Func),
    {_, Time1} = statistics(runtime),
    {_, Time2} = statistics(wall_clock),
    U1 = Time1 * 1000 / Num,
    U2 = Time2 * 1000 / Num,
    io:format("~p [total: ~p(~p)ms avg: ~.3f(~.3f)us]~n", [Label, Time1, Time2, U1, U2]),
    [Label, Time1, Time2];
do_time_compare(Num, {Label, Func, ProcessNun}) ->
    Pids = do_time_compare_init(Num, 0, ProcessNun, Func, []),
    statistics(runtime),
    statistics(wall_clock),
    [erlang:send(Pid, go) || Pid <- Pids],
    do_time_compare_water(length(Pids)),
    {_, Time1} = statistics(runtime),
    {_, Time2} = statistics(wall_clock),
    U1 = Time1 * 1000 / Num,
    U2 = Time2 * 1000 / Num,
    io:format("~p [total: ~p(~p)ms avg: ~.3f(~.3f)us]~n", [Label, Time1, Time2, U1, U2]),
    [Label, Time1, Time2].

do_time_compare_init(0, _Start, _, _Func, Pids) -> Pids;
do_time_compare_init(Num, Start, 1, Func, Pids) ->
    Self = self(),
    Pid = spawn_link(fun() -> do_time_compare_worker(Self, Func, Start + 1, Start+Num) end),
    [Pid|Pids];
do_time_compare_init(Num, Start, ProcessNun, Func, Pids) ->
    Self = self(),
    DoRound = Num div ProcessNun,
    Pid = spawn_link(fun() -> do_time_compare_worker(Self, Func, Start + 1, Start + DoRound) end),
    do_time_compare_init(Num - DoRound, Start + DoRound, ProcessNun-1, Func, [Pid|Pids]).

do_time_compare_water(0) -> ok;
do_time_compare_water(Num) ->
    receive
    ok -> 
        do_time_compare_water(Num - 1);
    Msg -> erlang:error(io_lib:format("bad message:~w", [Msg]))
    end.

do_time_compare_worker(Pid, Func, From, To) ->
    receive
    go -> 
        %% ?INFO("~w", [{From, To}]),
        do_time_compare_f1(From, To, Func),
        Pid ! ok;
    _ ->
        ok
    end.

do_time_compare_f1(Max, Max, Func) -> 
    Func(Max);
do_time_compare_f1(I, Max, Func) -> 
    Func(I), 
    do_time_compare_f1(I+1, Max, Func).
%% 比较结果
%% @private
do_time_compare_f2([], _) ->
    ok;
do_time_compare_f2([[L, T1, T2] | T], [_, TB1, TB2] = TB) ->
    io:format("~20s = ~9.2fms [~8.2f%] ~9.2fms [~8.2f%]~n", [L, T1 + 0.0, T1 / (TB1 + 0.00000001) * 100, T2 + 0.0, T2 / (TB2 + 0.00000001) * 100]),
    do_time_compare_f2(T, TB).
