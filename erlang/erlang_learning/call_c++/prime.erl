%%------------------------------------------------
%% @Author: enjolras <enjolras@163.com>
%%
%% @File: prime.erl
%% @Create Date: 2014-05-23
%%
%%-----------------------------------------------

-module(prime).

-export([
    load/0
    ,findPrime/1
]).

load() ->
    erlang:load_nif("./prime", 0).

findPrime(_) -> 
    io:format("this function is not defined!~n").
