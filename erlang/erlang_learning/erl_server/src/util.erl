%%%-------------------------------------------------------------------
%%% @author enjolras
%%% create_date 2014-06-09
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-include("log.hrl").

-module(util).

-export([stacktrace/0]).

stacktrace() ->
    try throw(a)
    catch throw:a ->
        erlang:get_stacktrace()
    end.

