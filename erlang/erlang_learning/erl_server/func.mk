define build_app_file
	@echo "creating app file : $(1)"
	@sed -e 's:@APP_NAME@:$(2):g' \
		-e 's:@APP_VSN@:$(3):g' \
		-e 's:@MODULES@:$(4):g' -e "s:\`:\':g" -e 's/,]/]/g' $(5) > $(1)
endef
