-module(hello_handler).
-behavior(cowboy_handler).

-export([init/2]).

init(Req, State) ->
    process_flag(trap_exit, true),
    erlang:display({"before_sleep", where_am_i()}),
    timer:sleep(3000),
    erlang:display("after_sleep"),
    Req = cowboy_req:reply(
        200,
        #{<<"content-type">> => <<"text/plain">>},
        <<"Hello Erlang!">>,
        Req
    ),
    {ok, Req, State}.

where_am_i() ->
    try
        throw(a)
    catch
        throw:a:Stacktrace ->
            Stacktrace
    end.
