-module(rf).
-compile(export_all).
-include_lib("kernel/include/file.hrl").

read(Filename)-> read(Filename, 1024 * 1024).

read(File, Bs)-> 
    case file:open(File, [raw, binary]) of 
        {ok, Fd} -> 
            scan_file(Fd, file:read(Fd, Bs), Bs); 
        {error, _} = E -> 
            E
    end . 

scan_file(Fd, {ok, _Binary}, Bs) -> 
    scan_file(Fd, file:read(Fd, Bs), Bs); 
scan_file(Fd, eof, _Bs) -> 
    file:close(Fd); 
scan_file(Fd, {error, _}, _Bs) -> 
    file:close(Fd). 

read1(Filename) -> 
    {ok, _Binary} = file:read_file(Filename), 
    ok. 

upmap(F, L) ->
    Parent = self(),
    Ref = make_ref(),
    [receive {Ref, Result} -> Result end
        || _ <- [spawn(fun() -> Parent ! {Ref, F(X)} end) || X <- L]].

read2(Filename)->
    PoolSize = erlang:system_info(thread_pool_size),
    read2(Filename, PoolSize).

read2(_, 0)->
    io:format("setting +A first");
read2(Filename, PoolSize)->
    {ok, FInfo} = file:read_file_info(Filename),
    Bs = FInfo#file_info.size div PoolSize,
    erlang:display([{bs, Bs}, {poolsize, PoolSize}]),
    upmap(fun (Off)->
                {ok, Fd} = file:open(Filename, [raw, binary]),
                {ok, _} = file:pread(Fd, Off * Bs, Bs),
                file:close(Fd),
                erlang:display([reading, block, Off * Bs, Bs, done]),
                ok
        end, lists:seq(0, PoolSize - 1)),
    ok.
