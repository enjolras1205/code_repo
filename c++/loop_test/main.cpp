/*
 *  Created on: 2014-11-07 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <vector>
using namespace std;

struct Test {
    Test(void) {cout<<"contstruct"<<endl;}
    ~Test(void) {cout<<"decontstruct"<<endl;}
    Test(const Test&t) {cout<<"copy contstruct"<<endl;}
    Test(const Test&&t) {cout<<"move contstruct"<<endl;}

    Test& operator =(const Test &t) {cout<<"copy assign"<<endl; return *this;}
    Test& operator =(const Test &&t) {cout<<"move assign"<<endl; return *this;}
    int member;
};

int main()
{
    std::vector<Test> test;
    test.push_back(Test());
    for (auto &it : test) {
    }
    return 0;
}
