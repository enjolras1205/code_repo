

/*
 *  Created on: 2016-05-04 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
using namespace std;

class Base {
public:
	virtual void print_sth(void) {
		cout<<"base"<<endl;
	}
	int arr[4];
};

class Derive : public Base {
public:
	virtual void print_sth(void) {
		cout<<"derive"<<endl;
	}
};

int main()
{
	Derive* d = new Derive();
	d->arr[-1] = 0xdeadbeaf;
	d->print_sth();
	return 0;
}
