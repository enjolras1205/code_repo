/*
 *  Created on: 2014-11-07 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

int main()
{
    list<int> l_list = { 1, 2, 3, 4, 4, 5 };
    int value = 3;
    list<int>::iterator it = std::find_if(l_list .begin(), l_list.end()
        ,[value](int val) { return val >= value; });
    cout<<*it<<endl;
    return 0;
}
