/*
 *  Created on: 2015-01-22 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <syslog.h>
#include <stdarg.h>
using namespace std;

int main()
{
    openlog("prepend", LOG_CONS | LOG_PID, LOG_LOCAL0);
    int i = 0;
    while (cin>>i) {
        syslog(LOG_DEBUG, "input:%d", i);
        syslog(LOG_INFO, "input:%d", i);
    }
    closelog();
    return 0;
}
