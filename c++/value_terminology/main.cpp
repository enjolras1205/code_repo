/*
 *  Created on: 2014-10-29 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <string>
using namespace std;

class Test {
public:
    Test(void) { cout<<"construct"<<endl; };
    ~Test(void) { cout<<"destruct"<<endl; };
    Test(const Test& o) { cout<<"copy construct"<<endl; };
    Test(const Test&& o) { cout<<"move construct"<<endl; };
    Test &operator=(const Test &o) { cout<<"copy assigment"<<endl; return *this;}
    Test &operator=(const Test &&o) { cout<<"move assigment"<<endl; return *this;}

};

int main()
{
    Test a;
    Test b = a;
    Test c = std::move(b);
    Test d;
    d = c;
    d = std::move(c);
    return 0;
}
