#include <iostream>
#include <thread>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/uio.h>

#define PORT 11111
#define ADDRESS "192.168.44.115"
#define PACKET_SIZE 286

/*
编译:
g++ -std=c++11 -o main main.cpp  -lpthread -O3
*/

inline std::string hex_to_str(const std::string &str)
{
    std::string result;
    for (size_t i = 0; i < str.length(); i += 2)
    {
        std::string byte = str.substr(i, 2);
        char chr = (char)(int)strtol(byte.c_str(), NULL, 16);
        result.push_back(chr);
    }
    return result;
}

void send_thread_func(int sock)
{
    // 只有中间的 session_id 会变。其它部分先计算好
    struct iovec iov[3];
    std::string content1 = hex_to_str("8181880e62656e63686d61726b5f7372706382046563686f8318");
    std::string content3 = hex_to_str("87183565343863613262346533313735393466653730366237357b2261223a226161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161227d");
    char buffer1[128] = {0};
    char buffer2[24] = {0};
    char buffer3[512] = {0};
    // 注意长度字节序为网络序
    uint16_t total_packet_len = htons(286);
    // header长度为固定的
    uint16_t header_len = htons(76);
    memcpy(buffer1, &total_packet_len, 2);
    memcpy(buffer1 + 2, &header_len, 2);
    memcpy(buffer1 + 4, content1.c_str(), content1.length());
    memcpy(buffer3, content3.c_str(), content3.length());
    iov[0].iov_base = buffer1;
    iov[0].iov_len = content1.length() + 4;
    iov[1].iov_base = buffer2;
    iov[1].iov_len = 24;
    iov[2].iov_base = buffer3;
    iov[2].iov_len = content3.length();

    int i = 0;
    while (true)
    {
        // 改变header session内容
        sprintf(buffer2, "%0d", i++);
        writev(sock, iov, 3);
    }
}

// 一个thread 死命收啊啊啊
void recv_thread_func(int sock)
{
    char buffer[2048] = {0};
    while (true)
    {
        recv(sock, buffer, sizeof(buffer), 0);
    }
}

int main()
{
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    std::thread send(send_thread_func, sock);
    std::thread recv(recv_thread_func, sock);

    send.join();
    recv.join();

    return 0;
}