/*
 *  Created on: 2014-12-01 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <unordered_set>
using namespace std;

int main()
{
    unordered_set<int> test;
    test.max_load_factor(0.0);

    for (int i = 0; i < 1; ++i) {
        test.insert(i);
    }

    return 0;
}
