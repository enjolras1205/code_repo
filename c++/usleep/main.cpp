/*
 *  Created on: 2015-01-21 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <unistd.h>
using namespace std;

int main()
{
    while (true) {
        usleep(2500);
        cout<<"."<<endl;
    }
    return 0;
}
