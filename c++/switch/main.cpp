// 查看汇编码，与if else不同，不是一个一个的cmp，而是一个跳转表

void func() {
	;
}

void func1() {
	;
}

int main()
{
	int i = 0;
	switch (i) {
		case 0:
			func();
			break;
		case 1:
			func1();
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			break;
		case 7:
			break;
		case 8:
			break;
		case 9:
			break;
// 		case 109:
// 			break;
// 		case 110:
// 			break;
		default:
			func1();
			break;
	}

	return 0;
};
