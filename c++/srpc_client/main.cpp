#include <iostream>
#include <thread>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define PORT 11111
#define ADDRESS "192.168.44.115"
// 0050004c 80BYTE
// std::string send_hex_contain = "8181880e62656e63686d61726b5f7372706382046563686f831835653438623833323463626339666462663535353065653687183565343862383332346362633966646266353535306565367b7d";
// 011e004c8181880e62656e63686d61726b5f7372706382046563686f831835653438636132623465333137353934666537303662373587183565343863613262346533313735393466653730366237357b2261223a226161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161227d 
#define PACKET_SIZE 286
std::string send_hex_contain = "8181880e62656e63686d61726b5f7372706382046563686f831835653438636132623465333137353934666537303662373587183565343863613262346533313735393466653730366237357b2261223a226161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161616161227d";

void send_thread_func(int sock)
{
  //
  size_t len = send_hex_contain.length();
  char buffer[8192] = {0};
  std::string send_contain;
  for (int i = 0; i < len; i += 2)
  {
    std::string byte = send_hex_contain.substr(i, 2);
    char chr = (char)(int)strtol(byte.c_str(), nullptr, 16);
    send_contain.push_back(chr);
  }
  // 注意长度字节序为网络序
  uint16_t total_packet_len = htons(286);
  memcpy(buffer, &total_packet_len, 2);
  // header长度为固定的
  uint16_t header_len = htons(76);
  memcpy(buffer + 2, &header_len, 2);
  memcpy(buffer + 4, send_contain.c_str(), send_contain.length());
  while (true) {
    send(sock, buffer, PACKET_SIZE + 2, 0);
  }
}

// 一个thread 死命收啊啊啊
void recv_thread_func(int sock)
{
  char buffer[2048] = {0};
  while (true)
  {
    recv(sock, buffer, sizeof(buffer), 0);
  }
}

int main()
{
  int sock = 0, valread;
  struct sockaddr_in serv_addr;
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\n Socket creation error \n");
    return -1;
  }
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
  if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nConnection Failed \n");
    return -1;
  }
  std::thread send(send_thread_func, sock);
  std::thread recv(recv_thread_func, sock);

  send.join();
  recv.join();

  return 0;
}