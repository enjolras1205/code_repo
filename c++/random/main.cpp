/*
 *  Created on: 2015-01-29 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <limits>
using namespace std;

const unsigned int rand_max = numeric_limits<unsigned int>::max();
unsigned int seed = 0;

int rand_with_seed(unsigned int seed) {
    return (unsigned int)(seed * 1103515245 + 12345) % rand_max;  
}

int rand(void) {
    seed++;
    return rand_with_seed(seed);
}

int main()
{
    cout<<"rand():"<<rand()<<endl;
    cout<<"rand():"<<rand()<<endl;
    return 0;
}
