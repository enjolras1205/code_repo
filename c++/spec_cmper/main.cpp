#include <iostream>
#include <set>
using namespace std;

struct For_Cmp {
	void set_value(int i1, int i2) {
		m_i1 = i1;
		m_i2 = i2;
	}
	int m_i1;
	int m_i2;
	inline bool operator<=(const For_Cmp &cmper) const {
		return m_i1 <= cmper.m_i1;
	}

	friend ostream& operator<<(ostream &out, const For_Cmp &cmper) {
		out << "m_i1:" << cmper.m_i1 << " m_i2: "
			<< cmper.m_i2;
		return out;
	}
};

typedef set<For_Cmp, std::less_equal<For_Cmp> > Cmper_Set;

template<typename T>
void print_stl_contain(const T &stl_contain) {
	cout<<"stl contain:"<<endl;
	for (T::const_iterator it = stl_contain.begin(); it != stl_contain.end(); ++it)
	{
		cout<<*it<<endl;
	}
}

int main()
{
	Cmper_Set cmp_set;
	For_Cmp	cmp1;
	cmp1.set_value(1, 2);
	cmp_set.insert(cmp1);
	cmp1.set_value(1, 3);
	cmp_set.insert(cmp1);
	print_stl_contain(cmp_set);
	cmp_set.erase(cmp1);
	print_stl_contain(cmp_set);

}
