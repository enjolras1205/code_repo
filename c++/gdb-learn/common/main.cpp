#include <iostream>
#include <unistd.h>
using namespace std;

struct Test{
	int a;
	int b;
	int c;
	int d;
	int i;
};

void fun1(){
	Test test;
	int array[4] = {1, 2, 3, 4};
	for (int i = 0; i < 100; i++){
		int a = 100;
		int b = 100;
		int c = 100;
		sleep(1);
		int d = 100;
		test.a = a;
		test.b = b;
		test.c = c;
		test.d = d;	
		test.i = i;
	}
}

void fun2(){
	int i = 100;
	i++;
}

void fun3(){
	fun1();
	fun2();
}

void fun4(){
	cout<<"func"<<endl;
}

void printtest(const Test& test){
	cout<<"test.a = "<<test.a<<endl;
	cout<<"test.b = "<<test.b<<endl;
	cout<<"test.c = "<<test.c<<endl;
	cout<<"test.d = "<<test.d<<endl;
	cout<<"test.i = "<<test.i<<endl;
}

int main(){
   	fun3();
	return 0;
}
