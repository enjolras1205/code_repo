/*
 *  Created on: 2014-12-10 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <stdio.h>
#include <iostream>
#include "lib.h"
using namespace std;

typedef int (*fun)();

int test(void)
{
    int a = 0;
    int b = 0;
    a = 1;
    b = 2;
    return 0;
}

int main()
{
    int a = 0;
    fun funa = get_counter;
    fun funb = test;
    while (cin>>a) {
        cout<<get_counter()<<endl;
        cout<<get_counter_addr()<<endl;
        printf("%p\n", funa);
    }
    return 0;
}
