#include "lib.h"

int counter = 0;

int get_counter(void)
{
    return ++counter;
}

int *get_counter_addr(void)
{
    return &counter;
}
