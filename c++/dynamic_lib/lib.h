/*
 *  Created on: 2014-12-10 
 *      Author: enjolras1205@gmail.com 
 *
 */

#ifndef LIB_H_
#define LIB_H_

int get_counter(void);
int *get_counter_addr(void);

#endif
