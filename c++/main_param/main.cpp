/*
 *  Created on: 2015-01-05 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
    cout<<"argc nums:"<<argc<<endl;
    for (int i = 0; i < argc; ++i) {
        cout<<"argv"<<i<<":"<<argv[i]<<endl;
    }
    int input;
    while (cin>>input) {
        cout<<"input:"<<input<<endl;
    }
    return 0;
}
