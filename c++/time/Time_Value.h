// -*- C++ -*-
/*
 * aa.cpp
 *
 *  Created on: Mar 19, 2012
 *      Author: ChenLong
 *
 *  此类大部分代码源自ACE_Time_Value
 */

#ifndef TIME_VALUE_H_
#define TIME_VALUE_H_

#include <iostream>
#include <string>
#include <sys/time.h>

class Time_Value {
	suseconds_t const static ONE_SECOND_IN_USECS = 1000000;

	friend bool operator > (const Time_Value &tv1, const Time_Value &tv2);
	friend bool operator < (const Time_Value &tv1, const Time_Value &tv2);
	friend bool operator >= (const Time_Value &tv1, const Time_Value &tv2);
	friend bool operator <= (const Time_Value &tv1, const Time_Value &tv2);
	friend Time_Value operator - (const Time_Value &tv1, const Time_Value &tv2);
	friend Time_Value operator + (const Time_Value &tv1, const Time_Value &tv2);
	friend bool operator == (const Time_Value &tv1, const Time_Value &tv2);
	friend bool operator != (const Time_Value &tv1, const Time_Value &tv2);

public:
	static const Time_Value zero;
	static Time_Value gettimeofday();
	static int sleep(const Time_Value &tv);

	Time_Value(long sec = 0, long usec = 0);
	Time_Value(const struct timeval &t);
	Time_Value(const Time_Value &tv);

	void set(long sec, long usec) {	this->tv_.tv_sec = sec; this->tv_.tv_usec = usec; }
	inline void set(const timeval &t) { this->tv_ = t; }
	inline void set(const Time_Value &tv) { this->tv_ = tv.get_tv(); }

	inline long sec(void) const { return this->tv_.tv_sec; }
	inline void sec(long sec)  { this->tv_.tv_sec = sec; }
	inline long usec(void) const  { return this->tv_.tv_usec; }
	inline void usec(long usec)  { this->tv_.tv_usec = usec; }

	inline const timeval & get_tv(void) const { return this->tv_;	}

	Time_Value & operator += (const Time_Value &tv);
	Time_Value & operator -= (const Time_Value &tv);
	operator timespec() const;

	void normalize(void);

	void debug_dump(void) const;
	void debug_dump(char *str, size_t *sl);

private:
	timeval tv_;
};



#endif /* TIME_VALUE_H_ */
