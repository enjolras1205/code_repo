/*
 *  Created on: 2014-10-31 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <unordered_map>
#include <map>
using namespace std;

typedef map<int, int> HashMap;
int main()
{
    HashMap test;
    test[0] = 0;
    HashMap::iterator it = test.begin();
    for (int i = 0; i < 10000; ++i) {
        test[i] = i+1;
    }
    // test.erase(0);
    cout<<it->first<<endl<<it->second<<endl;
    for (; it != test.end(); ++it) {
        cout<<it->first<<endl;
    }
    return 0;
}
