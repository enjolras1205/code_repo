#include <signal.h>
#include <stdio.h>

void my_sigint_handler(int signum)
{
	printf("Received signal %d\n", signum);
} 

int main() 
{ 
    char choicestr[20]; 
    int choice; 
    while(1) 
    { 
        puts("1. Ignore control-c"); 
        puts("2. Custom handle control-c"); 
        puts("3. Use the default handle control-c"); 
        puts("4. Raise a SIGSEGV on myself."); 
        printf("Enter your choice: "); 
        fgets(choicestr, 20, stdin); 
        sscanf(choicestr, "%d", &choice); 
        switch(choice) 
        { 
        case 1: 
            signal(SIGINT, SIG_IGN); 
            break;
        case 2:
            signal(SIGINT, my_sigint_handler);
            break;
        case 3:
            signal(SIGINT, SIG_DFL);
            break;
        case 4:
            raise(SIGSEGV);
            break;
        default:
            signal(SIGINT, SIG_DFL);
            puts("default\n");
            break;
        }
    
    }
    return 0;
}
