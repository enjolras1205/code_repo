/*
 *  Created on: 2015-01-06 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <limits>
#include <stdio.h>
using namespace std;

const int max_size = std::numeric_limits<int>::digits10 + 1 /*sign*/ + 1 /*0-terminator*/;
std::string itos(int n)
{
   char buffer[max_size] = {0};
   snprintf(buffer, max_size, "%d", n);
   return std::string(buffer);
}

int main()
{
    int nums;
    string str;

    cout<<"please input a number"<<endl;
    while (cin>>nums) {
        cout<<"input number :"<<nums<<endl;
        cout<<"split it : ";
        str = itos(nums);
        for (string::iterator it = str.begin(); it != str.end(); ++it) {
            cout<<*it<<" ";
        }
        cout<<endl;
    }
    cout<<"input non number"<<endl;

    return 0;
}
