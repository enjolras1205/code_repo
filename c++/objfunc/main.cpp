// 仿函数

#include <iostream>
using namespace std;

template<class Cmper>
void print(int a, int b, Cmper *p) {
	if ((*p)()) {
		cout<<a;
	} else {
		cout<<b;
	}
	cout<<endl;
}

class compare_class
{
public:
	compare_class(bool b_value) : m_i(b_value){}
	bool operator()()const{return m_i;}
private:
	bool m_i;
};

int main()
{
	print(1, 2, &compare_class(false));
	return 0;
}
