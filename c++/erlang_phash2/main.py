
def atom_hash(s):
    h = 0
    g = 0
    for v in s:
        h = (h << 4) + ord(v)
        g = h & 0xf0000000
        if g:
            h ^= (g >> 24)
            h ^= g
    return h


for num in range(10):
    value = "test{0}@ubuntu".format(num)
    hash_val = atom_hash(value)
    print(value, hash_val)

for num in range(10):
    value = "p10445_user_info_logic{num}@p10445_user_info_logic{num}.".format(
        num=num)
    hash_val = atom_hash(value)
    print(value, hash_val)
