#!/bin/bash 
PTMALLOC="echo start of ptmalloc && ./ptmalloc_main"
TCMALLOC="echo start of tcmalloc && ./tcmalloc_main"
JEMALLOC="echo start of jemalloc && ./jemalloc_main"
TIME=10000000
THREAD=4

eval $PTMALLOC" 512 $TIME 1"
eval $TCMALLOC" 512 $TIME 1"
eval $JEMALLOC" 512 $TIME 1"

eval $PTMALLOC" 512 $TIME $THREAD"
eval $TCMALLOC" 512 $TIME $THREAD"
eval $JEMALLOC" 512 $TIME $THREAD"

eval $PTMALLOC" 10240 $TIME 1"
eval $TCMALLOC" 10240 $TIME 1"
eval $JEMALLOC" 10240 $TIME 1"

eval $PTMALLOC" 10240 $TIME $THREAD"
eval $TCMALLOC" 10240 $TIME $THREAD"
eval $JEMALLOC" 10240 $TIME $THREAD"
