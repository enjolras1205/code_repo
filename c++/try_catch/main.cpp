// 本质就是一个longjmp，所以后面的代码就不执行了，不处理异常会导致程序退出

#include <iostream>
using namespace std;

void func(void) {
	cout<<"before throw"<<endl;
	throw(1);
	cout<<"after throw"<<endl;
}

int main()
{
	try
	{
		func();
	}
	catch (...)
	{
		cout<<"catch sth"<<endl;
	}

	return 0;
}
