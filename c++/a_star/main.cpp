/*
 *  Created on: 2014-11-02 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <set>
using namespace std;

struct Path_Node {
    int x, y;
	int g_val, h_val;

	inline int f(void) const;

	bool operator==(const Path_Node &node) {
		return x == node.x && y == node.y;
	}

	class greater {
	public:
		inline bool operator()(const Path_Node *a, const Path_Node *b) {
			return (a->f()) > (b->f());
		}
	};

	bool operator<(const Path_Node &node) const {
		return f() < node.f();
	}
};

int Path_Node::f(void) const { return g_val + h_val; }

typedef std::set<Path_Node> Path_Node_Set;

int main()
{
    Path_Node node1;
    node1.x = 5;
    node1.y = 6;
    node1.g_val = 5;
    node1.h_val = 6;
    Path_Node node2;
    node2.x = 6;
    node2.y = 5;
    node2.g_val = 6;
    node2.h_val = 5;
    Path_Node_Set set;
    set.insert(node1);
    set.insert(node2);
    cout<<set.size()<<endl;
    set.erase(node2);
    cout<<set.size()<<endl;

    return 0;
}
