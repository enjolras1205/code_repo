#include <iostream>
#include <set>
using namespace std;

int main() 
{
    set<int> int_set;
    for (int i = 0; i < 100; ++i) {
        int_set.insert(i);
    }

    for (int i = 0; i < 100000; ++i) {
        for (auto it = int_set.begin(); it != int_set.end(); ++it) {
            // do nothing
        }
    }

    return 0;
}
