// ConsoleApplication.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <unistd.h>
using namespace std;

class Hello {
public:
    Hello();
    ~Hello();
private:
	int i;
};

Hello::Hello() {
    // cout << "hello1" << endl;
	i = 0x1234;
}

Hello::~Hello() {
    // cout << "hello2" << endl;
	// delete i;
}

class Base
{
public:
	Base() { 
		// cout <<"Base1"<<endl; 
	};
	~Base() { 
		// cout <<"Base2"<<endl; 
	};

    int i;
};

class Dx : public Base {
public:
	Dx() { 
//		cout <<"dx1"<<endl; 
	};
	~Dx() { 
		// cout <<"dx2"<<endl; 
	};

    int b;
    Hello c;
	int *d;
};

int main()
{
    Base* a = nullptr;
    a = new Dx();
	void *xx = &(a->i);
	xx = xx + 3;
	int *cc = (int*)xx;
	cout << std::hex << *cc << endl;
    // while (true) {
    //     a = new Dx();
    //     delete a;
	// 	// sleep(1);
    // }

    return 0;
}
