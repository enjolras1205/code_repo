/*
 *  Created on: 2014-11-22 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <array>
using namespace std;

typedef int ethr_sint32_t;
#define erts_aint32_t ethr_sint32_t 
#define ERTS_SSI_FLG_SLEEPING		(((erts_aint32_t) 1) << 0)
#define ERTS_SSI_FLG_POLL_SLEEPING 	(((erts_aint32_t) 1) << 1)
#define ERTS_SSI_FLG_TSE_SLEEPING 	(((erts_aint32_t) 1) << 2)
#define ERTS_SSI_FLG_WAITING		(((erts_aint32_t) 1) << 3)
#define ERTS_SSI_FLG_SUSPENDED	 	(((erts_aint32_t) 1) << 4)
#define ERTS_SSI_FLG_MSB_EXEC	 	(((erts_aint32_t) 1) << 5)

#define erts_atomic32_cmpxchg_acqb ethr_atomic32_cmpxchg_acqb

static inline ethr_sint32_t ETHR_ATMC32_FUNC__(cmpxchg_acqb)(ethr_atomic32_t *var, ethr_sint32_t val, ethr_sint32_t old_val)
{
    ethr_sint32_t res;
    res = (ethr_sint32_t) ETHR_NATMC32_FUNC__(cmpxchg_mb)(var, (ETHR_NAINT32_T__) val, (ETHR_NAINT32_T__) old_val);
    return res;
}

static erts_aint32_t sched_prep_cont_spin_wait() 
{
    int ssi_flags = 0;
    int oflgs;
    int nflgs = (ERTS_SSI_FLG_SLEEPING
			   | ERTS_SSI_FLG_WAITING);
    int xflgs = ERTS_SSI_FLG_WAITING;

    do {
		oflgs = erts_atomic32_cmpxchg_acqb(&ssi_flags, nflgs, xflgs);
		if (oflgs == xflgs)
	    	return nflgs;
		xflgs = oflgs;
		nflgs |= oflgs & (ERTS_SSI_FLG_SUSPENDED|ERTS_SSI_FLG_MSB_EXEC);
    } while (oflgs & ERTS_SSI_FLG_WAITING);
    return oflgs;
}


int main()
{
    int a = sched_prep_cont_spin_wait();
    int b = sched_prep_cont_spin_wait();
    return 0;
}
