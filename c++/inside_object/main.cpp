/*
 *  Created on: 2014-10-31 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
using namespace std;

class A {
    public:
        int a;
        virtual void v(){};
};

class B : public virtual A {
    public:
        int b;
        virtual void w(){};
};

class C : public virtual A {
    public:
        int c;
        virtual void x(){};
};

class D : public B, public C {
    public:
        int d;
        virtual void y(){};
};

int main()
{
    A a;
    B b;
    C c;
    D d;
    cout<<sizeof(a)<<endl;
    cout<<sizeof(b)<<endl;
    cout<<sizeof(c)<<endl;
    cout<<sizeof(d)<<endl;
    return 0;
}
