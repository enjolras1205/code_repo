#!/bin/sh
 
if test $# -ne 3; then
    echo "Usage: `basename $0 .sh` program core cmd" 1>$2
    echo "For example: `basename $0 .sh` ./main core.1111 bt" 1>&2
    exit 1
fi
 
if test ! -r $1; then
    echo "Process $1 not found." 1>&2
    exit 1
fi
 
result=""
GDB=${GDB:-/usr/bin/gdb}
# Run GDB, strip out unwanted noise.
result=`$GDB --quiet -nx $1 $2 <<EOF 2>&1
$3
EOF`
echo "$result" | egrep -A 1000 -e "^\(gdb\)" | egrep -B 1000 -e "^\(gdb\)"
