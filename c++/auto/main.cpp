/*
 *  Created on: 2014-12-24 
 *      Author: enjolras1205@gmail.com 
 *
 */

#include <iostream>
#include <vector>
using namespace std;

struct Test {
    Test(void) {
        cout<<"test construct"<<endl;
    }
    ~Test(void) {
        cout<<"test deconstruct"<<endl;
    }
};

typedef vector<Test> TVec;

int main()
{
    TVec vec;
    vec.push_back(Test());
    auto it = vec.begin();

    for (auto &it : vec) {
    }

    return 0;
}
