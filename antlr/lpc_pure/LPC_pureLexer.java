// $ANTLR 3.5.2 LPC_pure.g 2015-03-17 10:55:45

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class LPC_pureLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int T__98=98;
	public static final int T__99=99;
	public static final int T__100=100;
	public static final int T__101=101;
	public static final int T__102=102;
	public static final int T__103=103;
	public static final int T__104=104;
	public static final int T__105=105;
	public static final int T__106=106;
	public static final int T__107=107;
	public static final int T__108=108;
	public static final int T__109=109;
	public static final int T__110=110;
	public static final int T__111=111;
	public static final int T__112=112;
	public static final int T__113=113;
	public static final int T__114=114;
	public static final int T__115=115;
	public static final int T__116=116;
	public static final int T__117=117;
	public static final int T__118=118;
	public static final int T__119=119;
	public static final int T__120=120;
	public static final int AT_STRING=4;
	public static final int CHARACTER_LITERAL=5;
	public static final int COMMENT=6;
	public static final int DECIMAL_LITERAL=7;
	public static final int EscapeSequence=8;
	public static final int Exponent=9;
	public static final int FloatTypeSuffix=10;
	public static final int HEX_LITERAL=11;
	public static final int HexDigit=12;
	public static final int IDENTIFIER=13;
	public static final int IntegerTypeSuffix=14;
	public static final int LESSTHAN=15;
	public static final int LETTER=16;
	public static final int LINE_COMMAND=17;
	public static final int LINE_COMMENT=18;
	public static final int NO_MEAN=19;
	public static final int OctalEscape=20;
	public static final int STRING_LITERAL=21;
	public static final int UnicodeEscape=22;
	public static final int WS=23;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public LPC_pureLexer() {} 
	public LPC_pureLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public LPC_pureLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "LPC_pure.g"; }

	// $ANTLR start "T__24"
	public final void mT__24() throws RecognitionException {
		try {
			int _type = T__24;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:2:7: ( '!' )
			// LPC_pure.g:2:9: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__24"

	// $ANTLR start "T__25"
	public final void mT__25() throws RecognitionException {
		try {
			int _type = T__25;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:3:7: ( '!=' )
			// LPC_pure.g:3:9: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__25"

	// $ANTLR start "T__26"
	public final void mT__26() throws RecognitionException {
		try {
			int _type = T__26;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:4:7: ( '#' )
			// LPC_pure.g:4:9: '#'
			{
			match('#'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__26"

	// $ANTLR start "T__27"
	public final void mT__27() throws RecognitionException {
		try {
			int _type = T__27;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:5:7: ( '#include' )
			// LPC_pure.g:5:9: '#include'
			{
			match("#include"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__27"

	// $ANTLR start "T__28"
	public final void mT__28() throws RecognitionException {
		try {
			int _type = T__28;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:6:7: ( '#pragma' )
			// LPC_pure.g:6:9: '#pragma'
			{
			match("#pragma"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__28"

	// $ANTLR start "T__29"
	public final void mT__29() throws RecognitionException {
		try {
			int _type = T__29;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:7:7: ( '#undef' )
			// LPC_pure.g:7:9: '#undef'
			{
			match("#undef"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__29"

	// $ANTLR start "T__30"
	public final void mT__30() throws RecognitionException {
		try {
			int _type = T__30;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:8:7: ( '%' )
			// LPC_pure.g:8:9: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__30"

	// $ANTLR start "T__31"
	public final void mT__31() throws RecognitionException {
		try {
			int _type = T__31;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:9:7: ( '%=' )
			// LPC_pure.g:9:9: '%='
			{
			match("%="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__31"

	// $ANTLR start "T__32"
	public final void mT__32() throws RecognitionException {
		try {
			int _type = T__32;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:10:7: ( '&&' )
			// LPC_pure.g:10:9: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__32"

	// $ANTLR start "T__33"
	public final void mT__33() throws RecognitionException {
		try {
			int _type = T__33;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:11:7: ( '&' )
			// LPC_pure.g:11:9: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__33"

	// $ANTLR start "T__34"
	public final void mT__34() throws RecognitionException {
		try {
			int _type = T__34;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:12:7: ( '&=' )
			// LPC_pure.g:12:9: '&='
			{
			match("&="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__34"

	// $ANTLR start "T__35"
	public final void mT__35() throws RecognitionException {
		try {
			int _type = T__35;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:13:7: ( '(' )
			// LPC_pure.g:13:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__35"

	// $ANTLR start "T__36"
	public final void mT__36() throws RecognitionException {
		try {
			int _type = T__36;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:14:7: ( ')' )
			// LPC_pure.g:14:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__36"

	// $ANTLR start "T__37"
	public final void mT__37() throws RecognitionException {
		try {
			int _type = T__37;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:15:7: ( '*' )
			// LPC_pure.g:15:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__37"

	// $ANTLR start "T__38"
	public final void mT__38() throws RecognitionException {
		try {
			int _type = T__38;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:16:7: ( '*=' )
			// LPC_pure.g:16:9: '*='
			{
			match("*="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__38"

	// $ANTLR start "T__39"
	public final void mT__39() throws RecognitionException {
		try {
			int _type = T__39;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:17:7: ( '+' )
			// LPC_pure.g:17:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__39"

	// $ANTLR start "T__40"
	public final void mT__40() throws RecognitionException {
		try {
			int _type = T__40;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:18:7: ( '++' )
			// LPC_pure.g:18:9: '++'
			{
			match("++"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__40"

	// $ANTLR start "T__41"
	public final void mT__41() throws RecognitionException {
		try {
			int _type = T__41;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:19:7: ( '+=' )
			// LPC_pure.g:19:9: '+='
			{
			match("+="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__41"

	// $ANTLR start "T__42"
	public final void mT__42() throws RecognitionException {
		try {
			int _type = T__42;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:20:7: ( ',' )
			// LPC_pure.g:20:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__42"

	// $ANTLR start "T__43"
	public final void mT__43() throws RecognitionException {
		try {
			int _type = T__43;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:21:7: ( '-' )
			// LPC_pure.g:21:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__43"

	// $ANTLR start "T__44"
	public final void mT__44() throws RecognitionException {
		try {
			int _type = T__44;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:22:7: ( '--' )
			// LPC_pure.g:22:9: '--'
			{
			match("--"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__44"

	// $ANTLR start "T__45"
	public final void mT__45() throws RecognitionException {
		try {
			int _type = T__45;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:23:7: ( '-=' )
			// LPC_pure.g:23:9: '-='
			{
			match("-="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__45"

	// $ANTLR start "T__46"
	public final void mT__46() throws RecognitionException {
		try {
			int _type = T__46;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:24:7: ( '->' )
			// LPC_pure.g:24:9: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__46"

	// $ANTLR start "T__47"
	public final void mT__47() throws RecognitionException {
		try {
			int _type = T__47;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:25:7: ( '.' )
			// LPC_pure.g:25:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__47"

	// $ANTLR start "T__48"
	public final void mT__48() throws RecognitionException {
		try {
			int _type = T__48;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:26:7: ( '..' )
			// LPC_pure.g:26:9: '..'
			{
			match(".."); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__48"

	// $ANTLR start "T__49"
	public final void mT__49() throws RecognitionException {
		try {
			int _type = T__49;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:27:7: ( '...' )
			// LPC_pure.g:27:9: '...'
			{
			match("..."); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__49"

	// $ANTLR start "T__50"
	public final void mT__50() throws RecognitionException {
		try {
			int _type = T__50;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:28:7: ( '/' )
			// LPC_pure.g:28:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__50"

	// $ANTLR start "T__51"
	public final void mT__51() throws RecognitionException {
		try {
			int _type = T__51;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:29:7: ( '/=' )
			// LPC_pure.g:29:9: '/='
			{
			match("/="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__51"

	// $ANTLR start "T__52"
	public final void mT__52() throws RecognitionException {
		try {
			int _type = T__52;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:30:7: ( ':' )
			// LPC_pure.g:30:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__52"

	// $ANTLR start "T__53"
	public final void mT__53() throws RecognitionException {
		try {
			int _type = T__53;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:31:7: ( '::' )
			// LPC_pure.g:31:9: '::'
			{
			match("::"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__53"

	// $ANTLR start "T__54"
	public final void mT__54() throws RecognitionException {
		try {
			int _type = T__54;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:32:7: ( ';' )
			// LPC_pure.g:32:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__54"

	// $ANTLR start "T__55"
	public final void mT__55() throws RecognitionException {
		try {
			int _type = T__55;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:33:7: ( '<<' )
			// LPC_pure.g:33:9: '<<'
			{
			match("<<"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__55"

	// $ANTLR start "T__56"
	public final void mT__56() throws RecognitionException {
		try {
			int _type = T__56;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:34:7: ( '<<=' )
			// LPC_pure.g:34:9: '<<='
			{
			match("<<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__56"

	// $ANTLR start "T__57"
	public final void mT__57() throws RecognitionException {
		try {
			int _type = T__57;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:35:7: ( '<=' )
			// LPC_pure.g:35:9: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__57"

	// $ANTLR start "T__58"
	public final void mT__58() throws RecognitionException {
		try {
			int _type = T__58;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:36:7: ( '=' )
			// LPC_pure.g:36:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__58"

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:37:7: ( '==' )
			// LPC_pure.g:37:9: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "T__60"
	public final void mT__60() throws RecognitionException {
		try {
			int _type = T__60;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:38:7: ( '>' )
			// LPC_pure.g:38:9: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__60"

	// $ANTLR start "T__61"
	public final void mT__61() throws RecognitionException {
		try {
			int _type = T__61;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:39:7: ( '>=' )
			// LPC_pure.g:39:9: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__61"

	// $ANTLR start "T__62"
	public final void mT__62() throws RecognitionException {
		try {
			int _type = T__62;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:40:7: ( '>>' )
			// LPC_pure.g:40:9: '>>'
			{
			match(">>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__62"

	// $ANTLR start "T__63"
	public final void mT__63() throws RecognitionException {
		try {
			int _type = T__63;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:41:7: ( '>>=' )
			// LPC_pure.g:41:9: '>>='
			{
			match(">>="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__63"

	// $ANTLR start "T__64"
	public final void mT__64() throws RecognitionException {
		try {
			int _type = T__64;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:42:7: ( '?' )
			// LPC_pure.g:42:9: '?'
			{
			match('?'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__64"

	// $ANTLR start "T__65"
	public final void mT__65() throws RecognitionException {
		try {
			int _type = T__65;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:43:7: ( '[' )
			// LPC_pure.g:43:9: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__65"

	// $ANTLR start "T__66"
	public final void mT__66() throws RecognitionException {
		try {
			int _type = T__66;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:44:7: ( ']' )
			// LPC_pure.g:44:9: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__66"

	// $ANTLR start "T__67"
	public final void mT__67() throws RecognitionException {
		try {
			int _type = T__67;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:45:7: ( '^' )
			// LPC_pure.g:45:9: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__67"

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:46:7: ( '^=' )
			// LPC_pure.g:46:9: '^='
			{
			match("^="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:47:7: ( 'auto' )
			// LPC_pure.g:47:9: 'auto'
			{
			match("auto"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "T__70"
	public final void mT__70() throws RecognitionException {
		try {
			int _type = T__70;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:48:7: ( 'break' )
			// LPC_pure.g:48:9: 'break'
			{
			match("break"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__70"

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:49:7: ( 'case' )
			// LPC_pure.g:49:9: 'case'
			{
			match("case"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:50:7: ( 'char' )
			// LPC_pure.g:50:9: 'char'
			{
			match("char"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:51:7: ( 'class' )
			// LPC_pure.g:51:9: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:52:7: ( 'const' )
			// LPC_pure.g:52:9: 'const'
			{
			match("const"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:53:7: ( 'continue' )
			// LPC_pure.g:53:9: 'continue'
			{
			match("continue"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:54:7: ( 'default' )
			// LPC_pure.g:54:9: 'default'
			{
			match("default"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "T__77"
	public final void mT__77() throws RecognitionException {
		try {
			int _type = T__77;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:55:7: ( 'define' )
			// LPC_pure.g:55:9: 'define'
			{
			match("define"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__77"

	// $ANTLR start "T__78"
	public final void mT__78() throws RecognitionException {
		try {
			int _type = T__78;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:56:7: ( 'defined' )
			// LPC_pure.g:56:9: 'defined'
			{
			match("defined"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__78"

	// $ANTLR start "T__79"
	public final void mT__79() throws RecognitionException {
		try {
			int _type = T__79;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:57:7: ( 'do' )
			// LPC_pure.g:57:9: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__79"

	// $ANTLR start "T__80"
	public final void mT__80() throws RecognitionException {
		try {
			int _type = T__80;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:58:7: ( 'else' )
			// LPC_pure.g:58:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__80"

	// $ANTLR start "T__81"
	public final void mT__81() throws RecognitionException {
		try {
			int _type = T__81;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:59:7: ( 'enum' )
			// LPC_pure.g:59:9: 'enum'
			{
			match("enum"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__81"

	// $ANTLR start "T__82"
	public final void mT__82() throws RecognitionException {
		try {
			int _type = T__82;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:60:7: ( 'extern' )
			// LPC_pure.g:60:9: 'extern'
			{
			match("extern"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__82"

	// $ANTLR start "T__83"
	public final void mT__83() throws RecognitionException {
		try {
			int _type = T__83;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:61:7: ( 'float' )
			// LPC_pure.g:61:9: 'float'
			{
			match("float"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__83"

	// $ANTLR start "T__84"
	public final void mT__84() throws RecognitionException {
		try {
			int _type = T__84;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:62:7: ( 'for' )
			// LPC_pure.g:62:9: 'for'
			{
			match("for"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__84"

	// $ANTLR start "T__85"
	public final void mT__85() throws RecognitionException {
		try {
			int _type = T__85;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:63:7: ( 'foreach' )
			// LPC_pure.g:63:9: 'foreach'
			{
			match("foreach"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__85"

	// $ANTLR start "T__86"
	public final void mT__86() throws RecognitionException {
		try {
			int _type = T__86;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:64:7: ( 'function' )
			// LPC_pure.g:64:9: 'function'
			{
			match("function"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__86"

	// $ANTLR start "T__87"
	public final void mT__87() throws RecognitionException {
		try {
			int _type = T__87;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:65:7: ( 'goto' )
			// LPC_pure.g:65:9: 'goto'
			{
			match("goto"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__87"

	// $ANTLR start "T__88"
	public final void mT__88() throws RecognitionException {
		try {
			int _type = T__88;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:66:7: ( 'if' )
			// LPC_pure.g:66:9: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__88"

	// $ANTLR start "T__89"
	public final void mT__89() throws RecognitionException {
		try {
			int _type = T__89;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:67:7: ( 'in' )
			// LPC_pure.g:67:9: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__89"

	// $ANTLR start "T__90"
	public final void mT__90() throws RecognitionException {
		try {
			int _type = T__90;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:68:7: ( 'inherit' )
			// LPC_pure.g:68:9: 'inherit'
			{
			match("inherit"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__90"

	// $ANTLR start "T__91"
	public final void mT__91() throws RecognitionException {
		try {
			int _type = T__91;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:69:7: ( 'int' )
			// LPC_pure.g:69:9: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__91"

	// $ANTLR start "T__92"
	public final void mT__92() throws RecognitionException {
		try {
			int _type = T__92;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:70:7: ( 'mapping' )
			// LPC_pure.g:70:9: 'mapping'
			{
			match("mapping"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__92"

	// $ANTLR start "T__93"
	public final void mT__93() throws RecognitionException {
		try {
			int _type = T__93;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:71:7: ( 'mixed' )
			// LPC_pure.g:71:9: 'mixed'
			{
			match("mixed"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__93"

	// $ANTLR start "T__94"
	public final void mT__94() throws RecognitionException {
		try {
			int _type = T__94;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:72:7: ( 'nomask' )
			// LPC_pure.g:72:9: 'nomask'
			{
			match("nomask"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__94"

	// $ANTLR start "T__95"
	public final void mT__95() throws RecognitionException {
		try {
			int _type = T__95;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:73:7: ( 'nosave' )
			// LPC_pure.g:73:9: 'nosave'
			{
			match("nosave"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__95"

	// $ANTLR start "T__96"
	public final void mT__96() throws RecognitionException {
		try {
			int _type = T__96;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:74:7: ( 'object' )
			// LPC_pure.g:74:9: 'object'
			{
			match("object"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__96"

	// $ANTLR start "T__97"
	public final void mT__97() throws RecognitionException {
		try {
			int _type = T__97;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:75:7: ( 'private' )
			// LPC_pure.g:75:9: 'private'
			{
			match("private"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__97"

	// $ANTLR start "T__98"
	public final void mT__98() throws RecognitionException {
		try {
			int _type = T__98;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:76:7: ( 'protected' )
			// LPC_pure.g:76:9: 'protected'
			{
			match("protected"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__98"

	// $ANTLR start "T__99"
	public final void mT__99() throws RecognitionException {
		try {
			int _type = T__99;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:77:7: ( 'public' )
			// LPC_pure.g:77:9: 'public'
			{
			match("public"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__99"

	// $ANTLR start "T__100"
	public final void mT__100() throws RecognitionException {
		try {
			int _type = T__100;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:78:8: ( 'register' )
			// LPC_pure.g:78:10: 'register'
			{
			match("register"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__100"

	// $ANTLR start "T__101"
	public final void mT__101() throws RecognitionException {
		try {
			int _type = T__101;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:79:8: ( 'return' )
			// LPC_pure.g:79:10: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__101"

	// $ANTLR start "T__102"
	public final void mT__102() throws RecognitionException {
		try {
			int _type = T__102;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:80:8: ( 'signed' )
			// LPC_pure.g:80:10: 'signed'
			{
			match("signed"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__102"

	// $ANTLR start "T__103"
	public final void mT__103() throws RecognitionException {
		try {
			int _type = T__103;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:81:8: ( 'sizeof' )
			// LPC_pure.g:81:10: 'sizeof'
			{
			match("sizeof"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__103"

	// $ANTLR start "T__104"
	public final void mT__104() throws RecognitionException {
		try {
			int _type = T__104;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:82:8: ( 'static' )
			// LPC_pure.g:82:10: 'static'
			{
			match("static"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__104"

	// $ANTLR start "T__105"
	public final void mT__105() throws RecognitionException {
		try {
			int _type = T__105;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:83:8: ( 'string' )
			// LPC_pure.g:83:10: 'string'
			{
			match("string"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__105"

	// $ANTLR start "T__106"
	public final void mT__106() throws RecognitionException {
		try {
			int _type = T__106;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:84:8: ( 'struct' )
			// LPC_pure.g:84:10: 'struct'
			{
			match("struct"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__106"

	// $ANTLR start "T__107"
	public final void mT__107() throws RecognitionException {
		try {
			int _type = T__107;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:85:8: ( 'switch' )
			// LPC_pure.g:85:10: 'switch'
			{
			match("switch"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__107"

	// $ANTLR start "T__108"
	public final void mT__108() throws RecognitionException {
		try {
			int _type = T__108;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:86:8: ( 'typedef' )
			// LPC_pure.g:86:10: 'typedef'
			{
			match("typedef"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__108"

	// $ANTLR start "T__109"
	public final void mT__109() throws RecognitionException {
		try {
			int _type = T__109;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:87:8: ( 'union' )
			// LPC_pure.g:87:10: 'union'
			{
			match("union"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__109"

	// $ANTLR start "T__110"
	public final void mT__110() throws RecognitionException {
		try {
			int _type = T__110;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:88:8: ( 'unsigned' )
			// LPC_pure.g:88:10: 'unsigned'
			{
			match("unsigned"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__110"

	// $ANTLR start "T__111"
	public final void mT__111() throws RecognitionException {
		try {
			int _type = T__111;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:89:8: ( 'varargs' )
			// LPC_pure.g:89:10: 'varargs'
			{
			match("varargs"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__111"

	// $ANTLR start "T__112"
	public final void mT__112() throws RecognitionException {
		try {
			int _type = T__112;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:90:8: ( 'void' )
			// LPC_pure.g:90:10: 'void'
			{
			match("void"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__112"

	// $ANTLR start "T__113"
	public final void mT__113() throws RecognitionException {
		try {
			int _type = T__113;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:91:8: ( 'volatile' )
			// LPC_pure.g:91:10: 'volatile'
			{
			match("volatile"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__113"

	// $ANTLR start "T__114"
	public final void mT__114() throws RecognitionException {
		try {
			int _type = T__114;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:92:8: ( 'while' )
			// LPC_pure.g:92:10: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__114"

	// $ANTLR start "T__115"
	public final void mT__115() throws RecognitionException {
		try {
			int _type = T__115;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:93:8: ( '{' )
			// LPC_pure.g:93:10: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__115"

	// $ANTLR start "T__116"
	public final void mT__116() throws RecognitionException {
		try {
			int _type = T__116;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:94:8: ( '|' )
			// LPC_pure.g:94:10: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__116"

	// $ANTLR start "T__117"
	public final void mT__117() throws RecognitionException {
		try {
			int _type = T__117;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:95:8: ( '|=' )
			// LPC_pure.g:95:10: '|='
			{
			match("|="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__117"

	// $ANTLR start "T__118"
	public final void mT__118() throws RecognitionException {
		try {
			int _type = T__118;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:96:8: ( '||' )
			// LPC_pure.g:96:10: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__118"

	// $ANTLR start "T__119"
	public final void mT__119() throws RecognitionException {
		try {
			int _type = T__119;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:97:8: ( '}' )
			// LPC_pure.g:97:10: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__119"

	// $ANTLR start "T__120"
	public final void mT__120() throws RecognitionException {
		try {
			int _type = T__120;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:98:8: ( '~' )
			// LPC_pure.g:98:10: '~'
			{
			match('~'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__120"

	// $ANTLR start "IDENTIFIER"
	public final void mIDENTIFIER() throws RecognitionException {
		try {
			int _type = IDENTIFIER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:563:2: ( LETTER ( LETTER | '0' .. '9' )* )
			// LPC_pure.g:563:4: LETTER ( LETTER | '0' .. '9' )*
			{
			mLETTER(); 

			// LPC_pure.g:563:11: ( LETTER | '0' .. '9' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0=='$'||(LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// LPC_pure.g:
					{
					if ( input.LA(1)=='$'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IDENTIFIER"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// LPC_pure.g:569:2: ( '$' | 'A' .. 'Z' | 'a' .. 'z' | '_' )
			// LPC_pure.g:
			{
			if ( input.LA(1)=='$'||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "CHARACTER_LITERAL"
	public final void mCHARACTER_LITERAL() throws RecognitionException {
		try {
			int _type = CHARACTER_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:575:5: ( '\\'' ( EscapeSequence |~ ( '\\'' | '\\\\' ) ) '\\'' )
			// LPC_pure.g:575:9: '\\'' ( EscapeSequence |~ ( '\\'' | '\\\\' ) ) '\\''
			{
			match('\''); 
			// LPC_pure.g:575:14: ( EscapeSequence |~ ( '\\'' | '\\\\' ) )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='\\') ) {
				alt2=1;
			}
			else if ( ((LA2_0 >= '\u0000' && LA2_0 <= '&')||(LA2_0 >= '(' && LA2_0 <= '[')||(LA2_0 >= ']' && LA2_0 <= '\uFFFF')) ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// LPC_pure.g:575:16: EscapeSequence
					{
					mEscapeSequence(); 

					}
					break;
				case 2 :
					// LPC_pure.g:575:33: ~ ( '\\'' | '\\\\' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CHARACTER_LITERAL"

	// $ANTLR start "STRING_LITERAL"
	public final void mSTRING_LITERAL() throws RecognitionException {
		try {
			int _type = STRING_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:579:5: ( '\"' ( EscapeSequence |~ ( '\\\\' | '\"' ) )* '\"' )
			// LPC_pure.g:579:8: '\"' ( EscapeSequence |~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// LPC_pure.g:579:12: ( EscapeSequence |~ ( '\\\\' | '\"' ) )*
			loop3:
			while (true) {
				int alt3=3;
				int LA3_0 = input.LA(1);
				if ( (LA3_0=='\\') ) {
					alt3=1;
				}
				else if ( ((LA3_0 >= '\u0000' && LA3_0 <= '!')||(LA3_0 >= '#' && LA3_0 <= '[')||(LA3_0 >= ']' && LA3_0 <= '\uFFFF')) ) {
					alt3=2;
				}

				switch (alt3) {
				case 1 :
					// LPC_pure.g:579:14: EscapeSequence
					{
					mEscapeSequence(); 

					}
					break;
				case 2 :
					// LPC_pure.g:579:31: ~ ( '\\\\' | '\"' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING_LITERAL"

	// $ANTLR start "AT_STRING"
	public final void mAT_STRING() throws RecognitionException {
		try {
			int _type = AT_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:583:4: ( '@LONG' ( options {greedy=false; } : . )* 'LONG' | '@W_CODE' ( options {greedy=false; } : . )* 'W_CODE' | '@CODE' ( options {greedy=false; } : . )* 'CODE' | '@long' ( options {greedy=false; } : . )* 'long' | '@HELP' ( options {greedy=false; } : . )* 'HELP' | '@Help' ( options {greedy=false; } : . )* 'Help' | '@TEXT' ( options {greedy=false; } : . )* 'TEXT' | '@ROOM_CODE' ( options {greedy=false; } : . )* 'ROOM_CODE' )
			int alt12=8;
			int LA12_0 = input.LA(1);
			if ( (LA12_0=='@') ) {
				switch ( input.LA(2) ) {
				case 'L':
					{
					alt12=1;
					}
					break;
				case 'W':
					{
					alt12=2;
					}
					break;
				case 'C':
					{
					alt12=3;
					}
					break;
				case 'l':
					{
					alt12=4;
					}
					break;
				case 'H':
					{
					int LA12_6 = input.LA(3);
					if ( (LA12_6=='E') ) {
						alt12=5;
					}
					else if ( (LA12_6=='e') ) {
						alt12=6;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 12, 6, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

					}
					break;
				case 'T':
					{
					alt12=7;
					}
					break;
				case 'R':
					{
					alt12=8;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}

			switch (alt12) {
				case 1 :
					// LPC_pure.g:583:6: '@LONG' ( options {greedy=false; } : . )* 'LONG'
					{
					match("@LONG"); 

					// LPC_pure.g:583:14: ( options {greedy=false; } : . )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( (LA4_0=='L') ) {
							int LA4_1 = input.LA(2);
							if ( (LA4_1=='O') ) {
								int LA4_3 = input.LA(3);
								if ( (LA4_3=='N') ) {
									int LA4_4 = input.LA(4);
									if ( (LA4_4=='G') ) {
										alt4=2;
									}
									else if ( ((LA4_4 >= '\u0000' && LA4_4 <= 'F')||(LA4_4 >= 'H' && LA4_4 <= '\uFFFF')) ) {
										alt4=1;
									}

								}
								else if ( ((LA4_3 >= '\u0000' && LA4_3 <= 'M')||(LA4_3 >= 'O' && LA4_3 <= '\uFFFF')) ) {
									alt4=1;
								}

							}
							else if ( ((LA4_1 >= '\u0000' && LA4_1 <= 'N')||(LA4_1 >= 'P' && LA4_1 <= '\uFFFF')) ) {
								alt4=1;
							}

						}
						else if ( ((LA4_0 >= '\u0000' && LA4_0 <= 'K')||(LA4_0 >= 'M' && LA4_0 <= '\uFFFF')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// LPC_pure.g:583:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop4;
						}
					}

					match("LONG"); 

					}
					break;
				case 2 :
					// LPC_pure.g:584:6: '@W_CODE' ( options {greedy=false; } : . )* 'W_CODE'
					{
					match("@W_CODE"); 

					// LPC_pure.g:584:16: ( options {greedy=false; } : . )*
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( (LA5_0=='W') ) {
							int LA5_1 = input.LA(2);
							if ( (LA5_1=='_') ) {
								int LA5_3 = input.LA(3);
								if ( (LA5_3=='C') ) {
									int LA5_4 = input.LA(4);
									if ( (LA5_4=='O') ) {
										int LA5_5 = input.LA(5);
										if ( (LA5_5=='D') ) {
											int LA5_6 = input.LA(6);
											if ( (LA5_6=='E') ) {
												alt5=2;
											}
											else if ( ((LA5_6 >= '\u0000' && LA5_6 <= 'D')||(LA5_6 >= 'F' && LA5_6 <= '\uFFFF')) ) {
												alt5=1;
											}

										}
										else if ( ((LA5_5 >= '\u0000' && LA5_5 <= 'C')||(LA5_5 >= 'E' && LA5_5 <= '\uFFFF')) ) {
											alt5=1;
										}

									}
									else if ( ((LA5_4 >= '\u0000' && LA5_4 <= 'N')||(LA5_4 >= 'P' && LA5_4 <= '\uFFFF')) ) {
										alt5=1;
									}

								}
								else if ( ((LA5_3 >= '\u0000' && LA5_3 <= 'B')||(LA5_3 >= 'D' && LA5_3 <= '\uFFFF')) ) {
									alt5=1;
								}

							}
							else if ( ((LA5_1 >= '\u0000' && LA5_1 <= '^')||(LA5_1 >= '`' && LA5_1 <= '\uFFFF')) ) {
								alt5=1;
							}

						}
						else if ( ((LA5_0 >= '\u0000' && LA5_0 <= 'V')||(LA5_0 >= 'X' && LA5_0 <= '\uFFFF')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// LPC_pure.g:584:44: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop5;
						}
					}

					match("W_CODE"); 

					}
					break;
				case 3 :
					// LPC_pure.g:585:6: '@CODE' ( options {greedy=false; } : . )* 'CODE'
					{
					match("@CODE"); 

					// LPC_pure.g:585:14: ( options {greedy=false; } : . )*
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( (LA6_0=='C') ) {
							int LA6_1 = input.LA(2);
							if ( (LA6_1=='O') ) {
								int LA6_3 = input.LA(3);
								if ( (LA6_3=='D') ) {
									int LA6_4 = input.LA(4);
									if ( (LA6_4=='E') ) {
										alt6=2;
									}
									else if ( ((LA6_4 >= '\u0000' && LA6_4 <= 'D')||(LA6_4 >= 'F' && LA6_4 <= '\uFFFF')) ) {
										alt6=1;
									}

								}
								else if ( ((LA6_3 >= '\u0000' && LA6_3 <= 'C')||(LA6_3 >= 'E' && LA6_3 <= '\uFFFF')) ) {
									alt6=1;
								}

							}
							else if ( ((LA6_1 >= '\u0000' && LA6_1 <= 'N')||(LA6_1 >= 'P' && LA6_1 <= '\uFFFF')) ) {
								alt6=1;
							}

						}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= 'B')||(LA6_0 >= 'D' && LA6_0 <= '\uFFFF')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// LPC_pure.g:585:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop6;
						}
					}

					match("CODE"); 

					}
					break;
				case 4 :
					// LPC_pure.g:586:6: '@long' ( options {greedy=false; } : . )* 'long'
					{
					match("@long"); 

					// LPC_pure.g:586:14: ( options {greedy=false; } : . )*
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( (LA7_0=='l') ) {
							int LA7_1 = input.LA(2);
							if ( (LA7_1=='o') ) {
								int LA7_3 = input.LA(3);
								if ( (LA7_3=='n') ) {
									int LA7_4 = input.LA(4);
									if ( (LA7_4=='g') ) {
										alt7=2;
									}
									else if ( ((LA7_4 >= '\u0000' && LA7_4 <= 'f')||(LA7_4 >= 'h' && LA7_4 <= '\uFFFF')) ) {
										alt7=1;
									}

								}
								else if ( ((LA7_3 >= '\u0000' && LA7_3 <= 'm')||(LA7_3 >= 'o' && LA7_3 <= '\uFFFF')) ) {
									alt7=1;
								}

							}
							else if ( ((LA7_1 >= '\u0000' && LA7_1 <= 'n')||(LA7_1 >= 'p' && LA7_1 <= '\uFFFF')) ) {
								alt7=1;
							}

						}
						else if ( ((LA7_0 >= '\u0000' && LA7_0 <= 'k')||(LA7_0 >= 'm' && LA7_0 <= '\uFFFF')) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// LPC_pure.g:586:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop7;
						}
					}

					match("long"); 

					}
					break;
				case 5 :
					// LPC_pure.g:587:6: '@HELP' ( options {greedy=false; } : . )* 'HELP'
					{
					match("@HELP"); 

					// LPC_pure.g:587:14: ( options {greedy=false; } : . )*
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( (LA8_0=='H') ) {
							int LA8_1 = input.LA(2);
							if ( (LA8_1=='E') ) {
								int LA8_3 = input.LA(3);
								if ( (LA8_3=='L') ) {
									int LA8_4 = input.LA(4);
									if ( (LA8_4=='P') ) {
										alt8=2;
									}
									else if ( ((LA8_4 >= '\u0000' && LA8_4 <= 'O')||(LA8_4 >= 'Q' && LA8_4 <= '\uFFFF')) ) {
										alt8=1;
									}

								}
								else if ( ((LA8_3 >= '\u0000' && LA8_3 <= 'K')||(LA8_3 >= 'M' && LA8_3 <= '\uFFFF')) ) {
									alt8=1;
								}

							}
							else if ( ((LA8_1 >= '\u0000' && LA8_1 <= 'D')||(LA8_1 >= 'F' && LA8_1 <= '\uFFFF')) ) {
								alt8=1;
							}

						}
						else if ( ((LA8_0 >= '\u0000' && LA8_0 <= 'G')||(LA8_0 >= 'I' && LA8_0 <= '\uFFFF')) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// LPC_pure.g:587:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop8;
						}
					}

					match("HELP"); 

					}
					break;
				case 6 :
					// LPC_pure.g:588:6: '@Help' ( options {greedy=false; } : . )* 'Help'
					{
					match("@Help"); 

					// LPC_pure.g:588:14: ( options {greedy=false; } : . )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0=='H') ) {
							int LA9_1 = input.LA(2);
							if ( (LA9_1=='e') ) {
								int LA9_3 = input.LA(3);
								if ( (LA9_3=='l') ) {
									int LA9_4 = input.LA(4);
									if ( (LA9_4=='p') ) {
										alt9=2;
									}
									else if ( ((LA9_4 >= '\u0000' && LA9_4 <= 'o')||(LA9_4 >= 'q' && LA9_4 <= '\uFFFF')) ) {
										alt9=1;
									}

								}
								else if ( ((LA9_3 >= '\u0000' && LA9_3 <= 'k')||(LA9_3 >= 'm' && LA9_3 <= '\uFFFF')) ) {
									alt9=1;
								}

							}
							else if ( ((LA9_1 >= '\u0000' && LA9_1 <= 'd')||(LA9_1 >= 'f' && LA9_1 <= '\uFFFF')) ) {
								alt9=1;
							}

						}
						else if ( ((LA9_0 >= '\u0000' && LA9_0 <= 'G')||(LA9_0 >= 'I' && LA9_0 <= '\uFFFF')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// LPC_pure.g:588:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop9;
						}
					}

					match("Help"); 

					}
					break;
				case 7 :
					// LPC_pure.g:589:6: '@TEXT' ( options {greedy=false; } : . )* 'TEXT'
					{
					match("@TEXT"); 

					// LPC_pure.g:589:14: ( options {greedy=false; } : . )*
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( (LA10_0=='T') ) {
							int LA10_1 = input.LA(2);
							if ( (LA10_1=='E') ) {
								int LA10_3 = input.LA(3);
								if ( (LA10_3=='X') ) {
									int LA10_4 = input.LA(4);
									if ( (LA10_4=='T') ) {
										alt10=2;
									}
									else if ( ((LA10_4 >= '\u0000' && LA10_4 <= 'S')||(LA10_4 >= 'U' && LA10_4 <= '\uFFFF')) ) {
										alt10=1;
									}

								}
								else if ( ((LA10_3 >= '\u0000' && LA10_3 <= 'W')||(LA10_3 >= 'Y' && LA10_3 <= '\uFFFF')) ) {
									alt10=1;
								}

							}
							else if ( ((LA10_1 >= '\u0000' && LA10_1 <= 'D')||(LA10_1 >= 'F' && LA10_1 <= '\uFFFF')) ) {
								alt10=1;
							}

						}
						else if ( ((LA10_0 >= '\u0000' && LA10_0 <= 'S')||(LA10_0 >= 'U' && LA10_0 <= '\uFFFF')) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// LPC_pure.g:589:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop10;
						}
					}

					match("TEXT"); 

					}
					break;
				case 8 :
					// LPC_pure.g:590:6: '@ROOM_CODE' ( options {greedy=false; } : . )* 'ROOM_CODE'
					{
					match("@ROOM_CODE"); 

					// LPC_pure.g:590:19: ( options {greedy=false; } : . )*
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( (LA11_0=='R') ) {
							int LA11_1 = input.LA(2);
							if ( (LA11_1=='O') ) {
								int LA11_3 = input.LA(3);
								if ( (LA11_3=='O') ) {
									int LA11_4 = input.LA(4);
									if ( (LA11_4=='M') ) {
										int LA11_5 = input.LA(5);
										if ( (LA11_5=='_') ) {
											int LA11_6 = input.LA(6);
											if ( (LA11_6=='C') ) {
												int LA11_7 = input.LA(7);
												if ( (LA11_7=='O') ) {
													int LA11_8 = input.LA(8);
													if ( (LA11_8=='D') ) {
														int LA11_9 = input.LA(9);
														if ( (LA11_9=='E') ) {
															alt11=2;
														}
														else if ( ((LA11_9 >= '\u0000' && LA11_9 <= 'D')||(LA11_9 >= 'F' && LA11_9 <= '\uFFFF')) ) {
															alt11=1;
														}

													}
													else if ( ((LA11_8 >= '\u0000' && LA11_8 <= 'C')||(LA11_8 >= 'E' && LA11_8 <= '\uFFFF')) ) {
														alt11=1;
													}

												}
												else if ( ((LA11_7 >= '\u0000' && LA11_7 <= 'N')||(LA11_7 >= 'P' && LA11_7 <= '\uFFFF')) ) {
													alt11=1;
												}

											}
											else if ( ((LA11_6 >= '\u0000' && LA11_6 <= 'B')||(LA11_6 >= 'D' && LA11_6 <= '\uFFFF')) ) {
												alt11=1;
											}

										}
										else if ( ((LA11_5 >= '\u0000' && LA11_5 <= '^')||(LA11_5 >= '`' && LA11_5 <= '\uFFFF')) ) {
											alt11=1;
										}

									}
									else if ( ((LA11_4 >= '\u0000' && LA11_4 <= 'L')||(LA11_4 >= 'N' && LA11_4 <= '\uFFFF')) ) {
										alt11=1;
									}

								}
								else if ( ((LA11_3 >= '\u0000' && LA11_3 <= 'N')||(LA11_3 >= 'P' && LA11_3 <= '\uFFFF')) ) {
									alt11=1;
								}

							}
							else if ( ((LA11_1 >= '\u0000' && LA11_1 <= 'N')||(LA11_1 >= 'P' && LA11_1 <= '\uFFFF')) ) {
								alt11=1;
							}

						}
						else if ( ((LA11_0 >= '\u0000' && LA11_0 <= 'Q')||(LA11_0 >= 'S' && LA11_0 <= '\uFFFF')) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// LPC_pure.g:590:47: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop11;
						}
					}

					match("ROOM_CODE"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AT_STRING"

	// $ANTLR start "HEX_LITERAL"
	public final void mHEX_LITERAL() throws RecognitionException {
		try {
			int _type = HEX_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:593:13: ( '0' ( 'x' | 'X' ) ( HexDigit )+ ( IntegerTypeSuffix )? )
			// LPC_pure.g:593:15: '0' ( 'x' | 'X' ) ( HexDigit )+ ( IntegerTypeSuffix )?
			{
			match('0'); 
			if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// LPC_pure.g:593:29: ( HexDigit )+
			int cnt13=0;
			loop13:
			while (true) {
				int alt13=2;
				int LA13_0 = input.LA(1);
				if ( ((LA13_0 >= '0' && LA13_0 <= '9')||(LA13_0 >= 'A' && LA13_0 <= 'F')||(LA13_0 >= 'a' && LA13_0 <= 'f')) ) {
					alt13=1;
				}

				switch (alt13) {
				case 1 :
					// LPC_pure.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt13 >= 1 ) break loop13;
					EarlyExitException eee = new EarlyExitException(13, input);
					throw eee;
				}
				cnt13++;
			}

			// LPC_pure.g:593:39: ( IntegerTypeSuffix )?
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0=='L'||LA14_0=='U'||LA14_0=='l'||LA14_0=='u') ) {
				alt14=1;
			}
			switch (alt14) {
				case 1 :
					// LPC_pure.g:593:39: IntegerTypeSuffix
					{
					mIntegerTypeSuffix(); 

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEX_LITERAL"

	// $ANTLR start "DECIMAL_LITERAL"
	public final void mDECIMAL_LITERAL() throws RecognitionException {
		try {
			int _type = DECIMAL_LITERAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:595:17: ( ( ( '0' .. '9' )+ ) ( IntegerTypeSuffix )? )
			// LPC_pure.g:595:19: ( ( '0' .. '9' )+ ) ( IntegerTypeSuffix )?
			{
			// LPC_pure.g:595:19: ( ( '0' .. '9' )+ )
			// LPC_pure.g:595:20: ( '0' .. '9' )+
			{
			// LPC_pure.g:595:20: ( '0' .. '9' )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( ((LA15_0 >= '0' && LA15_0 <= '9')) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// LPC_pure.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

			// LPC_pure.g:595:31: ( IntegerTypeSuffix )?
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0=='L'||LA16_0=='U'||LA16_0=='l'||LA16_0=='u') ) {
				alt16=1;
			}
			switch (alt16) {
				case 1 :
					// LPC_pure.g:595:31: IntegerTypeSuffix
					{
					mIntegerTypeSuffix(); 

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DECIMAL_LITERAL"

	// $ANTLR start "HexDigit"
	public final void mHexDigit() throws RecognitionException {
		try {
			// LPC_pure.g:601:10: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
			// LPC_pure.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HexDigit"

	// $ANTLR start "IntegerTypeSuffix"
	public final void mIntegerTypeSuffix() throws RecognitionException {
		try {
			// LPC_pure.g:605:2: ( ( 'u' | 'U' ) ( 'l' | 'L' ) | ( 'u' | 'U' ) | ( 'l' | 'L' ) )
			int alt17=3;
			int LA17_0 = input.LA(1);
			if ( (LA17_0=='U'||LA17_0=='u') ) {
				int LA17_1 = input.LA(2);
				if ( (LA17_1=='L'||LA17_1=='l') ) {
					alt17=1;
				}

				else {
					alt17=2;
				}

			}
			else if ( (LA17_0=='L'||LA17_0=='l') ) {
				alt17=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}

			switch (alt17) {
				case 1 :
					// LPC_pure.g:605:4: ( 'u' | 'U' ) ( 'l' | 'L' )
					{
					if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// LPC_pure.g:606:4: ( 'u' | 'U' )
					{
					if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// LPC_pure.g:607:4: ( 'l' | 'L' )
					{
					if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IntegerTypeSuffix"

	// $ANTLR start "Exponent"
	public final void mExponent() throws RecognitionException {
		try {
			// LPC_pure.g:612:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
			// LPC_pure.g:612:12: ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// LPC_pure.g:612:22: ( '+' | '-' )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0=='+'||LA18_0=='-') ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// LPC_pure.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// LPC_pure.g:612:33: ( '0' .. '9' )+
			int cnt19=0;
			loop19:
			while (true) {
				int alt19=2;
				int LA19_0 = input.LA(1);
				if ( ((LA19_0 >= '0' && LA19_0 <= '9')) ) {
					alt19=1;
				}

				switch (alt19) {
				case 1 :
					// LPC_pure.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt19 >= 1 ) break loop19;
					EarlyExitException eee = new EarlyExitException(19, input);
					throw eee;
				}
				cnt19++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Exponent"

	// $ANTLR start "FloatTypeSuffix"
	public final void mFloatTypeSuffix() throws RecognitionException {
		try {
			// LPC_pure.g:615:17: ( ( 'f' | 'F' | 'd' | 'D' ) )
			// LPC_pure.g:
			{
			if ( input.LA(1)=='D'||input.LA(1)=='F'||input.LA(1)=='d'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FloatTypeSuffix"

	// $ANTLR start "EscapeSequence"
	public final void mEscapeSequence() throws RecognitionException {
		try {
			// LPC_pure.g:619:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) | OctalEscape )
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0=='\\') ) {
				int LA20_1 = input.LA(2);
				if ( (LA20_1=='\"'||LA20_1=='\''||LA20_1=='\\'||LA20_1=='b'||LA20_1=='f'||LA20_1=='n'||LA20_1=='r'||LA20_1=='t') ) {
					alt20=1;
				}
				else if ( ((LA20_1 >= '0' && LA20_1 <= '7')) ) {
					alt20=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 20, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}

			switch (alt20) {
				case 1 :
					// LPC_pure.g:619:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
					{
					match('\\'); 
					if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// LPC_pure.g:620:9: OctalEscape
					{
					mOctalEscape(); 

					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EscapeSequence"

	// $ANTLR start "OctalEscape"
	public final void mOctalEscape() throws RecognitionException {
		try {
			// LPC_pure.g:625:5: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
			int alt21=3;
			int LA21_0 = input.LA(1);
			if ( (LA21_0=='\\') ) {
				int LA21_1 = input.LA(2);
				if ( ((LA21_1 >= '0' && LA21_1 <= '3')) ) {
					int LA21_2 = input.LA(3);
					if ( ((LA21_2 >= '0' && LA21_2 <= '7')) ) {
						int LA21_4 = input.LA(4);
						if ( ((LA21_4 >= '0' && LA21_4 <= '7')) ) {
							alt21=1;
						}

						else {
							alt21=2;
						}

					}

					else {
						alt21=3;
					}

				}
				else if ( ((LA21_1 >= '4' && LA21_1 <= '7')) ) {
					int LA21_3 = input.LA(3);
					if ( ((LA21_3 >= '0' && LA21_3 <= '7')) ) {
						alt21=2;
					}

					else {
						alt21=3;
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 21, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}

			switch (alt21) {
				case 1 :
					// LPC_pure.g:625:9: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '3') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// LPC_pure.g:626:9: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// LPC_pure.g:627:9: '\\\\' ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OctalEscape"

	// $ANTLR start "UnicodeEscape"
	public final void mUnicodeEscape() throws RecognitionException {
		try {
			// LPC_pure.g:632:5: ( '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit )
			// LPC_pure.g:632:9: '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit
			{
			match('\\'); 
			match('u'); 
			mHexDigit(); 

			mHexDigit(); 

			mHexDigit(); 

			mHexDigit(); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UnicodeEscape"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:634:5: ( ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' | '..' ) )
			// LPC_pure.g:634:8: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' | '..' )
			{
			// LPC_pure.g:634:8: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' | '..' )
			int alt22=6;
			switch ( input.LA(1) ) {
			case ' ':
				{
				alt22=1;
				}
				break;
			case '\r':
				{
				alt22=2;
				}
				break;
			case '\t':
				{
				alt22=3;
				}
				break;
			case '\f':
				{
				alt22=4;
				}
				break;
			case '\n':
				{
				alt22=5;
				}
				break;
			case '.':
				{
				alt22=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}
			switch (alt22) {
				case 1 :
					// LPC_pure.g:634:9: ' '
					{
					match(' '); 
					}
					break;
				case 2 :
					// LPC_pure.g:634:13: '\\r'
					{
					match('\r'); 
					}
					break;
				case 3 :
					// LPC_pure.g:634:18: '\\t'
					{
					match('\t'); 
					}
					break;
				case 4 :
					// LPC_pure.g:634:23: '\\u000C'
					{
					match('\f'); 
					}
					break;
				case 5 :
					// LPC_pure.g:634:32: '\\n'
					{
					match('\n'); 
					}
					break;
				case 6 :
					// LPC_pure.g:634:39: '..'
					{
					match(".."); 

					}
					break;

			}

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:638:5: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// LPC_pure.g:638:9: '/*' ( options {greedy=false; } : . )* '*/'
			{
			match("/*"); 

			// LPC_pure.g:638:14: ( options {greedy=false; } : . )*
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( (LA23_0=='*') ) {
					int LA23_1 = input.LA(2);
					if ( (LA23_1=='/') ) {
						alt23=2;
					}
					else if ( ((LA23_1 >= '\u0000' && LA23_1 <= '.')||(LA23_1 >= '0' && LA23_1 <= '\uFFFF')) ) {
						alt23=1;
					}

				}
				else if ( ((LA23_0 >= '\u0000' && LA23_0 <= ')')||(LA23_0 >= '+' && LA23_0 <= '\uFFFF')) ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// LPC_pure.g:638:42: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop23;
				}
			}

			match("*/"); 

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "LINE_COMMENT"
	public final void mLINE_COMMENT() throws RecognitionException {
		try {
			int _type = LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:642:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
			// LPC_pure.g:642:7: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
			{
			match("//"); 

			// LPC_pure.g:642:12: (~ ( '\\n' | '\\r' ) )*
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( ((LA24_0 >= '\u0000' && LA24_0 <= '\t')||(LA24_0 >= '\u000B' && LA24_0 <= '\f')||(LA24_0 >= '\u000E' && LA24_0 <= '\uFFFF')) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// LPC_pure.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop24;
				}
			}

			// LPC_pure.g:642:26: ( '\\r' )?
			int alt25=2;
			int LA25_0 = input.LA(1);
			if ( (LA25_0=='\r') ) {
				alt25=1;
			}
			switch (alt25) {
				case 1 :
					// LPC_pure.g:642:26: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMENT"

	// $ANTLR start "NO_MEAN"
	public final void mNO_MEAN() throws RecognitionException {
		try {
			int _type = NO_MEAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:646:5: ( '\\\\' )
			// LPC_pure.g:646:7: '\\\\'
			{
			match('\\'); 
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NO_MEAN"

	// $ANTLR start "LESSTHAN"
	public final void mLESSTHAN() throws RecognitionException {
		try {
			int _type = LESSTHAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:650:5: ( '<' )
			// LPC_pure.g:650:7: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESSTHAN"

	// $ANTLR start "LINE_COMMAND"
	public final void mLINE_COMMAND() throws RecognitionException {
		try {
			int _type = LINE_COMMAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// LPC_pure.g:654:5: ( ( '#if' | '#else' | '#elif' | '#endif' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
			// LPC_pure.g:654:7: ( '#if' | '#else' | '#elif' | '#endif' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
			{
			// LPC_pure.g:654:7: ( '#if' | '#else' | '#elif' | '#endif' )
			int alt26=4;
			int LA26_0 = input.LA(1);
			if ( (LA26_0=='#') ) {
				int LA26_1 = input.LA(2);
				if ( (LA26_1=='i') ) {
					alt26=1;
				}
				else if ( (LA26_1=='e') ) {
					int LA26_3 = input.LA(3);
					if ( (LA26_3=='l') ) {
						int LA26_4 = input.LA(4);
						if ( (LA26_4=='s') ) {
							alt26=2;
						}
						else if ( (LA26_4=='i') ) {
							alt26=3;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 26, 4, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}
					else if ( (LA26_3=='n') ) {
						alt26=4;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// LPC_pure.g:654:8: '#if'
					{
					match("#if"); 

					}
					break;
				case 2 :
					// LPC_pure.g:654:14: '#else'
					{
					match("#else"); 

					}
					break;
				case 3 :
					// LPC_pure.g:654:22: '#elif'
					{
					match("#elif"); 

					}
					break;
				case 4 :
					// LPC_pure.g:654:30: '#endif'
					{
					match("#endif"); 

					}
					break;

			}

			// LPC_pure.g:654:40: (~ ( '\\n' | '\\r' ) )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( ((LA27_0 >= '\u0000' && LA27_0 <= '\t')||(LA27_0 >= '\u000B' && LA27_0 <= '\f')||(LA27_0 >= '\u000E' && LA27_0 <= '\uFFFF')) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// LPC_pure.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop27;
				}
			}

			// LPC_pure.g:654:54: ( '\\r' )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0=='\r') ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// LPC_pure.g:654:54: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMAND"

	@Override
	public void mTokens() throws RecognitionException {
		// LPC_pure.g:1:8: ( T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | IDENTIFIER | CHARACTER_LITERAL | STRING_LITERAL | AT_STRING | HEX_LITERAL | DECIMAL_LITERAL | WS | COMMENT | LINE_COMMENT | NO_MEAN | LESSTHAN | LINE_COMMAND )
		int alt29=109;
		alt29 = dfa29.predict(input);
		switch (alt29) {
			case 1 :
				// LPC_pure.g:1:10: T__24
				{
				mT__24(); 

				}
				break;
			case 2 :
				// LPC_pure.g:1:16: T__25
				{
				mT__25(); 

				}
				break;
			case 3 :
				// LPC_pure.g:1:22: T__26
				{
				mT__26(); 

				}
				break;
			case 4 :
				// LPC_pure.g:1:28: T__27
				{
				mT__27(); 

				}
				break;
			case 5 :
				// LPC_pure.g:1:34: T__28
				{
				mT__28(); 

				}
				break;
			case 6 :
				// LPC_pure.g:1:40: T__29
				{
				mT__29(); 

				}
				break;
			case 7 :
				// LPC_pure.g:1:46: T__30
				{
				mT__30(); 

				}
				break;
			case 8 :
				// LPC_pure.g:1:52: T__31
				{
				mT__31(); 

				}
				break;
			case 9 :
				// LPC_pure.g:1:58: T__32
				{
				mT__32(); 

				}
				break;
			case 10 :
				// LPC_pure.g:1:64: T__33
				{
				mT__33(); 

				}
				break;
			case 11 :
				// LPC_pure.g:1:70: T__34
				{
				mT__34(); 

				}
				break;
			case 12 :
				// LPC_pure.g:1:76: T__35
				{
				mT__35(); 

				}
				break;
			case 13 :
				// LPC_pure.g:1:82: T__36
				{
				mT__36(); 

				}
				break;
			case 14 :
				// LPC_pure.g:1:88: T__37
				{
				mT__37(); 

				}
				break;
			case 15 :
				// LPC_pure.g:1:94: T__38
				{
				mT__38(); 

				}
				break;
			case 16 :
				// LPC_pure.g:1:100: T__39
				{
				mT__39(); 

				}
				break;
			case 17 :
				// LPC_pure.g:1:106: T__40
				{
				mT__40(); 

				}
				break;
			case 18 :
				// LPC_pure.g:1:112: T__41
				{
				mT__41(); 

				}
				break;
			case 19 :
				// LPC_pure.g:1:118: T__42
				{
				mT__42(); 

				}
				break;
			case 20 :
				// LPC_pure.g:1:124: T__43
				{
				mT__43(); 

				}
				break;
			case 21 :
				// LPC_pure.g:1:130: T__44
				{
				mT__44(); 

				}
				break;
			case 22 :
				// LPC_pure.g:1:136: T__45
				{
				mT__45(); 

				}
				break;
			case 23 :
				// LPC_pure.g:1:142: T__46
				{
				mT__46(); 

				}
				break;
			case 24 :
				// LPC_pure.g:1:148: T__47
				{
				mT__47(); 

				}
				break;
			case 25 :
				// LPC_pure.g:1:154: T__48
				{
				mT__48(); 

				}
				break;
			case 26 :
				// LPC_pure.g:1:160: T__49
				{
				mT__49(); 

				}
				break;
			case 27 :
				// LPC_pure.g:1:166: T__50
				{
				mT__50(); 

				}
				break;
			case 28 :
				// LPC_pure.g:1:172: T__51
				{
				mT__51(); 

				}
				break;
			case 29 :
				// LPC_pure.g:1:178: T__52
				{
				mT__52(); 

				}
				break;
			case 30 :
				// LPC_pure.g:1:184: T__53
				{
				mT__53(); 

				}
				break;
			case 31 :
				// LPC_pure.g:1:190: T__54
				{
				mT__54(); 

				}
				break;
			case 32 :
				// LPC_pure.g:1:196: T__55
				{
				mT__55(); 

				}
				break;
			case 33 :
				// LPC_pure.g:1:202: T__56
				{
				mT__56(); 

				}
				break;
			case 34 :
				// LPC_pure.g:1:208: T__57
				{
				mT__57(); 

				}
				break;
			case 35 :
				// LPC_pure.g:1:214: T__58
				{
				mT__58(); 

				}
				break;
			case 36 :
				// LPC_pure.g:1:220: T__59
				{
				mT__59(); 

				}
				break;
			case 37 :
				// LPC_pure.g:1:226: T__60
				{
				mT__60(); 

				}
				break;
			case 38 :
				// LPC_pure.g:1:232: T__61
				{
				mT__61(); 

				}
				break;
			case 39 :
				// LPC_pure.g:1:238: T__62
				{
				mT__62(); 

				}
				break;
			case 40 :
				// LPC_pure.g:1:244: T__63
				{
				mT__63(); 

				}
				break;
			case 41 :
				// LPC_pure.g:1:250: T__64
				{
				mT__64(); 

				}
				break;
			case 42 :
				// LPC_pure.g:1:256: T__65
				{
				mT__65(); 

				}
				break;
			case 43 :
				// LPC_pure.g:1:262: T__66
				{
				mT__66(); 

				}
				break;
			case 44 :
				// LPC_pure.g:1:268: T__67
				{
				mT__67(); 

				}
				break;
			case 45 :
				// LPC_pure.g:1:274: T__68
				{
				mT__68(); 

				}
				break;
			case 46 :
				// LPC_pure.g:1:280: T__69
				{
				mT__69(); 

				}
				break;
			case 47 :
				// LPC_pure.g:1:286: T__70
				{
				mT__70(); 

				}
				break;
			case 48 :
				// LPC_pure.g:1:292: T__71
				{
				mT__71(); 

				}
				break;
			case 49 :
				// LPC_pure.g:1:298: T__72
				{
				mT__72(); 

				}
				break;
			case 50 :
				// LPC_pure.g:1:304: T__73
				{
				mT__73(); 

				}
				break;
			case 51 :
				// LPC_pure.g:1:310: T__74
				{
				mT__74(); 

				}
				break;
			case 52 :
				// LPC_pure.g:1:316: T__75
				{
				mT__75(); 

				}
				break;
			case 53 :
				// LPC_pure.g:1:322: T__76
				{
				mT__76(); 

				}
				break;
			case 54 :
				// LPC_pure.g:1:328: T__77
				{
				mT__77(); 

				}
				break;
			case 55 :
				// LPC_pure.g:1:334: T__78
				{
				mT__78(); 

				}
				break;
			case 56 :
				// LPC_pure.g:1:340: T__79
				{
				mT__79(); 

				}
				break;
			case 57 :
				// LPC_pure.g:1:346: T__80
				{
				mT__80(); 

				}
				break;
			case 58 :
				// LPC_pure.g:1:352: T__81
				{
				mT__81(); 

				}
				break;
			case 59 :
				// LPC_pure.g:1:358: T__82
				{
				mT__82(); 

				}
				break;
			case 60 :
				// LPC_pure.g:1:364: T__83
				{
				mT__83(); 

				}
				break;
			case 61 :
				// LPC_pure.g:1:370: T__84
				{
				mT__84(); 

				}
				break;
			case 62 :
				// LPC_pure.g:1:376: T__85
				{
				mT__85(); 

				}
				break;
			case 63 :
				// LPC_pure.g:1:382: T__86
				{
				mT__86(); 

				}
				break;
			case 64 :
				// LPC_pure.g:1:388: T__87
				{
				mT__87(); 

				}
				break;
			case 65 :
				// LPC_pure.g:1:394: T__88
				{
				mT__88(); 

				}
				break;
			case 66 :
				// LPC_pure.g:1:400: T__89
				{
				mT__89(); 

				}
				break;
			case 67 :
				// LPC_pure.g:1:406: T__90
				{
				mT__90(); 

				}
				break;
			case 68 :
				// LPC_pure.g:1:412: T__91
				{
				mT__91(); 

				}
				break;
			case 69 :
				// LPC_pure.g:1:418: T__92
				{
				mT__92(); 

				}
				break;
			case 70 :
				// LPC_pure.g:1:424: T__93
				{
				mT__93(); 

				}
				break;
			case 71 :
				// LPC_pure.g:1:430: T__94
				{
				mT__94(); 

				}
				break;
			case 72 :
				// LPC_pure.g:1:436: T__95
				{
				mT__95(); 

				}
				break;
			case 73 :
				// LPC_pure.g:1:442: T__96
				{
				mT__96(); 

				}
				break;
			case 74 :
				// LPC_pure.g:1:448: T__97
				{
				mT__97(); 

				}
				break;
			case 75 :
				// LPC_pure.g:1:454: T__98
				{
				mT__98(); 

				}
				break;
			case 76 :
				// LPC_pure.g:1:460: T__99
				{
				mT__99(); 

				}
				break;
			case 77 :
				// LPC_pure.g:1:466: T__100
				{
				mT__100(); 

				}
				break;
			case 78 :
				// LPC_pure.g:1:473: T__101
				{
				mT__101(); 

				}
				break;
			case 79 :
				// LPC_pure.g:1:480: T__102
				{
				mT__102(); 

				}
				break;
			case 80 :
				// LPC_pure.g:1:487: T__103
				{
				mT__103(); 

				}
				break;
			case 81 :
				// LPC_pure.g:1:494: T__104
				{
				mT__104(); 

				}
				break;
			case 82 :
				// LPC_pure.g:1:501: T__105
				{
				mT__105(); 

				}
				break;
			case 83 :
				// LPC_pure.g:1:508: T__106
				{
				mT__106(); 

				}
				break;
			case 84 :
				// LPC_pure.g:1:515: T__107
				{
				mT__107(); 

				}
				break;
			case 85 :
				// LPC_pure.g:1:522: T__108
				{
				mT__108(); 

				}
				break;
			case 86 :
				// LPC_pure.g:1:529: T__109
				{
				mT__109(); 

				}
				break;
			case 87 :
				// LPC_pure.g:1:536: T__110
				{
				mT__110(); 

				}
				break;
			case 88 :
				// LPC_pure.g:1:543: T__111
				{
				mT__111(); 

				}
				break;
			case 89 :
				// LPC_pure.g:1:550: T__112
				{
				mT__112(); 

				}
				break;
			case 90 :
				// LPC_pure.g:1:557: T__113
				{
				mT__113(); 

				}
				break;
			case 91 :
				// LPC_pure.g:1:564: T__114
				{
				mT__114(); 

				}
				break;
			case 92 :
				// LPC_pure.g:1:571: T__115
				{
				mT__115(); 

				}
				break;
			case 93 :
				// LPC_pure.g:1:578: T__116
				{
				mT__116(); 

				}
				break;
			case 94 :
				// LPC_pure.g:1:585: T__117
				{
				mT__117(); 

				}
				break;
			case 95 :
				// LPC_pure.g:1:592: T__118
				{
				mT__118(); 

				}
				break;
			case 96 :
				// LPC_pure.g:1:599: T__119
				{
				mT__119(); 

				}
				break;
			case 97 :
				// LPC_pure.g:1:606: T__120
				{
				mT__120(); 

				}
				break;
			case 98 :
				// LPC_pure.g:1:613: IDENTIFIER
				{
				mIDENTIFIER(); 

				}
				break;
			case 99 :
				// LPC_pure.g:1:624: CHARACTER_LITERAL
				{
				mCHARACTER_LITERAL(); 

				}
				break;
			case 100 :
				// LPC_pure.g:1:642: STRING_LITERAL
				{
				mSTRING_LITERAL(); 

				}
				break;
			case 101 :
				// LPC_pure.g:1:657: AT_STRING
				{
				mAT_STRING(); 

				}
				break;
			case 102 :
				// LPC_pure.g:1:667: HEX_LITERAL
				{
				mHEX_LITERAL(); 

				}
				break;
			case 103 :
				// LPC_pure.g:1:679: DECIMAL_LITERAL
				{
				mDECIMAL_LITERAL(); 

				}
				break;
			case 104 :
				// LPC_pure.g:1:695: WS
				{
				mWS(); 

				}
				break;
			case 105 :
				// LPC_pure.g:1:698: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 106 :
				// LPC_pure.g:1:706: LINE_COMMENT
				{
				mLINE_COMMENT(); 

				}
				break;
			case 107 :
				// LPC_pure.g:1:719: NO_MEAN
				{
				mNO_MEAN(); 

				}
				break;
			case 108 :
				// LPC_pure.g:1:727: LESSTHAN
				{
				mLESSTHAN(); 

				}
				break;
			case 109 :
				// LPC_pure.g:1:736: LINE_COMMAND
				{
				mLINE_COMMAND(); 

				}
				break;

		}
	}


	protected DFA29 dfa29 = new DFA29(this);
	static final String DFA29_eotS =
		"\1\uffff\1\65\1\72\1\74\1\77\2\uffff\1\101\1\104\1\uffff\1\110\1\112\1"+
		"\116\1\120\1\uffff\1\123\1\125\1\130\3\uffff\1\132\22\54\1\uffff\1\175"+
		"\6\uffff\1\61\30\uffff\1\u0081\7\uffff\1\u0083\5\uffff\1\u0085\3\uffff"+
		"\7\54\1\u008d\7\54\1\u0095\1\u0098\17\54\13\uffff\7\54\1\uffff\4\54\1"+
		"\u00bd\2\54\1\uffff\1\54\1\u00c1\1\uffff\26\54\1\u00d9\1\54\1\u00db\1"+
		"\u00dc\5\54\1\u00e2\1\u00e3\3\54\1\uffff\1\54\1\u00e8\1\54\1\uffff\24"+
		"\54\1\u00fe\2\54\1\uffff\1\u0101\2\uffff\1\u0102\1\u0103\3\54\2\uffff"+
		"\1\54\1\u0108\2\54\1\uffff\2\54\1\u010d\17\54\1\u011d\2\54\1\uffff\1\54"+
		"\1\u0121\3\uffff\2\54\1\u0125\1\u0126\1\uffff\4\54\1\uffff\1\u012b\1\u012c"+
		"\1\u012d\2\54\1\u0130\1\54\1\u0132\1\u0133\1\u0134\1\u0135\1\u0136\1\u0137"+
		"\1\u0138\1\54\1\uffff\3\54\1\uffff\1\54\1\u013e\1\u013f\2\uffff\1\u0140"+
		"\1\54\1\u0142\1\u0143\3\uffff\1\u0144\1\54\1\uffff\1\54\7\uffff\1\u0147"+
		"\1\54\1\u0149\1\54\1\u014b\3\uffff\1\u014c\3\uffff\1\54\1\u014e\1\uffff"+
		"\1\u014f\1\uffff\1\u0150\2\uffff\1\u0151\4\uffff";
	static final String DFA29_eofS =
		"\u0152\uffff";
	static final String DFA29_minS =
		"\1\11\1\75\1\145\1\75\1\46\2\uffff\1\75\1\53\1\uffff\1\55\1\56\1\52\1"+
		"\72\1\uffff\1\74\2\75\3\uffff\1\75\1\165\1\162\1\141\1\145\2\154\1\157"+
		"\1\146\1\141\1\157\1\142\1\162\1\145\1\151\1\171\1\156\1\141\1\150\1\uffff"+
		"\1\75\6\uffff\1\130\5\uffff\1\146\22\uffff\1\56\7\uffff\1\75\5\uffff\1"+
		"\75\3\uffff\1\164\1\145\1\163\2\141\1\156\1\146\1\44\1\163\1\165\1\164"+
		"\1\157\1\162\1\156\1\164\2\44\1\160\1\170\1\155\1\152\1\151\1\142\2\147"+
		"\1\141\1\151\1\160\1\151\1\162\2\151\13\uffff\1\157\1\141\1\145\1\162"+
		"\2\163\1\141\1\uffff\1\145\1\155\1\145\1\141\1\44\1\143\1\157\1\uffff"+
		"\1\145\1\44\1\uffff\1\160\1\145\2\141\1\145\1\166\1\164\1\154\1\151\1"+
		"\165\1\156\1\145\1\164\1\151\1\164\1\145\1\157\1\151\1\141\1\144\1\141"+
		"\1\154\1\44\1\153\2\44\1\163\1\164\1\151\1\165\1\156\2\44\1\162\1\164"+
		"\1\141\1\uffff\1\164\1\44\1\162\1\uffff\1\151\1\144\1\163\1\166\1\143"+
		"\1\141\1\145\1\151\1\163\1\162\1\145\1\157\1\151\1\156\2\143\1\144\1\156"+
		"\1\147\1\162\1\44\1\164\1\145\1\uffff\1\44\2\uffff\2\44\1\156\1\154\1"+
		"\145\2\uffff\1\156\1\44\1\143\1\151\1\uffff\1\151\1\156\1\44\1\153\1\145"+
		"\2\164\2\143\1\164\1\156\1\144\1\146\1\143\1\147\1\164\1\150\1\145\1\44"+
		"\1\156\1\147\1\uffff\1\151\1\44\3\uffff\1\165\1\164\2\44\1\uffff\1\150"+
		"\1\157\1\164\1\147\1\uffff\3\44\1\145\1\164\1\44\1\145\7\44\1\146\1\uffff"+
		"\1\145\1\163\1\154\1\uffff\1\145\2\44\2\uffff\1\44\1\156\2\44\3\uffff"+
		"\1\44\1\145\1\uffff\1\162\7\uffff\1\44\1\144\1\44\1\145\1\44\3\uffff\1"+
		"\44\3\uffff\1\144\1\44\1\uffff\1\44\1\uffff\1\44\2\uffff\1\44\4\uffff";
	static final String DFA29_maxS =
		"\1\176\1\75\1\165\2\75\2\uffff\2\75\1\uffff\1\76\1\56\1\75\1\72\1\uffff"+
		"\2\75\1\76\3\uffff\1\75\1\165\1\162\2\157\1\170\1\165\1\157\1\156\1\151"+
		"\1\157\1\142\1\165\1\145\1\167\1\171\1\156\1\157\1\150\1\uffff\1\174\6"+
		"\uffff\1\170\5\uffff\1\156\22\uffff\1\56\7\uffff\1\75\5\uffff\1\75\3\uffff"+
		"\1\164\1\145\1\163\2\141\1\156\1\146\1\172\1\163\1\165\1\164\1\157\1\162"+
		"\1\156\1\164\2\172\1\160\1\170\1\163\1\152\1\157\1\142\1\164\1\172\1\162"+
		"\1\151\1\160\1\163\1\162\1\154\1\151\13\uffff\1\157\1\141\1\145\1\162"+
		"\1\163\1\164\1\151\1\uffff\1\145\1\155\1\145\1\141\1\172\1\143\1\157\1"+
		"\uffff\1\145\1\172\1\uffff\1\160\1\145\2\141\1\145\1\166\1\164\1\154\1"+
		"\151\1\165\1\156\1\145\1\164\1\165\1\164\1\145\1\157\1\151\1\141\1\144"+
		"\1\141\1\154\1\172\1\153\2\172\1\163\1\164\1\151\1\165\1\156\2\172\1\162"+
		"\1\164\1\141\1\uffff\1\164\1\172\1\162\1\uffff\1\151\1\144\1\163\1\166"+
		"\1\143\1\141\1\145\1\151\1\163\1\162\1\145\1\157\1\151\1\156\2\143\1\144"+
		"\1\156\1\147\1\162\1\172\1\164\1\145\1\uffff\1\172\2\uffff\2\172\1\156"+
		"\1\154\1\145\2\uffff\1\156\1\172\1\143\1\151\1\uffff\1\151\1\156\1\172"+
		"\1\153\1\145\2\164\2\143\1\164\1\156\1\144\1\146\1\143\1\147\1\164\1\150"+
		"\1\145\1\172\1\156\1\147\1\uffff\1\151\1\172\3\uffff\1\165\1\164\2\172"+
		"\1\uffff\1\150\1\157\1\164\1\147\1\uffff\3\172\1\145\1\164\1\172\1\145"+
		"\7\172\1\146\1\uffff\1\145\1\163\1\154\1\uffff\1\145\2\172\2\uffff\1\172"+
		"\1\156\2\172\3\uffff\1\172\1\145\1\uffff\1\162\7\uffff\1\172\1\144\1\172"+
		"\1\145\1\172\3\uffff\1\172\3\uffff\1\144\1\172\1\uffff\1\172\1\uffff\1"+
		"\172\2\uffff\1\172\4\uffff";
	static final String DFA29_acceptS =
		"\5\uffff\1\14\1\15\2\uffff\1\23\4\uffff\1\37\3\uffff\1\51\1\52\1\53\23"+
		"\uffff\1\134\1\uffff\1\140\1\141\1\142\1\143\1\144\1\145\1\uffff\1\147"+
		"\1\150\1\153\1\2\1\1\1\uffff\1\5\1\6\1\155\1\3\1\10\1\7\1\11\1\13\1\12"+
		"\1\17\1\16\1\21\1\22\1\20\1\25\1\26\1\27\1\24\1\uffff\1\30\1\34\1\151"+
		"\1\152\1\33\1\36\1\35\1\uffff\1\42\1\154\1\44\1\43\1\46\1\uffff\1\45\1"+
		"\55\1\54\40\uffff\1\136\1\137\1\135\1\146\1\4\1\32\1\31\1\41\1\40\1\50"+
		"\1\47\7\uffff\1\70\7\uffff\1\101\2\uffff\1\102\44\uffff\1\75\3\uffff\1"+
		"\104\27\uffff\1\56\1\uffff\1\60\1\61\5\uffff\1\71\1\72\4\uffff\1\100\25"+
		"\uffff\1\131\2\uffff\1\57\1\62\1\63\4\uffff\1\74\4\uffff\1\106\17\uffff"+
		"\1\126\3\uffff\1\133\3\uffff\1\66\1\73\4\uffff\1\107\1\110\1\111\2\uffff"+
		"\1\114\1\uffff\1\116\1\117\1\120\1\121\1\122\1\123\1\124\5\uffff\1\65"+
		"\1\67\1\76\1\uffff\1\103\1\105\1\112\2\uffff\1\125\1\uffff\1\130\1\uffff"+
		"\1\64\1\77\1\uffff\1\115\1\127\1\132\1\113";
	static final String DFA29_specialS =
		"\u0152\uffff}>";
	static final String[] DFA29_transitionS = {
			"\2\62\1\uffff\2\62\22\uffff\1\62\1\1\1\56\1\2\1\54\1\3\1\4\1\55\1\5\1"+
			"\6\1\7\1\10\1\11\1\12\1\13\1\14\1\60\11\61\1\15\1\16\1\17\1\20\1\21\1"+
			"\22\1\57\32\54\1\23\1\63\1\24\1\25\1\54\1\uffff\1\26\1\27\1\30\1\31\1"+
			"\32\1\33\1\34\1\54\1\35\3\54\1\36\1\37\1\40\1\41\1\54\1\42\1\43\1\44"+
			"\1\45\1\46\1\47\3\54\1\50\1\51\1\52\1\53",
			"\1\64",
			"\1\71\3\uffff\1\66\6\uffff\1\67\4\uffff\1\70",
			"\1\73",
			"\1\75\26\uffff\1\76",
			"",
			"",
			"\1\100",
			"\1\102\21\uffff\1\103",
			"",
			"\1\105\17\uffff\1\106\1\107",
			"\1\111",
			"\1\114\4\uffff\1\115\15\uffff\1\113",
			"\1\117",
			"",
			"\1\121\1\122",
			"\1\124",
			"\1\126\1\127",
			"",
			"",
			"",
			"\1\131",
			"\1\133",
			"\1\134",
			"\1\135\6\uffff\1\136\3\uffff\1\137\2\uffff\1\140",
			"\1\141\11\uffff\1\142",
			"\1\143\1\uffff\1\144\11\uffff\1\145",
			"\1\146\2\uffff\1\147\5\uffff\1\150",
			"\1\151",
			"\1\152\7\uffff\1\153",
			"\1\154\7\uffff\1\155",
			"\1\156",
			"\1\157",
			"\1\160\2\uffff\1\161",
			"\1\162",
			"\1\163\12\uffff\1\164\2\uffff\1\165",
			"\1\166",
			"\1\167",
			"\1\170\15\uffff\1\171",
			"\1\172",
			"",
			"\1\173\76\uffff\1\174",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\176\37\uffff\1\176",
			"",
			"",
			"",
			"",
			"",
			"\1\71\7\uffff\1\177",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u0080",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u0082",
			"",
			"",
			"",
			"",
			"",
			"\1\u0084",
			"",
			"",
			"",
			"\1\u0086",
			"\1\u0087",
			"\1\u0088",
			"\1\u0089",
			"\1\u008a",
			"\1\u008b",
			"\1\u008c",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u008e",
			"\1\u008f",
			"\1\u0090",
			"\1\u0091",
			"\1\u0092",
			"\1\u0093",
			"\1\u0094",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\7\54\1\u0096"+
			"\13\54\1\u0097\6\54",
			"\1\u0099",
			"\1\u009a",
			"\1\u009b\5\uffff\1\u009c",
			"\1\u009d",
			"\1\u009e\5\uffff\1\u009f",
			"\1\u00a0",
			"\1\u00a1\14\uffff\1\u00a2",
			"\1\u00a3\22\uffff\1\u00a4",
			"\1\u00a5\20\uffff\1\u00a6",
			"\1\u00a7",
			"\1\u00a8",
			"\1\u00a9\11\uffff\1\u00aa",
			"\1\u00ab",
			"\1\u00ac\2\uffff\1\u00ad",
			"\1\u00ae",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\u00af",
			"\1\u00b0",
			"\1\u00b1",
			"\1\u00b2",
			"\1\u00b3",
			"\1\u00b4\1\u00b5",
			"\1\u00b6\7\uffff\1\u00b7",
			"",
			"\1\u00b8",
			"\1\u00b9",
			"\1\u00ba",
			"\1\u00bb",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\4\54\1\u00bc"+
			"\25\54",
			"\1\u00be",
			"\1\u00bf",
			"",
			"\1\u00c0",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"\1\u00c2",
			"\1\u00c3",
			"\1\u00c4",
			"\1\u00c5",
			"\1\u00c6",
			"\1\u00c7",
			"\1\u00c8",
			"\1\u00c9",
			"\1\u00ca",
			"\1\u00cb",
			"\1\u00cc",
			"\1\u00cd",
			"\1\u00ce",
			"\1\u00cf\13\uffff\1\u00d0",
			"\1\u00d1",
			"\1\u00d2",
			"\1\u00d3",
			"\1\u00d4",
			"\1\u00d5",
			"\1\u00d6",
			"\1\u00d7",
			"\1\u00d8",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u00da",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u00dd",
			"\1\u00de",
			"\1\u00df",
			"\1\u00e0",
			"\1\u00e1",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u00e4",
			"\1\u00e5",
			"\1\u00e6",
			"",
			"\1\u00e7",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u00e9",
			"",
			"\1\u00ea",
			"\1\u00eb",
			"\1\u00ec",
			"\1\u00ed",
			"\1\u00ee",
			"\1\u00ef",
			"\1\u00f0",
			"\1\u00f1",
			"\1\u00f2",
			"\1\u00f3",
			"\1\u00f4",
			"\1\u00f5",
			"\1\u00f6",
			"\1\u00f7",
			"\1\u00f8",
			"\1\u00f9",
			"\1\u00fa",
			"\1\u00fb",
			"\1\u00fc",
			"\1\u00fd",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u00ff",
			"\1\u0100",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0104",
			"\1\u0105",
			"\1\u0106",
			"",
			"",
			"\1\u0107",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0109",
			"\1\u010a",
			"",
			"\1\u010b",
			"\1\u010c",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u010e",
			"\1\u010f",
			"\1\u0110",
			"\1\u0111",
			"\1\u0112",
			"\1\u0113",
			"\1\u0114",
			"\1\u0115",
			"\1\u0116",
			"\1\u0117",
			"\1\u0118",
			"\1\u0119",
			"\1\u011a",
			"\1\u011b",
			"\1\u011c",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u011e",
			"\1\u011f",
			"",
			"\1\u0120",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"",
			"\1\u0122",
			"\1\u0123",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\3\54\1\u0124"+
			"\26\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"\1\u0127",
			"\1\u0128",
			"\1\u0129",
			"\1\u012a",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u012e",
			"\1\u012f",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0131",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0139",
			"",
			"\1\u013a",
			"\1\u013b",
			"\1\u013c",
			"",
			"\1\u013d",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0141",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0145",
			"",
			"\1\u0146",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u0148",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"\1\u014a",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"",
			"\1\u014d",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"\1\54\13\uffff\12\54\7\uffff\32\54\4\uffff\1\54\1\uffff\32\54",
			"",
			"",
			"",
			""
	};

	static final short[] DFA29_eot = DFA.unpackEncodedString(DFA29_eotS);
	static final short[] DFA29_eof = DFA.unpackEncodedString(DFA29_eofS);
	static final char[] DFA29_min = DFA.unpackEncodedStringToUnsignedChars(DFA29_minS);
	static final char[] DFA29_max = DFA.unpackEncodedStringToUnsignedChars(DFA29_maxS);
	static final short[] DFA29_accept = DFA.unpackEncodedString(DFA29_acceptS);
	static final short[] DFA29_special = DFA.unpackEncodedString(DFA29_specialS);
	static final short[][] DFA29_transition;

	static {
		int numStates = DFA29_transitionS.length;
		DFA29_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA29_transition[i] = DFA.unpackEncodedString(DFA29_transitionS[i]);
		}
	}

	protected class DFA29 extends DFA {

		public DFA29(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 29;
			this.eot = DFA29_eot;
			this.eof = DFA29_eof;
			this.min = DFA29_min;
			this.max = DFA29_max;
			this.accept = DFA29_accept;
			this.special = DFA29_special;
			this.transition = DFA29_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | IDENTIFIER | CHARACTER_LITERAL | STRING_LITERAL | AT_STRING | HEX_LITERAL | DECIMAL_LITERAL | WS | COMMENT | LINE_COMMENT | NO_MEAN | LESSTHAN | LINE_COMMAND );";
		}
	}

}
