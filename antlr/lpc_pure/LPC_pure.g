/** ANSI C ANTLR v3 grammar

Translated from Jutta Degener's 1995 ANSI C yacc grammar by Terence Parr
July 2006.  The lexical rules were taken from the Java grammar.

Jutta says: "In 1985, Jeff Lee published his Yacc grammar (which
is accompanied by a matching Lex specification) for the April 30, 1985 draft
version of the ANSI C standard.  Tom Stockfisch reposted it to net.sources in
1987; that original, as mentioned in the answer to question 17.25 of the
comp.lang.c FAQ, can be ftp'ed from ftp.uu.net,
   file usenet/net.sources/ansi.c.grammar.Z.
I intend to keep this version as close to the current C Standard grammar as
possible; please let me know if you discover discrepancies. Jutta Degener, 1995"

Generally speaking, you need symbol table info to parse C; typedefs
define types and then IDENTIFIERS are either types or plain IDs.  I'm doing
the min necessary here tracking only type names.  This is a good example
of the global scope (called Symbols).  Every rule that declares its usage
of Symbols pushes a new copy on the stack effectively creating a new
symbol scope.  Also note rule declaration declares a rule scope that
lets any invoked rule see isTypedef boolean.  It's much easier than
passing that info down as parameters.  Very clean.  Rule
direct_declarator can then easily determine whether the IDENTIFIER
should be declared as a type name.

I have only tested this on a single file, though it is 3500 lines.

This grammar requires ANTLR v3.0.1 or higher.

Terence Parr
July 2006
*/
grammar LPC_pure;

options {
    backtrack=true;
    memoize=true;
    k=2;//k=2
}



translation_unit
	: precompile* external_declaration*
	;

/** Either a function definition or any other kind of C decl/def.
 *  The LL(*) analysis algorithm fails to deal with this due to
 *  recursion in the declarator rules.  I'm putting in a
 *  manual predicate here so that we don't backtrack over
 *  the entire function.  Further, you get a better error
 *  as errors within the function itself don't make it fail
 *  to predict that it's a function.  Weird errors previously.
 *  Remember: the goal is to avoid backtrack like the plague
 *  because it makes debugging, actions, and errors harder.
 *
 *  Note that k=1 results in a much smaller predictor for the 
 *  fixed lookahead; k=2 made a few extra thousand lines. ;)
 *  I'll have to optimize that in the future.
 */

precompile
  : //'#include' includeName
    '#include' includeName

  | '#''define' key=IDENTIFIER ('('(IDENTIFIER(','IDENTIFIER)*)?')')? (value=expression | value=selection_statement |compound_statement)?

  | '#undef' IDENTIFIER
  | 'inherit' name=primary_expression ';'

  | '#pragma' IDENTIFIER
//  | precompile_selection_statement
  //| declare_var	
  ;
  
//precompile_selection_statement
//  : ('#ifdef' | '#ifndef' | '#if') (precompile_condition) (precompile|statement|declaration|external_declaration)* ('#elif' (precompile_condition) (precompile|statement|declaration|external_declaration)* )? (options {k=1; backtrack=false;}:'#else' (precompile|statement|declaration|external_declaration)*)? '#endif'//k=1
//  ;
  
precompile_condition
    : precompile_logical_or_expression
    ;
    
precompile_logical_or_expression
    : precompile_logical_and_expression (('|'|'||') precompile_logical_and_expression)*
    ;

precompile_logical_and_expression
    : precompile_not_expression (('&'|'&&') precompile_not_expression)*
    ;
  
precompile_not_expression
    : '!'? precompile_relational_expression
    ;
    
precompile_relational_expression
    : precompile_unit (('=='|'!=') precompile_unit)*
    ;
    
precompile_unit
    : 'defined' '('(IDENTIFIER)')'
    | constant
    | IDENTIFIER
    ;
  
includeName
    : '<' (options {greedy=false;}:~('>'))* '>'
//    | '<' (IDENTIFIER|'.'|'..')? (('/'|'\\') (IDENTIFIER |'.'|'..'))* '.' ('c'|'h') '>'
    | STRING_LITERAL
    ;
    
//declare_var
//	:('DECLARE_RUN_VAR'|'DECLARE_STATIC_VAR') '('
//	 IDENTIFIER ',' (array_expression | map_expression) 
//	 ')' ';'
//	;
    
external_declaration
//options {k=1;}//k=1
	: ( declaration_specifiers? declarator declaration* '{' )=> function_definition
	| ( declaration_specifiers? func_dec_declarator declaration* ';' )=> function_declaration
	| declaration
//	| precompile_selection_statement
	| precompile
	;

function_definition
	:	declaration_specifiers? declarator 

		(	compound_statement				// ANSI style
		)
	;
	
function_declaration
  : declaration_specifiers? declarator ';'
  ;

declaration
	: 'typedef' declaration_specifiers? 
	  init_declarator_list ';' // special case, looking for typedef	
	| declaration_specifiers init_declarator_list? ';'
	| 'class' IDENTIFIER '{' declaration* '}'
	;

declaration_specifiers
	:(permission_specifier
	  | storage_class_specifier
	  | type_specifier
	  | type_qualifier
    )+ 
	;

init_declarator_list
	: init_declarator (',' init_declarator)*
	;

init_declarator
	: declarator ('=' initializer)?
	;
	
permission_specifier
	: 'public'
	| 'protected'
	| 'private'
	;
	
storage_class_specifier
	: 'extern'
	| 'static'
	| 'auto'
	| 'register'
	| 'nosave'
	| 'nomask'
	;

type_specifier
	: 'void'
	| 'char'
	| 'int'
	| 'float'
	| 'signed'
	| 'unsigned'
	| 'string'
	| 'mapping'
	| 'mixed'
	| 'object'
	| 'varargs'
	| 'function'
	| 'class' IDENTIFIER
	| struct_or_union_specifier
	| enum_specifier
//	| type_id
	;

//type_id
//    :   {isTypeName(input.LT(1).getText())}? IDENTIFIER
//    	{System.out.println($IDENTIFIER.text+" is a type");}
//    ;

struct_or_union_specifier
options {k=3;}
	: struct_or_union IDENTIFIER? '{' struct_declaration_list '}'
	| struct_or_union IDENTIFIER
	;

struct_or_union
	: 'struct'
	| 'union'
	;

struct_declaration_list
	: struct_declaration+
	;

struct_declaration
	: specifier_qualifier_list struct_declarator_list ';'
	;

specifier_qualifier_list
	: ( type_qualifier | type_specifier )+
	;

struct_declarator_list
	: struct_declarator (',' struct_declarator)*
	;

struct_declarator
	: declarator (':' constant_expression)?
	| ':' constant_expression
	;

enum_specifier
options {k=3;}
	: 'enum' '{' enumerator_list '}'
	| 'enum' IDENTIFIER '{' enumerator_list '}'
	| 'enum' IDENTIFIER
	;

enumerator_list
	: enumerator (',' enumerator)*
	;

enumerator
	: IDENTIFIER ('=' constant_expression)?
	;

type_qualifier
	: 'const'
	| 'volatile'
	;

declarator
	: (pointer)? direct_declarator
	| pointer
	;

direct_declarator
	: (IDENTIFIER| '(' declarator ')')
        declarator_suffix*
	;

declarator_suffix
	:   '[' constant_expression ']'
    |   '[' ']'
    |   '(' parameter_type_list ')'
    |   '(' identifier_list ')'
    |   '(' ')'
	;
	
func_dec_declarator
  : (pointer)? func_dec_direct_declarator
  | pointer
  ;

func_dec_direct_declarator
  : (
    IDENTIFIER  
  )
       ( '(' parameter_type_list ')' | '(' ')' )
  ;
  
pointer
	: '*' type_qualifier+ pointer?
	| '*' pointer
	| '*'
	;

parameter_type_list
	: parameter_list (',' '...')?
	;

parameter_list
	: parameter_declaration (',' parameter_declaration)*
	;

parameter_declaration
	: declaration_specifiers (declarator|abstract_declarator)*
	;

identifier_list
	: IDENTIFIER (',' IDENTIFIER)*
	;

type_name
	: specifier_qualifier_list abstract_declarator?
	;

abstract_declarator
	: pointer direct_abstract_declarator?
	| direct_abstract_declarator
	;

direct_abstract_declarator
	:	( '(' abstract_declarator ')' | abstract_declarator_suffix ) abstract_declarator_suffix*
	;

abstract_declarator_suffix
	:	'[' ']'
	|	'[' constant_expression ']'
	|	'(' ')'
	|	'(' parameter_type_list ')'
	;
	
initializer
	: assignment_expression
	//| '({' (initializer_list ','?)? '})'
	//| '([' (initializer_list ','?)? '])'
	;

initializer_list
	: initializer (',' initializer)*
	;

// E x p r e s s i o n s

argument_expression_list
	:   arg = assignment_expression 
	    (',' arg = assignment_expression 
	       ','?
	    )*
	;

additive_expression
	: (a=multiplicative_expression) ('+' b=multiplicative_expression  | '-' multiplicative_expression)*
	;

multiplicative_expression
	: (cast_expression) ('*' cast_expression | '/' cast_expression | '%' cast_expression)*
	;

cast_expression
	: '(' type_name ')' cast_expression
	| unary_expression
	;

unary_expression
	: postfix_expression
	| '++' unary_expression
	| '--' unary_expression
	| unary_operator cast_expression
	| 'sizeof' unary_expression
	| 'sizeof' '(' type_name ')'
	;

postfix_expression
	: primary_expression 
    ( '[' expression ']' 
    | '(' 
      ')' 
    | '(' IDENTIFIER ',' (array_expression | map_expression) ')'
    | '(' 
          (argument_expression_list | 'class' IDENTIFIER)
      
          ')' 
    | '.' (str = IDENTIFIER|constant) 
    | '->' funName=IDENTIFIER
    | '++' 
    | '--'
    | '::' funName=IDENTIFIER 
    )*
	;

unary_operator
	: '&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

primary_expression
	: a=(STRING_LITERAL | IDENTIFIER | AT_STRING)  
		    
	    (STRING_LITERAL | b=IDENTIFIER | function_expression | AT_STRING
	       
			)* 
	| c=constant 
	| '(' expression ')'
	| map_expression 
	| array_expression
	| function_expression 
	| '<'primary_expression
	| '::' IDENTIFIER
	;

function_expression
  : '(' ':' ':' ')'
  | '(' ':' expression ','? ':' ')'
	;
	
array_expression
	: '(' '{' '}' ')'
	| '(' '{' expression ','? '}' ')'
	;

map_expression
	: '(' '[' ']' ')'
	| '(' '[' map_element (',' map_element)* ','? ']' ')'
	;

map_element
	: assignment_expression ':' assignment_expression
	;
/**/
	
constant
	: HEX_LITERAL /*{ if ("var" != typeName) typeName =  "int"; }*/
	| DECIMAL_LITERAL /*{ if ("var" != typeName) typeName =  "int"; }*/
	| CHARACTER_LITERAL /*{ if ("var" != typeName) typeName =  "char"; }*/
	| STRING_LITERAL /*{ if ("var" != typeName) typeName =  "string"; }*/
//	| FLOATING_POINT_LITERAL
	| AT_STRING
    ;

/////

expression
	: assignment_expression (',' assignment_expression)*
	;

constant_expression
	: conditional_expression
	;

assignment_expression
	: lvalue assignment_operator assignment_expression
	| conditional_expression
	;
	
lvalue
	:	unary_expression
	;

assignment_operator
	: '='
	| '*='
	| '/='
	| '%='
	| '+='
	| '-='
	| '<<='
	| '>>='
	| '&='
	| '^='
	| '|='
	;


conditional_expression
	: logical_or_expression ('?' expression ':' expression)?
	;

logical_or_expression
	: logical_and_expression ('||' logical_and_expression)*
	;

logical_and_expression
	: (inclusive_or_expression|lvalue assignment_operator assignment_expression) ('&&' expression)*
	;

inclusive_or_expression
	: exclusive_or_expression ('|' exclusive_or_expression)*
	;

exclusive_or_expression
	: and_expression ('^' and_expression)*
	;

and_expression
	: equality_expression ('&' equality_expression)*
	;
equality_expression
	: relational_expression (('=='|'!=') relational_expression)*
	;

relational_expression
	: shift_expression ((LESSTHAN|'>'|'<='|'>=') shift_expression)*
	;

shift_expression
	: scope_expression (('<<'|'>>') scope_expression)*
	;

scope_expression
  : additive_expression ('..' additive_expression?)?
  ;
// S t a t e m e n t s

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
//	| precompile_selection_statement
	;




labeled_statement
	: IDENTIFIER ':' statement?
	| 'case' constant_expression ':' statement?
	| 'default' ':' statement?
	;

compound_statement
	: '{' declaration* statement_list? '}'
	;

statement_list
	: statement+
	;

expression_statement
	: ';'
	| expression ';'?
	;

selection_statement
	: 'if' '(' expression ')' (statement) (options {k=1; backtrack=false;}:'else' (statement))?//k=1
	| 'switch' '(' expression ')' statement
	;

iteration_statement
	: 'while' '(' expression ')' statement
	| 'do' statement 'while' '(' expression ')' ';'
	| 'for' '(' ((declaration_specifiers init_declarator_list? | expression) ','?)* ';' expression_statement expression? ')' statement
	| 'foreach' '(' declaration_specifiers? declarator (',' declaration_specifiers? declarator)* 'in' expression ')' statement
	;

jump_statement
	: 'goto' IDENTIFIER ';'
	| 'continue' ';'
	| 'break' ';'
	| 'return' ';'
	| 'return' expression ';'
	;

IDENTIFIER
	:	LETTER (LETTER|'0'..'9')*
	;
	
fragment
LETTER
	:	'$'
	|	'A'..'Z'
	|	'a'..'z'
	|	'_'
	;

CHARACTER_LITERAL
    :   '\'' ( EscapeSequence | ~('\''|'\\') ) '\''
    ;

STRING_LITERAL
    :  '"' ( EscapeSequence | ~('\\'|'"') )* '"'
    ;   

AT_STRING
   : '@LONG' ( options {greedy=false;} : . )* 'LONG'
   | '@W_CODE' ( options {greedy=false;} : . )* 'W_CODE'
   | '@CODE' ( options {greedy=false;} : . )* 'CODE'
   | '@long' ( options {greedy=false;} : . )* 'long'
   | '@HELP' ( options {greedy=false;} : . )* 'HELP'
   | '@Help' ( options {greedy=false;} : . )* 'Help'
   | '@TEXT' ( options {greedy=false;} : . )* 'TEXT'
   | '@ROOM_CODE' ( options {greedy=false;} : . )* 'ROOM_CODE'
   ;  
       
HEX_LITERAL : '0' ('x'|'X') HexDigit+ IntegerTypeSuffix? ;

DECIMAL_LITERAL : ('0'..'9'+) IntegerTypeSuffix? ;

//OCTAL_LITERAL : '0' ('0'..'7')+ IntegerTypeSuffix? ;

fragment
HexDigit : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
IntegerTypeSuffix
	:	('u'|'U') ('l'|'L')
	|	('u'|'U')
	| ('l'|'L')
	;
	

fragment
Exponent : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
FloatTypeSuffix : ('f'|'F'|'d'|'D') ;

fragment
EscapeSequence
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   OctalEscape
    ;

fragment
OctalEscape
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UnicodeEscape
    :   '\\' 'u' HexDigit HexDigit HexDigit HexDigit
    ;

WS  :  (' '|'\r'|'\t'|'\u000C'|'\n' | '..') {$channel=HIDDEN;}
    ;

COMMENT
    :   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

LINE_COMMENT
    : '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;
    
NO_MEAN
    : '\\' {$channel=HIDDEN;}
    ;
    
LESSTHAN
    : '<'
    ;
    
LINE_COMMAND 
    : ('#if'|'#else'|'#elif'|'#endif') ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;