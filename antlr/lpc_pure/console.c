////////////////////////////////////////////////////////
//File   :console.c
//Purpose:lpc没有shell 这个文件用于测试、学习语法 用gm命令 call 来调用此object的函数
//Data   :2014-12-15
//by     :enjolras
////////////////////////////////////////////////////////
// #include <timer.h>
#include <time_ctrl.h>
#include <remote_server_call.h>
#include <assets.h>

mapping object_save_test = ([]);
mapping map_test = ([]);
int* arr_test = ({});

class coordinate { int x; int y; }
void test_class()
{
	class coordinate temp;
	class coordinate *newpath = ({});
	string class_str;

	temp = new(class coordinate);
	temp->x = 2;
	temp->y = 2;
	newpath += ({ temp });
	temp = new(class coordinate);
	temp->x = 5;
	temp->y = 5;
	newpath += ({ temp });
	debug_message(sprintf("test %d %d", temp->x, temp->y));
	debug_message(sprintf("test %O", newpath));

	class_str = save_variable(temp);
	debug_message(sprintf("class_str : %s", class_str));
}

void test_func()
{
	function f1, f2, f3, f4;
	f1 = (: debug_message :);
	evaluate(f1, "test_func1");

	f2 = (: call_other, this_object(), "test_func2" :);
	evaluate(f2, "test_func2");

	f3 = (: $1 > 0 && $2 < 0 :);
	debug_message(sprintf("test_func3 %d", evaluate(f3, 1, 1)));

	f4 = function(mixed param) {debug_message(param);debug_message("test_func4_too");};
	evaluate(f4, "test_func4");
}
void test_func2(mixed param)
{
	debug_message(param);
}

void test_object(int param)
{
	switch (param) {
		case 1:
			debug_message(sprintf("test_object1 %O", objects()));
			break;
		case 2:
			object_save_test["hello"] = "world";
			break;
		case 3:
			object_save_test["hello"] = "WORLD";
			break;
		case 4:
			save_object("console", 1);
			break;
		case 5:
			restore_object("console");
			break;
		case 6:
			debug_message(sprintf("hello %s", object_save_test["hello"]));
			break;
		default:
			debug_message("do nothing");
			break;
	}
}

void test_array_opt(int opt)
{
	int i;
	int *array;
	switch (opt) {
		case 1:
			for (i = 0; i < 10000; ++i)	{
				arr_test += ({i});
			}
			break;
		case 2:
			array = arr_test;
			break;
		case 3:
			arr_test -= ({10});
			arr_test -= ({100});
			arr_test -= ({1000});
			arr_test -= ({5000});
			arr_test -= ({8000});
			break;
		case 4:
			arr_test += ({10});
			arr_test += ({100});
			arr_test += ({1000});
			arr_test += ({5000});
			arr_test += ({8000});
			break;
		case 5:
			foreach (int val in arr_test) {
			}
		case 6: {
			int *arr = ({});
			debug_message(sprintf("%d", arr[0]));
			break;
		}
		case 7: {
			int *arr = ({1, 2, 3});
			debug_message(sprintf("%O", arr[0..100]));
			break;
		}
		case 8: {
			int *arr = ({1, 1, 2, 2, 3, 3});
			debug_message(sprintf("%O", arr - ({1, 2})));
			break;
		}
		case 9: {
			int *arr = ({1});
			int val = arr[1];
			break;
		}
		case 10: {
			int *arr = ({1, 2, 3, 4, 5, 6});
			debug_message(sprintf("%O", arr[0..1] + arr[3..]));
			debug_message(sprintf("%O", arr[0..<2]));
			break;
		}
		default:
			break;
	}
}

void test_array(int opt)
{
	int ms_time = time_expression(test_array_opt(opt));
	debug_message(sprintf("array opt %d ns_time %d", opt, ms_time));
}

void test_mapping_opt(int opt)
{
	int *array;
	int i;
	switch (opt) {
		case 1:
			for (i = 0; i < 10000; ++i)	{
				map_test[i] = i;
			}
			break;
		case 2:
			array = keys(map_test);
			break;
		case 3:
			map_delete(map_test, 10);
			map_delete(map_test, 100);
			map_delete(map_test, 1000);
			map_delete(map_test, 5000);
			map_delete(map_test, 8000);
			break;
		case 4:
			map_test[10] = 10;
			map_test[100] = 100;
			map_test[1000] = 1000;
			map_test[5000] = 5000;
			map_test[8000] = 8000;
			break;
		case 5:
			foreach (int key, int value in map_test) {
			}
			break;
		case 6:	// 对undefinedp变量foreach会抛出异常
			foreach (int key in map_test["undefinedp"]) {
				debug_message("sth");
			}
			break;
		case 7: {
			mapping test;
			// mapp(test[0]);
			// test[0] = 0;
			break;
		}
		case 8: {
			mapping test1 = ([1:1, "1" : 2]);
			map_mapping(test1, (: debug_message(sprintf("key : %O, value : %O", $1, $2)) :));
			test1 = map_mapping(test1, (: 100 :));
			debug_message(sprintf("%O", test1));
			break;
		}
		case 9: {
			mapping test = ([]);
			debug_message(sprintf("%d", sizeof(test[1])));
			break;
		}
		case 10: {
			mapping test = ([]);
			mapping test1 = test[1];
			test[1] = ([]);
			test1 = test[1];
			debug_message(sprintf("%O", test));
			break;
		}
		case 11: {
			mapping test = ([]);
			string val = test[0];
			if (test[0] == 0) {
				debug_message("equal");
			} else {
				debug_message("not equal");
			}
			if (undefinedp(0)) {
				debug_message("undefinedp(0)");
			}
			if (undefinedp(test[0])) {
				debug_message("undefinedp(test[0])");
			}
			if (undefinedp(val)) {
				debug_message("undefinedp(val)");
			}
			break;
		}
		case 12: {
			mapping test1 = ([1 : 100, 2 : 202]);
			mapping test2 = ([3 : 300, 2 : 201]);
			mapping test3 = ([1 : 2, 100 : 200]);
			mapping test4 = ([2 : 3, 201 : 300]);
			debug_message(sprintf("test1 + test2 %O", test1 + test2));
			debug_message(sprintf("test3 * test4 %O", test3 * test4));
			break;
		}
		case 13: {
			mapping test1 = ([]);
			mapping test2 = copy(test1[0]);
			debug_message(sprintf("%O", test2));
			break;
		}
		case 14: { // 与mapping不同，array是值拷贝
			int *arr;
			mapping test1 = (["array" : ({1, 2, 3})]);

			arr = test1["array"];
			arr = arr - ({1});
			debug_message(sprintf("%O", test1));
			test1["array"] = arr;
			debug_message(sprintf("%O", test1));
			arr = arr - ({2});
		}
		default:
			break;
	}
}

void test_mapping(int opt)
{
	int ms_time = time_expression(test_mapping_opt(opt));
	debug_message(sprintf("mapping opt %d ns_time %d", opt, ms_time));
}

// 用作用域括起来的时候可以用到了再定义
void test_var_init()
{
	{
		int pos1 = 1;
		debug_message(sprintf("pos %d", pos1));
	}
	{
		int pos2 = 2;
		debug_message(sprintf("pos %d", pos2));
	}
}

void do_destruct()
{
	debug_message(sprintf("do_destruct %s", __FILE__));
}

void register_timeup()
{
	debug_message("register_timeup");
	crontab(5, "time_up", ({1}));
	// init_crontab(time(), 5, "time_up", ({2}), false);
}

void time_up(int param)
{
	debug_message(sprintf("time_up%d", param));
}

void test_str_case1(string type)
{
	switch (type) {
		case "修罗":
			debug_message("修罗");
			break;
		case "鬼王":
			debug_message("鬼王");
			break;
		case "天庭":
			debug_message("天庭");
			break;
		case "沉默":
			break;
		default:
			debug_message("default");
			break;
	}
}

void test_str_case(string str)
{
	switch (str) {
		case "test":
			debug_message("test");
			break;
		case "abcd":
			debug_message("abcd");
			break;
		case "silence":
			break;
		default:
			debug_message("default");
			break;
	}
}

void test_str_case_loop()
{
	int i = 0;
	test_str_case1("鬼王");
	test_str_case1("呵呵");
	for (i = 0; i < 99999; ++i) {
		test_str_case1("沉默");
	}
}

// sscanf无法从最外层开始获取内层，所以$call不支持mapping嵌套
void test_scanf()
{
	string s, sArg;
	sArg = "([1 : ([])])";
	sscanf(sArg, "([%s])", s);
	debug_message(s);

	{
		int grade, type;
		string hole_key;
		string hole_id = "50191234";
		grade = atoi(hole_id[2..2]);
		type = atoi(hole_id[3..3]);
		hole_key = hole_id[4..];
		debug_message(sprintf("grade:%d type:%d hole_key:%s", grade, type, hole_key));
	}
}

void test_str()
{
#define MACRO_TEST "macro_test"
	debug_message(MACRO_TEST "test");
}

void test_uniq()
{
	debug_message(sprintf("%O", uniq_arr(({1, 1, 2, 3, 3, 2, 2, "hello", "hello", "haha"}))));
}

void test_json_to_mapping()
{
	mapping map;
	string str = "{ 1:[1, 2], \"haha\":{ 1:1, \"haha\" : \"haha\"} }";
//	string str = "{ \"1\" : \"2\" }";
	map = COM->json_2_mp(str);
	debug_message(sprintf("str : %s", str));
	debug_message(sprintf("map : %O", map));
}

void test_call_remote()
{
	regist_remote_server_call("test_call_remote_cb", "test_call_remote_cb");
	call_remote_server("test_call_remote_cb", ({}), 0, "test_call_fail", ({}), 1);
}

void test_call_fail()
{
	debug_message("remote_call_fail");
}

void test_call_remote_cb()
{
	debug_message("test_call_remote_cb");
}

void test_getconfig()
{
	int i = 0;
	debug_message(sprintf("hostnum : %d", COM->get_hostnum()));
	for (i = 0; i < 13; ++i) {
		debug_message(sprintf("getconfig%d : %O", i, get_config(i)));
	}
}

void test_push_msg(int usernum)
{
#define PUSH_D "sys/daemon/thrid_part_net"
	string token1 = "token1";
	string pushmsg = "pushmsg";
	PUSH_D->add_user_phone_token(usernum, "ios", token1);
	PUSH_D->add_user_phone_token(usernum, "android", token1);

	PUSH_D->push_message_to_user(usernum, pushmsg);

	PUSH_D->del_user_phone_token(usernum, "ios", token1);
	PUSH_D->del_user_phone_token(usernum, "android", token1);
}

void test_mijing_db()
{
	"mijing/mijing_db/mijing_grade1"->show();
}

void test_string_hash()
{
	string str = "aA90";
	debug_message(sprintf("str : %s 1 : %d 2 : %d 3 : %d 4 : %d", str, str[0], str[1], str[<2], str[<1]));
}

// 测试秘境哈希函数的分布
void test_string_hash_dist()
{
#define GLOBALKEY "/cmd/globalkey.c"
	string hole_id;
	int i;
	int hash;
	mapping hash_dist = ([]);

	GLOBALKEY->set_hole("00000000");

	for(i = 0; i < 9999; ++i) {
		hole_id = GLOBALKEY->get_global_res_key(0, 1, 1);
		hash = "mijing/mijing"->get_hash_key(hole_id);
		hash_dist[hash] = hash_dist[hash] + 1;
	}
	foreach (int key, int nums in hash_dist) {
		log_file("hash_dist", sprintf("key : %d, nums : %d\n", key, nums));
	}
}

// not support unsigned
void test_uint()
{
	// unsigned int i = -1;
	// debug_message(sprintf("unsigned int i : %d", i));
}

void test_add_overflow()
{
	int a = 2147483647;
	int b = 2147483649;
	debug_message(sprintf("add : %d", a + b));
}

int test_res(int usernum)
{
	ASSETS_M->add(usernum, ASSETS_TYPE_GOLD, 10000, ASSETS_ADD_REASON_GM);
	ASSETS_M->add(usernum, ASSETS_TYPE_ENERGY, 10000, ASSETS_ADD_REASON_GM);
	ASSETS_M->add(usernum, ASSETS_TYPE_PRESTIGE, 10000, ASSETS_ADD_REASON_GM);
	// ASSETS_M->sub(usernum, ASSETS_TYPE_GOLD, 300, ASSETS_SUB_REASON_GM);
	// ASSETS_M->sub(usernum, ASSETS_TYPE_GOLD, 150, ASSETS_SUB_REASON_GM);
	ASSETS_M->show(usernum);
	return 1;
}

// catch 的返回值是0 或者throw的变量，catch不会因为异常不执行下面的语句
string ret_str(string a)
{
	return sprintf("ret str : %s", a);
}
string throw_str(string a)
{
	a[100] = 1;
	// throw (0);
	// throw (sprintf("throw str : %s", a));
}
void test_catch()
{
	// debug_message(sprintf("%O", catch(ret_str("test"))));
	// debug_message(sprintf("%O", catch(throw_str("test"))));
	// debug_message(sprintf("%O", catch(call_other(this_object(), "ret_str", "test"))));
	// debug_message(sprintf("%O", catch(call_other(this_object(), "throw_str", "test"))));
	catch(throw_str("haha"));
	debug_message("haha");
	throw_str("haha1");
	debug_message("haha1");
}

// 测试set
// 可以set 会报redo log data type wrong 32
// 看engine/gos/redo_log.c代码可知array的元素只能是string或int
void test_safe_set(int usernum)
{
	mixed *test = ({(["hole_id" : 1, "record" : 1, 100 : "hehe"]), (["hole_id" : 2, "record" : 2])});
	SafeSetmixUser(usernum, "test.test_array", test, 0);
	// SafeSetmixUser(usernum, "test.test_array", test, 1);
}

int do_nothing1()
{
}

void do_nothing2()
{
}

// 函数若没有ret，或者是对void取值，都会返回0
// 一般按惯例，成功应该返回0（几乎所有的system call都是这样），
// 成功只有一个, 而失败有多个，整形可分为0与非0
// 但LPC在没有返回的时候，默认返回了0，所以，不能用0作为成功的返回值了
// 下面的情况不能通过编译会更好些
void test_null_ret()
{
	debug_message(sprintf("don't ret %d", do_nothing1()));
	if (0 == do_nothing1()) {
		debug_message("nothing1");
	}
	debug_message(sprintf("void %d", do_nothing2()));
	if (0 == do_nothing2()) {
		debug_message("nothing2");
	}
}

void test_type()
{
	mixed val;
	int i_val;

	val = 2;
	debug_message(sprintf("type %s", typeof(val)));

	val = 2.5 + 3;
	debug_message(sprintf("type %s", typeof(val)));

	val = to_int(val);
	debug_message(sprintf("type %s", typeof(val)));

	i_val = 2.5 + 4;
	debug_message(sprintf("type %s nums : %d", typeof(i_val), i_val));
}

void func1(mixed param)
{
	debug_message(sprintf("%O", param));
}

void test_mixed()
{
	func1("str");
	func1( ({1, 2}) );
}

void test_package(int usernum)
{
	string package = "package/package";
	package->add(usernum, 100001, 15, "test");
	package->add(usernum, 200001, 15, "test");
	package->del(usernum, "100001", 5, "test");
	package->dump(usernum);
}

void test_multi(int x, int y)
{
	if (CMN_UTIL->is_mult_ok(x, y)) {
		debug_message(sprintf("%d * %d ok", x, y));
	} else {
		debug_message(sprintf("%d * %d not ok", x, y));
	}
}

// 本地MACRO覆盖 globals.h
// 注释前
// char/console.c 542 : test_macro_same : 17
// 注释后
// char/console.c 542 : test_macro_same : "/char/master"
// #define MASTER 17
void test_macro_same()
{
	debug_message(sprintf("test_macro_same : %O", MASTER));
}

void create()
{
	debug_message(sprintf("create %s", __FILE__));
 	// register_timeup();
}
