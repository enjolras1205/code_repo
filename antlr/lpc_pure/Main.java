import org.antlr.runtime.*;
 
public class Main {
    public static void main(String[] args) throws Exception {
        ANTLRInputStream input = new ANTLRInputStream(System.in);
        LPC_pureLexer lexer = new LPC_pureLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LPC_pureParser parser = new LPC_pureParser(tokens);
        parser.translation_unit();
    }
}
