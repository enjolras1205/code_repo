	.file	"main.c"
	.text
.globl main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$10, -4(%rbp)
	cmpl	$19, -4(%rbp)
	jg	.L2
	addl	$1, -4(%rbp)
	jmp	.L3
.L2:
	cmpl	$20, -4(%rbp)
	jle	.L4
	subl	$1, -4(%rbp)
	jmp	.L3
.L4:
	addl	$1, -4(%rbp)
.L3:
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 4.4.7 20120313 (Red Hat 4.4.7-3)"
	.section	.note.GNU-stack,"",@progbits
