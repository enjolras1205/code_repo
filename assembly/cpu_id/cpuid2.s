#cpuid2.s View the CPUID Vendor ID string using C library calls
.section .data
output:
    .asciz "The processor Vender ID is %s\n"
.section .bss
    .comm buffer, 12
.section .text
    .globl main
main:
	movl $0, %eax
	cpuid
	movl $buffer, %edi
	movl %ebx, (%edi)
	movl %edx, 4(%edi)
	movl %ecx, 8(%edi)
#	pushq $buffer
#	pushq $output
    movq $buffer, %rsi
    movq $output, %rdi
    movq $0, %rax
    call printf
	addq $16, %rsp
	pushq $0
	call exit
